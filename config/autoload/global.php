<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
function pr(){
    $args = func_get_args();
    if ( (array_key_exists(0, $args)) && (!array_key_exists(1, $args))  ){
        $var = $args[0];
    }
    else{
        $var = $args;
    }
    //$var = (is_array($var))?$var:array($var);
    echo '<div style="background:black;color:lightgray;padding:10px;font-size:12px;border-bottom:1px dashed lightgray;text-align:left;">';
    echo '<pre>';
    print_r($var);
    echo '</pre>';
    echo '</div>';
}
function pe(){
    $args = func_get_args();
    if ( (array_key_exists(0, $args)) && (!array_key_exists(1, $args))  ){
        $var = $args[0];
    }
    else{
        $var = $args;
    }
    pr($var);
    die();
}


return array(

);

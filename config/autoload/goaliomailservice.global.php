<?php
/**
 * GoalioMailService Configuration
 *
 * If you have a ./config/autoload/ directory set up for your project, you can
 * drop this config file in it and change the values as you wish.
 */
$email = include 'email.local.php';
$settings = array(

    /**
     * Transport Class
     *
     * Name of Zend Transport Class to use
     */
    'type' => 'Smtp',
    'options' => $email['email']['transport']['smtp']['options']
    /**
     * End of GoalioMailService configuration
     */
);

/**
 * You do not need to edit below this line
 */
return array(
    'goaliomailservice' => $settings,
);

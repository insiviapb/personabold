<?php
namespace Audience;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\EventManager\Event;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\ViewModel;


class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $app = $e->getParam('application');
        $sm  = $app->getServiceManager();

        $personaService = $sm->get('persona.service')->getEventManager();
        $personaService->attach('sendSurvey.post', array($this, 'sendSurveyToRespondents'));
        $personaService->attach('processSharePersona.post', array( $this, 'sendSharedPersona'));
        $personaService->attach($sm->get('persona.service.listener.persona'));
    }

    /**
     * Send Survey
     * @param Event $event
     */
    public function sendSurveyToRespondents(Event $event)
    {
        $sm      = $event->getTarget()->getServiceLocator();
        $data = $event->getParam('data');
        $respondents = $event->getParam('respondents');
        foreach ($respondents as $val) {
            
            $this->sendSurvey($val, $data, $sm);
        }
    }
    
    public function sendSharedPersona(Event $event)
    {
        $sm      = $event->getTarget()->getServiceLocator();
        $data    = $event->getParam('shared');
        $emails  = $event->getParam('emails');
        
        foreach ($emails as $key => $email) {
            $this->processPersona($email, $data, $sm);
        }
    }
    
    /**
     * moving send survey separately
     * @param type $surveyDetails
     * @param type $data
     * @param type $sm
     */
    public function sendSurvey($surveyDetails, $data, $sm)
    {
        
        $config  = $sm->get('Config')['email']['message']['send-survey'];
        $view = new ViewModel(array('respondent' => $surveyDetails, 'message' => $data['message']));
        $view->setTemplate('email/send-survey.phtml');
        $html = $sm->get('view_manager')
            ->getRenderer()
            ->render($view);

        $htmlMimePart = new MimePart($html);
        $htmlMimePart->setType('text/html');
        $mimeMessage = new MimeMessage();
        $mimeMessage->addPart($htmlMimePart);

        $message = new Message();
        $message->addFrom($config['from'], $config['name'])
                ->setSubject($config['subject']);
        $message->setBody($mimeMessage);
        $message->addTo($surveyDetails['email']);

        $mail = $sm->get('web.email.transport');
        $mail->send($message);

        $message = null;
        $mail    = null;
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    /**
     * Process Persona
     * @param type $personaDetails
     * @param type $sm
     */
    public function processPersona($email, $personaDetails, $sm)
    {
        
        $config  = $sm->get('Config')['email']['message']['share-persona'];
        $view = new ViewModel(array('shared' => $personaDetails));
        $view->setTemplate('email/send-share-persona.phtml');
        $html = $sm->get('view_manager')
            ->getRenderer()
            ->render($view);

        $htmlMimePart = new MimePart($html);
        $htmlMimePart->setType('text/html');
        $mimeMessage = new MimeMessage();
        $mimeMessage->addPart($htmlMimePart);

        $message = new Message();
        $message->addFrom($config['from'], $config['name'])
                ->setSubject($config['subject']);
        $message->setBody($mimeMessage);
        $message->addTo($email);

        $mail = $sm->get('web.email.transport');
        $mail->send($message);

        $message = null;
        $mail    = null;
    }
    
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'audience.email.message.survey' => function($sm) {
                    $config  = $sm->get('Config')['email']['message']['send-survey'];
                    $message = new Message();
                    $message->addFrom($config['from'], $config['name'])
                        ->setSubject($config['subject']);
                    return $message;
                },

            ),
        );
    }
}

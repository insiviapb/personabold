<?php
return array(
    'router' => array(
        'routes' => array(
            'share' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/share'
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'sharePersona' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route'    => '[/:personaShortId]',
                            'defaults' => array(
                                'controller' => 'audience.controller.persona',
                                'action'     => 'sharePersona',
                            ),
                        ),
                    ),
                    'security' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route'    => '/secure[/:shortId]',
                            'defaults' => array(
                                'controller' => 'audience.controller.persona',
                                'action'     => 'security',
                            ),
                        ),
                    ),
                    'notfound' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route'    => '/not-found',
                            'defaults' => array(
                                'controller' => 'audience.controller.persona',
                                'action'     => 'sharedNotFound',
                            ),
                        ),
                    ),
                    'validatePassword' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route'    => '/validate-password',
                            'defaults' => array(
                                'controller' => 'audience.controller.persona',
                                'action'     => 'validatePassword',
                            ),
                        ),
                    ),
                ),
            ),
            'persona' => array(
                'type' => 'literal',
                'options' => array(
                    'route'    => '/persona',
                    'defaults' => array(
                        'controller' => 'audience.controller.persona',
                        'action'     => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'persona' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '[/:uuid]',
                            'defaults' => array(
                                'controller' => 'audience.controller.persona',
                                'action'     => 'index',
                            ),
                            'constraints' => array(
                                'uuid' => \Planbold\Module::PLANBOLD_UUID_PATTERN
                            )
                        ),
                    ),
                    'surveyResult' => array(
                        'type' => 'segment',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/survey-result[/:uuid]',
                            'defaults' => array(
                                'controller' => 'audience.controller.persona',
                                'action'     => 'surveyResult',
                            ),
                            'constraints' => array(
                                'uuid' => \Planbold\Module::PLANBOLD_UUID_PATTERN
                            )
                        ),
                    ),
                    'compare' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/compare',
                            'defaults' => array(
                                'controller' => 'audience.controller.persona',
                                'action'     => 'compare',
                            ),
                        ),
                    ),
                    'revisionPage' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/revision',
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'revision' => array(
                                'type' => 'Segment',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '[/:revisionId]',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona',
                                        'action'     => 'revisionPage',
                                    ),
                                ),
                            ),
                            'revertPersona' => array(
                                'type' => 'Segment',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/revert-persona[/:revisionId]',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'revertPersona',
                                    ),
                                ),
                            ),
                        )
                    ),
                    'process' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/process',
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'fetchPersonaData' => array(
                                'type' => 'Segment',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/fetchPersonaData[/:personaId][/:clientUuid]',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'fetchPersonaData',
                                    ),
                                ),
                            ),
                            'fetchPersonaRevisionData' => array(
                                'type' => 'Segment',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/fetchRevisionData[/:revisionId]',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'fetchPersonaRevisionData',
                                    ),
                                ),
                            ),
                            'savePersona' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/savePersona',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'savePersona',
                                    ),
                                ),
                            ),
                            'saveArcheTypePersona' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/saveArchetypeData',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'saveArchetypeData',
                                    ),
                                ),
                            ),
                            'savePersonaDetails' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/savePersonaDetails',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'savePersonaDetails',
                                    ),
                                ),
                            ),
                            'savePersonaSectionPosition' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/savePersonaSectionPosition',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'savePersonaSectionPosition',
                                    ),
                                ),
                            ),
                            'savePersonaData' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/savePersonaData',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'savePersonaData',
                                    ),
                                ),
                            ),
                            'getSurveyDataByRespondents' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getSurveyDataByRespondents',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getSurveyDataByRespondents',
                                    ),
                                ),
                            ),
                            'getIndustryByRespondents' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getIndustryByRespondents',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getIndustryByRespondents',
                                    ),
                                ),
                            ),
                            'getGenderByRespondents' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getGenderByRespondents',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getGenderByRespondents',
                                    ),
                                ),
                            ),
                            'getSurveyResultByRespondent' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getSurveyResultByRespondent',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getSurveyResultByRespondent',
                                    ),
                                ),
                            ),
                            'getJobtypeByRespondents' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getJobtypeByRespondents',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getJobtypeByRespondents',
                                    ),
                                ),
                            ),
                            'getArchetypeByRespondents' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getArchetypeByRespondents',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getArchetypeByRespondents',
                                    ),
                                ),
                            ),
                            'getSurveyRespondents' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getSurveyRespondents',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getSurveyRespondents',
                                    ),
                                ),
                            ),
                            'getPersonaByIndustry' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getPersonaByIndustry',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getPersonaByIndustry',
                                    ),
                                ),
                            ),
                            'getIndustryDescription' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getIndustryDescription',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getIndustryDescription',
                                    ),
                                ),
                            ),
                            'saveIndustryDescription' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/saveIndustryDescription',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'saveIndustryDescription',
                                    ),
                                ),
                            ),
                            'getPersonaData' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getPersonaData',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getPersonaData',
                                    ),
                                ),
                            ),
                            'getPersonaFields' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getPersonaFields',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getPersonaFields',
                                    ),
                                ),
                            ),
                            'getChannels' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getChannels',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getChannels',
                                    ),
                                ),
                            ),
                            'getPersonaChannels' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getPersonaChannels',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getPersonaChannels',
                                    ),
                                ),
                            ),
                            'savePersonaChannel' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/savePersonaChannel',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'savePersonaChannel',
                                    ),
                                ),
                            ),
                            'savePersonaMotivation' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/savePersonaMotivation',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'savePersonaMotivation',
                                    ),
                                ),
                            ),
                            'getPersonaMotivations' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getPersonaMotivations',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getPersonaMotivations',
                                    ),
                                ),
                            ),
                            'getMotivations' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getMotivations',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getMotivations',
                                    ),
                                ),
                            ),
                            'savePersonaKnowledge' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/savePersonaKnowledge',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'savePersonaKnowledge',
                                    ),
                                ),
                            ),
                            'getPersonaKnowledge' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getPersonaKnowledge',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getPersonaKnowledge',
                                    ),
                                ),
                            ),
                            'getPersonaInfo' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/getPersonaInfo',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'getPersonaInfo',
                                    ),
                                ),
                            ),
                            'sendSurvey' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/sendSurvey',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'sendSurvey',
                                    ),
                                ),
                            ),
                            'saveSurvey' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/saveSurvey',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'saveSurvey',
                                    ),
                                ),
                            ),
                            'exportPersonaPDF' => array(
                                'type' => 'Segment',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/export-single-pdf[/:uuid][/:templateUuid][/:isMultiple]',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'exportPersonaPDF',
                                    ),
                                ),
                            ),
                            'checkPackages' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/checkPackages',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'checkPackages',
                                    ),
                                ),
                            ),
                            'addPersonaClient' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/addPersonaClient',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'addPersonaClient',
                                    ),
                                ),
                            ),
                            'exportPersonaTemplate' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/export-persona-template',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'exportPersonaTemplate',
                                    ),
                                ),
                            ),
                            'uploadFiles' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/upload-files',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'uploadFiles',
                                    ),
                                ),
                            ),
                            'clientFiles' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/client-files',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'clientFiles',
                                    ),
                                ),
                            ),
                            'saveGettingStarted' => array(
                                'type' => 'Segment',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/save-getting-started',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'saveGettingStarted',
                                    ),
                                ),
                            ),
                            'deletePersonaData' => array(
                                'type' => 'Segment',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/delete-persona-data',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'deletePersonaData',
                                    ),
                                ),
                            ),
                            'sharePersonaData' => array(
                                'type' => 'Segment',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/share-persona',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'sharePersona',
                                    ),
                                ),
                            ),
                            'enableSharing' => array(
                                'type' => 'Segment',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/enable-sharing',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'enableSharing',
                                    ),
                                ),
                            ),
                            'enablePersonaPassword' => array(
                                'type' => 'Segment',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/enable-persona-password',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.persona.process',
                                        'action'     => 'enablePersonaPasssword',
                                    ),
                                ),
                            ),
                        )
                    ),
                    'clientPage'  => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route'    => '/client',
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'addPersona' => array(
                                'type' => 'Segment',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/add-persona[/:uuid]',
                                    'defaults' => array(
                                        'controller' => 'audience.controller.client',
                                        'action'     => 'addPersona',
                                    ),
                                ),
                            ),
                            'settings' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route'    => '/settings',
                                ),
                                'may_terminate' => true,
                                'child_routes' => array(
                                    'settingsPage' => array(
                                        'type' => 'Segment',
                                        'priority' => 1000,
                                        'options' => array(
                                            'route'    => '[/:uuid]',
                                            'defaults' => array(
                                                'controller' => 'audience.controller.client',
                                                'action'     => 'settingsClient',
                                            ),
                                        ),
                                    ),
                                    'files' => array(
                                        'type' => 'Literal',
                                        'priority' => 1000,
                                        'options' => array(
                                            'route'    => '/files',
                                            'defaults' => array(
                                                'controller' => 'audience.controller.client',
                                                'action'     => 'filePage',
                                            ),
                                        ),
                                    ),
                                    'themeManagement' => array(
                                        'type' => 'Literal',
                                        'priority' => 1000,
                                        'options' => array(
                                            'route'    => '/theme-management',
                                            'defaults' => array(
                                                'controller' => 'audience.controller.client',
                                                'action'     => 'theme',
                                            ),
                                        ),
                                    ),
                                )
                            ),
                            'process' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/process',
                                ),
                                'may_terminate' => true,
                                'child_routes' => array(
                                    'getClientPersona' => array(
                                        'type' => 'Segment',
                                        'priority' => 1000,
                                        'options' => array(
                                            'route'    => '/get-client-persona',
                                            'defaults' => array(
                                                'controller' => 'audience.controller.client',
                                                'action'     => 'getClientPersona',
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
            'audience' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/audience',
                    'defaults' => array(
                        'controller' => 'audience.controller.index',
                        'action'     => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'clientPersona' => array(
                        'type' => 'Segment',
                        'priority' => 1000,
                        'options' => array(
                            'route'    => '[/:clientUuid]',
                            'defaults' => array(
                                'controller' => 'audience.controller.index',
                                'action'     => 'client',
                            ),
                        ),
                    ),
                    'survey' => array(
                        'type' => 'segment',
                        'priority' => 1000,
                        'options' => array(
                            'route'    => '/survey[/:uuid]',
                            'defaults' => array(
                                'controller' => 'audience.controller.index',
                                'action'     => 'survey',
                            ),
                            'constraints' => array(
                                'uuid' => \Planbold\Module::PLANBOLD_UUID_PATTERN
                            )
                        ),
                    ),
                )
            ),

        )
    ), // router
    'controllers' => array(
        'invokables' => array(
            'audience.controller.index'          => 'Audience\Controller\IndexController',
            'audience.controller.persona'        => 'Audience\Controller\PersonaController',
            'audience.controller.persona.process' => 'Audience\Controller\PersonaProcessController',
            'audience.controller.client'         => 'Audience\Controller\ClientController'
        ),
        'factories' => array(

        ),
    ), // controllers
    'service_manager' => array(
        'factories' => array(
            'audience.form.sendSurvey'   => 'Audience\Service\Factory\SendSurveyFormFactory',
            'client.navigation.settings' => 'Audience\Service\Factory\ClientNavigationFactory',
        ),
        'invokables' => array(
            'persona.service'                  => 'Audience\\Service\\Persona',
            'account.service'                  => 'Audience\\Service\\Account',
            'persona.service.listener.persona' => 'Audience\\Service\\Listener\\PersonaEventListener',
        )
    ), // service_manager
    'view_helpers' => array(
        'invokables' => array(

        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'template_map' => array(

        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                __DIR__ . '/../asset',
            ),
        ),
    ),
    'navigation' => array(
        'client-navigations' => array(
            array(
                'label' => 'Client Settings',
                'uri'   => '#',
                'pages' => array(
                    array(
                        'label' => 'Update Details',
                        'route' => 'persona/clientPage/settings/settingsPage'
                    ),
                    array(
                        'label' => 'Upload Files',
                        'route' => 'persona/clientPage/settings/files'
                    ),
                    array(
                        'label' => 'Theme Management',
                        'route' => 'persona/clientPage/settings/themeManagement'
                    )
                )
            )
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'route-white-list' => array(
        // 'consumer/feeds/addthis',

    )
);
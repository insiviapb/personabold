<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Audience\Form;

use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

use Zend\Form\Form;
use Planbold\Form\Element;

/**
 * Class for SendSurveyForm Form
 *
 * @author  <aleks.daloso@gmail.com>
 */
class SendSurvey extends Form implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    protected $sm;
    
    public function __construct($name)
    {
        parent::__construct($name);

        $this->add(new Element\MultiCheckBoxDesc('check-questions', array()))
            ->add(new Element\TextName('message', array('maxlength' => 100)));
    }


}

<?php
/**
 * User's Form
 * @author Oliver Pasigna <opasigna@insivia.com>
 */
namespace Audience\Form;

use Zend\Captcha as Captcha;
use Zend\Form\Form;
use Zend\Form\Element;

class SurveyForm extends Form
{

    public function __construct($name)
    {
        parent::__construct($name);

        $this->setName($name);
        
        // username
        $this->add(array(
            'name' => 'username',
            'type' => 'Text',
            'attributes' => array(
                'id'          => 'username',
                'value'       => '',
                'placeholder' => 'username',
                'tabindex'    => '1'
            )
        ));
        
        // uuid
        $this->add(array(
            'name' => 'uuid',
            'type' => 'Hidden',
            'attributes' => array(
                'id'    => 'uuid',
                'value'    =>'',
            )
        ));

        //Email
        $this->add(array(
            'type'       => 'Text',
            'name'       => 'email',
            'attributes' => array(
                'maxlength'   => '200',
                'tabindex'    => '2',
                'id'          => 'email',
                'placeholder' => 'Email'
            )
        ));

        // Display Name
        $this->add(array(
            'type'       => 'Text',
            'name'       => 'display_name',
            'attributes' => array(
                'maxlength' => '45',
                'tabindex'  => '3',
                'id'        => 'display_name',
                'placeholder'   => 'Display Name'
            )
        ));

        // firstName
        $this->add(array(
            'type'       => 'Text',
            'name'       => 'first_name',
            'attributes' => array(
                'maxlength'     => '120',
                'tabindex'      => '4',
                'id'            => 'first_name',
                'placeholder'   => 'First Name'
            )
        ));
        
        // lastName
        $this->add(array(
            'type'       => 'Text',
            'name'       => 'last_name',
            'attributes' => array(
                'maxlength'     => '120',
                'tabindex'      => '5',
                'id'            => 'last_name',
                'placeholder'   => 'Last Name'
            )
        ));

        // title
        $this->add(array(
            'type'       => 'Text',
            'name'       => 'title',
            'attributes' => array(
                'maxlength'     => '120',
                'tabindex'      => '6',
                'id'            => 'title',
                'placeholder'   => 'Title'
            )
        ));
        
        // phone
        $this->add(array(
            'type'       => 'Text',
            'name'       => 'phone',
            'attributes' => array(
                'maxlength'     => '30',
                'tabindex'      => '7',
                'id'            => 'phone',
                'placeholder'   => 'Phone'
            )
        ));
        
        // Address
        $this->add(array(
            'type'       => 'Text',
            'name'       => 'address',
            'attributes' => array(
                'maxlength'     => '1000',
                'tabindex'      => '8',
                'id'            => 'address',
                'placeholder'   => 'Address'
            )
        ));
    }
}
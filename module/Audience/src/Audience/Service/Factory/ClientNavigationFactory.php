<?php

namespace Audience\Service\Factory;

use Zend\Navigation\Service\DefaultNavigationFactory;

class ClientNavigationFactory extends DefaultNavigationFactory
{
    /**
     * @return string
     */
    protected function getName()
    {
        return 'client-navigations';
    }
}

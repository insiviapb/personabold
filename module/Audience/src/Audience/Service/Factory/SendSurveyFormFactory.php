<?php
namespace Audience\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Audience\Form\SendSurvey;
/**
 * Send Survey Form Factory
 *
 * @author Aleks Daloso <aleks.daloso@gmail.com>
 */
class SendSurveyFormFactory implements FactoryInterface
{
    /**
     * Create a service for DoctrineObject Hydrator
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $form = new SendSurvey('sendSurveyForm');
        return $form;
    }
}

<?php
namespace Audience\Service;

use Planbold\Entity\SurveyQuestions;
use Planbold\Mapper\PersonaSections;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;
use Zend\Stdlib\Hydrator;

class Persona implements ServiceLocatorAwareInterface, EventManagerAwareInterface
{
    use ServiceLocatorAwareTrait;

    use EventManagerAwareTrait;

    /**
     * @var PersonaMapperInterface
     */
    private $personaMapper;

    /**
     * @var PersonaChannelsMapperInterface
     */
    private $personaChannelsMapper;

    /**
     * @var SurveyQuestionsMapperInterface
     */
    private $surveyQuestionsMapper;

    /**
     * @var SurveyMapperInterface
     */
    private $surveyMapper;

    /**
     * @var SurveyRespondentMapperInterface
     */
    private $surveyRespondentMapper;

    /**
     * @var SurveyRespondentDataMapperInterface
     */
    private $surveyRespondentDataMapper;

    /**
     * @var PersonaSectionsMapperInterface
     */
    private $personaSectionsMapper;

    /**
     * @var PersonaMotivationsMapper
     */
    private $personaMotivationsMapper;

    /**
     * @var JobtypeMapper
     */
    private $jobtypeMapper;

    /**
     * @var MotivationsMapperInterface
     */
    private $motivationsMapper;

    /**
     * @var PersonaDataMapperInterface
     */
    private $personaDataMapper;

    /**
     * @var PersonaKnowledgeMapperInterface
     */
    private $personaKnowledgeMapper;

    /**
     * @var GlobalChannelsDataMapperInterface
     */
    private $globalChannelsDataMapper;

    /**
     * @var PersonaFieldsMapperInterface
     */
    private $personaFieldsMapper;

    /**
     * @var AccountIndustryMapperInterface;
     */
    private $accountIndustryMapper;

    /**
     * @var ArchetypeMapperInterface
     */
    private $archetypeMapper;
    
    /**
     * @var AccountMapperInterface 
     */
    private $accountMapper;
    
    /**
     * @var personaRevisionMapper 
     */
    private $personaRevisionMapper;

    /*
     * @var $agencyClientMapper
     */
    private $agencyClientMapper;
    
    /**
     * Account File Mapper
     * @var type 
     */
    private $accountFileMapper;
    
    /**
     * Get Services
     * @var type
     */
    protected $service;
    
    /**
     * Getting started uuid
     * @var type 
     */
    protected $gettingStartedUuid;
    
    /**
     * @var StripeAccountMapper 
     */
    protected $stripeAccountMapper;
    
    /**
     * Headshot mapper
     * @var type 
     */
    protected $headshotMapper;
    
    /**
     * Persona Shared Email Mapper
     * @var type 
     */
    protected $personaSharedEmailMapper;

    /**
     * Get Service Object
     * @param string $serviceName
     * @return type
     */
    private function getService($serviceName)
    {
        return $this->getServiceLocator()->get($serviceName);
    }

    /**
     * @var Hydrator\ClassMethods
     */
    protected $formHydrator;

    /**
     * Return the Form Hydrator
     * @return \Zend\Stdlib\Hydrator\ClassMethods
     */
    public function getFormHydrator()
    {
        if (!$this->formHydrator instanceof Hydrator\HydratorInterface) {
            $this->setFormHydrator($this->getService('zfcuser_register_form_hydrator'));
        }

        return $this->formHydrator;
    }

    /**
     * Set the Form Hydrator to use
     * @param Hydrator\HydratorInterface $formHydrator
     * @return Voice Talent
     */
    public function setFormHydrator(Hydrator\HydratorInterface $formHydrator)
    {
        $this->formHydrator = $formHydrator;
        return $this;
    }

    /**
     * @param \Planbold\Mapper\MapperInterface $personaMapper
     * @return $this
     */
    public function setPersonaMapper(\Planbold\Mapper\MapperInterface $personaMapper)
    {
        $this->personaMapper = $personaMapper;
        return $this;
    }

    /**
     * @return array|PersonaMapperInterface|object
     */
    public function getPersonaMapper()
    {
        if (null === $this->personaMapper) {
            $this->personaMapper = $this->getService('planbold.mapper.persona');
        }

        return $this->personaMapper;
    }
    
    /**
     * @return array|HeadshotMapperInterface|object
     */
    public function getHeadshotMapper()
    {
        if (null === $this->headshotMapper) {
            $this->headshotMapper = $this->getService('planbold.mapper.headshot');
        }

        return $this->headshotMapper;
    }

    /**
     * @param \Planbold\Mapper\MapperInterface $archetypeMapper
     * @return $this
     */
    public function setArchetypeMapper(\Planbold\Mapper\MapperInterface $archetypeMapper)
    {
        $this->archetypeMapper = $archetypeMapper;
        return $this;
    }

    /**
     * Get Industry Mapper
     * @return type
     */
    private function getIndustryMapper()
    {
        if (!isset($this->industryMapper)) {
            $this->industryMapper = $this->getService('planbold.mapper.industry');
        }
        return $this->industryMapper;
    }

    /**
     * Get AccountIndustry Mapper
     *
     * @return AccountIndustryMapperInterface|type
     */
    public function getAccountIndustryMapper()
    {
        if (null === $this->accountIndustryMapper) {
            $this->accountIndustryMapper = $this->getService('planbold.mapper.accountIndustry');
        }
        return $this->accountIndustryMapper;
    }

    /**
     * @return ArchetypeMapperInterface|type
     */
    public function getArchetypeMapper()
    {
        if (null === $this->archetypeMapper) {
            $this->archetypeMapper = $this->getService('planbold.mapper.archetype');
        }

        return $this->archetypeMapper;
    }

    /**
     * @return PersonaDataMapperInterface|type
     */
    public function getPersonaDataMapper()
    {
        if (null === $this->personaDataMapper) {
            $this->personaDataMapper = $this->getService('planbold.mapper.personaData');
        }

        return $this->personaDataMapper;
    }

    /**
     * @return PersonaFieldsMapperInterface|type
     */
    public function getPersonaFieldsMapper()
    {
        if (null === $this->personaFieldsMapper) {
            $this->personaFieldsMapper = $this->getService('planbold.mapper.personaFields');
        }
        return $this->personaFieldsMapper;
    }

    /**
     * @return GlobalChannelsDataMapperInterface|type
     */
    public function getGlobalChannelsMapper()
    {
        if (null === $this->globalChannelsDataMapper) {
            $this->globalChannelsDataMapper = $this->getService('planbold.mapper.globalChannels');
        }

        return $this->globalChannelsDataMapper;
    }

    /**
     * @return PersonaChannelsMapperInterface|type
     */
    public function getPersonaChannelsMapper()
    {
        if (null === $this->personaChannelsMapper) {
            $this->personaChannelsMapper = $this->getService('planbold.mapper.personaChannels');
        }
        return $this->personaChannelsMapper;
    }

    /**
     * @return PersonaMotivationsMapper|type
     */
    public function getPersonaMotivationsMapper()
    {
        if (null === $this->personaMotivationsMapper) {
            $this->personaMotivationsMapper = $this->getService('planbold.mapper.personaMotivations');
        }
        return $this->personaMotivationsMapper;
    }

    /**
     * @return MotivationsMapperInterface|type
     */
    public function getMotivationsMapper()
    {
        if (null === $this->motivationsMapper) {
            $this->motivationsMapper = $this->getService('planbold.mapper.motivations');
        }
        return $this->motivationsMapper;
    }

    /**
     * @return PersonaKnowledgeMapperInterface|type
     */
    public function getPersonaKnowledgeMapper()
    {
        if (null === $this->personaKnowledgeMapper) {
            $this->personaKnowledgeMapper = $this->getService('planbold.mapper.personaKnowledge');
        }
        return $this->personaKnowledgeMapper;
    }

    /**
     * @return JobtypeMapper|type
     */
    public function getJobtypeMapper()
    {
        if (null === $this->jobtypeMapper) {
            $this->jobtypeMapper = $this->getService('planbold.mapper.jobtype');
        }
        return $this->jobtypeMapper;
    }

    /**
     * @return SurveyMapperInterface|type
     */
    public function getSurveyMapper()
    {
        if (null === $this->surveyMapper) {
            $this->surveyMapper = $this->getService('planbold.mapper.survey');
        }

        return $this->surveyMapper;
    }

    /**
     * @return SurveyQuestionsMapperInterface|type
     */
    public function getSurveyQuestionsMapper() {
        if(null === $this->surveyQuestionsMapper) {
            $this->surveyQuestionsMapper = $this->getService('planbold.mapper.surveyQuestions');
        }

        return $this->surveyQuestionsMapper;
    }
    /**
     * @return SurveyRespondentMapperInterface|type
     */
    public function getSurveyRespondentMapper()
    {
        if(null === $this->surveyRespondentMapper) {
            $this->surveyRespondentMapper = $this->getService('planbold.mapper.surveyRespondent');
        }
        return $this->surveyRespondentMapper;
    }

    /**
     * @return SurveyRespondentDataMapperInterface|type
     */
    public function getSurveyRespondentDataMapper()
    {
        if(null === $this->surveyRespondentDataMapper) {
            $this->surveyRespondentDataMapper = $this->getService('planbold.mapper.surveyRespondentData');
        }
        return $this->surveyRespondentDataMapper;
    }

    /**
     * @return PersonaSectionsMapperInterface|type
     */
    public function getPersonaSectionsMapper()
    {
        if(null === $this->personaSectionsMapper) {
            $this->personaSectionsMapper = $this->getService('planbold.mapper.personaSections');
        }
        return $this->personaSectionsMapper;
    }
    
    /**
     * Get Account File Mapper
     * @return type
     */
    private function getAccountFileMapper()
    {
        if (!isset($this->accountFileMapper)) {
            $this->accountFileMapper = $this->getService('planbold.mapper.accountFile');
        }
        return $this->accountFileMapper;
    }
    
    /**
     * @return AccountMapperInterface|type
     */
    public function getAccountMapper()
    {
        if(null === $this->accountMapper) {
            $this->accountMapper = $this->getService('planbold.mapper.account');
        }
        return $this->accountMapper;
    }
    
    /**
     * @return PersonaRevisionMapperInterface|type
     */
    public function getPersonaRevisionMapper()
    {
        if(null === $this->personaRevisionMapper) {
            $this->personaRevisionMapper = $this->getService('planbold.mapper.personaRevision');
        }
        return $this->personaRevisionMapper;
    }
    
    /**
     * @return AgencyClientMapperInterface|type
     */
    public function getAgencyClientMapper()
    {
        if(null === $this->agencyClientMapper) {
            $this->agencyClientMapper = $this->getService('planbold.mapper.agencyClient');
        }
        return $this->agencyClientMapper;
    }
    
    /**
     * @return StripeAccountMapperInterface|type
     */
    public function getStripeAccountMapper()
    {
        if(null === $this->stripeAccountMapper) {
            $this->stripeAccountMapper = $this->getService('planbold.mapper.stripeAccount');
        }
        return $this->stripeAccountMapper;
    }
    
    /**
     * @return PersonaSharedEmailMapperInterface|type
     */
    public function getPersonaSharedEmailMapper()
    {
        if (null === $this->personaSharedEmailMapper) {
            $this->personaSharedEmailMapper = $this->getService('planbold.mapper.personaSharedEmail');
        }
        return $this->personaSharedEmailMapper;
    }
    
    /**
     * Get All Industries
     * @return type
     */
    public function getAllIndustries()
    {
        $industries = $this->getIndustryMapper()->fetchAll();
        return $industries->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function getAllSurveyQuestions()
    {
        $questions = $this->getSurveyQuestionsMapper()->fetchAll();
        return $questions->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }



    /**
     * Get Persona
     * @param $uuid
     * @return array
     */
    public function getPersonaByUuid($uuid)
    {
        $persona = $this->getPersonaMapper()->findOneBy(array('uuid' => $uuid));
        $persona = $persona->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $persona = count($persona) == 0 ? array() : $persona[0];
    }
    
    /**
     * Get Persona By ShortCode
     * @param $uuid
     * @return array
     */
    public function getPersonaByShortCode($shortCode)
    {
        $persona = $this->getPersonaMapper()->findOneBy(array('shortCode' => $shortCode, 'publicFlag' => true));
        $persona = $persona->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $persona = count($persona) == 0 ? array() : $persona[0];
    }

    /**
     * Get all Persona by Account
     * @return mixed $persona
     */
    public function getAllPersona()
    {
        $account = $this->getAccount();
        $persona = $this->getPersonaMapper()->fetchAll(array('account' => $account));
        return $persona->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
    
    /**
     * Get Client Account
     * @param type $clientUuid
     * @return type
     */
    public function getAllPersonaByClient($clientUuid)
    {
        $account = $this->getClientAccount($clientUuid);
        $persona = $this->getPersonaMapper()->fetchAll(array('account' => $account));
        return $persona->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * @return array Motivations
     */
    public function getAllMotivations()
    {
        $motivations = $this->getMotivationsMapper()->fetchAll();
        return $motivations->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * @return array GlobalChannels
     */
    public function getAllGlobalChannels()
    {
        $channels = $this->getGlobalChannelsMapper()->fetchAll();
        return $this->formatGlobalChannels($channels->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY));
    }
    
    /**
     * format Global Channels
     * @param type $data
     */
    public function formatGlobalChannels($data)
    {
        $channels = array();
        foreach ($data as $channelData) {
            if ($channelData['parentId'] == 0) {
                $channels[$channelData['id']] = $channelData;
            } else {
                if (!isset($channels[$channelData['parentId']]['child'])) {
                    $channels[$channelData['parentId']]['child'] = array();
                }
                array_push($channels[$channelData['parentId']]['child'], $channelData);
            }
        }
        return $channels;
    }

    /**
     * @param $personaId
     * @return array PersonaKnowledge
     */
    public function getKnowledgeByPersona($personaId)
    {
        $personaKnowledge = $this->getPersonaKnowledgeMapper()->fetchAll(array('personaId' => $personaId));
        return $personaKnowledge->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * @param $persona_id
     * @return array
     */
    public function getChannelsByPersona($persona_id)
    {
        $personaChannels = $this->getPersonaChannelsMapper()->fetchAll(array('personaId' => $persona_id));
        return $personaChannels->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * Get Motivations by persona_id
     * @param $persona_id
     * @return mixed
     */
    public function getMotivationsByPersona($persona_id)
    {
        $personaMotivations = $this->getPersonaMotivationsMapper()->fetchAll(array('personaId' => $persona_id));
        return $personaMotivations->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }


    /**
     * @return array PersonaFields
     */
    public function getAllPersonaFields($params = array())
    {
        $personaFields = $this->getPersonaFieldsMapper()->fetchAll($params);
        return $personaFields->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * Get all Persona by Account Industry
     *
     * @return mixed $persona
     */
    public function getAllPersonaByIndustry($industry_id)
    {
        $industry = $this->getIndustryMapper()->findOneBy(array('id' => $industry_id));
        $account = $this->getAccount();
        $persona = $this->getPersonaMapper()->fetchAll(array('account' => $account, 'industry' => $industry));
        return $persona->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * Get All Persona Data by persona_id
     * @param $persona_id
     * @return mixed
     */
    public function getAllPersonaDataByPersona($persona_id)
    {
        $persona = $this->getPersonaMapper()->findOneById($persona_id);
        $personaData = $this->getPersonaDataMapper()->fetchAll(array('persona' => $persona));
        return $personaData->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }


    /**
     * Get AccountIndustry by id
     * @param $industry_id
     * @return array
     */
    public function getAccountIndustry($industry_id)
    {
        $userAccount = $this->getAccount();
        $accountIndustry = $this->getAccountIndustryMapper()->findOneBy(array('account' => $userAccount, 'industry' => $industry_id));
        return $accountIndustry = count($accountIndustry) == 0 ? array() : $accountIndustry[0];
    }

    /**
     * Get User
     * @return type
     */
    public function getUser()
    {
        $authService = $this->getService('zfcuser_auth_service');
        $currentUser = $authService->getIdentity();
        return $currentUser;
    }

    /**
     * Get Current Account
     * @return type
     */
    public function getAccount()
    {
        $currentUser = $this->getUser();
        $this->account = $currentUser->getAccount();
        return $this->account;
    }
    
    /**
     * Get Client Account
     * @param type $clientUuid
     * @return type
     */
    public function getClientAccount($clientUuid)
    {
        $currentUser   = $this->getUser();
        $clientAccount = $this->getAccountMapper()->findOneBy(array('uuid' => $clientUuid));
        return $clientAccount;
    }

    /**
     * Get All JobType
     * @return mixed
     */
    public function getAllJobtype()
    {
        $jobtype = $this->getJobtypeMapper()->fetchAll(array('deletedAt' => null));
        return $jobtype->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * Get All Archetype
     * @return mixed
     */
    public function getAllArchetype()
    {
        $archetype = $this->getArchetypeMapper()->fetchAll();
        return $archetype->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * @param $data
     * @return bool|int
     */
    public function savePersonaChannel($data)
    {
        $channelField   = $this->getPersonaFieldsMapper()->findOneBy(array('id' => $data['channelFieldId']));
        $persona        = $this->getPersonaMapper()->findOneById($data['personaId']);
        $personaData    = $this->getPersonaDataMapper()->findOneBy(array('persona' => $persona, 'personaFields' => $channelField));
        
        $em = $this->getService('Doctrine\\ORM\\EntityManager');
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            
            if ( count($personaData) == 0 ) {
                $personaData = new \Planbold\Entity\PersonaData();
            } 
            $personaData->setContent(json_encode(array()));
            if ( isset($data['content']) ) {
                $personaData->setContent(json_encode($data['content']));
            }
            $personaData->setPersonaFields($channelField);
            $personaData->setPersona($persona);
            $personaData->setDataType($channelField->getType());
            $personaData->setTitle(null);
            $persona->setUpdatedAt(new \DateTime());
            
            $em->persist($personaData);
            $em->persist($persona);

            $em->flush();
            $em->getConnection()->commit();
            $this->getEventManager()->trigger(
                'personaRevision.post',
                $this,
                array('personaData'     => array(
                      'revisionDetails' => '[channels]&nbsp;Channels, ',
                      'id'              => $persona->getId(),
                      'revisionId'      => $data['revisionId'],
                      'userId'          => $this->getUser()->getId(),
                      'username'        => $this->getUser()->getUsername(),
                      'accountId'       => $this->getUser()->getAccount()->getId(),
                      'account'         => $persona->getAccount(),
                      'user'            => $this->getUser(),
                      'headshot'        => $persona->getHeadshot(),
                      'isDeleted'       => false
                ))
            );
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            return false;
        }
        
        // payment subscription update
        $paymentService = $this->getService('payment.service');
        $paymentService->processSubscription($this->getUser());
        
        return $persona->getId();
    }
    
    /**
     * @param $data
     * @return bool|int
     */
    public function savePersonaMotivation($data)
    {
        $motivationField   = $this->getPersonaFieldsMapper()->findOneBy(array('id' => $data['motivationFieldId']));
        $persona           = $this->getPersonaMapper()->findOneById($data['personaId']);
        $personaData       = $this->getPersonaDataMapper()->findOneBy(array('persona' => $persona, 'personaFields' => $motivationField));
        
        $em = $this->getService('Doctrine\\ORM\\EntityManager');
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            
            if ( count($personaData) == 0 ) {
                $personaData = new \Planbold\Entity\PersonaData();
            } 
            $personaData->setContent(json_encode(array()));
            if ( isset($data['content']) ) {
                $personaData->setContent(json_encode($data['content']));
            }
            $personaData->setPersonaFields($motivationField);
            $personaData->setPersona($persona);
            $personaData->setDataType($motivationField->getType());
            $personaData->setTitle(null);
            $persona->setUpdatedAt(new \DateTime());
            $em->persist($personaData);
            $em->persist($persona);

            $em->flush();
            $em->getConnection()->commit();
            $this->getEventManager()->trigger(
                'personaRevision.post',
                $this,
                array('personaData'     => array(
                      'revisionDetails' => '[motivation]&nbsp;Motivation, ',
                      'id'              => $persona->getId(),
                      'revisionId'      => $data['revisionId'],
                      'userId'          => $this->getUser()->getId(),
                      'username'        => $this->getUser()->getUsername(),
                      'accountId'       => $this->getUser()->getAccount()->getId(),
                      'account'         => $persona->getAccount(),
                      'user'            => $this->getUser(),
                      'headshot'        => $persona->getHeadshot(),
                      'isDeleted'       => false
                ))
            );
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            return false;
        }
        
        // payment subscription update
        $paymentService = $this->getService('payment.service');
        $paymentService->processSubscription($this->getUser());
        
        return $persona->getId();
    }
    
    /**
     * @param $data
     * @return bool|int
     */
    public function savePersonaKnowledge($data)
    {
        $knowledgeField    = $this->getPersonaFieldsMapper()->findOneBy(array('id' => $data['knowledgeFieldId']));
        $persona           = $this->getPersonaMapper()->findOneById($data['personaId']);
        $personaData       = $this->getPersonaDataMapper()->findOneBy(array('persona' => $persona, 'personaFields' => $knowledgeField));
        
        $em = $this->getService('Doctrine\\ORM\\EntityManager');
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            
            if ( count($personaData) == 0 ) {
                $personaData = new \Planbold\Entity\PersonaData();
            } 
            $personaData->setContent(json_encode(array()));
            if ( isset($data['content']) ) {
                $personaData->setContent(json_encode($data['content']));
            }
            $personaData->setPersonaFields($knowledgeField);
            $personaData->setPersona($persona);
            $personaData->setDataType($knowledgeField->getType());
            $personaData->setTitle(null);
            $persona->setUpdatedAt(new \DateTime());
            $em->persist($persona);
            $em->persist($personaData);

            $em->flush();
            $em->getConnection()->commit();
            $this->getEventManager()->trigger(
                'personaRevision.post',
                $this,
                array('personaData'     => array(
                      'revisionDetails' => '[knowledge]&nbsp;Knowledge, ',
                      'id'              => $persona->getId(),
                      'revisionId'      => $data['revisionId'],
                      'userId'          => $this->getUser()->getId(),
                      'username'        => $this->getUser()->getUsername(),
                      'accountId'       => $this->getUser()->getAccount()->getId(),
                      'account'         => $persona->getAccount(),
                      'user'            => $this->getUser(),
                      'headshot'        => $persona->getHeadshot(),
                      'isDeleted'       => false
                ))
            );
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            return false;
        }
        
        // payment subscription update
        $paymentService = $this->getService('payment.service');
        $paymentService->processSubscription($this->getUser());
        
        return $persona->getId();
    }

    /**
     * Save Persona Data
     * @param $data
     * @return int id
     */
    public function savePersonaData($data)
    {
        $persona = $this->getPersonaMapper()->findOneById($data['persona_id']);
        $personaField = $this->getPersonaFieldsMapper()->findOneById($data['persona_field_id']);
        $personaData = $this->getPersonaDataMapper()->findOneBy(array('persona' => $persona, 'personaFields' => $personaField));
        
        if (count($personaData) == 0) {
            $personaData = new \Planbold\Entity\PersonaData();
            $personaData->setPersona($persona);
        }
        if ($data['field'] == "title") {
            $personaData->setTitle($data['content']);
        } else {
            $personaData->setContent($data['content']);
            if (count($personaField) != 0) {
                $personaData->setPersonaFields($personaField);
            }
        }

        $this->getEventManager()->trigger(__FUNCTION__, $this, array('personaData' => $personaData, 'data' => $data));
        $em = $this->getService('Doctrine\\ORM\\EntityManager');
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            $persona->setUpdatedAt(new \DateTime());
            
            $em->persist($persona);
            $em->persist($personaData);

            $em->flush();
            $em->getConnection()->commit();
            $this->getEventManager()->trigger(
                'personaRevision.post',
                $this,
                array('personaData'     => array(
                      'revisionDetails' => '[personadata]&nbsp;Persona Data, ',
                      'id'              => $personaData->getPersona()->getId(),
                      'revisionId'      => $data['revisionId'],
                      'userId'          => $this->getUser()->getId(),
                      'username'        => $this->getUser()->getUsername(),
                      'accountId'       => $this->getUser()->getAccount()->getId(),
                      'account'         => $persona->getAccount(),
                      'user'            => $this->getUser(),
                      'headshot'        => $persona->getHeadshot(),
                      'isDeleted'       => false
                ))
            );
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            return false;
        }
        
        // update payment process
        $authService    = $this->getService('zfcuser_auth_service');
        $paymentService = $this->getService('payment.service');
        $currentUser    = $authService->getIdentity();
        $paymentService->processSubscription($currentUser);
        
        return $personaData->getId();
    }

    /**
     * Save/ Update Industry Description per Account
     *
     * @param $data
     * @return \Planbold\Entity\Industry
     */
    public function updateIndustryDescription($data)
    {
        $userAccount = $this->getAccount();
        $accountIndustry = $this->getAccountIndustry($data['industry_id']);

        if (count($accountIndustry) == 0) {
            $accountIndustry = new \Planbold\Entity\AccountIndustry();
            $industry = $this->getIndustryMapper()->findOneBy(array('id' => $data['industry_id']));
            $accountIndustry->setIndustryName($industry->getIndustryName());
            $accountIndustry->setAccount($userAccount);
            $accountIndustry->setIndustry($data['industry_id']);
        }
        $accountIndustry->setDescription($data['description']);

        $this->getEventManager()->trigger(__FUNCTION__, $this, array('accountIndustry' => $accountIndustry, 'data' => $data));
        $em = $this->getService('Doctrine\\ORM\\EntityManager');
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            $em->persist($accountIndustry);

            $em->flush();
            $em->getConnection()->commit();
            $this->getEventManager()->trigger(
                __FUNCTION__ . '.post',
                $this,
                array('accountIndustry' => $accountIndustry, 'data' => $data)
            );
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            return $e->getMessage();
        }
        
        $authService    = $this->getService('zfcuser_auth_service');
        $paymentService = $this->getService('payment.service');
        $currentUser    = $authService->getIdentity();
        $paymentService->processSubscription($currentUser);
        
        return $accountIndustry->getIndustry();
    }

    /**
     * Get Persona Sections By Column uuid
     * @param $personaId
     * @return mixed
     */
    public function getPersonaSectionsByColumn($personaUuid, $columns)
    {
        try {
            $persona = $this->getPersonaMapper()->findOneByUuid($personaUuid);
            $data = [];
            foreach($columns as $val) {
                $personaSection = $this->getPersonaSectionsMapper()->fetchAll(array('personaId' => $persona->getId(), 'columnName' => $val));
                $personaSection =  $personaSection->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
                if(!empty($personaSection)) {
                    $data['column-'.$val] = $personaSection;
                }
            }
            return $data;
        } catch (\Exception $e){
            return false;
        }

    }

    /**
     * Save Persona Sections
     * @param $persona_uuid
     * @param $sections
     * @return bool
     */
    public function savePersonaSections($persona_uuid, $sections)
    {
        try {
            $persona = $this->getPersonaMapper()->findOneByUuid($persona_uuid);

            $em = $this->getServiceLocator()->get('Doctrine\\ORM\\EntityManager');
            if(is_array($sections)) {
                $personaSectionData = array();
                foreach ($sections as $section) {
                    $personaSection = $this->getPersonaSectionsMapper()->findOneBy(array('personaId' => $persona->getId(), 'sectionName' => $section['sectionName']));
                    if(count($personaSection) == 0){
                        $personaSection = new \Planbold\Entity\PersonaSections();
                        $personaSection->setPersonaId($persona->getId());
                        $personaSection->setSectionName($section['sectionName']);
                    }
                    $personaSection->setColumnName($section['column']);
                    $personaSection->setPosition($section['position']);
                    try {
                        $personaSectionData[] = $personaSection;
                        $em->persist($personaSection);
                        
                    } catch (\Exception $e) {
                        pe($e->getMessage());
                        return false;
                    }
                }
                $persona->setUpdatedAt(new \DateTime());
                $em->persist($persona);
                $em->flush();
                return true;
            }
        } catch (\Exception $e) {
            pe($e->getMessage());
        }
        $authService    = $this->getService('zfcuser_auth_service');
        $paymentService = $this->getService('payment.service');
        $currentUser    = $authService->getIdentity();
        $paymentService->processSubscription($currentUser);
    }

    /**
     * Get Survey by Survey_Respondent
     * @param $survey_respondent_uuid
     */
    public function getRespondentsSurveyByUuid($survey_respondent_uuid)
    {
        $respondent = $this->getSurveyRespondentMapper()->findOneBy(array('uuid' => $survey_respondent_uuid));
        return $respondent->getSurvey();
    }

    /**
     * Save Survey Respondents
     * @param $data
     * @return bool
     */
    public function saveSurveyRespondents($data)
    {
        try {
            $respondent = $this->getSurveyRespondentMapper()->findOneBy(array('uuid' => $data['uuid']));
            $respondent->setAge($data['age']);
            $respondent->setGender($data['gender']);
            $respondent->setArchetype($this->getArchetypeMapper()->findOneById($data['archetype']));
            $respondent->setIndustry($this->getIndustryMapper()->findOneById($data['industry']));
            $respondent->setJobtype($this->getJobtypeMapper()->findOneById($data['jobtype']));
            $respondent->setJobTitle($data['jobTitle']);
            $respondent->setJobDescription($data['jobDescription']);
            $respondent->setBasicLife($data['basicLife']);
            $respondent->setResponded(true);

            $em = $this->getServiceLocator()->get('Doctrine\\ORM\\EntityManager');

            if(!empty($data['personaFieldlist'])) {
                $surveyData = array();
                foreach ($data['personaFieldlist'] as $key => $val) {
                    $fieldId = str_replace('fieldId', '', $key);
                    $personaField = $this->getPersonaFieldsMapper()->findOneById($fieldId);
                    $content = json_encode($val);
                    $surveyRespondentData = $this->getSurveyRespondentDataMapper()->findOneBy(array('personaFields' => $personaField, 'surveyRespondent' => $respondent));
                    if(count($surveyRespondentData) == 0){
                        $surveyRespondentData = new \Planbold\Entity\SurveyRespondentData();
                        $surveyRespondentData->setSurveyRespondent($respondent);
                        $surveyRespondentData->setPersonaFields($personaField);
                    }
                    $surveyRespondentData->setTitle($personaField->getTitle());
                    $surveyRespondentData->setContent($content);
                    $surveyRespondentData->setDataType('list');
                    try {
                        $surveyData[] = $surveyRespondentData;
                        $em->persist($surveyRespondentData);
                    } catch (\Exception $e) {
                        pe($e->getMessage());
                        return false;
                    }
                }
            }

            $this->getSurveyRespondentMapper()->update($respondent);
            
            $authService    = $this->getService('zfcuser_auth_service');
            $paymentService = $this->getService('payment.service');
            $currentUser    = $authService->getIdentity();
            $paymentService->processSubscription($currentUser);
            
            return true;
        } catch(\Exception $e) {
            pe('Error',$e->getMessage());
            return false;
        }

    }

    /**
     * Send Email Survey link form
     * @param $posts
     * @return bool
     */
    public function sendSurvey($data)
    {

        $survey = $this->getSurveyMapper()->findOneBy(array('personaId' => $data['persona_id'], 'user' => $this->getUser()));

        if (count($survey) == 0) {
            $survey = new \Planbold\Entity\Survey();
            $survey->setPersona($data['persona_id']);
            $survey->setUser($this->getUser());
        }

        $survey->setMessage($data['message']);
        $survey->setQuestions($data['questions']);
        $survey->setCustomTitles(json_encode($data['customTitles']));

        $survey->setPersona($data['persona_id']);

        $respondents = array();
        $em = $this->getServiceLocator()->get('Doctrine\\ORM\\EntityManager');
        $em->getConnection()->beginTransaction(); // suspend auto-commit

        try {
            if (!empty($data['sendTo'])) {
                foreach ($data['sendTo'] as $val) {
                    $respondent = $this->getSurveyRespondentMapper()->findOneBy(array('survey' => $survey, 'email' => $val));
                    if( count($respondent) === 0 ) {
                        $respondent  = new \Planbold\Entity\SurveyRespondent();
                        $respondent->setSurvey($survey);
                    }

                    $respondent->setEmail($val);
                    $respondents[] = array('email' => $respondent->getEmail(),
                                           'uuid'  => $respondent->getUuid());
                    $em->persist($respondent);
                }
            }
            $em->persist($survey);
            $em->flush();
            $em->getConnection()->commit();
            $this->getEventManager()->trigger(
                __FUNCTION__.'.post',
                $this,
                array(
                    'respondents' => $respondents,
                    'data'        => $data
                )
            );

            $authService    = $this->getService('zfcuser_auth_service');
            $paymentService = $this->getService('payment.service');
            $currentUser    = $authService->getIdentity();
            $paymentService->processSubscription($currentUser);

            return true;
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            return false;
        }

    }

    /**
     * Get Industry By Respondents id
     * @param $respondents
     * @return mixed
     */
    public function getIndustryByRespondents($respondents)
    {
        $industry = $this->getSurveyRespondentMapper()->fetchIndustryGroupBy($respondents);
        return $industry->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * Get Gender by Respondents id
     * @param $respondents
     * @return mixed
     */
    public function getGenderByRespondents($respondents)
    {
        $gender = $this->getSurveyRespondentMapper()->fetchGenderGroupBy($respondents);
        return $gender->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * Get Age by Respondents id
     * @param $respondents
     * @return mixed
     */
    public function getAgeByRespondents($respondents)
    {
        $age = $this->getSurveyRespondentMapper()->fetchAgeGroupBy($respondents);
        return $age->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * Get Archetype By Respondents
     * @param $respondents
     * @return mixed
     */
    public function getArchetypeByRespondents($respondents)
    {
        $industry = $this->getSurveyRespondentMapper()->fetchArchetypeGroupBy($respondents);
        return $industry->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * Get Jobtype by Respondents id
     * @param $respondents
     * @return mixed
     */
    public function getJobtypeByRespondents($respondents)
    {
        $jobtype = $this->getSurveyRespondentMapper()->fetchJobtypeGroupBy($respondents);
        return $jobtype->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * Get Survey Respondents Count by persona_id
     *
     * @param $persona_id
     * @return bool|int
     */
    public function getSurveyRespondentsCountByPersona($persona_id)
    {
        $survey = $this->getSurveyByPersona($persona_id);


        if($survey) {
            $respondents = $this->getSurveyRespondentsBySurvey($survey->getId());
            return count($respondents);
        }
        return false;
    }

    /**
     * Get Survey By persona_id
     * @param $persona_id
     * @return mixed
     */
    public function getSurveyByPersona($persona_id)
    {
        $survey = $this->getSurveyMapper()->findOneBy(array('personaId' => $persona_id));
        return $survey;
    }

    /**
     * Get Survey Respondents by Survey
     * @param $survey
     * @return mixed
     */
    public function getSurveyRespondentsBySurvey($survey)
    {
        $survey = $this->getSurveyRespondentMapper()->fetchAll(array('survey' => $survey, 'responded' => true));
        return $survey->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * Get SurveyData By Respondents
     * @param array $respondents
     * @return bool
     */
    public function getSurveyDataByRespondents(array $respondents = array())
    {
        try {
            $respData = $this->getSurveyRespondentDataMapper()->fetchAllByRespondents($respondents);
            return $respData->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        } catch(\Exception $e) {
            pe($e->getMessage());
            return false;
        }


    }

    /**
     * Save persona by Field
     * @param $user
     * @param $data
     * @param $clientAccount = null (optional)
     * @return bool|\Planbold\Entity\Persona
     * @throws Exception\InvalidArgumentException
     */
    public function savePersona($user, $data, $clientAccount = null)
    {
        $isCreated = false;
        // for client account
        if ($clientAccount !== null ) {
            $userAccount = $clientAccount;
        } else {
            $userAccount = $user->getAccount();
        }
       
        if (isset($data['persona_id']) && !empty($data['persona_id'])) {
            $persona = $this->getPersonaMapper()->findOneById($data['persona_id']);
           
        } else {
            $persona = new \Planbold\Entity\Persona();
            $persona->setUser($user);
            $persona->setAccount($userAccount);
            $persona->setAge('{"min":0,"max":100}');
            $persona->setGender('{"male":50,"female":50}');
            $persona->setHeadshot('/img/headshots/1x400.jpg');
            // set default value
            $persona->setArchivedAt(null);
            $persona->setShortCode($this->shortHash());
            $persona->setPublicFlag(false);
            $isCreated = true;
        }
        $isDelete = false;
        
        $content = $data['content'];
        switch ($data['field']) {
            case 'name':
                $persona->setName($content);
                break;
            case 'jobTitle':
                $persona->setJobTitle($content);
                break;
            case 'jobtype':
                $jobtype = $this->getJobtypeMapper()->findOneById($content);
                $persona->setJobtype($jobtype);
                break;
            case 'jobDescription':
                $persona->setJobDescription($content);
                break;
            case 'headshot':
                $persona->setHeadshot($content);
                break;
            case 'age':
                $persona->setAge($content);
                break;
            case 'gender':
                $persona->setGender($content);
                break;
            case 'location':
                $persona->setLocation($content);
                break;
            case 'basicLife':
                $persona->setBasicLife($content);
                break;
            case 'archetype':
                $archetype = $this->getArchetypeMapper()->findOneById($content);
                $persona->setArchetype($archetype);
                break;
            case 'industry':

                $industry = $this->getIndustryMapper()->findOneBy(array('id' => $content));

                $accountIndustry = $this->getAccountIndustryMapper()->fetchOneBy(array('account' => $userAccount, 'industry' => $content));
                if (count($accountIndustry) == 0) {
                    $accountIndustry = new \Planbold\Entity\AccountIndustry();
                    $accountIndustry->setAccount($userAccount);
                }
                $accountIndustry->setIndustry($content);
                $accountIndustry->setIndustryName($industry->getIndustryName());
                $accountIndustry->setDescription($industry->getDescription());
                $persona->setIndustry($industry);
                break;
            case 'delete':
                $isDelete= true;
                $persona->setArchivedAt(new \DateTime());
                break;
            default:
                return false;
        }

        $this->getEventManager()->trigger(__FUNCTION__, $this, array('persona' => $persona, 'data' => $data));
        $em = $this->getService('Doctrine\\ORM\\EntityManager');
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            if ( isset($accountIndustry) ) {
                $em->persist($accountIndustry);
            }
            $em->persist($persona);
            $em->flush();
            $em->getConnection()->commit();
            $this->getEventManager()->trigger(
                'personaRevision.post',
                $this,
                array('personaData'     => array(
                      'revisionDetails' => '[personadetails]&nbsp;Persona Details, ',
                      'id'              => $persona->getId(),
                      'revisionId'      => $data['revisionId'],
                      'userId'          => $user->getId(),
                      'username'        => $user->getUsername(),
                      'accountId'       => $user->getAccount()->getId(),
                      'account'         => $persona->getAccount(),
                      'user'            => $user,
                      'headshot'        => $persona->getHeadshot(),
                      'isDeleted'       => $isDelete,
                      'isCreated'       => $isCreated
                ))
            );
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
        }
        
        // update payment process
        $paymentService = $this->getService('payment.service');
        $paymentService->processSubscription($user);
        
        return $persona;
    }
    
    /**
     * 
     * @param type $posts
     */
    public function saveGettingStarted($posts)
    {
        $em = $this->getService('Doctrine\\ORM\\EntityManager');
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
        
            $user        = $this->getUser();
            $userAccount = $user->getAccount();
            $persona     = new \Planbold\Entity\Persona();
            $persona->setUser($user);
            $persona->setAccount($userAccount);
            $persona->setAge('{"min":0,"max":100}');
            $persona->setGender('{"male":50,"female":50}');
            $persona->setHeadshot('/img/headshots/1x400.jpg');
            // set default value
            $persona->setArchivedAt(null);
            $persona->setName($posts['persona-name']);
            $persona->setPublicFlag(false);
            $isCreated = true;
            $isDelete  = false;

            $industry = $this->getIndustryMapper()->findOneBy(array('uuid' => $posts['persona-industry']));

            $accountIndustry = $this->getAccountIndustryMapper()->fetchOneBy(array('account' => $userAccount, 'industry' => $posts['persona-industry']));
            if (count($accountIndustry) == 0) {
                $accountIndustry = new \Planbold\Entity\AccountIndustry();
                $accountIndustry->setAccount($userAccount);
            }
            $accountIndustry->setIndustry($industry->getId());
            $accountIndustry->setIndustryName($industry->getIndustryName());
            $accountIndustry->setDescription($industry->getDescription());
            $persona->setIndustry($industry);

            $archetype = $this->getArchetypeMapper()->findOneBy(array('uuid' => $posts['persona-industry']));
            $persona->setArchetype($archetype);
        
            if ( isset($accountIndustry) ) {
                $em->persist($accountIndustry);
            }
            $em->persist($persona);
            $em->flush();
            $em->getConnection()->commit();
            $this->getEventManager()->trigger(
                'personaRevision.post',
                $this,
                array('personaData'     => array(
                      'revisionDetails' => '[personadetails]&nbsp;Persona Details, ',
                      'id'              => $persona->getId(),
                      'revisionId'      => uniqid('REV-'),
                      'userId'          => $user->getId(),
                      'username'        => $user->getUsername(),
                      'accountId'       => $userAccount->getId(),
                      'account'         => $userAccount,
                      'user'            => $user,
                      'headshot'        => $persona->getHeadshot(),
                      'isDeleted'       => $isDelete,
                      'isCreated'       => $isCreated
                ))
            );
            
            // set uuid
            $this->gettingStartedUuid = $persona->getUuid();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            return false;
        }
        
        // update payment process
        $paymentService = $this->getService('payment.service');
        $paymentService->processSubscription($user);
        
        return true;
    }
    
    /**
     * Return getting started uuid
     * @return type
     */
    public function getGettingStartedUuid()
    {
        return $this->gettingStartedUuid;
    }
    
    /**
     * save persona details
     * @param type $user
     * @param type $data
     */
    public function savePersonaDetails($user, $data)
    {  
        if ( isset($data['persona_id']) && !empty($data['persona_id'])) {
            $persona = $this->getPersonaMapper()->findOneById($data['persona_id']);
        }
        
        if ( isset($data['content']['age']) ) {
            $persona->setAge($data['content']['age']);
        }
        if ( isset($data['content']['gender']) ) {
            $persona->setGender($data['content']['gender']);
        }
        
        $em = $this->getService('Doctrine\\ORM\\EntityManager');
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            $em->persist($persona);
            $em->flush();
            $em->getConnection()->commit();
            $this->getEventManager()->trigger(
                'personaRevision.post',
                $this,
                array('personaData'     => array(
                      'revisionDetails' => '[age]&nbsp;Persona Age/Gender, ',
                      'id'              => $persona->getId(),
                      'revisionId'      => $data['revisionId'],
                      'userId'          => $user->getId(),
                      'username'        => $user->getUsername(),
                      'accountId'       => $persona->getAccount()->getId(),
                      'account'         => $persona->getAccount(),
                      'user'            => $user,
                      'headshot'        => $persona->getHeadshot(),
                      'isDeleted'       => false
                ))
            );
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
        }
        
        // update payment process
        $paymentService = $this->getService('payment.service');
        $paymentService->processSubscription($user);
        
        return $persona;
    }
    
    /**
     * Get Client Persona
     * @param type $posts
     * @return type
     */
    public function getClientPersona($posts)
    {
        if ( isset($posts['uuid']) ) {
            $account = $this->getAccountMapper()->findOneBy(array('uuid' => $posts['uuid']));
            return $this->getPersonaMapper()->fetchAll(array('account' => $account))->getResult();
        } else {
            return array();
        }
    }
    
    /**
     * Get PersonaRevision
     * @param type $revisionId
     * @return type
     */
    public function getPersonaRevision($revisionId)
    {
        return $this->formatPersonaRevision($this->getPersonaRevisionMapper()->findOneBy(array('revisionId' => $revisionId)));
    }
    
    /**
     * Get all persona revision
     * @param type $personaId
     */
    public function getAllPersonaRevision($personaId)
    {
        return $this->getPersonaRevisionMapper()->fetchAll(array('personaId' => $personaId))->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
    
    /**
     * Format Persona Revision
     * @param type $personaData
     * @return array
     */
    private function formatPersonaRevision($personaData)
    {
        $retArray = 0;
        if ( count($personaData) > 0 ) {
            $retArray = array(
                'personaId'   => $personaData->getPersonaId(),
                'personaName' => $personaData->getPersonaName(),
                'content'     => json_decode($personaData->getContent(), true)
            );
        }
        return $retArray;
    }
    
    /**
     * Revert Changes on persona
     * @param type $revisionId
     * @return boolean
     */
    public function revertPersona($revisionId)
    {
        $rawPersona = $this->getPersonaRevision($revisionId);
        
        $em = $this->getService('Doctrine\\ORM\\EntityManager');
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            
            if ( $revisionId == "0" ) {
                return false;
            }
            
            $personaData     = $rawPersona['content']['persona'];
            $personaSections = $rawPersona['content']['personaOrder'];
            $personaFields   = $rawPersona['content']['personaFields'];
            
            $personaObj = $this->getPersonaMapper()->findOneById($personaData['id']);
            $personaObj->setName($personaData['name']);
            $personaObj->setJobTitle($personaData['jobTitle']);
            $personaObj->setGender($personaData['gender']);
            $personaObj->setAge($personaData['age']);
            $personaObj->setJobTitle($personaData['jobTitle']);
            $personaObj->setJobDescription($personaData['jobDescription']);
            $personaObj->setHeadshot($personaData['headshot']);
            
            if ( isset($personaData['industryId']) ) {
                $personaObj->setIndustry($this->getIndustryMapper()->findOneBy(array('id' =>$personaData['industryId'])));
            }
            if ( isset($personaData['jobtypeId']) ) {
                $personaObj->setJobtype($this->getJobtypeMapper()->findOneById($personaData['jobtypeId']));
            }
            if ( isset($personaData['archetypeId']) ) {
                $personaObj->setArchetype($this->getArchetypeMapper()->findOneById($personaData['archetypeId']));
            }
            $personaObj->setBasicLife($personaData['basicLife']);
            
            $em->persist($personaObj);
            
            // persona Data fields
            foreach ($personaFields as $fields) {
                $personaDataObj = $this->getPersonaDataMapper()->findOneBy(array('id' =>$fields['id']));
                
                if ( count($personaDataObj) <= 0 ) {
                    $personaDataObj = new \Planbold\Entity\PersonaData();
                }
                
                if ( isset($fields['personaFieldId']) ) {
                    $personaDataObj->setPersonaFields($this->getPersonaFieldsMapper()->findOneById($fields['personaFieldId']));
                }
                
                $personaDataObj->setTitle($fields['title']);
                $personaDataObj->setContent($fields['content']);
                
                $em->persist($personaDataObj);
            }

            $em->flush();
            $em->getConnection()->commit();
            return array('success' =>true, 'personaId' => $personaObj->getUuid());
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            var_dump('<pre>', $e->getMessage());exit;
            return false;
        }
    }
    
    /**
     * Get Client Details
     * @param type $uuid
     * @return type
     */
    public function getClientDetails($uuid)
    {
        if ( !empty($uuid) ) {
            $account = $this->getAccountMapper()->findOneBy(array('uuid' => $uuid));
        }
        return $this->formatAccountDetails($account);
    }
    
    /**
     * Format Account Details
     * @param type $account
     */
    public function formatAccountDetails($account)    
    {
        $returnVal = array();
        if (!empty($account) ) {
            $returnVal['name']        = $account->getName();
            $returnVal['description'] = $account->getDescription();
            $returnVal['uuid']        = $account->getUuid();
        }
        return $returnVal;
    }
    
    /**
     * Add/Update Files
     * @param type $posts
     * @return boolean
     */
    public function addUpdateFiles($posts)
    {
        try {
            if ( isset($$posts['uuid']) ) {
                $accountFile = $this->getAccountFileMapper()->findOneBy(array('uuid' => $posts['uuid']));
            } else {
                $accountFile =  new \Planbold\Entity\AccountFile();
                $accountFile->setAccount($this->getAccountMapper()->findOneBy(array('uuid' => $posts['clientUuid'])));
            }
            $accountFile->setName($posts['txt-name']);
            $accountFile->setPath($posts['file']);

            if ( isset($posts['uuid']) ) {
                $this->getAccountFileMapper()->update($accountFile);
            } else {
                $this->getAccountFileMapper()->insert($accountFile);
            }
            $this->getEventManager()->trigger(
                'addUpdateFiles.post',
                $this,
                array('accountFile'     => $accountFile)
            );
            return true;
        } catch(\Exception $e) {
            var_dump($e->getMessage());exit;
            return false;
        }
    }
    
    /**
     * process Webhooks
     * @param type $webhook
     * @return boolean
     */
    public function processWebHook($webhook)
    {
        $em = $this->getService('Doctrine\\ORM\\EntityManager');
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            $stripeAccount = $this->getStripeAccountMapper()->findOneBy(array('accountId' => $webhook['data']->customer));

            if ( count($stripeAccount) == 0 ) {
                $stripeAccount = new \Planbold\Entity\StripeAccount();
            }
            if ( $webhook['type'] == 'charge.failed' ) {
                $stripeAccount->setStripeStatus(false);
            }
            if ( $webhook['type'] == 'charge.succeeded' ) {
                $stripeAccount->setStripeStatus(false);
            }
            $em->persist($stripeAccount);
            $em->flush();
            $em->getConnection()->commit();
            return true;
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            return false;
        }
    }
    
    /**
     * Delete Persona Data
     * @param type $data
     */
    public function deletePersonaData($data)
    {
        try {
            if ( isset($data['persona_data_id']) ) {
                $persona = $this->getPersonaMapper()->findBy(array('id' => $data['persona_id']));
                if ( count($persona) ) {
                    $personaData = $this->getPersonaDataMapper()->findOneBy(array('id' => $data['persona_data_id'], 'persona' => $persona));
                    if ( count($personaData) ) {
                        $this->getPersonaDataMapper()->delete($personaData);
                        $this->getEventManager()->trigger(
                            'personaRevision.post',
                            $this,
                            array('personaData'     => array(
                                  'revisionDetails' => '[personadata]&nbsp;Persona Data, ',
                                  'id'              => $persona->getId(),
                                  'revisionId'      => $data['revisionId'],
                                  'userId'          => $this->getUser()->getId(),
                                  'username'        => $this->getUser()->getUsername(),
                                  'accountId'       => $this->getUser()->getAccount()->getId(),
                                  'account'         => $persona->getAccount(),
                                  'user'            => $this->getUser(),
                                  'headshot'        => $persona->getHeadshot(),
                                  'isDeleted'       => false
                            ))
                        );
                        return true;
                    }
                } 
            }
            return false;
        } catch(\Exception $e ){
            return false;
        }
    }
    
    /**
     * Get Headshots 
     * @return type
     */
    public function getHeadshot()
    {
        return $this->getHeadshotMapper()->fetchAll()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
    
    /**
     * Get all Persona Industry
     */
    public function getAllPersonaIndustry($account = null)
    {
        if ( $account == null ) {
            $account = $this->getUser()->getAccount();
        }
        $persona = $this->getPersonaMapper()->fetchAll(array('account' => $account))->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        
        $arrIndustryIds = array();
        foreach($persona as $personaData) {
            if ( !empty($personaData['industryId'])) {
                array_push($arrIndustryIds, $personaData['industryId']);
            }
        }
        $industries = $this->getIndustryMapper()->fetchAllPersonaIndustry($arrIndustryIds)->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $industries;
    }
    
    /**
     * Save Archetype Details
     * @param type $user
     * @param type $data
     * @param type $clientAccount
     * @return boolean
     */
    public function saveArchetypeData($user, $data, $clientAccount = null)
    {
        $isCreated = false;
        $isDelete  = false;
        // for client account
        if ($clientAccount !== null ) {
            $userAccount = $clientAccount;
        } else {
            $userAccount = $user->getAccount();
        }
         if (isset($data['persona_id']) && !empty($data['persona_id'])) {
            $persona = $this->getPersonaMapper()->findOneById($data['persona_id']);
         } else {
             return false;
         }
         
        if (count($persona) > 0 ) { 
            $em = $this->getService('Doctrine\\ORM\\EntityManager');
            $em->getConnection()->beginTransaction(); // suspend auto-commit
            try {
                //job title
                $persona->setJobTitle($data['jobTitle']);
                //job type
                $jobtype = $this->getJobtypeMapper()->findOneById($data['jobtype']);
                $persona->setJobtype($jobtype);
                //job description
                $persona->setJobDescription($data['jobDescription']);
                //basic life
                $persona->setBasicLife($data['basicLife']);
                //archetype
                $archetype = $this->getArchetypeMapper()->findOneById($data['archetype']);
                $persona->setArchetype($archetype);
                //industry
                $industry = $this->getIndustryMapper()->findOneBy(array('id' => $data['industry']));
                $accountIndustry = $this->getAccountIndustryMapper()->fetchOneBy(array('account' => $userAccount, 'industry' => $content));
                if (count($accountIndustry) == 0) {
                    $accountIndustry = new \Planbold\Entity\AccountIndustry();
                    $accountIndustry->setAccount($userAccount);
                }
                $accountIndustry->setIndustry($content);
                $accountIndustry->setIndustryName($industry->getIndustryName());
                $accountIndustry->setDescription($industry->getDescription());
                $persona->setIndustry($industry);

                if ( isset($accountIndustry) ) {
                    $em->persist($accountIndustry);
                }
                $em->persist($persona);
                $em->flush();
                $em->getConnection()->commit();
                $this->getEventManager()->trigger(
                    'personaRevision.post',
                    $this,
                    array('personaData'     => array(
                          'revisionDetails' => '[archetype]&nbsp;Persona Archetype, ',
                          'id'              => $persona->getId(),
                          'revisionId'      => $data['revisionId'],
                          'userId'          => $user->getId(),
                          'username'        => $user->getUsername(),
                          'accountId'       => $user->getAccount()->getId(),
                          'account'         => $persona->getAccount(),
                          'user'            => $user,
                          'headshot'        => $persona->getHeadshot(),
                          'isDeleted'       => $isDelete,
                          'isCreated'       => $isCreated
                    ))
                );
            } catch (\Exception $e) {
                $em->getConnection()->rollback();
                return false;
            }

            // update payment process
            $paymentService = $this->getService('payment.service');
            $paymentService->processSubscription($user);

            return $persona;
        }
        return false;
    }
    
    /**
     * Short Code Hash based on microtime
     * @return string
     */
    public static function shortHash()
    {
        $hash = base_convert(time(), 10, 36);
        return $hash;
    }
    
    /***
     * Migrate ShortCode
     */
    public function migrateShortCode()
    {
        try {
            $personaData = $this->getPersonaMapper()->cleanfetchAll()->getResult();
            foreach( $personaData as $personaDetails ) {
                $personaDetails->setShortCode($this->shortHash());
                $this->getPersonaMapper()->update($personaDetails);
                sleep(2);
            }
        } catch(\Exception $e) {
            var_dump($e->getMessage());exit;
        }
    }
    
    /**
     * Process Share Persona
     * @param type $params
     * @return boolean
     */
    public function processSharePersona($params)
    {
        try {
            
            $personaSharedEmail = $this->getPersonaSharedEmailMapper()->findOneBy(array('persona' => $this->getPersonaMapper()->findBy(array('id' => $params['persona_id']))));
            
            if ( count($personaSharedEmail) == 0 ) {
                $personaSharedEmail = new \Planbold\Entity\PersonaSharedEmail();
                $personaSharedEmail->setEmails($params['emails']);
                $this->getPersonaSharedEmailMapper()->insert($personaSharedEmail);
            } else {
                $personaSharedEmail->setEmails($params['emails']);
                $this->getPersonaSharedEmailMapper()->update($personaSharedEmail);
            }
            $personaData = $this->getPersonaMapper()->findOneBy(array('id' => $params['persona_id']))->getResult();
            
            $userDetails = $this->getUser();
            $email       = $userDetails->getFirstName();
            $firstName   = $userDetails->getEmail();
            $shortCode   = $personaData[0]['shortCode'];
            $password    = $personaData[0]['password'];
            
            $this->getEventManager()->trigger(
                __FUNCTION__.'.post',
                $this,
                array(
                    'shared' => array(
                        'email'     => $email,
                        'firstName' => $firstName,
                        'shortCode' => $shortCode,
                        'password'  => md5($password)
                    ),
                    'emails' => explode(',', $params['emails'])
                )
            );
            return true;
        } catch (Exception $ex) {
            var_dump($ex->getMessage());exit;
            return false;
        }
    }
    
    /**
     * Enable Password
     * @param type $params
     * @return boolean
     */
    public function processEnablePersonaPassword($params)
    {
        try {
            $personaData = $this->getPersonaMapper()->findOneBy(array('id' => $params['persona_id']))->getResult();
            
            if ( count($personaData) > 0 ) {
            
                $persona     = $this->getPersonaMapper()->findBy(array('id' => $params['persona_id']));
                
                if ( isset($params['havePassword']) && (boolean) $params['havePassword']) {
                    $persona->setPassword($params['txt-password']);
                } else {
                    $persona->setPassword(null);
                }
                $this->getPersonaMapper()->update($persona);
                return true;
            }
        } catch (Exception $ex) {
            //do nothing
        }
        return false;
    }
    
    /**
     * Enable Sharing
     * @param type $params
     * @return boolean
     */
    public function processEnableSharing($params)
    {
        try {
            $personaData = $this->getPersonaMapper()->findOneBy(array('id' => $params['persona_id']))->getResult();
            
            if ( count($personaData) > 0 ) {
            
                $persona     = $this->getPersonaMapper()->findBy(array('id' => $params['persona_id']));
                $persona->setPublicFlag($params['enableSharing']);
                $this->getPersonaMapper()->update($persona);

                return true;
            }
        } catch (Exception $ex) {
            //do nothing
        }
        return false;
    }
    
    /**
     * Validate Persona Password
     * @param type $params
     * @return boolean
     */
    public function validatePersonaPassword( $params )
    {
        try {
            $personaData = $this->getPersonaMapper()->findOneBy(array('id' => $params['persona_id']))->getResult();
            
            if ( count($personaData) > 0 ) {
            
                $persona     = $this->getPersonaMapper()->findBy(array('id' => $params['persona_id']));
                $persona->setPublicFlag($params['enableSharing']);
                $this->getPersonaMapper()->update($persona);

                return true;
            }
        } catch (Exception $ex) {
            //do nothing
        }
        return false;
    }
}
<?php
namespace Audience\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;

class Account implements ServiceLocatorAwareInterface, EventManagerAwareInterface
{
    use ServiceLocatorAwareTrait;

    use EventManagerAwareTrait;

    /**
     * @var AccountMapperInterface
     */
    protected $accountMapper;
    
    /**
     * @var AgencyClientMapperInterface 
     */
    protected $agencyClientMapper;
    
    /**
     * @var Persona Revision Mapper 
     */
    protected $personaRevisionMapper;

    /**
     * Get Service Object
     * @param string $serviceName
     * @return type
     */
    private function getService($serviceName)
    {
        return $this->getServiceLocator()->get($serviceName);
    }
    
    /**
     * @return AccountMapperInterface|type
     */
    public function getAccountMapper()
    {
        if(null === $this->accountMapper) {
            $this->accountMapper = $this->getService('planbold.mapper.account');
        }
        return $this->accountMapper;
    }
    
    /**
     * @return AgencyClientMapperInterface|type
     */
    public function getAgencyClientMapper()
    {
        if(null === $this->agencyClientMapper) {
            $this->agencyClientMapper = $this->getService('planbold.mapper.agencyClient');
        }
        return $this->agencyClientMapper;
    }
    
    /**
     * Persona Revision 
     * @return type
     */
    public function getPersonaRevisionMapper()
    {
        if(null === $this->personaRevisionMapper) {
            $this->personaRevisionMapper = $this->getService('planbold.mapper.personaRevision');
        }
        return $this->personaRevisionMapper;
    }
    
    /**
     * Add Persona Client
     * @param type $currentUser
     * @param type $data
     * @return boolean
     */
    public function addPersonaClient($currentUser, $data)
    {
        try {
            if ( count($this->getAgencyClientMapper()->validateAgencyClientExist(array('client-name'=> $data['client-name'], 'account' => $currentUser->getAccount()))) > 0) {
                return array('success' => false, 'message' => 'Client Name has already added');
            }
            
            if ( $data['action'] == 'add' ) {
                $account = new \Planbold\Entity\Account();
                $account->setName($data['client-name']);
                $account->setDescription($data['client-description']);

                $this->getAccountMapper()->insert($account);
                $agencyClient = new \Planbold\Entity\AgencyClient();
                $agencyClient->setAccount($currentUser->getAccount());
                $agencyClient->setClientId($account->getId());

                $this->getAgencyClientMapper()->insert($agencyClient);
                return array('success'=>true, 'action' => 'add', 'data' => $this->getAllClient($currentUser->getAccount()));
            } else {
                $account = $this->getAccountMapper()->findOneBy(array('uuid' => $data['uuid']));
                $account->setName($data['client-name']);
                $account->setDescription($data['client-description']);
                $this->getAccountMapper()->update($account);
                return array('success'=>true, 'action' => 'update');
            }
        } catch (\Exception $e) {
            return array('success'=> true, 'message' => 'An error occured. Please try again later.');
        }
    }
    
    /**
     * Get All Client
     * @param type $account
     * @return type
     */
    public function getAllClient($account)
    {
        return $this->getAgencyClientMapper()->getAllClient($account);
    }
    
    /**
     * Get client account by uuid
     * @param type $uuid
     * @return type
     */
    public function getClientAccountByUuid($uuid)
    {
        return $this->getAccountMapper()->findOneBy(array('uuid' => $uuid));
    }
    
    /**
     * Get All Persona Revision
     * @param type $accountId
     * @return type
     */
    public function getAllPersonaRevision($accountId)
    {
        return $this->getPersonaRevisionMapper()->fetchAll(array('accountId' => $accountId))->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}
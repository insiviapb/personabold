<?php
/**
 * Persona EventListener
 *
 * @link
 * @copyright Copyright (c) 2015
 */

namespace Audience\Service\Listener;

use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Aws\S3\Exception\S3Exception;
use Aws\Sdk as AwsSdk;

/**
 * Persona Event Listener
 * @author  Oliver Pasigna <opasigna@insivia.com>
 * @SuppressWarnings(PHPMD)
 */
class PersonaEventListener implements ListenerAggregateInterface, ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    
    /**
     * List of listeners
     *
     * @var \Zend\Stdlib\CallbackHandler[]
     */
    protected $listeners = [];
    
    /**
     * Attach listeners to an event manager
     * @param  EventManagerInterface $events
     * @return void
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('personaRevision.post', [$this, 'savePersonaRevision'], 1000);
        $this->listeners[] = $events->attach('addUpdateFiles.post', [$this, 'uploadFiles'], 900);
        $this->listeners[] = $events->attach('addUpdateFiles.post', [$this, 'updateFiles'], 800);
        
    }
    
    /**
     * Detach listeners from an event manager
     * @param  EventManagerInterface $events
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    
    public function postUpload(EventInterface $event)
    {
    }
    
    
    /**
     * Listen to the "api.del.success" event
     * @param EventInterface $event
     */
    public function delFailed(EventInterface $event)
    {
    }
    
    
    
    /**
     * Save Persona Revision
     * @param EventInterface $event
     */
    public function savePersonaRevision(EventInterface $event)
    {
        $personaData           = $event->getParam('personaData');
        $personaMapper         = $this->getServiceLocator()->get('planbold.mapper.persona');
        $personaRevisionMapper = $this->getServiceLocator()->get('planbold.mapper.personaRevision');
        $personaDataMapper     = $this->getServiceLocator()->get('planbold.mapper.personaData');
        $personaOrderMapper    = $this->getServiceLocator()->get('planbold.mapper.personaSections');
        $newRevision           = false;
        
        try {
            $persona = $personaMapper->findOneBy(array('id'=> $personaData['id']))->getResult();
            if ( count($persona) > 0 ) {
                $persona = $persona[0];
            }
            
            $isCreated = false;
            if ( isset($personaData['isCreated']) ) {
                $isCreated = true;
            }
            
            $personaObject     = $personaMapper->findOneById($personaData['id']);
            $personaDataFields = $personaDataMapper->fetchAll(array('persona' => $personaObject))->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            $personaOrder      = $personaOrderMapper->fetchAll(array('personaId' => $personaData['id']))->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            
            //personaRevision Content
            $personaRevisionContent = array(
                'persona'       => $persona,
                'personaFields' => $personaDataFields,
                'personaOrder'  => $personaOrder
            );
            // check persona Revision
            $personaRevision = $personaRevisionMapper->findOneBy(array('revisionId' => $personaData['revisionId']));
            
            if ( count($personaRevision) == 0 ) { 
                $newRevision     = true;
                $personaRevision = new \Planbold\Entity\PersonaRevision();
                $personaRevision->setPersonaId($personaData['id']);
                $personaRevision->setRevisionId($personaData['revisionId']);
            }
            $personaRevision->setContent(json_encode($personaRevisionContent));
            $personaRevision->setPersonaName($persona['name']);
            
            // fix issue on redundant data on acitivity list
            $revisionData = str_replace($personaData['revisionDetails'], '', $personaRevision->getRevisionDetails());
            
            $personaRevision->setRevisionDetails($revisionData.$personaData['revisionDetails']);
            $personaRevision->setUserId($personaData['userId']);
            $personaRevision->setUsername(ucwords($personaData['username']));
            $personaRevision->setAccountId($personaData['accountId']);
            $personaRevision->setDeleteFlag($personaData['isDeleted']);
            $personaRevision->setUserImage(($personaData['user']->getImagePath() == null ? '//www.gravatar.com/avatar/74e65bc9c46c7c2d68bf7918a7628b38?s=30' : $personaData['user']->getImagePath()));
            $personaRevision->setCompany($personaData['account']->getName());
            $personaRevision->setPersonaImage(strlen(trim($personaData['headshot'])) == 0 ? 'https://s3-us-west-2.amazonaws.com/planbold-qa/headshot/1x400.jpg' : $personaData['headshot'] );
            
            
            if ( $newRevision ) { 
                $personaRevision->setCreateFlag($isCreated);
                $personaRevisionMapper->insert($personaRevision);
            } else {
                $personaRevisionMapper->update($personaRevision);
            }            
       
        } catch (\Exception $e) {
            var_dump($e->getMessage());exit;
            $event->stopPropagation(true);
        }
    }
    
    public function uploadFiles(EventInterface $event)
    {
        $accountFile  = $event->getParam('accountFile');
        $config       = $this->getServiceLocator()->get('Config');
        $awsSdk       = $this->getServiceLocator()->get(AwsSdk::class);
        $s3Config     = $config['s3'];
        $s3Client     = $awsSdk->createS3();
        $baseFile     = explode('.',basename($accountFile->getPath()));
        try {
            $handle = fopen($accountFile->getPath(), 'r');
            $s3Client->putObject(array(
                'Bucket'       => $s3Config['plan']['bucket'],
                'Key'          => $baseFile[0].'.jpg',
                'Body'         => $handle,
                'ACL'          => 'public-read',
                'CacheControl' => 'max-age=172800',
            ));
            fclose($handle);

            //delete files
            unlink($accountFile->getName());

        } catch (S3Exception $e) {
            // @todo log this exception
            $event->stopPropagation(true);
        }
    }
    
    /**
     * Account File
     * @param EventInterface $event
     */
    public function updateFiles(EventInterface $event)
    {
        $accountFile = $event->getParam('accountFile');
        $config      = $this->getServiceLocator()->get('Config');
        $s3Config    = $config['s3'];
        $s3Link      = $s3Config['url'].$s3Config['plan']['bucket'];
        $baseFile    = explode('.',basename($accountFile->getPath()));
        $accountFile->setPath($s3Link.'/'.$baseFile[0].'.jpg');
        $mapper = $this->getServiceLocator()->get('planbold.mapper.accountFile');
        try {
            $mapper->update($accountFile);
        } catch (\Exception $e) {
            $event->stopPropagation(true);
        }
    }
}

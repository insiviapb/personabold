<?php
namespace Audience\Controller;

use Audience\Controller\IndexController as IndexController;
use Zend\Json\Json;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;


class PersonaProcessController extends IndexController
{
    /**
     * Save Persona
     * @return bool|JsonModel
     */
    public function savePersonaAction()
    {

        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }

        $postParams     = $this->params()->fromPost();
        $authService    = $this->getService('zfcuser_auth_service');
        $currentUser    = $authService->getIdentity();
        $personaService = $this->getServiceLocator()->get('persona.service');
        $clientUser     = null;
        
        if ( isset($_SESSION['currentClientUuid']) && !empty($_SESSION['currentClientUuid']) ) {
            $accountService = $this->getService('planbold.mapper.account');
            $clientUser = $accountService->findOneBy(array('uuid' => $_SESSION['currentClientUuid']));
        }
        $result = $personaService->savePersona($currentUser, $postParams, $clientUser);
        return  new JsonModel(array('success' => array('persona_id' => $result->getId(), 'uuid' => $result->getUuid())));
    }
    
    /**
     * Save Persona Details
     * @return bool|JsonModel
     */
    public function savePersonaDetailsAction()
    {

        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }

        $postParams     = $this->params()->fromPost();
        $authService    = $this->getService('zfcuser_auth_service');
        $currentUser    = $authService->getIdentity();
        $personaService = $this->getServiceLocator()->get('persona.service');
        
        $result = $personaService->savePersonaDetails($currentUser, $postParams);
        return  new JsonModel(array('success' => array('persona_id' => $result->getId(), 'uuid' => $result->getUuid())));
    }

    /**
     * Save Persona Data
     * @return JsonModel
     */
    public function savePersonaDataAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts = $this->getRequest()->getPost()->toArray();
        $personaService = $this->getService('persona.service');

        $personaData = $personaService->savePersonaData($posts);

        if($personaData !== false) {

            return new JsonModel(array(
                'success' => true,
                'persona_data' => array(
                    'persona_id'        => $posts['persona_id'],
                    'persona_field_id'  => $posts['persona_field_id'],
                    'persona_data_id'   => $personaData,
                    'uid'               => $posts['uid' ]
                )
            ));
        }

        return new JsonModel(array('success' => false));
    }

    /**
     * Get Survey Data by Respondents
     * @return JsonModel
     */
    public function getSurveyDataByRespondentsAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $personaService = $this->getService('persona.service');
        $posts = $this->getRequest()->getPost()->toArray();
        $surveyData = $personaService->getSurveyDataByRespondents($posts['respondents']);
        if(!empty($surveyData)) {
            return new JsonModel(array(
                'success'       => true,
                'data'       => $surveyData
            ));
        }
        return new JsonModel(array(
            'success'       => false
        ));
    }

    /**
     * Get Industry By Respondents
     * @return JsonModel
     */
    public function getIndustryByRespondentsAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts = $this->getRequest()->getPost()->toArray();

        $personaService = $this->getService('persona.service');
        $industry = $personaService->getIndustryByRespondents($posts['respondents']);

        if(!empty($industry)) {
            return new JsonModel(array(
                'success'       => true,
                'data'       => $industry
            ));
        }
        return new JsonModel(array(
            'success' => false
        ));
    }

    /**
     * Get gender by respondents
     * @return JsonModel
     */
    public function getGenderByRespondentsAction()
    {
        if ($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts = $this->getRequest()->getPost()->toArray();

        $personaService = $this->getService('persona.service');
        $gender = $personaService->getGenderByRespondents($posts['respondents']);

        if(!empty($gender)) {
            return new JsonModel(array(
                'success'       => true,
                'data'       => $gender
            ));
        }
        return new JsonModel(array(
            'success'       => false
        ));
    }

    /**
     * Get Archetype By Respondents
     * @return JsonModel
     */
    public function getArchetypeByRespondentsAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts = $this->getRequest()->getPost()->toArray();

        $personaService = $this->getService('persona.service');
        $archetype = $personaService->getArchetypeByRespondents($posts['respondents']);

        if(!empty($archetype)) {
            return new JsonModel(array(
                'success'       => true,
                'data'       => $archetype
            ));
        }
        return new JsonModel(array(
            'success'       => false
        ));
    }

    /**
     * Get JobType By Respondents
     * @return JsonModel
     */
    public function getJobtypeByRespondentsAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts = $this->getRequest()->getPost()->toArray();

        $personaService = $this->getService('persona.service');
        $jobtype = $personaService->getJobtypeByRespondents($posts['respondents']);

        if(!empty($jobtype)) {
            return new JsonModel(array(
                'success'       => true,
                'data'       => $jobtype
            ));
        }
        return new JsonModel(array(
            'success'       => false
        ));
    }

    /**
     * Get Survey Result By Respondents
     * @return JsonModel
     */
    public function getSurveyResultByRespondentAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts = $this->getRequest()->getPost()->toArray();

        $respondents = $posts['respondents'];

        $personaService = $this->getService('persona.service');
        $data = array();
        try {
            $data['age'] = $personaService->getAgeByRespondents($respondents);
            $data['genders'] = $personaService->getGenderByRespondents($respondents);
            $data['industries'] = $personaService->getIndustryByRespondents($respondents);
            $data['archetypes'] = $personaService->getArchetypeByRespondents($respondents);
            $data['jobtypes'] = $personaService->getJobtypeByRespondents($respondents);
            $data['surveyData'] = $personaService->getSurveyDataByRespondents($respondents);


            return new JsonModel(array(
                'success' => true,
                'data'    => $data
            ));
        } catch (\Exception $e) {
            return new JsonModel(array(
                'success' => false,
                'data'    => $e->getMessage()
            ));
        }
    }


    /**
     * Get Persona
     * @return bool|JsonModel
     */
    public function getPersonaAction()
    {
        if ($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }

        $posts = $this->getRequest()->getPost()->toArray();

        $personaService = $this->getService('persona.service');

        $data = [];
        $data['motivations'] = $personaService->getAllMotivations();
        $data['channels'] = $personaService->getAllGlobalChannels();

        $persona_id = $posts['persona_id'];

        if(!empty($persona_id)) {

            $personaData = $personaService->getAllPersonaDataByPersona($persona_id);
            if(count($personaData) != 0) {
                $data['personaData'] = $personaData;
            }

            $personaKnowledge = $personaService->getKnowledgeByPersona($persona_id);
            if(count($personaKnowledge) != 0) {
               $data['personaKnowledge'] = $personaKnowledge;
            }

            $personaMotivations = $personaService->getMotivationsByPersona($persona_id);
            if(count($personaMotivations) != 0) {
                $data['personaMotivations'] = $personaMotivations;
            }

            $personaChannels = $personaService->getChannelsByPersona($persona_id);
            if(count($personaChannels) != 0) {
                $data['personaChannels'] = $personaChannels;
            }
        }


        return new JsonModel($data);

    }

    /**
     * Get PersonalInfo
     * @return JsonModel
     */
    public function getPersonaInfoAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }


        $posts =  $this->getRequest()->getPost()->toArray();
        $uuid = $posts['uuid'];

        $personaService = $this->getServiceLocator()->get('persona.service');
        $persona = array();
        $archetype = $personaService->getAllArchetype();
        $industries = $personaService->getAllIndustries();
        $jobtype = $personaService->getAllJobtype();
        $hasPersona = false;
        if( isset($posts['uuid']) ) {
            $persona = $personaService->getPersonaByUuid($uuid);
            if(count($persona) != 0) {
                $hasPersona = true;
            }
        }
        return new JsonModel(array(
            'success'       => true,
            'persona'       => $persona,
            'hasPersona'    => $hasPersona,
            'archetypes'    => $archetype,
            'industries'    => $industries,
            'jobtype'       => $jobtype

        ));
    }

    /**
     * Get Persona Data
     * @return JsonModel
     */
    public function getPersonaDataAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }

        $posts = $this->getRequest()->getPost()->toArray();
        $personaService = $this->getService('persona.service');

        $personaData = $personaService->getAllPersonaDataByPersona($posts['id']);
        if (count($personaData) != 0) {
            return new JsonModel(array(
                'success' => true,
                'data'    => $personaData
            ));
        }

        return new JsonModel(array('success'   => false));
    }

    /** 
     * Get Industry Description
     * @return bool|JsonModel
     */
    public function getIndustryDescriptionAction()
    {
        if ($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }

        $posts = $this->getRequest()->getPost()->toArray();
        $personaService = $this->getService('persona.service');

        $accountIndustry = $personaService->getAccountIndustry($posts['industry']);
        if(count($accountIndustry) > 0) {
            return new JsonModel(array(
                'success'   => true,
                'industry'  => array(
                    'id'            => $accountIndustry->getIndustry(),
                    'name'          => $accountIndustry->getIndustryName(),
                    'description'   => $accountIndustry->getDescription())
            ));
        }

        return new JsonModel(array(
            'success'   => false,
            'industry'  => array(
                'id' => $posts['industry'],
            )
        ));
    }

    /**
     * Get Channels
     * @return bool|JsonModel
     */
    public function getChannelsAction()
    {
        if ($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $personaService = $this->getService('persona.service');
        $globalChannels = $personaService->getAllGlobalChannels();
        if(count($globalChannels) != 0) {
            return new JsonModel(array('success' => true, 'globalChannels' => $globalChannels));
        }

        return new JsonModel(array('success' => false));
    }

    /**
     * Get Motivations
     * @return JsonModel
     */
    public function getMotivationsAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $personaService = $this->getService('persona.service');
        $motivations = $personaService->getAllMotivations();
        if (count($motivations) != 0) {
            return new JsonModel(array('success' => true, 'motivations' => $motivations));
        }

        return new JsonModel(array('success' => false));
    }

    /**
     * Get Persona Channels
     * @return bool|JsonModel
     */
    public function getPersonaChannelsAction()
    {
        if ($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }

        $posts = $this->getRequest()->getPost()->toArray();

        $personaService = $this->getService('persona.service');
        $personaChannels = $personaService->getChannelsByPersona($posts['persona_id']);

        if (count($personaChannels) != 0) {
            return new JsonModel(array(
                'success' => true,
                'personaChannels' => $personaChannels
            ));
        }

        return new JsonModel(array('success' => false));
    }

    /**
     * Save persona's section positions
     *
     * @return bool|JsonModel
     */
    public function savePersonaSectionPositionAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }

        $posts = $this->getRequest()->getPost()->toArray();
        $personaService = $this->getService('persona.service');

        $persona_uuid = isset($posts['persona_uuid'])? $posts['persona_uuid'] : '';

        if(isset($posts['data'])) {
            $personaSections = array();
            foreach ($posts['data'] as $val) {
                parse_str($val['items'], $items);
                $column = $val['column'];

                if(is_array($items['item'])) {
                    foreach($items['item'] as $key => $item) {
                        array_push($personaSections, array('sectionName' => $item, 'column' => $column, 'position' => $key));
                    }
                }
            }
            if($personaService->savePersonaSections($persona_uuid, $personaSections)) {
                return new JsonModel(array('success'   => true));
            }


        }
        return new JsonModel(array('success'   => false));
    }
    
    /**
     * Get Persona Knowledge
     * @return bool|JsonModel
     */
    public function getPersonaKnowledgeAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }

        $posts = $this->getRequest()->getPost()->toArray();

        $personaService = $this->getService('persona.service');
        $personaKnowledge = $personaService->getKnowledgeByPersona($posts['persona_id']);
        if(count($personaKnowledge) != 0) {
            return new JsonModel(array(
                'success'       => true,
                'personaKnowledge'   => $personaKnowledge
            ));
        }

        return new JsonModel(array('success' => false));
    }

    /**
     * Fetch Persona Data like channels, archetype, motivations etc
     * @return ViewModel
     */
    public function fetchPersonaDataAction()
    {
        $personaService  = $this->getService('persona.service');
        $settingsService = $this->getService('settings.service');
        $personaId       = $this->params()->fromRoute('personaId', 0);
        $clientUuid      = $this->params()->fromRoute('clientUuid', 0);
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'archetype'       => json_encode($personaService->getAllArchetype()),
            'industries'      => json_encode($settingsService->getAllIndustriesSetting()),
            'motivations'     => json_encode($personaService->getAllMotivations()),
            'jobtype'         => json_encode($personaService->getAllJobtype()),
            'channels'        => json_encode($personaService->getAllGlobalChannels()),
            'knowledge'       => json_encode($personaService->getKnowledgeByPersona($personaId))
        ))->setTerminal(true);

        return $viewModel;

    }
    
    /**
     * Fetch Persona Revision Data like channels, archetype, motivations etc
     * @return ViewModel
     */
    public function fetchPersonaRevisionDataAction()
    {
        $personaService  = $this->getService('persona.service');
        $revisionId      = $this->params()->fromRoute('revisionId', 0);
        $rawPersona      = $personaService->getPersonaRevision($revisionId);
        $personaId       = isset($rawPersona['content']['persona']) ? $rawPersona['content']['persona']['id'] : 0;
        
        
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'knowledge'       => json_encode($personaService->getKnowledgeByPersona($personaId)),
            'pmotivations'    => json_encode($personaService->getMotivationsByPersona($personaId))
        ))->setTerminal(true);

        return $viewModel;

    }

    /**
     * Get Persona Motivation
     * @return bool|JsonModel
     */
    public function getPersonaMotivationsAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }

        $posts = $this->getRequest()->getPost()->toArray();

        $personaService = $this->getService('persona.service');
        $personaMotivations = $personaService->getMotivationsByPersona($posts['persona_id']);
        if(count($personaMotivations) != 0) {
            return new JsonModel(array(
                'success'       => true,
                'personaMotivations'   => $personaMotivations
            ));
        }

        return new JsonModel(array('success'   => false));
    }

    /**
     * Send Survey
     * @return JsonModel
     */
    public function sendSurveyAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts          = $this->getRequest()->getPost()->toArray();
        $personaService = $this->getService('persona.service');
        if ( $personaService->sendSurvey($posts) ) {
            $data = array('success' => true,
                'message' => 'Survey successfully sent!');
        } else {
            $data = array('success' => false,
                'message' => 'Failed to send survey!');
        }
        return new JsonModel($data);
    }

    /**
     * Save Survey
     * @return type
     */
    public function saveSurveyAction()
    {
        $posts       = $this->getRequest()->getPost()->toArray();
        $personaService = $this->getService('persona.service');

        if($personaService->saveSurveyRespondents($posts) && !empty($posts['uuid'])) {
            $this->flashMessenger()->addSuccessMessage('Survey successfully saved! Thank you for answering our survey!');
        } else {
            $this->flashMessenger()->addErrorMessage('Oops! Something went wrong, please try again.');
        }

        return $this->redirect()->toRoute('audience/survey', array('uuid' => $posts['uuid']));
    }

    /**
     * Get Persona Fields
     * @return bool|JsonModel
     */
    public function getPersonaFieldsAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }

        $personaService = $this->getService('persona.service');
        $personaFields = $personaService->getAllPersonaFields();
        if(count($personaFields) != 0) {
            return new JsonModel(array('success' => true, 'personaFields' => $personaFields));
        }

        return new JsonModel(array('success' => false));
    }

    /**
     * Save Persona Channel
     * @return JsonModel
     */
    public function savePersonaChannelAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts = $this->getRequest()->getPost()->toArray();
        $personaService = $this->getService('persona.service');
        $result = $personaService->savePersonaChannel($posts);
        if ( $result !== false ) {
            return new JsonModel(array('success' => true));
        }
        return new JsonModel(array('success' => false));
    }
    
    /**
     * Save Persona Motivation
     * @return JsonModel
     */
    public function savePersonaMotivationAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts = $this->getRequest()->getPost()->toArray();
        $personaService = $this->getService('persona.service');
        $result = $personaService->savePersonaMotivation($posts);
        if ( $result !== false ) {
            return new JsonModel(array('success' => true));
        }
        return new JsonModel(array('success' => false));
    }

    /**
     * Save Persona Knowledge
     * @return JsonModel
     */
    public function savePersonaKnowledgeAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts = $this->getRequest()->getPost()->toArray();
        $personaService = $this->getService('persona.service');
        $result = $personaService->savePersonaKnowledge($posts);
        if($result !== false) {
            return new JsonModel(array('success' => true));
        }
        return new JsonModel(array('success' => false));
    }

    /**
     * Save Industry Description
     * @return JsonModel
     */
    public function saveIndustryDescriptionAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $postParams = $this->params()->fromPost();
        $personaService = $this->getService('persona.service');
        $result = $personaService->updateIndustryDescription($postParams);
        return new JsonModel(array(
            'success'     => true,
            'industry_id' => $result
        ));
    }

    /**
     * Get Persona By Industry
     * @return JsonModel
     */
    public function getPersonaByIndustryAction() 
    {
        if( $this->validateProcesses() !== true ) {
            return $this->validateProcesses();
        }
        $posts = $this->getRequest()->getPost()->toArray();

        $personaService = $this->getService('persona.service');
        $persona = $personaService->getAllPersonaByIndustry($posts['industry']);

        return new JsonModel(array(
            'success'   => true,
            'persona'   => $persona
        ));
    }
    
    /**
     * Export Personal PDF
     * @return type
     */
    public function exportPersonaPDFAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        
        $templateService = $this->getService('templates.service');
        $uuid            = $this->params()->fromRoute('uuid', 0);
        $templateUuid    = $this->params()->fromRoute('templateUuid', 0);
        $isMultiple      = $this->params()->fromRoute('isMultiple', 0);
        if ( $uuid == "0" || $templateUuid == "0" ) {
            return new JsonModel(array('success' => false, 'message' => 'Missing persona id and template uuid, please check.'));
        }
        $result = $templateService->processSinglePersonaData($uuid, $templateUuid, $isMultiple);
        if ( $result == false ) {
            return new JsonModel(array('success' => false, 'message' => 'Template not found.'));
        }
        $output          = $this->getService('mvlabssnappy.pdf.service')->getOutputFromHtml($result);
        $response        = $this->getResponse();
        $headers         = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/pdf');
        $headers->addHeaderLine('Content-Disposition', "attachment; filename=\"export". ".pdf\"");
        $response->setContent($output);
        return $response;
    }
    
    /**
     * Check Packages base on type given (survey, template, client)
     * @return JsonModel
     */
    public function checkPackagesAction() 
    {
        if( $this->validateProcesses() !== true ) {
            return $this->validateProcesses();
        }
        $paymentService = $this->getService('payment.service');
        $postParams     = $this->params()->fromPost();
        $authService    = $this->getService('zfcuser_auth_service');
        $currentUser    = $authService->getIdentity();
        $result         = $paymentService->validateSubscription($currentUser, $postParams);
        return new JsonModel($result);
    }
    
    /**
     * Add Persona Client
     * @return JsonModel
     */
    public function addPersonaClientAction()
    {
        if( $this->validateProcesses() !== true ) {
            return $this->validateProcesses();
        }
        $postParams     = $this->params()->fromPost();
        $accountService = $this->getService('account.service');
        $authService    = $this->getService('zfcuser_auth_service');
        $currentUser    = $authService->getIdentity();
        $result         = $accountService->addPersonaClient($currentUser, $postParams);
        return new JsonModel($result);
    }
    
    /**
     * Revert Persona base on Revision Id
     * @return type
     */
    public function revertPersonaAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        
        $revisionId      = $this->params()->fromRoute('revisionId', "0");
        $personaService  = $this->getService('persona.service');
        $result          = $personaService->revertPersona($revisionId);
        if ( $result !== false ) {
            return $this->redirect()->toRoute('persona/persona', array('uuid'=> $result['personaId']));
        } else {
            return $this->redirect()->toRoute('persona');
        }        
    }
    
    /**
     * export Persona Templates
     * @return type
     */
    public function exportPersonaTemplateAction()
    {
        if ( $this->validateProcesses() !== true ) {
            return $this->validateProcesses();
        }
        $postParams      = $this->params()->fromPost();
        $templateService = $this->getService('templates.service');
        $config          = $this->getService('Config');
        $dateFormat      = date_format(new \Datetime(), 'YmdHis');
        $result = $templateService->processSinglePersonaData($postParams['personaUuid'], $postParams['uuid'], $postParams['isMultiple']);
        if ( $result == false ) {
            return new JsonModel(array('success' => false, 'message' => 'Template not found.'));
        }
        
        $output = $this->getService('mvlabssnappy.pdf.service')->getOutputFromHtml($result, 
                    array('page-size'   => $postParams['pageSize'],
                          'orientation' => $postParams['orientation']));
        $pdfFile = md5($dateFormat).'.pdf';
        file_put_contents($config['export-persona']['dir'].$pdfFile, $output);
        return new JsonModel(array('success'    => true,
                                   'pdfPreview' => $config['planbold']['pdf_url'].$pdfFile));
    }
    
    /**
     * Upload Photo
     * @return type
     */
    public function uploadFilesAction()
    {
        if ($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $uploadForm = $this->getService('user.form.uploadimage');
        $posts      = $this->getRequest()->getPost()->toArray();
        $files      = $this->getRequest()->getFiles()->toArray();
        $formData   = array_merge($posts, $files);

        $uploadForm->setData($formData);
        if ($uploadForm->isValid()) {
            $data     = $uploadForm->getData();
            $data['upload-type']  = $formData['upload-type'];
            $data['persona-id'] = (isset($formData['upload-persona-id'])) ? $formData['upload-persona-id'] : null ;
            $userFilePath = explode('/',$data['userImage']['tmp_name']);
            return new JsonModel(array('success'=> true, 'file' => $userFilePath[count($userFilePath)-1]));
        } else {
            return new JsonModel(array('success' => false, 'data' => 'File validation failed. Just receive gif, png, jpg and jpeg.'));
        }
    }
    
    /**
     * Client Files
     * @return JsonModel
     */
    public function clientFilesAction()
    {
        if ($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts          = $this->getRequest()->getPost()->toArray();
        $personaService = $this->getService('persona.service');
        $config         = $this->getService('Config');
        $posts['file']  = $config['upload']['dir'].'/'.$posts['txt-files'];
        $posts['clientUuid'] = $_SESSION['settingsClientUuid'];
        $result         = $personaService->addUpdateFiles($posts);
        return new JsonModel(array('success' => $result));
    }
    
    /**
     * Save getting started
     * @return type
     */
    public function saveGettingStartedAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        
        $posts          = $this->getRequest()->getQuery()->toArray();
        $personaService = $this->getService('persona.service');
        if ( $personaService->saveGettingStarted($posts) ) {
            return $this->redirect()->toRoute('persona/persona', array('uuid' => $personaService->getGettingStartedUuid()));
        }
        return $this->redirect()->toRoute('dashboard');
        
    }
    
    /**
     * Delete Persona Data
     * @return JsonModel
     */
    public function deletePersonaDataAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }

        $posts          = $this->params()->fromPost();
        $personaService = $this->getServiceLocator()->get('persona.service');
        
        if ( $personaService->deletePersonaData($posts) ) {
            return new JsonModel(array('success' => true));
        }
        return new JsonModel(array('success' => false));
    }
    
    /**
     * Save Archetype Data
     * @return JsonModel
     */
    public function saveArchetypeDataAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        
        $postParams     = $this->params()->fromPost();
        $authService    = $this->getService('zfcuser_auth_service');
        $currentUser    = $authService->getIdentity();
        $personaService = $this->getServiceLocator()->get('persona.service');
        $clientUser     = null;
        
        if ( isset($_SESSION['currentClientUuid']) && !empty($_SESSION['currentClientUuid']) ) {
            $accountService = $this->getService('planbold.mapper.account');
            $clientUser = $accountService->findOneBy(array('uuid' => $_SESSION['currentClientUuid']));
        }
        
        $result = $personaService->saveArchetypeData($currentUser, $postParams, $clientUser);
        return  new JsonModel(array('success' => array('persona_id' => $result->getId(), 'uuid' => $result->getUuid())));
    }
    
    /**
     * SharePersona Action
     * @return JsonModel
     */
    public function sharePersonaAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        
        $postParams     = $this->params()->fromPost();
        $personaService = $this->getServiceLocator()->get('persona.service');
        
        if ( $personaService->processSharePersona($postParams) ) {
            return new JsonModel(array('success' => true));
        }
        return new JsonModel(array('success' => false));
    }
    
    /**
     * Enable Sharing Action
     * @return JsonModel
     */
    public function enableSharingAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        
        $postParams     = $this->params()->fromPost();
        $personaService = $this->getServiceLocator()->get('persona.service');
        
        if ( $personaService->processEnableSharing($postParams) ) {
            return new JsonModel(array('success' => true));
        }
        return new JsonModel(array('success' => false));
    }
    
    public function enablePersonaPassswordAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        
        $postParams     = $this->params()->fromPost();
        $personaService = $this->getServiceLocator()->get('persona.service');
        
        if ( $personaService->processEnablePersonaPassword($postParams) ) {
            return new JsonModel(array('success' => true));
        }
        return new JsonModel(array('success' => false));
    }
}
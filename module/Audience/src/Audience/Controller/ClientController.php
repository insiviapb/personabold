<?php
namespace Audience\Controller;

use Audience\Controller\IndexController as IndexController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class ClientController extends IndexController
{
    
    /**
     * Saving new persona under client
     * @return type
     */
    public function addPersonaAction()
    {
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        };
        // check if the customer have already setup their account
        if ( ! $this->validateCards() ) {
            $this->redirect()->toRoute('audience');
        }
        
        $clientUuid     = $this->params()->fromRoute('uuid', "0");
        
        if ( $clientUuid != "0" ) {
            
            $accountService = $this->getService('account.service');
            $clientAccount  = $accountService->getClientAccountByUuid($clientUuid);
            
            $authService    = $this->getService('zfcuser_auth_service');
            $currentUser    = $authService->getIdentity();
            $personaService = $this->getService('persona.service');
            $arrData = array(
                'content'    => $clientAccount->getName(),
                'field'      => 'name',
                'revisionId' => uniqid('REV-')
            );
            $result = $personaService->savePersona($currentUser, $arrData, $clientAccount);
            // redirect to created persona
            $this->redirect()->toRoute('persona/persona', array('uuid' => $result->getUuid()));
        } else {
            $this->redirect()->toRoute('persona/clientPage');
        }
    }
    
    /**
     * Get Client Persona
     * @return \Audience\Controller\JsonModel
     */
    public function getClientPersonaAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts          = $this->getRequest()->getPost()->toArray();
        $personaService = $this->getService('persona.service');
        $clientPersona  = $personaService->getClientPersona($posts);
        return new JsonModel(array('success' => true, 'data' => array($posts['uuid'] => $clientPersona)));
    }
    
    /**
     * Client Portal
     * @return ViewModel
     */
    public function settingsClientAction()
    {
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        };
        
        $uuid = $this->params()->fromRoute('uuid', 0);
        // set the current client settings uuid 
        $_SESSION['settingsClientUuid'] = $uuid;
        
        $personaService = $this->getService('persona.service');
        $clientDetails  = $personaService->getClientDetails($uuid);
        return new ViewModel(array('clientDetails' => $clientDetails));
    }
    
    /**
     * Client Theme settings
     * @return ViewModel
     */
    public function themeAction()
    {
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        };
        
        $uuid         = $_SESSION['settingsClientUuid'];
        $themeService = $this->getService('theme.service');
        $account      = $themeService->getClientAccount($uuid);
        return new ViewModel(array('colors' => (array)json_decode($themeService->getClientAccountColors($account)),
                                   'account' => $account));
    }
    
    /**
     * Client File Page
     * @return ViewModel
     */
    public function filePageAction()
    {
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        };
        
        $uuid         = $_SESSION['settingsClientUuid'];
        $fileService  = $this->getService('file.service');
        $account      = $fileService->getClientAccount($uuid);
        $accountFiles = $fileService->getAccountFiles($account);
        return new ViewModel(array('accountFiles' => $accountFiles));
    }
}

<?php

namespace Audience\Controller;

use Audience\Controller\IndexController as IndexController;
use Herrera\Json\Exception\Exception;
use Zend\View\Model\ViewModel;


class PersonaController extends IndexController
{
    /**
     * Persona Main Page
     * @return ViewModel
     */
    public function indexAction()
    {
        // validate login session
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        };
        
        $freeTrial = $this->getUserFreeTrial();
        
        // check if the customer have already setup their account and check if it is over the free trial
        if ( !$this->validateCards() && $freeTrial == 0 ) {
            $this->redirect()->toRoute('audience');
        }

        $uuid            = $this->params()->fromRoute('uuid', 0);
        $templateService = $this->getService('templates.service');
        $personaService  = $this->getService('persona.service');
        
        $persona = array();
        $personaFields    = $personaService->getAllPersonaFields();
        $sendSurveyForm   = $this->getService('audience.form.sendSurvey');
        $revisions        = array();
        $hasPersona       = false;
        
        if( $uuid ) {
            $persona       = $personaService->getPersonaByUuid($uuid);
            $revisions     = $personaService->getAllPersonaRevision($persona['id']);
            $surveyCount   = $personaService->getSurveyRespondentsCountByPersona($persona['id']);
            $hasPersona    = true;
            $personaSections = $personaService->getPersonaSectionsByColumn($uuid, array(1,2,3));
            $personaData     = $personaService->getAllPersonaDataByPersona($persona['id']);

            // get custom data and add as personaFields
            foreach($personaData as $val) {
                $customData = array();
                if(empty($val['personaFieldId'])) {
                    $customData = array(
                        'id'          => $val['id'],
                        'title'       => $val['title'],
                        'isCustom'    => true,
                        'type'        => 'list'
                    );

                    array_push($personaFields, $customData);
                }
            }
        }
        
        // Default sort of persona sections
        $columns = array(
            'column-1' => ['agegender', 'archeindustryjob'],
            'column-2' => [],
            'column-3' => ['channels', 'motivations', 'knowledges']
        );
        // set the default items in column2
        foreach ($personaFields as $val) {
            if ( !in_array(strtolower($val['title']), $columns['column-3']) && $val['type'] == 'list') {
                array_push($columns['column-2'], strtolower(trim(preg_replace("![^a-z0-9]+!i", "", $val['id']))));
            }
        }

        // modify the arrangements of columns' items
        if ( !empty($personaSections) ) {
            foreach ($personaSections as $key => $items) {
                
                $columns[$key] = [];
                foreach($items as $item) {
                    $columns[$key][] = $item['sectionName'];
                }
            }
        }
        return new ViewModel(array(
            'persona'         => $persona,
            'sendSurveyForm'  => $sendSurveyForm,
            'hasPersona'      => $hasPersona,
            'personaColumns'  => $columns,
            'personaData'     => isset($personaData) ? $personaData : array(),
            'personaSections' => isset($personaSections) ? $personaSections : null,
            'surveyCount'     => isset($surveyCount)? $surveyCount : 0,
            'personaFields'   => $personaFields,
            'templateGallery' => json_encode($templateService->getPublishedPersonaTemplates()),
            'revisionId'      => uniqid('REV-'),
            'headshots'       => json_encode($personaService->getHeadshot()),
            'revisions'       => count($revisions) > 0 ? $revisions : array()
        ));
    }
    
    /**
     * Persona Revision Main Page
     * @return ViewModel
     */
    public function revisionPageAction()
    {
        // validate login session
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        };

        $freeTrial = $this->getUserFreeTrial();
        
        // check if the customer have already setup their account
        if ( ! $this->validateCards() && $freeTrial == 0 ) {
            $this->redirect()->toRoute('audience');
        }
        
        $revisionId      = $this->params()->fromRoute('revisionId', 0);
        $personaService  = $this->getService('persona.service');
        $templateService = $this->getService('templates.service');
        $persona = array();
        $personaFields = $personaService->getAllPersonaFields();
                
        $hasPersona = false;
        if( $revisionId ) {
            $rawPersona = $personaService->getPersonaRevision($revisionId);
            $revisions  = $personaService->getAllPersonaRevision($rawPersona['personaId']);

            if ( count($rawPersona) > 0 ) {
                $persona         = $rawPersona['content']['persona'];
                $surveyCount     = $personaService->getSurveyRespondentsCountByPersona($persona['id']);
                $hasPersona      = true;
                $personaSections = $rawPersona['content']['personaOrder'];
                $personaData     = $rawPersona['content']['personaFields'];
            }
        }

        // Default sort of persona sections
        $columns = array(
            'column-1' => ['agegender', 'archeindustryjob'],
            'column-2' => [],
            'column-3' => ['channels', 'motivations', 'knowledges']
        );

        // set the default items in column2
        foreach ($personaFields as $val) {
            if ( !in_array(strtolower($val['title']), $columns['column-3'])) {
                array_push($columns['column-2'], strtolower(trim(preg_replace("![^a-z0-9]+!i", "", $val['title']))));
            }
        }

        // modify the arrangements of columns' items
        if ( isset($personaSections) ) {
            foreach ($personaSections as $key => $item) {
                $columns[$key] = [];
                if ( $key == 'column-2' && !in_array($item['sectionName'], $columns['column-3'])) {
                    $columns[$key][] = $item['sectionName'];
                }
            }
        }
        return new ViewModel(array(
            'persona'         => $persona,
            'hasPersona'      => $hasPersona,
            'personaColumns'  => $columns,
            'personaData'     => isset($personaData)? $personaData : null,
            'personaSections' => isset($personaSections) ? $personaSections : null,
            'surveyCount'     => !empty($surveyCount)? $surveyCount: 0,
            'personaFields'   => $personaFields,
            'templateGallery' => json_encode($templateService->getPublishedPersonaTemplates()),
            'revisionId'      => $revisionId,
            'revisions'       => $revisions
        ));
    }
    
    /**
     * SharePersona
     * @return ViewModel
     */
    public function sharePersonaAction()
    {
        $personaShortId  = $this->params()->fromRoute('personaShortId', 0);
        $templateService = $this->getService('templates.service');
        $personaService  = $this->getService('persona.service');
        $persona = array();
        
        //check if persona is shared 
        if ( $personaShortId === 0 ) {
            return $this->redirect()->toRoute('share/security', 
                    array('redirect' => $this->getRequest()->getRequestUri()));
        }
        $persona = $personaService->getPersonaByShortCode($personaShortId);
        
        if ( !$persona['publicFlag'] ) {
            $this->layout('layout/web');
            return $this->notFoundAction();
        }
        
        if ( (!empty($persona['password']) && (empty($_SESSION['persona-password']) || $_SESSION['persona-password'] != 'validPassword') ) ||
            (!empty($_SESSION['persona-password']) && $_SESSION['persona-password'] != 'validPassword')) {
            return $this->redirect()->toRoute('share/security', 
                    array('shortId' => $personaShortId));
        }
        
        $personaFields    = $personaService->getAllPersonaFields();
        $sendSurveyForm   = $this->getService('audience.form.sendSurvey');
        $revisions        = array();
        $hasPersona       = false;
        
        if( $personaShortId ) {
            $revisions     = $personaService->getAllPersonaRevision($persona['id']);
            $surveyCount   = $personaService->getSurveyRespondentsCountByPersona($persona['id']);
            $hasPersona    = true;
            $personaSections = $personaService->getPersonaSectionsByColumn($persona['uuid'], array(1,2,3));
            $personaData     = $personaService->getAllPersonaDataByPersona($persona['id']);

            // get custom data and add as personaFields
            foreach($personaData as $val) {
                $customData = array();
                if(empty($val['personaFieldId'])) {
                    $customData = array(
                        'id'          => $val['id'],
                        'title'       => $val['title'],
                        'isCustom'    => true,
                        'type'        => 'list'
                    );

                    array_push($personaFields, $customData);
                }
            }
        }
        
        // Default sort of persona sections
        $columns = array(
            'column-1' => ['agegender', 'archeindustryjob'],
            'column-2' => [],
            'column-3' => ['channels', 'motivations', 'knowledges']
        );
        // set the default items in column2
        foreach ($personaFields as $val) {
            if ( !in_array(strtolower($val['title']), $columns['column-3']) && $val['type'] == 'list') {
                array_push($columns['column-2'], strtolower(trim(preg_replace("![^a-z0-9]+!i", "", $val['id']))));
            }
        }

        // modify the arrangements of columns' items
        if ( !empty($personaSections) ) {
            foreach ($personaSections as $key => $items) {
                
                $columns[$key] = [];
                foreach($items as $item) {
                    $columns[$key][] = $item['sectionName'];
                }
            }
        }
        
        $this->layout('layout/share');
        
        return new ViewModel(array(
            'persona'         => $persona,
            'sendSurveyForm'  => $sendSurveyForm,
            'hasPersona'      => $hasPersona,
            'personaColumns'  => $columns,
            'personaData'     => isset($personaData) ? $personaData : array(),
            'personaSections' => isset($personaSections) ? $personaSections : null,
            'surveyCount'     => isset($surveyCount)? $surveyCount : 0,
            'personaFields'   => $personaFields,
            'templateGallery' => json_encode($templateService->getPublishedPersonaTemplates()),
            'revisionId'      => uniqid('REV-'),
            'headshots'       => json_encode($personaService->getHeadshot()),
            'revisions'       => count($revisions) > 0 ? $revisions : array()
        ));
    }

    /**
     * Compare different persona
     * @return ViewModel
     */
    public function compareAction()
    {

        // validate login session
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        };

        $personaService = $this->getService('persona.service');
        $templateService = $this->getService('templates.service');
        $params = $this->getRequest()->getQuery()->toArray();
        $personas = array();
        $personaChannels = array();
        $personaMotivations = array();
        $personaKnowledges = array();
        $personaData = array();
        $personaUuids = [];
        if(array_key_exists('persona', $params)) {
            foreach ($params['persona'] as $val) {
                $personaUuids[] = $val;
                $persona = $personaService->getPersonaByUuid($val);
                if ( !isset($accountId) ) {
                    $accountId = $persona['actId'];
                }
                $personaChannels[] = $personaService->getChannelsByPersona($persona['id']);
                $personaMotivations[] = $personaService->getMotivationsByPersona($persona['id']);
                $personaKnowledges[] = $personaService->getKnowledgeByPersona($persona['id']);
                $personaData['persona' . $persona['id']] =  $personaService->getAllPersonaDataByPersona($persona['id']);
                $personas[] = array(
                        'persona' => $persona,
                    );
            }

            return new ViewModel(array(
                'personaUuids'      => $personaUuids,
                'personas'          => $personas,
                'personaMotivations'=> $personaMotivations,
                'personaChannels'   => $personaChannels,
                'personaKnowledges' => $personaKnowledges,
                'personaFields'     => json_encode(array('personaFields' => $personaService->getAllPersonaFields())),
                'personaList'       => $personaService->getAllPersonaByClient($accountId),
                'personaDataLists'  => json_encode($personaData),
                'templateGallery'   => json_encode($templateService->getPublishedPersonaTemplates())
            ));
        } else {
            return $this->redirect()->toRoute("audience");
        }

    }

    /**
     * Survey Result Page
     * @return bool|ViewModel
     */
    public function surveyResultAction()
    {
        // validate login session
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        };

        $persona_uuid = $this->params()->fromRoute('uuid', 0);
        $personaService = $this->getService('persona.service');
        $persona = $personaService->getPersonaByUuid($persona_uuid);
        $survey = $personaService->getSurveyByPersona($persona['id']);
        $respondents = array();

        if($survey) {
            $respondents = $personaService->getSurveyRespondentsBySurvey($survey->getId());
            $surveyCount = $personaService->getSurveyRespondentsCountByPersona($persona['id']);
        }

        return new ViewModel(array(
            'persona'           => $persona,
            'respondents'       => $respondents,
            'respodentCount'    => isset($surveyCount)? $surveyCount : 0 ,
            'personaFields'     => $personaService->getAllPersonaFields(),
        ));
    }
    
    /**
     * Security
     * @return ViewModel
     */
    public function securityAction()
    {
        $this->layout('layout/web');
        $shortId = $this->params()->fromRoute('shortId', 0);
        
        
        return new ViewModel(array(
            'shortId' => $shortId
        ));
    }
    
    /**
     * Validate Password
     * @return type
     */
    public function validatePasswordAction()
    {
        $posts = $this->getRequest()->getQuery()->toArray();
        $personaService = $this->getService('persona.service');
        
        $personaData = $personaService->getPersonaByShortCode($posts['hdn-short-id']);
        
        if ( $posts['credential'] == md5($personaData['password']) ) {
            $_SESSION['persona-password'] = 'validPassword';
            return $this->redirect()->toRoute('share/sharePersona', 
                    array('personaShortId' => $posts['hdn-short-id']));
        } else if ( count($personaData) > 0 ) {
            $this->flashMessenger()->addErrorMessage("The password you've enter is wrong. Please input the valid password.");
            return $this->redirect()->toRoute('share/security', 
                    array('shortId' => $posts['hdn-short-id']));
        } else {
            $this->flashMessenger()->addErrorMessage("The persona is not public.");
            return $this->redirect()->toRoute('share/security', 
                    array('shortId' => $posts['hdn-short-id']));
        }
    }
}

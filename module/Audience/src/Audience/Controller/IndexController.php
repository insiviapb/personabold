<?php

namespace Audience\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class IndexController extends AbstractActionController
{
    /**
     * Index Action
     * @return ViewModel
     */
    public function indexAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        $personaService  = $this->getService('persona.service');
        $themeService    = $this->getService('theme.service');
        $accountService  = $this->getService('account.service');
        $authService     = $this->getService('zfcuser_auth_service');
        $currentUser     = $authService->getIdentity();
        
        //unset currentClient Account details
        unset($_SESSION['currentClientUuid']);
        return new ViewModel(array(
            'personas'   => $personaService->getAllPersona(),
            'colors'     => (array)json_decode($themeService->getAccountColors()),
            'industries' => $personaService->getAllPersonaIndustry(),
            'haveCards'  => $this->validateCards() ? 1 : 0,
            'allClient'  => $accountService->getAllClient($currentUser->getAccount()),
            'clientUuid' => $currentUser->getAccount()->getUuid()
        ));
    }
    
    /**
     * Client Action
     * @return ViewModel
     */
    public function clientAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        $clientUuid      = $this->params()->fromRoute('clientUuid', 0);
        $personaService  = $this->getService('persona.service');
        $themeService    = $this->getService('theme.service');
        $accountService  = $this->getService('account.service');
        $authService     = $this->getService('zfcuser_auth_service');
        $currentUser     = $authService->getIdentity();
        
        // set the current client uuid 
        $_SESSION['currentClientUuid'] = $clientUuid; 
        
        $view = new ViewModel(array(
            'personas'   => $personaService->getAllPersonaByClient($clientUuid),
            'colors'     => (array)json_decode($themeService->getAccountColors()),
            'industries' => $personaService->getAllPersonaIndustry($personaService->getClientAccount($clientUuid)),
            'haveCards'  => $this->validateCards() ? 1 : 0,
            'clientUuid' => $clientUuid,
            'allClient'  => $accountService->getAllClient($currentUser->getAccount())
        ));
        $view->setTemplate('audience/index/index.phtml');
        return $view;
    }

    public function surveyAction()
    {
        $uuid = $this->params()->fromRoute('uuid', 0);
        $personaService = $this->getServiceLocator()->get('persona.service');
        $settingsService = $this->getServiceLocator()->get('settings.service');
        $industries = $settingsService->getAllIndustries();
        $personaFields = $personaService->getAllPersonaFields();
        $archetype = $personaService->getAllArchetype();
        $jobtype = $personaService->getAllJobtype();

        $survey = $personaService->getRespondentsSurveyByUuid($uuid);
        $this->layout('layout/web');
        return new ViewModel(array(
            'uuid'          => $uuid,
            'industries'    => $industries,
            'questions'     => json_decode($survey->getQuestions(), true),
            'customTitles'  => json_decode($survey->getCustomTitles(), true),
            'personaFields' => $personaFields,
            'jobtypes'      => $jobtype,
            'archetypes'    => $archetype
        ));
    }

    /**
     * validate login
     * @return boolean
     */
    public function validateLogin()
    {
        $user = $this->getServiceLocator()->get('user.service');
        //validate session
        if (!$user->validateLogin()) {
            return $this->redirect()->toRoute('signin', array(), array( 'query' =>
                array('redirect' => $this->getRequest()->getRequestUri()) ) );
        }
        return true;
    }

    /**
     * Validate Processes that called by ajax
     * @return boolean
     */
    public function validateProcesses()
    {
        $response = $this->getResponse();
        if (!$this->getRequest()->isXmlHttpRequest()) {
            // give empty response
            $response->setStatusCode(200);
            $response->setContent("");
            return $response;
        }

        // validate login session
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }

        return true;
    }

    /**
     * ShortCut for GetServiceLocator
     * @param type $serviceName
     * @return type
     */
    public function getService($serviceName)
    {
        return $this->getServiceLocator()->get($serviceName);
    }
    
    /**
     * check Cards if stripe account have already setup credit cards
     * @return boolean
     */
    public function validateCards()
    {
        $returnVal = false;
        
        $paymentService  = $this->getService('payment.service');
        if ( count($paymentService->getStripeCards()) > 0 ) {
            $returnVal = true;
        }
        return $returnVal;
    }
    
    /**
     * get user free trial
     * @return type
     */
    public function getUserFreeTrial()
    {
        $authService = $this->getService('zfcuser_auth_service');
        $currentUser = $authService->getIdentity();
        
        $dateDiff    = date_diff(new \DateTime(), $currentUser->getCreatedAt())->days;
        $expiration  = ( $dateDiff ) >= 15 ? 0 : 15 - ( $dateDiff );
        return $expiration;
    }
}

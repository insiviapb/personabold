<?php

namespace User;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\EventManager\Event;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\ViewModel;
use Zend\Mail\Message;
use Zend\InputFilter\InputFilter;
use Planbold\InputFilter\Input;
use User\Form\UserForm as UserForm;
use User\Form\AccountForm as AccountForm;
use User\Form\UploadImage as UploadImage;

class Module {

    public function onBootstrap(MvcEvent $e) 
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $app = $e->getParam('application');
        $sm  = $app->getServiceManager();

        // listen user registration
        $userEvent = $sm->get('user.service')->getEventManager();
        $userEvent->attach('register.post', array($this, 'sendRegistrationEmail'));
        $userEvent->attach('register.post', array($this, 'registrationNotification'));

        $userService = $sm->get('user.service');
        $userService->getEventManager()->attach($sm->get('user.service.listener.users'));
        // Attaching publisher details in whole page
        $authService = $sm->get('zfcuser_auth_service');
        $currentUser = $authService->getIdentity();
        if (null !== $currentUser) {
            $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
            $accountColors = $sm->get('theme.service')->getAccountColors();
            $viewModel->accountColors = $accountColors;
            $viewModel->user      = $currentUser;
            $stripeAccountMapper  = $sm->get('planbold.mapper.stripeAccount');
            $stripeCardsMapper    = $sm->get('planbold.mapper.stripeCards');
            $dateDiff             = date_diff(new \DateTime(), $currentUser->getCreatedAt())->days;
            $viewModel->expiration = ( $dateDiff ) >= 15 ? 0 : 15 - ( $dateDiff );
            $stripeAccount        = $stripeAccountMapper->fetchOneBy(array('account' => $currentUser->getAccount()));   
            $stripeCards          = $stripeCardsMapper->fetchPackages($stripeAccount);
            $viewModel->haveCards = count($stripeCards) > 0 ? true : false;
            if ( ($viewModel->haveCards === false && $viewModel->expiration == 0) ||
                 (count($stripeAccount) > 0 && $viewModel->haveCards === true && $stripeAccount->getStripeStatus() === false)) {
                $e->getApplication()->getEventManager()->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) {
                    $controller = $e->getTarget();
                    if ( $e->getRouteMatch()->getMatchedRouteName() != 'settings/subscription-info' ) {
                        $controller->plugin('redirect')->toRoute('settings/subscription-info');
                    }
                }, 100);
            }
        }
    }

    public function getConfig() 
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() 
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function onDispatch(MvcEvent $e) 
    {
        $controller = $e->getTarget();
        $controller->layout('layout/web');
    }

    public function registrationNotification(Event $event) {
        $sm = $event->getTarget()->getServiceLocator();
        $user = $event->getParam('user');

        $view = new ViewModel(array('user' => $user));
        $view->setTemplate('email/registration-notification.phtml');
        $html = $sm->get('view_manager')
            ->getRenderer()
            ->render($view);

        $htmlMimePart = new MimePart($html);
        $htmlMimePart->setType('text/html');
        $mimeMessage = new MimeMessage();
        $mimeMessage->addPart($htmlMimePart);

        $message = $sm->get('registration.notification.message');
        $message->setBody($mimeMessage);

        $mail = $sm->get('web.email.transport');
        $mail->send($message);
    }

    public function sendRegistrationEmail(Event $event)
    {
        $sm = $event->getTarget()->getServiceLocator();
        $user = $event->getParam('user');

        $view = new ViewModel(array('user' => $user));
        $view->setTemplate('email/user-registration.phtml');
        $html = $sm->get('view_manager')
                ->getRenderer()
                ->render($view);

        $htmlMimePart = new MimePart($html);
        $htmlMimePart->setType('text/html');
        $mimeMessage = new MimeMessage();
        $mimeMessage->addPart($htmlMimePart);

        $message = $sm->get('user.email.message.registration');
        $message->setBody($mimeMessage);
        $message->addTo($user->getEmail());

        $mail = $sm->get('web.email.transport');
        $mail->send($message);
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'settings.userform' => function($sm) {
                    return new UserForm('user-form');
                },
                'settings.accountform' => function($sm) {
                    return new AccountForm('account-form');
                },
                'user.email.message.registration' => function($sm) {
                    $config = $sm->get('Config')['email']['message']['user-account-registration'];
                    $message = new Message();
                    $message->addFrom($config['from'], $config['name'])
                            ->setSubject($config['subject']);
                    return $message;
                },
                'registration.notification.message' => function($sm) {
                    $config = $sm->get('Config')['email']['message']['registration-notification'];
                    $message = new Message();
                    $message->addFrom($config['from'], $config['name'])
                        ->addTo($config['email_notify'])
                        ->setSubject($config['subject']);
                    return $message;
                },
                'user.email.message.invite' => function($sm) {
                    $config = $sm->get('Config')['email']['message']['invite-user'];
                    $message = new Message();
                    $message->addFrom($config['from'], $config['name'])
                            ->setSubject($config['subject']);
                    return $message;
                },
                'user.form.uploadimage' => function($sm) {
                    $form = new UploadImage('uploadImage');
                    $inputFilter = new InputFilter();
                    $inputFilter->add(new Input\UserImage($sm));
                    $form->setInputFilter($inputFilter);
                    return $form;
                }
            ),
        );
    }

}

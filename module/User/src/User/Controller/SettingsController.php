<?php
namespace User\Controller;

use User\Controller\IndexController as IndexController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class SettingsController extends IndexController
{
    /**
     * Index Action
     * @return ViewModel
     */
    public function indexAction()
    {
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        }
        $settingsService = $this->getService('settings.service');
        $themeService    = $this->getService('theme.service');
        $accountDetails  = $settingsService->getUser()->getAccount();
        $accountForm     = $this->getService('settings.accountform');
        return new ViewModel(array('account' => $accountDetails,
                                   'form'    => $accountForm,
                                   'colors' => (array)json_decode($themeService->getAccountColors())));
    }
    
    /**
     * Users Profile Action
     * @return ViewModel
     */
    public function usersProfileAction()
    {
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        }
        $settingsService = $this->getService('settings.service');
        $userDetails     = $settingsService->getUser();
        $userForm        = $this->getService('settings.userform');
        return new ViewModel(array('user' => $userDetails,
                                   'form' => $userForm));
    }
    
    /**
     * Industry Page
     * @return ViewModel
     */
    public function industryAction()
    {
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        }
        $settingsService  = $this->getService('settings.service');
        $allClientAccount = $settingsService->getAllClientAccount();
        $accountDetails   = $settingsService->getUser()->getAccount();
        $industries       = $settingsService->getAllClientIndustriesSetting($accountDetails);
        $subIndustries    = $settingsService->getAllSubIndustriesByParentIndustry($industries);

        return new ViewModel(array(
            'industries' => $industries,
            'allClient'  => $allClientAccount,
            'subIndustries' => $subIndustries
        ));
        }
    
    /**
     * Subscription Info Page
     * @return ViewModel
     */
    public function subscriptionInfoAction()
    {
        // validate login session
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        }
        $paymentService = $this->getService('payment.service');
        $creditForm     = $this->getService('payment.form.creditcard');
        $billingForm    = $this->getService('payment.form.billing');
        $authService    = $this->getService('zfcuser_auth_service');
        $packageData    = $paymentService->getCurrentPackage();
        $stripeDetails  = $paymentService->getStripeCards();
        $stripeShipping = $paymentService->getStripeShipping();
        $currentUser    = $authService->getIdentity();
        $allPackages    = $paymentService->getAllPackages();
        return new ViewModel(array('package'        => $packageData, 
                                   'stripeDetails'  => $stripeDetails,
                                   'stripeShipping' => $stripeShipping,
                                   'creditForm'     => $creditForm,
                                   'billingForm'    => $billingForm,
                                   'user'           => $currentUser,
                                   'allPackages'    => $allPackages));
    }
    
    /**
     * Pending Past Payment Page
     * @return ViewModel
     */
    public function pendingPastPaymentAction()
    {
        // validate login session
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        }
        $paymentService  = $this->getService('payment.service');
        $pendingPayment  = $paymentService->getPendingPayment();
        $previousPayment = $paymentService->getPreviousPayment();
        return new ViewModel(array('pendingPayment'  => $pendingPayment,
                                   'previousPayment' => $previousPayment));
    }

    /**
     * Main Page of Brand
     * @return ViewModel
     */
    public function themeAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        $themeService = $this->getService('theme.service');
        return new ViewModel(array('colors' => (array)json_decode($themeService->getAccountColors()),
            'account' => $themeService->getAccount()));
    }

    /**
     * Users Page
     * @return ViewModel
     */
    public function usersAction()
    {
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        }
        $userService = $this->getService('user.service');
        $users       = $userService->getAllUsersListing();
        $user        = $userService->getAllUsers();
        return new ViewModel(array('user' => $users, 'users' => $user));
    }
    
    /**
     * Invoice Page
     * @return ViewModel
     */
    public function invoiceAction()
    {
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        }
        $paymentService    = $this->getService('payment.service');
        $invoiceId         = $this->params()->fromRoute('invoiceId');
        $arrInvoiceDetails = $paymentService->getInvoiceDetails($invoiceId);
        return new ViewModel(array('invoiceDetails' => $arrInvoiceDetails));
    }
    
    /**
     * Persona Templates
     * @return ViewModel
     */
    public function personaTemplatesAction() 
    {
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        }
        $templateService = $this->getService('templates.service');
        return new ViewModel(array(
            'templateGallery' => $templateService->getPersonaTemplates()
        ));
    }
    
    /**
     * Persona Template Guidelines
     * @return ViewModel
     */
    public function personaTemplateGuideAction()
    {
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        }
        return new ViewModel(array('success' => true));
    }
    
    /**
     * Template Details
     * @return ViewModel
     */
    public function templateDetailsAction()
    {
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        }
        $templateRevision = $this->getService('templates.service');
        $templateUuid     = $this->params()->fromRoute('uuid');
        $templateDetails  = $templateRevision->getLatestRevision($templateUuid);
        return new ViewModel(array('templateUuid'    => $templateUuid,
                                   'templateDetails' => $templateDetails));
    }
    
    /**
     * Alert Action
     * @return ViewModel
     */
    public function alertAction()
    {
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        }
        
        $authService    = $this->getService('zfcuser_auth_service');
        $currentUser    = $authService->getIdentity();
        return new ViewModel(array('user' => $currentUser));
    }
    
    /**
     * Persona Fields Page
     * @return ViewModel
     */
    public function personaFieldsAction()
    {
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        }
        $settingsService = $this->getService('settings.service');
        $personaFields   = $settingsService->getPersonaFields();
        return new ViewModel(array(
            'personaFields' => $personaFields
        ));
    }
    
    /**
     * Account files
     * @return ViewModel
     */
    public function accountFilesAction()
    {
        if ( $this->validateLogin() !== true ) {
            return $this->validateLogin();
        }
        $settingsService = $this->getService('settings.service');
        $accountFiles    = $settingsService->getAccountFiles();
        return new ViewModel(array(
            'accountFiles' => $accountFiles
        ));
    }
}
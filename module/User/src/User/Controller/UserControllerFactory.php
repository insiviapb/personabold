<?php

namespace User\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UserControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $services  = $serviceLocator->getServiceLocator();
        $redirectCallback = $services->get('zfcuser_redirect_callback');
        
        /* @var UserController $controller */
        return new UserController($redirectCallback);
    }
}

<?php
namespace User\Controller;

use User\Controller\IndexController as IndexController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class SettingsProcessController extends IndexController
{
    
    /**
     * Industry Page
     * @return ViewModel
     */
    public function industryAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts           = $this->getRequest()->getPost()->toArray();
        $settingsService = $this->getService('settings.service');
        $insertUpdateIndustry = $settingsService->addUpdateIndustries($posts);
        if (isset($posts['action']) && $posts['action'] == 'delete') {
            if ( $settingsService->deleteIndustry($posts) ) {
                return new JsonModel(array(
                    'success'    => true,
                    'industries' => $settingsService->getAllIndustriesSetting()
                ));
            } 
        } else {

            if ( $insertUpdateIndustry['response'] ) {
                return new JsonModel(array(
                    'success'    => true,
                    'industries' => $settingsService->getAllIndustriesSetting()
                ));
            } else {
                return new JsonModel(array(
                    'success'     => false,
                    'message'     => $insertUpdateIndustry['details']
                ));
            }
        }
        
        return new JsonModel(array(
            'success'    => false
        ));
    }

    /**
     * Process Colors
     * @return bool|JsonModel
     */
    public function colorsAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts        = $this->getRequest()->getPost()->toArray();
        $themeService = $this->getService('theme.service');
        return new JsonModel(array('success' => $themeService->processActColors($posts)));
    }
    
    /**
     * Process Users Invitation
     * @return JsonModel
     */
    public function usersAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts       = $this->getRequest()->getPost()->toArray();
        $userService = $this->getService('user.service');
        
        if ( $userService->inviteUsers($posts) ) {
            $data = array('success' => true, 
                          'data'    => $userService->formatUserData($userService->getAllUsers()),
                          'message' => 'User Profile has been successfully updated.');
        } else {
            
            $message = 'User Profile updating failed. Please try again later.';
            if ( !empty($userService->getErrorMessage()) ) {
                $message = $userService->getErrorMessage();
            }
            $data = array('success' => false,
                          'message' => $message);
        }
        return new JsonModel($data);
    }
    
    /**
     * Process Updating of Users
     * @return JsonModel
     */
    public function updateUsersAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts       = $this->getRequest()->getPost()->toArray();
        $userService = $this->getService('user.service');
        if ( $userService->updateInvitedUsers($posts) ) {
            $data = array('success' => true, 
                          'data'    => $userService->formatUserData($userService->getAllUsers()),
                          'message' => 'User Profile has been successfully updated.');
        } else {
            $data = array('success' => false,
                          'message' => 'User Profile updating failed. Please try again later.');
        }
        return new JsonModel($data);
    }
    
    /**
     * Delete Users
     * @return JsonModel
     */
    public function deleteUsersAction()
    {
        if($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts       = $this->getRequest()->getPost()->toArray();
        $userService = $this->getService('user.service');
        
        if ( $userService->deleteInvitedUsers($posts) ) {
            $data = array('success' => true, 
                          'data'    => $userService->formatUserData($userService->getAllUsers()),
                          'message' => 'User Profile has been successfully deleted.');
        } else {
            $data = array('success' => false,
                          'message' => 'User Profile deletion failed. Please try again later.');
        }
        return new JsonModel($data);
    }
    
    /**
     * Process User Profile
     * @return JsonModel
     */
    public function userProfileAction()
    {
      
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        $posts = $this->getRequest()->getPost()->toArray();
        $userService = $this->getService('user.service');
        if ( $userService->updateUser($posts) ) {
            $this->flashMessenger()->addSuccessMessage('User Profile has been successfully updated.');
        } else {
            $this->flashMessenger()->addErrorMessage('User Profile updating failed. Please try again later.');
        }
        return $this->redirect()->toRoute('settings/user-profile');
    }
    
    /**
     * Process Account
     * @return type
     */
    public function accountAction()
    {
        $posts       = $this->getRequest()->getPost()->toArray();
        $userService = $this->getService('user.service');
        
        if (  $userService->updateAccount($posts) ) {
            $this->flashMessenger()->addSuccessMessage('Account has been successfully updated.');
        } else {
            $this->flashMessenger()->addErrorMessage('Account updating failed. Please try again later.');
        }
        return $this->redirect()->toRoute('settings');
    }
    
    /**
     * Upload Photo
     * @return type
     */
    public function uploadPhotoAction()
    {
        if ($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $uploadForm = $this->getService('user.form.uploadimage');
        $posts      = $this->getRequest()->getPost()->toArray();
        $files      = $this->getRequest()->getFiles()->toArray();
        $formData   = array_merge($posts, $files);

        $uploadForm->setData($formData);
        if ($uploadForm->isValid()) {
            $data     = $uploadForm->getData();
            $data['upload-type']  = $formData['upload-type'];
            $data['persona-id'] = (isset($formData['upload-persona-id'])) ? $formData['upload-persona-id'] : null ;
            $data['userFilePath'] = $data['userImage']['tmp_name'];
            $result   = $this->getService('user.service')->uploadPersonaPhoto($data);
            return new JsonModel($result);
        } else {
            return new JsonModel(array('success' => false, 'data' => 'File validation failed. Just receive gif, png, jpg and jpeg.'));
        }
    }
    
    /**
     * Add/Update Credit Cards
     * @return JsonModel
     */
    public function addUpdateCardsAction()
    {
        if ($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts          = $this->getRequest()->getPost()->toArray();
        $paymentService = $this->getService('payment.service');
        $resultArray    = $paymentService->addUpdateCreditCard($posts);
        return new JsonModel($resultArray);
    }
    
    /**
     * Add/Update Billing
     * @return JsonModel
     */
    public function addUpdateBillingAction()
    {
        if ($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts          = $this->getRequest()->getPost()->toArray();
        $paymentService = $this->getService('payment.service');
        $resultArray    = $paymentService->addUpdateBilling($posts);
        return new JsonModel($resultArray);
    }
    
    /**
     * Process Template Details
     * @return JsonModel
     */
    public function processTemplateDetailsAction()
    {
        if ($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts           = $this->getRequest()->getPost()->toArray();
        $templateService = $this->getService('templates.service');
        $result          = $templateService->addUpdateTemplate($posts);
        return new JsonModel($result);
    }
    
    /**
     * Published Persona Templates
     * @return JsonModel
     */
    public function publishedPersonaTemplatesAction()
    {
        if ($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts           = $this->getRequest()->getPost()->toArray();
        $templateService = $this->getService('templates.service');
        $result          = $templateService->publishedPersonaTemplates($posts);
        return new JsonModel($result);
    }
    
    /**
     * PreviewPersonaTemplate
     * @return type
     */
    public function previewPersonaPDFAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        $uuid            = $this->params()->fromRoute('uuid', 0);
        $templateService = $this->getService('templates.service');
        $authService     = $this->getService('zfcuser_auth_service');
        $currentUser     = $authService->getIdentity();
        $result          = $templateService->processPersonaData($currentUser, $uuid);
        if ( $result == false ) {
            return;
        }
        $output          = $this->serviceLocator->get('mvlabssnappy.pdf.service')->getOutputFromHtml($result, array('page-size'   => 'A4'));
        $response        = $this->getResponse();
        $headers         = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/pdf');
        $headers->addHeaderLine('Content-Disposition', "attachment; filename=\"export". ".pdf\"");
        $response->setContent($output);
        return $response;
    }
    
    /**
     * PreviewPersonaTemplate
     * @return type
     */
    public function previewPersonaHTMLAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        $uuid            = $this->params()->fromRoute('uuid', 0);
        $templateService = $this->getService('templates.service');
        $authService     = $this->getService('zfcuser_auth_service');
        $currentUser     = $authService->getIdentity();
        $result          = $templateService->processPersonaData($currentUser, $uuid);
        if ( $result == false ) {
            return new JsonModel(array('success' => false));
        }
        print_r($result);exit;
    }
    
    /**
     * Download Invoice Page
     * @return ViewModel
     */
    public function downloadInvoiceAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        $sm                = $this->getServiceLocator();
        $paymentService    = $sm->get('payment.service');
        $invoiceId         = $this->params()->fromRoute('invoiceId');
        $arrInvoiceDetails = $paymentService->getInvoiceDetails($invoiceId);
        
        $view = new ViewModel(array('invoiceDetails' => $arrInvoiceDetails));
        $view->setTerminal(true);
        $view->setTemplate('user/settings/download-invoice.phtml');
        $viewRenderer = $this->serviceLocator->get('view_manager')->getRenderer();
        $htmlOutput = $viewRenderer->render($view);
        $output   = $this->serviceLocator->get('mvlabssnappy.pdf.service')->getOutputFromHtml($htmlOutput, array(), array('width'=> '400'));
        $response = $this->getResponse();
        $headers  = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/pdf');
        $headers->addHeaderLine('Content-Disposition', "attachment; filename=\"export-" .  $arrInvoiceDetails['id']. ".pdf\"");
        $response->setContent($output);
        return $response;
    }
    
    /**
     * Update Subscription
     * @return JsonModel
     */
    public function updateSubscriptionAction()
    {
        if ($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts           = $this->getRequest()->getPost()->toArray();
        $paymentService  = $this->getService('payment.service');
        $authService     = $this->getService('zfcuser_auth_service');
        $currentUser     = $authService->getIdentity();
        $result          = $paymentService->updateSubscription($currentUser, $posts);
        return new JsonModel($result);
    }
    
    /**
     * Update Notifications
     * @return JsonModel
     */
    public function updateNotificationAction()
    {
        if ($this->validateProcesses() !== true) {
            return $this->validateProcesses();
        }
        $posts           = $this->getRequest()->getPost()->toArray();
        $authService     = $this->getService('zfcuser_auth_service');
        $settingsService = $this->getService('settings.service');
        $currentUser     = $authService->getIdentity();
        $result          = $settingsService->updateNotification($currentUser, $posts);
        return new JsonModel(array('success' => $result));
    }
    
    /**
     * persona fields Action
     * @return JsonModel
     */
    public function personaFieldsAction()
    {
        $posts           = $this->getRequest()->getPost()->toArray();
        $settingsService = $this->getService('settings.service');
        if (isset($posts['action']) && $posts['action'] == 'delete') {
            if ( $settingsService->deletePersonaFields($posts) ) {
                return new JsonModel(array(
                    'success'       => true,
                    'personaFields' => $settingsService->getAllPersonaFieldsSetting()
                ));
            } 
        } else {
            if ( $settingsService->addUpdatePersonaFields($posts) ) {
                return new JsonModel(array(
                    'success'       => true,
                    'personaFields' => $settingsService->getAllPersonaFieldsSetting()
                ));
            } 
        }
        $errMsg = 'An error occured. Please try again later.';
        if ( strpos($settingsService->getErrorMessage(), 'Duplicate') > 0 ) {
            $errMsg = 'Persona Field has been already added. Please select other title.';
        }
        
        
        return new JsonModel(array(
            'success'    => false,
            'message'    => $errMsg
        ));
    }
}
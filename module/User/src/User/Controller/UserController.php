<?php

namespace User\Controller;

use Zend\View\Model\ViewModel;
use ZfcUser\Controller\UserController as ZfcUserUserController;

class UserController extends ZfcUserUserController
{
    const ROUTE_REGISTER = 'register';
    const ROUTE_LOGIN    = 'signin';
    const CONTROLLER_NAME = 'user.controller.user';

    /**
     * @param callable $redirectCallback
     */
    public function __construct($redirectCallback)
    {
        parent::__construct($redirectCallback);
    }


    public function loginAction()
    {
        $this->layout('layout/web');
        if ($this->zfcUserAuthentication()->hasIdentity()) {
            return $this->redirect()->toRoute($this->getOptions()->getLoginRedirectRoute());
        }

        $request    = $this->getRequest();
        $form       = $this->getLoginForm();
        $forceLogin = $this->getServiceLocator()->get('forcelogin.service');
        $userMapper = $this->getServiceLocator()->get('planbold.mapper.user');

        if ($this->getOptions()->getUseRedirectParameterIfPresent() && $request->getQuery()->get('redirect')) {
            $redirect = $request->getQuery()->get('redirect');
        } else {
            $redirect = false;
        }

        if (!$request->isPost()) {
            return array(
                'loginForm' => $form,
                'redirect'  => $redirect,
                'enableRegistration' => $this->getOptions()->getEnableRegistration(),
            );
        }
        if (isset($this->getRequest()->getHeaders()->get('Cookie')->user)) {
            $cookieData = $this->getRequest()->getHeaders()->get('Cookie')->user;
        }
        $uri    = $this->getRequest()->getUri();
        $host   = $uri->getHost();
        $userData   = !empty($cookieData) ? ($this->getRequest()->getHeaders()->get('Cookie')->user['host'] == $host ? $this->getRequest()->getHeaders()->get('Cookie')->user : NULL) : NULL;
        
        if ( empty($userData) ) {
            $form->setData($request->getPost());
            if (!$form->isValid()) {
                $this->flashMessenger()->setNamespace('zfcuser-login-form')->addMessage($this->failedLoginMessage);
                return $this->redirect()->toUrl($this->url()->fromRoute(self::ROUTE_LOGIN).($redirect ? '?redirect='. rawurlencode($redirect) : ''));
            }

            // clear adapters
            $this->zfcUserAuthentication()->getAuthAdapter()->resetAdapters();
            $this->zfcUserAuthentication()->getAuthService()->clearIdentity();
            return $this->forward()->dispatch(self::CONTROLLER_NAME, array('action' => 'authenticate'));
        } else {
            $userData = json_decode(stripslashes($userData), true);
            if (isset($userData['uuid'])) {
                $userDetails = $userMapper->findOneByUuid($userData['uuid']);
                // clear adapters
                $this->zfcUserAuthentication()->getAuthAdapter()->resetAdapters();
                $this->zfcUserAuthentication()->getAuthService()->clearIdentity();
                if (count($userDetails) != 0) {
                    $login = $this->zfcUserAuthentication()->getAuthService()->authenticate(new $forceLogin($userDetails));
                    return $this->redirect()->toUrl($this->url()->fromRoute(SITE_ENV).($redirect ? '?redirect='. rawurlencode($redirect) : ''));
                }
            } 
            $this->flashMessenger()->setNamespace('zfcuser-login-form')->addMessage($this->failedLoginMessage);
            return $this->redirect()->toUrl($this->url()->fromRoute(static::ROUTE_LOGIN).($redirect ? '?redirect='. rawurlencode($redirect) : ''));

        }
    }
    
    /**
     * General-purpose authentication action
     */
    public function authenticateAction()
    {
        if ($this->zfcUserAuthentication()->hasIdentity()) {
            return $this->redirect()->toRoute($this->getOptions()->getLoginRedirectRoute());
        }

        $adapter = $this->zfcUserAuthentication()->getAuthAdapter();
        $redirect = $this->params()->fromPost('redirect', $this->params()->fromQuery('redirect', false));

        $result = $adapter->prepareForAuthentication($this->getRequest());

        // Return early if an adapter returned a response
        if ($result instanceof Response) {
            return $result;
        }

        $auth = $this->zfcUserAuthentication()->getAuthService()->authenticate($adapter);

        if (!$auth->isValid()) {
            $this->flashMessenger()->setNamespace('zfcuser-login-form')->addMessage($this->failedLoginMessage);
            $adapter->resetAdapters();
            return $this->redirect()->toUrl(
                $this->url()->fromRoute(static::ROUTE_LOGIN) .
                ($redirect ? '?redirect='. rawurlencode($redirect) : '')
            );
        }

        $this->setPersistentCookie();
        
        $redirect = $this->redirectCallback;
        return $redirect();
    }

    public function registerAction()
    {
        $userService = $this->getUserService();
        $userService->setRegisterForm($this->getRegisterForm());
        $register = parent::registerAction();
        $this->layout('layout/web');
        return $register;
    }

    public function getRegisterForm()
    {
        if (!$this->registerForm) {
            $this->setRegisterForm($this->getServiceLocator()->get('user.form.register'));
        }

        return $this->registerForm;
    }

    /**
     * Getters/setters for DI stuff
     */
    public function getUserService()
    {
        if (!$this->userService) {
            $this->userService = $this->getServiceLocator()->get('user.service');
        }
        return $this->userService;
    }

    /**
     * validate login
     * @return boolean
     */
    public function validateLogin()
    {
        $user = $this->getServiceLocator()->get('user.service');
        //validate session
        if (!$user->validateLogin()) {
            return $this->redirect()->toRoute('signin', array(), array( 'query' =>
                   array('redirect' => $this->getRequest()->getRequestUri()) ) );
        }
        return true;
    }

    /**
     * Validate Processes that called by ajax
     * @return boolean
     */
    public function validateProcesses()
    {
        $response = $this->getResponse();
        if (!$this->getRequest()->isXmlHttpRequest()) {
            // give empty response
            $response->setStatusCode(200);
            $response->setContent("");
            return $response;
        }
        // validate login session
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return true;
    }
    
    /**
     * Update Passsword Page
     * @return ViewModel
     */
    public function updatePasswordAction()
    {
        return new ViewModel();
    }
    
    /**
     * Manage Questions Actions
     * @return ViewModel
     */
    public function questionsAction()
    {
        return new ViewModel();
    }
    
    /**
     * Manage Persona Boxes Action
     * @return ViewModel
     */
    public function personaBoxesAction()
    {
        return new ViewModel();
    }
    
    /**
     * set persistent cookie
     */
    public function setPersistentCookie()
    {
        $uri    = $this->getRequest()->getUri();
        $host   = $uri->getHost();
        $uuid = $this->zfcUserAuthentication()->getIdentity()->getUuid();
        $cookie = new \Zend\Http\Header\SetCookie('user', json_encode(array('uuid' => $uuid, 'host' => $host)), strtotime('+1 Year', time()), '/', $host); // 1 Year Duration
        $this->getServiceLocator()->get('Response')->getHeaders()->addHeader($cookie);
    }
}

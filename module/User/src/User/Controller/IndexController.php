<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }

    public function dashboardAction()
    {
        // validate login session
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        
        $accountService = $this->getService('account.service');
        $authService    = $this->getService('zfcuser_auth_service');
        $currentUser    = $authService->getIdentity();
        $personaRev     = $accountService->getAllPersonaRevision($currentUser->getAccount());
        
        $viewModel      = new ViewModel();
        if ( count($personaRev) <= 0 ) {
            $personaService  = $this->getService('persona.service');
            $settingsService = $this->getService('settings.service');
            $archetype       = $personaService->getAllArchetype();
            $industries      = $settingsService->getAllIndustriesSetting();
            $viewModel->setTemplate('user/index/getting-started.phtml');
            $viewModel->setVariables(array('archetype'   => $archetype,
                                           'industries'  => $industries));
        } else {
            $allClient  = $accountService->getAllClient($currentUser->getAccount());
            $viewModel->setVariables(array('allClient'       => $allClient,
                                           'personaRevision' => $personaRev));
        }
        return $viewModel;
    }

    public function visionAction()
    {
        // validate login session
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }

        return new ViewModel();
    }
    public function cultureAction()
    {
        // validate login session
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }

        return new ViewModel();
    }

    public function brandAction()
    {
        // validate login session
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }

        return new ViewModel();
    }

    /**
     * validate login
     * @return boolean
     */
    public function validateLogin()
    {
        $user = $this->getServiceLocator()->get('user.service');
        //validate session
        if (!$user->validateLogin()) {
            return $this->redirect()->toRoute('signin', array(), array( 'query' =>
                array('redirect' => $this->getRequest()->getRequestUri()) ) );
        }
        return true;
    }

    /**
     * Validate Processes that called by ajax
     * @return boolean
     */
    public function validateProcesses()
    {
        $response = $this->getResponse();
        if (!$this->getRequest()->isXmlHttpRequest()) {
            // give empty response
            $response->setStatusCode(200);
            $response->setContent("");
            return $response;
        }

        // validate login session
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }

        return true;
    }
    
    /**
     * ShortCut for GetServiceLocator
     * @param type $serviceName
     * @return type
     */
    public function getService($serviceName) 
    {
        return $this->getServiceLocator()->get($serviceName);
    }
}

<?php
namespace User\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use \Planbold\Entity as Entity;
use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\ViewModel;

class Settings implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Industry Mapper
     * @var type 
     */
    protected $industryMapper;

    /**
     * Account Mapper
     * @var type 
     */
    protected $accountMapper;
    
    /**
     * Current Account
     * @var type 
     */
    protected $account;

    /**
     * Get Services
     * @var type 
     */
    protected $service;
    
    /**
     * Agency Client Mapper
     * @var type 
     */
    protected $agencyClientMapper;
    
    /**
     * User Mapper
     * @var type 
     */
    protected $userMapper;
    
    /**
     * @var personaRevisionMapper 
     */
    protected $personaRevisionMapper;
   
    /**
     * @var personaFieldMapper 
     */
    protected $personaFieldsMapper;

    /**
     * Get account files
     * @var type 
     */
    protected $accountFileMapper;

    /**
     * Error Message
     * @var type 
     */
    protected $errMessage;
    
    /**
     * super admin mapper
     * @var type 
     */
    protected $superAdminMapper;

    /**
     * Get Service Object
     * @param type $serviceName
     * @return type
     */
    private function getService($serviceName)
    {
        return $this->getServiceLocator()->get($serviceName);
    }
    
    /**
     * Get Industry Mapper
     * @return type
     */
    private function getIndustryMapper()
    {
        if (!isset($this->industryMapper)) {
            $this->industryMapper = $this->getService('planbold.mapper.industry');
        }
        return $this->industryMapper;
    }
    
    /**
     * Get Account Mapper
     * @return type
     */
    private function getAccountMapper()
    {
        if (!isset($this->accountMapper)) {
            $this->accountMapper = $this->getService('planbold.mapper.account');
        }
        return $this->accountMapper;
    }
    
    /**
     * Get Agency Client Mapper
     * @return type
     */
    private function getAgencyClientMapper()
    {
        if (!isset($this->agencyClientMapper)) {
            $this->agencyClientMapper = $this->getService('planbold.mapper.agencyClient');
        }
        return $this->agencyClientMapper;
    }
    
    /**
     * Get User Mapper
     * @return type
     */
    private function getUserMapper()
    {
        if (!isset($this->userMapper)) {
            $this->userMapper = $this->getService('planbold.mapper.user');
        }
        return $this->userMapper;
    }
    
    /**
     * Get Persona Revision Mapper
     * @return type
     */
    private function getPersonaRevisionMapper()
    {
        if (!isset($this->personaRevisionMapper)) {
            $this->personaRevisionMapper = $this->getService('planbold.mapper.personaRevision');
        }
        return $this->personaRevisionMapper;
    }
    
    /**
     * Get Persona Fields Mapper
     * @return type
     */
    private function getPersonaFieldsMapper()
    {
        if (!isset($this->personaFieldsMapper)) {
            $this->personaFieldsMapper = $this->getService('planbold.mapper.personaFields');
        }
        return $this->personaFieldsMapper;
    }
    
    /**
     * Get Account Files Mapper
     * @return type
     */
    private function getAccountFilesMapper()
    {
        if (!isset($this->accountFileMapper)) {
            $this->accountFileMapper = $this->getService('planbold.mapper.accountFile');
        }
        return $this->accountFileMapper;
    }
    
    /**
     * getSuperUserMapper
     * @return MapperInterface
     */
    public function getSuperUserMapper()
    {
        if (null === $this->superAdminMapper) {
            $this->superAdminMapper = $this->getServiceLocator()->get('planbold.mapper.superAdmin');
        }
        return $this->superAdminMapper;
    }
    
    /**
     * Get User
     * @return type
     */
    public function getUser()
    {
        $authService = $this->getService('zfcuser_auth_service');
        $currentUser = $authService->getIdentity();
        return $currentUser;
    }
    
    /**
     * Get Current Account
     * @return type
     */
    public function getAccount()
    {
        $currentUser   = $this->getUser();
        $this->account = $currentUser->getAccount();
        return $this->account;
    }
    
    /**
     * GEt all client account details
     * @return type
     */
    public function getAllClientAccount()
    {
        $account = $this->getAccount();
        $allClientAccount = $this->getAgencyClientMapper()->getAllAccountDetails($account);
        array_unshift($allClientAccount, array('uuid' => $account->getUuid(),
                                               'name' => $account->getName()));
        return $allClientAccount;
    }
    
    /**
     * Get All Industries under one company
     * @return type
     */
    public function getAllIndustries()
    {
        $industries = $this->getIndustryMapper()->fetchAll();
        return $industries->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
    
    /**
     * Get All Industries by client
     * @return type
     */
    public function getAllIndustriesByClient($accountUuid = null)
    {
        if ( $accountUuid == null || $accountUuid == 0 ) {
            $clientAccount = $this->getAccount();
        } else {
            $clientAccount = $this->getAccountMapper()->findOneBy(array('uuid' => $accountUuid));
        }
        
        $industries = $this->getIndustryMapper()->fetchAll(array('account' => $clientAccount));
        return $industries->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
    
    /**
     * Get All Industries Setting with company
     * @return type
     */
    public function getAllIndustriesSetting()
    {
        $industries = $this->getIndustryMapper()->fetchAllWithSub();
        return $industries->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
    
    /**
     * Get All Industries Setting with company
     * @return type
     */
    public function getAllClientIndustriesSetting($account)
    {
        $industries = $this->getIndustryMapper()->fetchAllWithSub(array('account' => $account, 'parentIndustry' => 0));
        return $industries->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * Get All Sub Industries Setting by Parent Id
     * @return type
     */
    public function getAllSubIndustriesByParentIndustry($parentIndustries)
    {
        $parentIndustryArray = array();
        foreach ($parentIndustries as $parentIndustry) {
            array_push($parentIndustryArray, $parentIndustry['id']);
        }
        
        $subIndustries = $this->getIndustryMapper()->fetchAllSub(array('parentIndustry' => $parentIndustryArray));
        return $subIndustries->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * Add/Update Industries
     * @param type $data
     * @return boolean
     */
    public function addUpdateIndustries($data)
    {
        try {
            $clientAccount = $this->getAccountMapper()->findOneBy(array('uuid' => $data['sct-client']));
            if ( strlen(trim($data['uuid'])) == 0 ) {
                $industry = new Entity\Industry();
            } else {
                $industry = $this->getIndustryMapper()->findOneBy(array('uuid' => $data['uuid']));
            }

            if (!empty($data['industry'])) {
                $industryAvailable = $this->getIndustryMapper()->findOneBy(array(
                    'industryName' => $data['industry'],
                    'account'      => $clientAccount->getId()
                ));

                if((isset($industryAvailable) || !empty($industryAvailable))) {
                    return array(
                      'response' => false, 
                      'details'  => 'Same Industry Name is existing in you account.'
                    );
                }
                $industry->setAccount($clientAccount);
                $industry->setParentIndustry(0);
                $industry->setIndustryName($data['industry']);
                $industry->setPhone($data['phone']);
                $industry->setDescription($data['description']);
            } else if(!empty($data['sub_industry'])) {
                $industryAvailable = $this->getIndustryMapper()->findOneBy(array(
                    'industryName' => $data['sub_industry'],
                    'parentIndustry' => $data['parent_id']
                ));

                if((isset($industryAvailable) || !empty($industryAvailable))) {
                    return array(
                        'response' => false,
                        'details'  => 'Sub-Industry Name is already existing.'
                    );
                } else {
                    $industryAvailable = $this->getIndustryMapper()->findOneBy(array(
                        'id' => (int)$data['parent_id']
                    ));
                }

                $industry->setAccount($industryAvailable->getAccount());
                $industry->setParentIndustry($data['parent_id']);
                $industry->setIndustryName($data['sub_industry']);
                $industry->setPhone($data['sub_phone']);
                $industry->setDescription($data['sub_description']);
            }
            
            if ( strlen(trim($data['uuid'])) == 0 ) {
                $this->getIndustryMapper()->insert($industry);
            } else {
                $this->getIndustryMapper()->update($industry);
            }
        } catch (\Exception $ex) {
            return array(
                'response' => false
            );
        }
        return array('response' => true);
    }
    
    /**
     * Delete Industry
     * @param type $data
     * @return boolean
     */
    public function deleteIndustry($data)
    {
        try {
            if ( strlen(trim($data['uuid'])) != 0 ) {
                $industry = $this->getIndustryMapper()->findOneBy(array('uuid' => $data['uuid']));
                $this->getIndustryMapper()->delete($industry);
          
            }
            return true;
        } catch (\Exception $ex) {
            return false;
        }
        return false;
    }
    
    /**
     * Update Notifications
     * @param type $user
     * @param type $data
     * @return array
     */
    public function updateNotification( $user, $data )
    {
        $haveAlert = (boolean)$data['haveAlert'];
        
        if ( $user !== null ) {
            $user->setHaveAlert($haveAlert);
            $user->setHours($data['hours']);
            $this->getUserMapper()->update($user);
            return true;
        }
        return false;
    }
    
    /**
     * process notifications
     */
    public function processEmailNotifications()
    {
        $users = $this->getUserMapper()->getActiveAlert();
        
        foreach ( $users as $user ) {
            
            $userId = $user['actId'];
            if ( isset($user['clientId']) && $user['clientId'] !== null ) {
                $userId .= ',' . $user['clientId'];
            }
            $userIds   = explode(',', $userId);
            $revisions = $this->getPersonaRevisionMapper()->fetchAllRevisionWithDate($userIds);
            $emailData = array(
                'user'      => $user,
                'revisions' => $revisions
            );
            $this->sendEmailNotifications($emailData);
        }
    }
    
    /**
     * Email Notifications
     * @param type $emailData
     */
    private function sendEmailNotifications($emailData)
    {
        $config  = $this->getService('Config')['email']['message']['send-notification'];
        $view    = new ViewModel($emailData);
        $view->setTemplate('email/send-notification.phtml');
        $html = $this->getService('view_manager')
                ->getRenderer()
                ->render($view);
        $htmlMimePart = new MimePart($html);
        $htmlMimePart->setType('text/html');
        $mimeMessage = new MimeMessage();
        $mimeMessage->addPart($htmlMimePart);

        $message = new Message();
        $message->addFrom($config['from'], $config['name'])
                ->setSubject($config['subject']);
        $message->setBody($mimeMessage);
        $message->addTo($emailData['user']['email']);

        $mail = $sm->get('web.email.transport');
        $mail->send($message);

        $message = null;
        $mail    = null;
    }
    
    /**
     * Get Persona Fields
     * @return type
     */
    public function getPersonaFields()
    {
        $personaFields = $this->getPersonaFieldsMapper()->fetchAll();
        return $personaFields->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
    
    /**
     * Delete Persona Fields
     * @param type $data
     * @return boolean
     */
    public function deletePersonaFields($data)
    {
        try {
            if ( strlen(trim($data['id'])) != 0 ) {
                $personaFields = $this->getPersonaFieldsMapper()->findOneBy(array('id' => $data['id']));
                $this->getPersonaFieldsMapper()->delete($personaFields);
          
            }
            return true;
        } catch (\Exception $ex) {
            $this->errMessage = $ex->getMessage();
            return false;
        }
        return false;
    }
    
    /**
     * Add/Update Persona Fields
     * @param type $data
     * @return boolean
     */
    public function addUpdatePersonaFields($data)
    {
        try {
            if ( strlen(trim($data['id'])) == 0 ) {
                $personaFields = new Entity\PersonaFields();
                $personaFields->setTitle($data['title']);
                $personaFields->setFieldCode($data['field_code']);
            } else {
                $personaFields = $this->getPersonaFieldsMapper()->findOneBy(array('id' => $data['id']));
            }
            $personaFields->setType($data['type']);
            $personaFields->setDescription($data['description']);
            
            if ( strlen(trim($data['id'])) == 0 ) {
                $this->getPersonaFieldsMapper()->insert($personaFields);
            } else {
                $this->getPersonaFieldsMapper()->update($personaFields);
            }
        } catch (\Exception $ex) {
            $this->errMessage = $ex->getMessage();
            return false;
        }
        return true;
    }
    
    /**
     * return error message
     * @return type
     */
    public function getErrorMessage()
    {
        return $this->errMessage;
    }
    
    /**
     * Get All Persona Fields Setting
     * @return type
     */
    public function getAllPersonaFieldsSetting()
    {
        return $this->getPersonaFieldsMapper()->fetchAll()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        
    }
    
    /**
     * Get Account Files
     * @return type
     */
    public function getAccountFiles()
    {
        $account = $this->getUser()->getAccount();
        return $this->getAccountFilesMapper()->fetchAll(array('account' => $account))->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
    
    /**
     * Check super user
     * @param type $identity
     * @return type
     */
    public function checkSuperUser($identity)
    {
        return $this->getSuperUserMapper()->findOneBy(array('userId' => $identity->getId()));
    }
}
<?php
namespace User\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use \Planbold\Entity as Entity;

class Theme implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

   
    /**
     * Get Services
     * @var type 
     */
    protected $service;
    
    /**
     * Account
     * @var type 
     */
    protected $account;
    
    /**
     * color widget
     * @var $colorWidget 
     */
    protected $colorWidgetMapper;
    
    /**
     * Account Mapper
     * @var type 
     */
    protected $accountMapper;
    
    /**
     * Get Service Object
     * @param type $serviceName
     * @return type
     */
    private function getService($serviceName)
    {
        return $this->getServiceLocator()->get($serviceName);
    }
    
    /**
     * Get User
     * @return type
     */
    public function getUser()
    {
        $authService = $this->getService('zfcuser_auth_service');
        $currentUser = $authService->getIdentity();
        return $currentUser;
    }
    
    /**
     * Get Current Account
     * @return type
     */
    public function getAccount()
    {
        $currentUser   = $this->getUser();
        $this->account = count($currentUser) > 0 ? $currentUser->getAccount() : array();
        return $this->account;
    }
    
    /**
     * Get Client Account
     * @param type $uuid
     * @return type
     */
    public function getClientAccount($uuid)
    {
        $account = $this->getAccountMapper()->findOneBy(array('uuid' => $uuid));
        return $account;
    }
    
    /**
     * Return Get ColorWidget Mapper
     * @return type
     */
    public function getColorWidgetMapper()
    {
        if ( null == $this->colorWidgetMapper ) {
            $this->colorWidgetMapper = $this->getService('planbold.mapper.colorWidget');
        }
        return $this->colorWidgetMapper;
    }
    
    /**
     * Return Get Account Mapper
     * @return type
     */
    public function getAccountMapper()
    {
        if ( null == $this->accountMapper ) {
            $this->accountMapper = $this->getService('planbold.mapper.account');
        }
        return $this->accountMapper;
    }
    
    /**
     * Get Account Colors
     * @return type
     */
    public function getAccountColors()
    {
        $colorWidget = $this->getColorWidgetMapper()->findOneBy(array('account' => $this->getAccount()));
        return count($colorWidget) > 0 ? $colorWidget->getJsonData() : '{}';
    }
    
    /**
     * Get Client Account Colors
     * @return type
     */
    public function getClientAccountColors($account)
    {
        $colorWidget = $this->getColorWidgetMapper()->findOneBy(array('account' => $account));
        return count($colorWidget) > 0 ? $colorWidget->getJsonData() : '{}';
    }
    
    /**
     * Process Account Colors
     * @param type $data
     * @return boolean
     */
    public function processActColors($data)
    {
        $account     = $this->getClientAccount($data['uuid']);
        $colorWidget = $this->getColorWidgetMapper()->findOneBy(array('account' => $account));
        if ( count($colorWidget) > 0 ) {
            $colorWidget->setJsonData(json_encode($data));
            $this->getColorWidgetMapper()->update($colorWidget);
        } else {
            $colorWidget = new Entity\ColorWidget();
            $colorWidget->setAccount($account);
            $colorWidget->setJsonData(json_encode($data));
            $this->getColorWidgetMapper()->insert($colorWidget);
        }
        return true;
    }
}
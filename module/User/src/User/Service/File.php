<?php
namespace User\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class File implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

   
    /**
     * Get Services
     * @var type 
     */
    protected $service;
    
    /**
     * Account
     * @var type 
     */
    protected $account;
    
    /**
     * Account Mapper
     * @var type 
     */
    protected $accountMapper;
    
    /**
     * Account File Mapper
     * @var type 
     */
    protected $accountFileMapper;
    
    /**
     * Get Service Object
     * @param type $serviceName
     * @return type
     */
    private function getService($serviceName)
    {
        return $this->getServiceLocator()->get($serviceName);
    }
    
    /**
     * Get User
     * @return type
     */
    public function getUser()
    {
        $authService = $this->getService('zfcuser_auth_service');
        $currentUser = $authService->getIdentity();
        return $currentUser;
    }
    
    /**
     * Get Current Account
     * @return type
     */
    public function getAccount()
    {
        $currentUser   = $this->getUser();
        $this->account = count($currentUser) > 0 ? $currentUser->getAccount() : array();
        return $this->account;
    }
    
    /**
     * Get Client Account
     * @param type $uuid
     * @return type
     */
    public function getClientAccount($uuid)
    {
        $account = $this->getAccountMapper()->findOneBy(array('uuid' => $uuid));
        return $account;
    }
    
    /**
     * Return Get Account Mapper
     * @return type
     */
    public function getAccountMapper()
    {
        if ( null == $this->accountMapper ) {
            $this->accountMapper = $this->getService('planbold.mapper.account');
        }
        return $this->accountMapper;
    }
    
    /**
     * Get Account File Mapper
     * @return type
     */
    public function getAccountFileMapper()
    {
        if ( null == $this->accountFileMapper ) {
            $this->accountFileMapper = $this->getService('planbold.mapper.accountFile');
        }
        return $this->accountFileMapper;
    }
    
    /**
     * Get Account Files
     * @param type $account
     * @return type
     */
    public function getAccountFiles($account)
    {
        $accountFiles = $this->getAccountFileMapper()->fetchAll(array('account' => $account))->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $accountFiles;
    }
    
}
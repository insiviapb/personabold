<?php
namespace User\Service\Email\Message;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mail\Message;

/**
 * Message Object For Registration Email
 *
 * @author Dolly Aswin <dolly.aswin@gmail.com>
 */
class RegistrationFactory implements FactoryInterface
{
    /**
     * Create a service for Registration Email
     *
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config  = $serviceLocator->get('Config')['email']['message']['user-account-registration'];
        $message = new Message();
        $message->addFrom($config['from'], $config['name'])
            ->setSubject($config['subject']);
        return $message;
    }
}

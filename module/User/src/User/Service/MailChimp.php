<?php
namespace User\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;
use \DrewM\MailChimp\MailChimp as MailChimpClient;

class MailChimp implements ServiceLocatorAwareInterface, EventManagerAwareInterface
{
    use ServiceLocatorAwareTrait;

    use EventManagerAwareTrait;
    
    /**
     * Client
     * @var type 
     */
    private $client;
    
    /**
     * API KEY
     * @var type 
     */
    private $apiKey;
    
    /**
     * List Id
     * @var type 
     */
    private $listId;
    
    /**
     * Set API key
     */
    public function setApiKey()
    {
        $config       = $this->getServiceLocator()->get('Config');
        $this->apiKey = $config['mail-chimp']['api-key'];
        $this->client = new MailChimpClient($this->apiKey);
    }
    
    /**
     * Set List Id
     */
    public function setListId()
    {
        $config       = $this->getServiceLocator()->get('Config');
        $this->listId = $config['mail-chimp']['list-id'];
    }
    
    /**
     * Email Subscribe
     * @param type $email
     */
    public function emailSubscribe($postData)
    {
        $this->setApiKey();
        $this->setListId();
        
        try {
            $this->client->post("lists/{$this->listId}/members", [
                'email_address' => $postData['email'],
                'status'        => 'subscribed',
                'merge_fields' => ['FNAME'=> $postData['firstName'], 'LNAME'=> $postData['lastName']]
            ]);
            return true;
        } catch (\Exception $e) {
            return false;
            // do nothing
        }
    }
    
    public function getList()
    {
        $this->setApiKey();
        $result = $this->client->get('lists');
    }
}
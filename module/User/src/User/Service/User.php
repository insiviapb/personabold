<?php
namespace User\Service;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Stdlib\Hydrator;
use ZfcUser\Service\User as ParentUser;
use Zend\Crypt\Password\Bcrypt;
use Zend\Math\Rand as ZfMathRand;

class User extends ParentUser implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const STATUS_SIGNUP = 'signup';

    const STATUS_CANCEL = 'cancel';

    const STATUS_ACTIVE = 'active';

    const STATUS_INACTIVE = 'inactive';
    
    const DEFAULT_PLAN = 'PLAN-1';

    /**
     * @var AccountMapperInterface 
     */
    protected $accountMapper;
    
    
    /**
     * @var Hydrator\ClassMethods
     */
    protected $formHydrator;
    /**
     * @var Form
     */
    protected $registerForm;

    /**
     * @var Persona Mapper
     */
    private $personaMapper;

    /**
     * @var User Role Mapper 
     */
    protected $userRoleMapper;
    
    /**
     * @var error message 
     */
    protected $errorMessage;
    
    /*
     * site user mapper
     */
    protected $siteUserMapper;
    
    /**
     * MailChimp Service
     * @var type 
     */
    protected $mailChimpService;


    /**
     * Return the Form Hydrator
     *
     * @return \Zend\Stdlib\Hydrator\ClassMethods
     */
    public function getFormHydrator()
    {
        if (!$this->formHydrator instanceof Hydrator\HydratorInterface) {
            $this->setFormHydrator($this->getServiceLocator()->get('zfcuser_register_form_hydrator'));
        }

        return $this->formHydrator;
    }

    /**
     * Set the Form Hydrator to use
     *
     * @param Hydrator\HydratorInterface $formHydrator
     * @return User
     */
    public function setFormHydrator(Hydrator\HydratorInterface $formHydrator)
    {
        $this->formHydrator = $formHydrator;
        return $this;
    }

    /**
     * @return Form
     */
    public function getRegisterForm()
    {
        if (null === $this->registerForm) {
            $this->registerForm = $this->getServiceLocator()->get('user.form.register');
        }
        return $this->registerForm;
    }

    /**
     * @param Form $registerForm
     * @return User
     */
    public function setRegisterForm(Form $registerForm)
    {   
        $this->registerForm = $registerForm;
        return $this;
    }
    
    /**
     * Get Account Mapper
     * @return AccountMapperInterface
     */
    public function getAccountMapper()
    {
        if (null === $this->accountMapper) {
            $this->accountMapper = $this->getServiceManager()->get('planbold.mapper.account');
        }
        return $this->accountMapper;
    }

    /**
     * Set Account Mapper Interface
     * @param AccountMApperInterface $accountMapper
     * @return $account
     */
    public function setAccountMapper($accountMapper)
    {
        $this->accountMapper = $accountMapper;
        return $this;
    }

    /**
     * @return array|PersonaMapperInterface|object
     */
    public function getPersonaMapper()
    {
        if(null === $this->personaMapper) {
            $this->personaMapper = $this->getServiceManager()->get('planbold.mapper.persona');
        }

        return $this->personaMapper;
    }


    /**
     * Get UserRole Mapper
     * @return UserRoleMapperInterface
     */
    public function getUserRoleMapper()
    {
        if (null === $this->userRoleMapper) {
            $this->userRoleMapper = $this->getServiceManager()->get('planbold.mapper.userRole');
        }
        return $this->userRoleMapper;
    }
    
    /**
     * Get Site User Mapper
     * @return UserMapperInterface
     */
    public function getSiteUserMapper()
    {
        if (null === $this->siteUserMapper) {
            $this->siteUserMapper = $this->getServiceManager()->get('planbold.mapper.user');
        }
        return $this->siteUserMapper;
    }
    
    /**
     * Set UserRole Mapper Interface
     * @param UserRoleMpperInterface $userRoleMapper
     * @return $userRoleMapper
     */
    public function setUserRoleMapper($userRoleMapper)
    {
        $this->userRoleMapper = $userRoleMapper;
        return $this;
    }
    
    /**
     * Get MailChimp Service
     * @return MailChimpService
     */
    public function getMailChimpService()
    {
        if (null === $this->mailChimpService) {
            $this->mailChimpService = $this->getServiceManager()->get('mailchimp.service');
        }
        return $this->mailChimpService;
    }
    
    /**
     * createFromForm
     *
     * @param array $data
     * @return \Planbolds\Entity\User
     * @throws Exception\InvalidArgumentException
     */
    public function register(array $data)
    {
        $class = $this->getOptions()->getUserEntityClass();
        $user  = new $class;
        $form  = $this->getRegisterForm();
        $form->setHydrator($this->getFormHydrator());
        $form->bind($user);
        
        // set email to username
        $data['username'] = $data['email'];
        
        $form->setData($data);
        if (!$form->isValid()) {
            return false;
        }

        $user = $form->getData();
        //register email in Mail Chimp
        try {
        
            $mailChimpService = $this->getMailChimpService();
            if ( !$mailChimpService->emailSubscribe($data) ) {
                return false;
            }
        } catch(\Exception $e) {
            var_dump($e->getMessage());exit;
        }
        // Activation code: generate 7 random integer
        $activation_code = ZfMathRand::getInteger(1000000,9999999);

        $account = new \Planbold\Entity\Account();
        $account->setName($data['companyName']);
        $ownerUserRole = $this->getUserRoleMapper()->getOwnerRole();
        $user->setUserRole($ownerUserRole);
        $bcrypt = new Bcrypt;
        $bcrypt->setCost($this->getOptions()->getPasswordCost());
        $user->setPassword($bcrypt->create($user->getPassword()));
        $user->setActivationCode($activation_code);
        $user->setHaveAlert(false);
        
        if ($this->getOptions()->getEnableUsername()) {
            $user->setUsername($data['email']);
            //create stripe account
            $stripeService  = $this->getServiceLocator()->get('stripe.service');
            $customer   = $stripeService->createCustomer($data['email']);
            $stripeData = new \Planbold\Entity\StripeAccount();
            $stripeData->setAccount($account);
            $stripeData->setAccountId($customer->id);
            $stripeData->setEmail($customer->email);
            $stripeData->setStripeStatus(true);
        }
        if ($this->getOptions()->getEnableDisplayName()) {
            $user->setDisplayName($data['display_name']);
        }

        // set user account/company
        $user->setAccount($account);

        // If user state is enabled, set the default state value
        if ($this->getOptions()->getEnableUserState()) {
            $user->setState($this->getOptions()->getDefaultUserState());
        }
        $em = $this->getServiceLocator()->get('Doctrine\\ORM\\EntityManager');
        try {
            $em->getConnection()->beginTransaction(); // suspend auto-commit

            $em->persist($user);
            $em->persist($account);
            $em->persist($stripeData);
            $em->flush();
            $em->getConnection()->commit();
            $this->getEventManager()->trigger(
                __FUNCTION__.'.post',
                $this,
                array('user' => $user, 'form' => $form)
            );
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
        }

        return $user;
    }

    /***
     * validate if user is still login
     */
    public function validateLogin()
    {
        $roleService = $this->getServiceLocator()->get('planbold.role');
        $user        = $roleService->getUser();

        if (!$user instanceof \Planbold\Entity\User) {
            return false;
        }
        return true;
    }
    
    /**
     * Get User
     * @return type
     */
    private function getUser()
    {
        $authService = $this->getServiceLocator()->get('zfcuser_auth_service');
        return $authService->getIdentity();
    }
    
    /**
     * Get Current Account
     * @return type
     */
    private function getAccount()
    {
        $currentUser = $this->getUser();
        return $currentUser->getAccount();
    }
    
    /**
     * Update User Details
     * @param type $data
     * @return boolean
     */
    public function updateUser($data)
    {
        try {
            $userDetails = $this->getUser();
            $userDetails->setFirstName($data['first_name']);
            $userDetails->setLastName($data['last_name']);
            $userDetails->setUsername($data['username']);
            $userDetails->setEmail($data['email']);
            $userDetails->setDisplayName($data['display_name']);
            $userDetails->setTitle($data['title']);
            $userDetails->setAddress($data['address']);
            $userDetails->setPhone($data['phone']);
            
            $this->getUserMapper()->update($userDetails);
            return true;
            
        } catch (\Exception $ex) {
            return false;
        }
    }
    
    /**
     * Update Invited Users
     * @param type $data
     * @return boolean
     */
    public function updateInvitedUsers($data)
    {
        if (!isset($data['edit-uuid'])) {
            return false;
        }
        $userMapper = $this->getServiceManager()->get('planbold.mapper.user');
        $users      = $userMapper->fetchOneByUuid($data['edit-uuid']);
        if (count($users) == 0) {
            return false;
        }
        try {
            $users->setUserRole($this->getUserRoleMapper()->getRoleBy(array('id'=>$data['edit-role'])));
            $this->getUserMapper()->update($users);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }
    
    /**
     * Delete Invited Users
     * @param type $data
     * @return boolean
     */
    public function deleteInvitedUsers($data)
    {
        if (!isset($data['uuid'])) {
            return false;
        }
        $userMapper = $this->getServiceManager()->get('planbold.mapper.user');
        $users      = $userMapper->fetchOneByUuid($data['uuid']);
        if (count($users) == 0) {
            return false;
        }
        try {
            $userMapper->delete($users);
            return true;
        } catch (\Exception $ex) {
            var_dump($ex);exit;
            return false;
        }
    }
    
    /**
     * Update Account
     * @param type $data
     * @return boolean
     */
    public function updateAccount($data)
    {
        try {
            $account = $this->getUser()->getAccount();
            $account->setName($data['company']);
            $account->setDescription($data['description']);
            $this->getAccountMapper()->update($account);
            return true;
            
        } catch (\Exception $ex) {
            return false;
        }
    }
    
    /**
     * Get All Users
     * @return type
     */
    public function getAllUsers()
    {
        $account = $this->getAccount();
        return $account->getUser();
    }
    
    /**
     * List all Users in Listing
     * @return type
     */
    public function getAllUsersListing()
    {
        $account = $this->getAccount();
        return $this->formatUsers($account->getUser());
    }
    
    /**
     * Format Users
     * @param type $users
     * @return array
     */
    public function formatUsers($users)
    {
        $arrUsers = array();
        foreach ( $users as $user ) {
            array_push($arrUsers, array(
                'firstname'  => $user->getFirstName(),
                'lastname'   => $user->getLastName(),
                'email'      => $user->getEmail(),
                'roleName'   => $user->getUserRole()->getName(),
                'roleId'     => $user->getUserRole()->getId(),
                'uuid'       => $user->getUuid(),
                'image'      => $user->getImagePath()
            ));
        }
        return $arrUsers;
    }
    
    /**
     * format user data
     * @param type $usersData
     * @return array
     */
    public function formatUserData($usersData)
    {
        $arrUsers = array();
        foreach ($usersData as $user) {
            array_push($arrUsers, array(
                'firstname' => $user->getFirstName(),
                'lastname'  => $user->getLastName(),
                'email'     => $user->getEmail(),
                'role'      => $user->getUserRole()->getName(),
                'uuid'      => $user->getUuid(),
                'roleId'    => $user->getUserRole()->getId()
            ));
        }
        return $arrUsers;
    }
    
    /**
     * Invite Users
     * @param type $data
     * @return boolean
     */
    public function inviteUsers($data)
    {
        try {
            
            //check if email is existing already
            if ( isset($data['email']) ) {
                $userDetails = $this->getSiteUserMapper()->fetchOneByEmail($data['email']);
                if ( count($userDetails) > 0 ) {
                    $this->errorMessage = 'Email is already existed. Please try other email.';
                    return false;
                }
            }
             
            $user   = new \Planbold\Entity\User();
            $bcrypt = new Bcrypt;
            $user->setFirstName($data['firstname']);
            $user->setLastName($data['lastname']);
            $user->setEmail($data['email']);
            $user->setAccount($this->getAccount());
            $user->setUserRole($this->getUserRoleMapper()->getRoleBy(array('id' => $data['role'])));
            $bcrypt->setCost($this->getOptions()->getPasswordCost());
            $user->setPassword($bcrypt->create(ZfMathRand::getInteger(1000000,9999999)));
            $user->setHaveAlert(false);
            $this->getUserMapper()->insert($user);
            //update payment subscription
            $paymentService = $this->getServiceLocator()->get('payment.service');
            $paymentService->processSubscription($user);
            
            $this->getEventManager()->trigger(
                __FUNCTION__.'.post',
                $this,
                array('user' => $user)
            );
            
            return true;
        } catch (\Exception $ex) {
            var_dump($ex->getMessage());exit;
            return false;
        }
    }
    
    /**
     * return error message
     * @return type
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }
    
    /**
     * Save Photo
     * @param type $data
     */
    public function savePhoto($data)
    {
        try {
            if ( $data['upload-type'] == 'company') {
                $account = $this->getAccount();
                $account->setLogo($data['userFilePath']);
                $this->getAccountMapper()->update($account);
                $this->getEventManager()->trigger(
                    'saveCompanyLogo.post',
                    $this,
                    array('account' => $account,
                        'data' => $data)
                );
            } else if ( $data['upload-type'] == 'persona' && !empty($data['persona-id'])) {
                $persona = $this->getPersonaMapper()->fetchOneBy(array('id' => $data['persona-id']));
                $persona->setHeadshot($data['userFilePath']);
                $this->getPersonaMapper()->update($persona);
                $this->getEventManager()->trigger(
                    'savePersonaHeadshot.post',
                    $this,
                    array('persona' => $persona,
                        'data' => $data)
                );
            } else {
                $user = $this->getUser();
                $user->setImagePath($data['userFilePath']);
                $this->getUserMapper()->update($user);
                $this->getEventManager()->trigger(
                    'saveUserPhoto.post',
                    $this,
                    array('user' => $user,
                          'data' => $data)
                );
            }
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }
    
    /**
     * Upload Persona Photo
     */
    public function uploadPersonaPhoto($data)
    {
        try {
            $config = $this->getServiceLocator()->get('Config');
            $this->getEventManager()->trigger(
                'uploadPersonaPhoto.post',
                $this,
                array('persona' => $data['userImage'])
            );
            
            $filename = $config['s3']['url'].$config['s3']['plan']['bucket'].'/'.basename($data['userImage']['tmp_name']);
            
            return array('success' => true, 'filename' => $filename);
        } catch (\Exception $ex) {
            return array('success' => false);
        }
    }
}
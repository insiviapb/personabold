<?php
namespace User\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZfcUser\Validator;
use ZfcUser\Form;

/**
 * RedirectCallback Factory
 *
 * @author Dolly Aswin <dolly.aswin@gmail.com>
 */
class RegisterFormFilterFactory implements FactoryInterface
{
    /**
     * Create a service for DoctrineObject Hydrator
     *
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $form    = $serviceLocator->get('zfcuser_register_form');
        $options = $serviceLocator->get('zfcuser_module_options');
        $form->setInputFilter(new Form\RegisterFilter(
            new Validator\NoRecordExists(array(
                'mapper' => $serviceLocator->get('zfcuser_user_mapper'),
                'key'    => 'email'
            )),
            new Validator\NoRecordExists(array(
                'mapper' => $serviceLocator->get('zfcuser_user_mapper'),
                'key'    => 'username'
            )),
            $options
        ));
        $inputFilter = $form->getInputFilter();
        $inputFilter->add(array(
            'name'       => 'firstName',
            'required'   => true,
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'options' => array(
                        'min' => 3,
                        'max' => 64,
                    ),
                ),
            ),
        ));
        $inputFilter->add(array(
            'name'       => 'lastName',
            'required'   => true,
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'options' => array(
                        'min' => 3,
                        'max' => 64,
                    ),
                ),
            ),
        ));
        $inputFilter->add(array(
            'name'       => 'companyName',
            'required'   => true,
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'options' => array(
                        'min' => 3,
                        'max' => 64,
                    ),
                ),
            ),
        ));
        
        
        return $inputFilter;
    }
}

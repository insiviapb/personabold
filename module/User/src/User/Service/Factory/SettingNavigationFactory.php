<?php

namespace User\Service\Factory;

use Zend\Navigation\Service\DefaultNavigationFactory;

class SettingNavigationFactory extends DefaultNavigationFactory
{
    /**
     * @return string
     */
    protected function getName()
    {
        return 'setting-navigations';
    }
}

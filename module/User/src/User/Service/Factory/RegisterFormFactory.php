<?php
namespace User\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * RedirectCallback Factory
 *
 * @author Aleks Daloso<adaloso@insivia.com>
 */
class RegisterFormFactory implements FactoryInterface
{
    /**
     * Create a service for DoctrineObject Hydrator
     *
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $form = $serviceLocator->get('zfcuser_register_form');
        $inputFilter = $serviceLocator->get('user.form.register.filter');
        $form->setInputFilter($inputFilter);
        $form->add(array(
            'name' => 'firstName',
            'options' => array(
                'label' => 'First Name',
            ),
            'attributes' => array(
                'type' => 'text'
            ),
        ));
        $form->add(array(
            'name' => 'lastName',
            'options' => array(
                'label' => 'Last Name',
            ),
            'attributes' => array(
                'type' => 'text'
            ),
        ));
        $form->add(array(
            'name' => 'companyName',
            'options' => array(
                'label' => 'Company Name',
            ),
            'attributes' => array(
                'type' => 'text'
            ),
        ));
        $form->add(array(
            'name' => 'userType',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'type'  => 'hidden',
                'value' => 'user'
            ),
        ));
        
        return $form;
    }
}

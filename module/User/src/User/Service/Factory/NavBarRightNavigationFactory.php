<?php

namespace User\Service\Factory;

use Zend\Navigation\Service\DefaultNavigationFactory;

class NavBarRightNavigationFactory extends DefaultNavigationFactory
{
    /**
     * @return string
     */
    protected function getName()
    {
        return 'user-navbar-right';
    }
}

<?php

namespace User\Service\Factory;

use Zend\Navigation\Service\DefaultNavigationFactory;

class NavBarLeftNavigationFactory extends DefaultNavigationFactory
{
    /**
     * @return string
     */
    protected function getName()
    {
        return 'user-navbar-left';
    }
}

<?php
/**
 * Notification EventListener
 *
 * @link
 * @copyright Copyright (c) 2015
 */

namespace User\Service\Listener;

use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\ViewModel;
use Aws\S3\Exception\S3Exception;
use Aws\Sdk as AwsSdk;

/**
 * User Event Listener
 * @author  Oliver Pasigna <opasigna@insivia.com>
 * @SuppressWarnings(PHPMD)
 */
class UserEventListener implements ListenerAggregateInterface, ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    
    const APPROVED_STATUS  = 'approved';
    
    const DECLINED_STATUS  = 'declined';
    
    const CANCELLED_STATUS = 'cancelled';
    
    /**
     * List of listeners
     *
     * @var \Zend\Stdlib\CallbackHandler[]
     */
    protected $listeners = [];
    
    /**
     * Attach listeners to an event manager
     *
     * @param  EventManagerInterface $events
     * @return void
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('savePersonaHeadshot.post', [$this, 'savePersonaHeadshot'], 1000);
        $this->listeners[] = $events->attach('savePersonaHeadshot.post', [$this, 'savePersonaHeadshotData'], 900);
        $this->listeners[] = $events->attach('saveCompanyLogo.post', [$this, 'saveCompanyLogo'], 800);
        $this->listeners[] = $events->attach('saveCompanyLogo.post', [$this, 'saveCompanyLogoData'], 700);
        $this->listeners[] = $events->attach('saveUserPhoto.post', [$this, 'saveUserPhoto'], 600);
        $this->listeners[] = $events->attach('saveUserPhoto.post', [$this, 'saveUserPhotoData'], 500);
        $this->listeners[] = $events->attach('inviteUsers.post', [$this, 'sendInviteUser'], 300);
        $this->listeners[] = $events->attach('uploadPersonaPhoto.post', [$this, 'uploadPersonaPhoto'], 200);
    }
    
    /**
     * Detach listeners from an event manager
     * @param  EventManagerInterface $events
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    
    public function postUpload(EventInterface $event)
    {
    }
    
    /**
     * Send Invite Users
     * @param EventInterface $event
     */
    public function sendInviteUser(EventInterface $event)
    {
        $sm   = $this->getServiceLocator();
        $user = $event->getParam('user');
        $view = new ViewModel(array('user' => $user));
        $view->setTemplate('email/invite-users.phtml');
        $html = $sm->get('view_manager')
                   ->getRenderer()
                   ->render($view);

        $htmlMimePart = new MimePart($html);
        $htmlMimePart->setType('text/html');
        $mimeMessage = new MimeMessage();
        $mimeMessage->addPart($htmlMimePart);

        $message = $sm->get('user.email.message.invite');
        $message->setBody($mimeMessage);
        $message->addTo($user->getEmail());

        $mail = $sm->get('web.email.transport');
        $mail->send($message);
    }
    
    /**
     * Listen to the "api.del.success" event
     * @param EventInterface $event
     */
    public function delFailed(EventInterface $event)
    {
    }

    /* Upload Persona Headshot Image To S3
     * @param EventInterface $event
     */
    public function savePersonaHeadshot(EventInterface $event)
    {
        $persona     = $event->getParam('persona');
        $config   = $this->getServiceLocator()->get('Config');
        $awsSdk   = $this->getServiceLocator()->get(AwsSdk::class);
        $s3Config = $config['s3'];
        $s3Client = $awsSdk->createS3();

        try {
            $handle = fopen($persona->getHeadshot(), 'r');
            $s3Client->putObject(array(
                'Bucket'       => $s3Config['plan']['bucket'],
                'Key'          => basename($persona->getHeadshot()).'.jpg',
                'Body'         => $handle,
                'ACL'          => 'public-read',
                'CacheControl' => 'max-age=172800',
            ));
            fclose($handle);

            //delete files
            unlink($persona->getHeadshot());

        } catch (S3Exception $e) {
            // @todo log this exception
            $event->stopPropagation(true);
        }
    }

    /**
     * Update Persona Headshot Image
     * @param EventInterface $event
     */
    public function savePersonaHeadshotData(EventInterface $event)
    {
        $persona = $event->getParam('persona');
        $config      = $this->getServiceLocator()->get('Config');
        $s3Config    = $config['s3'];
        $s3Link      = $s3Config['url'].$s3Config['plan']['bucket'];
        $persona->setHeadshot($s3Link.'/'.basename($persona->getHeadshot()).'.jpg');
        $mapper = $this->getServiceLocator()->get('planbold.mapper.persona');
        try {
            $mapper->update($persona);
        } catch (\Exception $e) {
            $event->stopPropagation(true);
        }
    }
    
    /* Upload Image To S3
     * @param EventInterface $event
     */
    public function saveUserPhoto(EventInterface $event)
    {
        $user     = $event->getParam('user');
        $config   = $this->getServiceLocator()->get('Config');
        $awsSdk   = $this->getServiceLocator()->get(AwsSdk::class);
        $s3Config = $config['s3'];
        $s3Client = $awsSdk->createS3();
       
        try {
             $handle = fopen($user->getImagePath(), 'r');
             $s3Client->putObject(array(
                 'Bucket'       => $s3Config['plan']['bucket'],
                 'Key'          => basename($user->getImagePath()).'.jpg',
                 'Body'         => $handle,
                 'ACL'          => 'public-read',
                 'CacheControl' => 'max-age=172800',
             ));
             fclose($handle);

             //delete files
             unlink($user->getImagePath());
            
        } catch (S3Exception $e) {
            // @todo log this exception
            $event->stopPropagation(true);
        }
    }
    
    /* Upload Company Logo To S3
     * @param EventInterface $event
     */
    public function saveCompanyLogo(EventInterface $event)
    {
        $account  = $event->getParam('account');
        $config   = $this->getServiceLocator()->get('Config');
        $awsSdk   = $this->getServiceLocator()->get(AwsSdk::class);
        $s3Config = $config['s3'];
        $s3Client = $awsSdk->createS3();
        try {
             $handle = fopen($account->getLogo(), 'r');
             $s3Client->putObject(array(
                 'Bucket'       => $s3Config['plan']['bucket'],
                 'Key'          => basename($account->getLogo()).'.jpg',
                 'Body'         => $handle,
                 'ACL'          => 'public-read',
                 'CacheControl' => 'max-age=172800',
             ));
             fclose($handle);

             //delete files
             unlink($account->getLogo());
            
        } catch (S3Exception $e) {
            // @todo log this exception
            $event->stopPropagation(true);
        }
    }
    
    /**
     * Update User Image
     * @param EventInterface $event
     */
    public function saveUserPhotoData(EventInterface $event)
    {
        $user = $event->getParam('user');
        $config      = $this->getServiceLocator()->get('Config');
        $s3Config    = $config['s3'];
        $s3Link      = $s3Config['url'].$s3Config['plan']['bucket'];
        $user->setImagePath($s3Link.'/'.basename($user->getImagePath()).'.jpg');
        $mapper = $this->getServiceLocator()->get('planbold.mapper.user');
        try {
            $mapper->update($user);
        } catch (\Exception $e) {
            $event->stopPropagation(true);
        }
    }
    
    /**
     * Update Company Image
     * @param EventInterface $event
     */
    public function saveCompanyLogoData(EventInterface $event)
    {
        $account     = $event->getParam('account');
        $config      = $this->getServiceLocator()->get('Config');
        $s3Config    = $config['s3'];
        $s3Link      = $s3Config['url'].$s3Config['plan']['bucket'];
        $account->setLogo($s3Link.'/'.basename($account->getLogo()).'.jpg');
        $mapper = $this->getServiceLocator()->get('planbold.mapper.account');
        try {
            $mapper->update($account);
        } catch (\Exception $e) {
            var_dump($e->getMessage());exit;
            $event->stopPropagation(true);
        }
    }
    
    /**
     * Upload Persona Photos
     */
    public function uploadPersonaPhoto(EventInterface $event)
    {
        $persona  = $event->getParam('persona');
        $config   = $this->getServiceLocator()->get('Config');
        $awsSdk   = $this->getServiceLocator()->get(AwsSdk::class);
        $s3Config = $config['s3'];
        $s3Client = $awsSdk->createS3();
        
        $handle = fopen($persona['tmp_name'], 'r');
        $s3Client->putObject(array(
            'Bucket'       => $s3Config['plan']['bucket'],
            'Key'          => basename($persona['tmp_name']),
            'Body'         => $handle,
            'ACL'          => 'public-read',
            'CacheControl' => 'max-age=172800',
        ));
        fclose($handle);

        //delete files
        unlink($persona['tmp_name']);

    }
}

<?php
/**
 * User's Form
 * @author Oliver Pasigna <opasigna@insivia.com>
 */
namespace User\Form;
use Zend\Form\Form;

class AccountForm extends Form
{

    public function __construct($name)
    {
        parent::__construct($name);

        $this->setName($name);
        
        // CompanyName
        $this->add(array(
            'name' => 'company',
            'type' => 'Text',
            'attributes' => array(
                'id'          => 'company',
                'value'       => '',
                'placeholder' => 'username',
                'tabindex'    => '1'
            )
        ));
        
        //Description
        $this->add(array(
            'type'       => 'Text',
            'name'       => 'description',
            'attributes' => array(
                'maxlength'   => '2000',
                'tabindex'    => '2',
                'id'          => 'description',
                'placeholder' => 'Description'
            )

        ));
    }
}
<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace User\Form;

use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Form\Form;
use Planbold\Form\Element;

/**
 * Class for Upload Image Form
 *
 * @author Oliver Pasigna <opasigna@gmail.com>
 */
class UploadImage extends Form implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    
    public function __construct($name)
    {
        parent::__construct($name);
        
        $this->add(new Element\UserImage());
    }
}

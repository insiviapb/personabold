<?php

return array(
    'router' => array(
        'routes' => array(
            'signin' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/signin',
                    'defaults' => array(
                        'controller' => 'user.controller.user',
                        'action' => 'login',
                    ),
                ),
            ),
            'register' => array(
                'type' => 'Literal',
                'priority' => 1000,
                'options' => array(
                    'route' => '/register',
                    'defaults' => array(
                        'controller' => 'user.controller.user',
                        'action' => 'register',
                    ),
                ),
            ),
            'dashboard' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/dashboard',
                    'defaults' => array(
                        'controller' => 'user.controller.index',
                        'action' => 'dashboard',
                    ),
                ),
            ),
            'culture' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/culture',
                    'defaults' => array(
                        'controller' => 'user.controller.index',
                        'action' => 'culture',
                    ),
                ),
            ),
            'vision' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/vision',
                    'defaults' => array(
                        'controller' => 'user.controller.index',
                        'action' => 'vision',
                    ),
                ),
            ),
            'settings' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/settings',
                    'defaults' => array(
                        'controller' => 'user.controller.settings',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'alert' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/alert',
                            'defaults' => array(
                                'controller' => 'user.controller.settings',
                                'action' => 'alert',
                            ),
                        ),
                    ),
                    'subscription-info' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/subscription-info',
                            'defaults' => array(
                                'controller' => 'user.controller.settings',
                                'action' => 'subscriptionInfo',
                            ),
                        ),
                    ),
                    'theme' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/theme',
                            'defaults' => array(
                                'controller' => 'user.controller.settings',
                                'action' => 'theme',
                            ),
                        ),
                    ),
                    'user-profile' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/user-profile',
                            'defaults' => array(
                                'controller' => 'user.controller.settings',
                                'action' => 'usersProfile',
                            ),
                        ),
                    ),
                    'pending-past-payments' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/pending-past-payment',
                            'defaults' => array(
                                'controller' => 'user.controller.settings',
                                'action' => 'pendingPastPayment',
                            ),
                        ),
                    ),
                    'users' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/users',
                            'defaults' => array(
                                'controller' => 'user.controller.settings',
                                'action' => 'users',
                            ),
                        ),
                    ),
                    'industry' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/industry',
                            'defaults' => array(
                                'controller' => 'user.controller.settings',
                                'action' => 'industry',
                            ),
                        ),
                    ),
                    'persona-fields' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/persona-fields',
                            'defaults' => array(
                                'controller' => 'user.controller.settings',
                                'action' => 'personaFields'
                            )
                        ),
                    ),
                    'invoice' => array(
                        'type' => 'Segment',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/invoice[/:invoiceId]',
                            'defaults' => array(
                                'controller' => 'user.controller.settings',
                                'action' => 'invoice'
                            )
                        ),
                    ),
                    'account-files' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/account-files',
                            'defaults' => array(
                                'controller' => 'user.controller.settings',
                                'action' => 'accountFiles'
                            )
                        ),
                    ),
                    'persona-templates' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/persona-templates',
                            'defaults' => array(
                                'controller' => 'user.controller.settings',
                                'action' => 'personaTemplates'
                            )
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'guidelines' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route' => '/guidelines',
                                    'defaults' => array(
                                        'controller' => 'user.controller.settings',
                                        'action' => 'personaTemplateGuide',
                                    ),
                                ),
                            ),
                            'details' => array(
                                'type' => 'Segment',
                                'options' => array(
                                    'route' => '/details[/:uuid]',
                                    'defaults' => array(
                                        'controller' => 'user.controller.settings',
                                        'action' => 'templateDetails',
                                    ),
                                ),
                            ),
                            'process' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route' => '/process',
                                    'may_terminate' => true,
                                ),
                                'child_routes' => array(
                                    'templateDetails' => array(
                                        'type' => 'Literal',
                                        'options' => array(
                                            'route' => '/template-details',
                                            'defaults' => array(
                                                'controller' => 'user.controller.settings.process',
                                                'action' => 'processTemplateDetails',
                                            ),
                                        ),
                                    ),
                                    'publishedTemplate' => array(
                                        'type' => 'Literal',
                                        'options' => array(
                                            'route' => '/published-persona-template',
                                            'defaults' => array(
                                                'controller' => 'user.controller.settings.process',
                                                'action' => 'publishedPersonaTemplates',
                                            ),
                                        ),
                                    ),
                                    'previewTemplatePDF' => array(
                                        'type' => 'Segment',
                                        'options' => array(
                                            'route' => '/preview-persona-pdf[/:uuid]',
                                            'defaults' => array(
                                                'controller' => 'user.controller.settings.process',
                                                'action' => 'previewPersonaPDF',
                                            ),
                                        ),
                                    ),
                                    'previewTemplateHTML' => array(
                                        'type' => 'Segment',
                                        'options' => array(
                                            'route' => '/preview-persona-html[/:uuid]',
                                            'defaults' => array(
                                                'controller' => 'user.controller.settings.process',
                                                'action' => 'previewPersonaHTML',
                                            ),
                                        ),
                                    ),
                                ),
                            )
                        ),
                    ),
                    'process' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/process',
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'colors' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route'    => '/colors',
                                    'defaults' => array(
                                        'controller' => 'user.controller.settings.process',
                                        'action'     => 'colors',
                                    ),
                                ),
                            ),
                            'account' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route' => '/account',
                                    'defaults' => array(
                                        'controller' => 'user.controller.settings.process',
                                        'action' => 'account',
                                    ),
                                ),
                            ),
                            'industry' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/industry',
                                    'defaults' => array(
                                        'controller' => 'user.controller.settings.process',
                                        'action' => 'industry',
                                    ),
                                ),
                            ),
                            'users' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/users',
                                    'defaults' => array(
                                        'controller' => 'user.controller.settings.process',
                                        'action' => 'users',
                                    ),
                                ),
                            ),
                            'update-users' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/update-user',
                                    'defaults' => array(
                                        'controller' => 'user.controller.settings.process',
                                        'action' => 'updateUsers',
                                    ),
                                ),
                            ),
                            'delete-users' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/delete-user',
                                    'defaults' => array(
                                        'controller' => 'user.controller.settings.process',
                                        'action' => 'deleteUsers',
                                    ),
                                ),
                            ),
                            'user-profile' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/user-profile',
                                    'defaults' => array(
                                        'controller' => 'user.controller.settings.process',
                                        'action' => 'userProfile',
                                    ),
                                ),
                            ),
                            'upload-photo' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/upload-photo',
                                    'defaults' => array(
                                        'controller' => 'user.controller.settings.process',
                                        'action' => 'uploadPhoto',
                                    ),
                                ),
                            ),
                            'add-update-cards' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/add-update-cards',
                                    'defaults' => array(
                                        'controller' => 'user.controller.settings.process',
                                        'action' => 'addUpdateCards',
                                    ),
                                ),
                            ),
                            'add-update-billing' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/add-update-billing',
                                    'defaults' => array(
                                        'controller' => 'user.controller.settings.process',
                                        'action' => 'addUpdateBilling',
                                    ),
                                ),
                            ),
                            'download-invoice' => array(
                                'type' => 'Segment',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/download-invoice[/:invoiceId]',
                                    'defaults' => array(
                                        'controller' => 'user.controller.settings.process',
                                        'action' => 'downloadInvoice',
                                    ),
                                ),
                            ),
                            'update-subscription' => array(
                                'type' => 'Literal',
                                'priority' => 1000,
                                'options' => array(
                                    'route' => '/update-subscription',
                                    'defaults' => array(
                                        'controller' => 'user.controller.settings.process',
                                        'action' => 'updateSubscription',
                                    ),
                                ),
                            ),
                            'update-notification' => array(
                                'type'     => 'Literal',
                                'priority' => 1000,
                                'options'  => array(
                                    'route' => '/update-notification',
                                    'defaults' => array(
                                        'controller' => 'user.controller.settings.process',
                                        'action' => 'updateNotification',
                                    ),
                                )
                            ),
                            'persona-fields' => array(
                                'type'     => 'Literal',
                                'priority' => 1000,
                                'options'  => array(
                                    'route' => '/persona-fields',
                                    'defaults' => array(
                                        'controller' => 'user.controller.settings.process',
                                        'action' => 'personaFields',
                                    ),
                                )
                            )
                        ),
                    ),
                ),
            ),
            'user' => array(
                'type' => 'Literal',
                'type' => 'Literal',
                'priority' => 1000,
                'options' => array(
                    'route' => '/user',
                    'defaults' => array(
                        'controller' => 'user.controller.index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'updatePassword' => array(
                        'type' => 'Segment',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/update-password[/:uuid]',
                            'defaults' => array(
                                'controller' => 'user.controller.user',
                                'action' => 'updatePassword',
                            ),
                        ),
                    ),
                )
            )
        )
    ), // router
    'controllers' => array(
        'invokables' => array(
            'user.controller.index' => 'User\Controller\IndexController',
            'user.controller.settings' => 'User\Controller\SettingsController',
            'user.controller.settings.process' => 'User\Controller\SettingsProcessController'
        ),
        'factories' => array(
            'user.controller.user' => 'User\Controller\UserControllerFactory'
        ),
    ), // controllers
    'service_manager' => array(
        'factories' => array(
            'user.form.register.filter'   => 'User\Service\Factory\RegisterFormFilterFactory',
            'user.form.register'          => 'User\Service\Factory\RegisterFormFactory',
            'user.navigation.navbarleft'  => 'User\Service\Factory\NavBarLeftNavigationFactory',
            'user.navigation.navbarright' => 'User\Service\Factory\NavBarRightNavigationFactory',
            'user.navigation.settings'    => 'User\Service\Factory\SettingNavigationFactory',
        ),
        'invokables' => array(
            'user.service'                => 'User\\Service\\User',
            'theme.service'               => 'User\\Service\\Theme',
            'settings.service'            => 'User\\Service\\Settings',
            'file.service'                => 'User\\Service\\File',
            'user.service.listener.users' => 'User\\Service\\Listener\\UserEventListener',
            'mailchimp.service'           => 'User\\Service\\MailChimp'
        )
    ), // service_manager
    'view_helpers' => array(
        'invokables' => array(
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'template_map' => array(
            'layout/user' => __DIR__ . '/../view/layout/layout.phtml',
            'layout/share' => __DIR__ . '/../view/layout/share-layout.phtml',
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                __DIR__ . '/../asset',
            ),
        ),
    ),
    'navigation' => array(
        'user-navbar-left' => array(
            array(
                'label' => '<i class="fa fa-dashboard"></i> <span>&nbsp; Dashboard</span>',
                'route' => 'dashboard'
            ),
            array(
                'label' => '<i class="fa fa-users"></i><span> &nbsp; Personas</span>',
                'route' => 'audience'
            ),
        ),
        'user-navbar-right' => array(
            array(
                'label' => '<i class="fa fa-gear"></i><span> &nbsp; Settings</span>',
                'route' => 'settings'
            ),
            array(
                'label' => '<i class="fa fa-info"></i><span> &nbsp; Help</span>',
                'route' => 'help'
            ),
        ),
        'setting-navigations' => array(
            array(
                'label' => 'Company Profile',
                'uri'   => '#',
                'pages' => array(
                    array(
                        'label' => 'Overview',
                        'route' => 'settings'
                    ),
                    array(
                        'label' => "User's Profile",
                        'route' => 'settings/user-profile'
                    )
                ),
            ),
            array(
                'label' => 'Account',
                'uri' => '#',
                'pages' => array(
                    array(
                        'label' => 'Alerts',
                        'route' => 'settings/alert'
                    ),
                    array(
                        'label' => 'Subscription Info',
                        'route' => 'settings/subscription-info'
                    ),
                    array(
                        'label' => 'Pending &amp; Past Payments',
                        'route' => 'settings/pending-past-payments'
                    ),
                    array(
                        'label' => 'Users',
                        'route' => 'settings/users'
                    ),
                ),
            ),
            array(
                'label' => 'Admins Settings',
                'uri' => '#',
                'pages' => array(
                    array(
                        'label' => 'Persona Templates',
                        'route' => 'settings/persona-templates'
                    ),
                    array(
                        'label' => 'Persona Templates Creation Guidelines',
                        'route' => 'settings/persona-templates/guidelines'
                    ),
                    array(
                        'label' => 'Persona Fields',
                        'route' => 'settings/persona-fields'
                    )
                ),
            ),
            array(
                'label' => 'Settings',
                'uri' => '#',
                'pages' => array(
                    array(
                        'label' => 'Industries',
                        'route' => 'settings/industry'
                    ),
                ),
            ),
        )
    ),
    'upload' => array(
        'dir' => './module/User/asset/files',
        'allowed_extensions' => array('jpeg', 'jpg', 'png'),
        'invalid_extension' => 'Just receive jpeg, jpg, and png',
    ),
);

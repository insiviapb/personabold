<?php
return array(
    'doctrine' => array(
        'driver' => array(
            'planbold_db_driver' => array(
                'class' => 'Doctrine\\ORM\\Mapping\\Driver\\YamlDriver',
                'paths' => array(
                    0 => __DIR__ . '/entity',
                ),
                'cache' => 'array',
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Planbold\Entity' => 'planbold_db_driver',
                ),
            ),
        ),
        'eventmanager' => array(
            'orm_default' => array(
                'subscribers' => array(
                    'Gedmo\Timestampable\TimestampableListener',
                    'Gedmo\Sluggable\SluggableListener',
                    'Gedmo\SoftDeleteable\SoftDeleteableListener'
                ),
            ),
        ),
        'configuration' => array(
            'orm_default' => array(
                'filters' => array(
                    'soft-deleteable' => 'Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter'
                )
            )
        )
    ),
    'data-fixture' => array(
        'fixtures' => __DIR__ . '/../src/Planbold/Fixture'
    ),
    'zfcuser'  => array(
        'UserEntityClass' => 'Planbold\Entity\User',
        'EnableDefaultEntities' => false
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Planbold\Service\Factory\AbstractMapperFactory' // planbold.mapper.{mapperName}
        ),
        'invokables'  => array(
            'web.service.listener.eventListener' => 'Web\Service\Listener\EventListener',
        ),
        'factories' => array(
            //'lisyn.form.addThisFile' => 'Lisyn\Service\Factory\AddThisFileFormFactory',
        ),
    ),
    'hydrators' => array(
        'factories' => array(
            'planbold.entity.userHydrator'                    => 'Planbold\\Hydrator\\UserEntityFactory',
            'planbold.entity.userRoleHydrator'                => 'Planbold\\Hydrator\\UserRoleEntityFactory',
            'planbold.entity.packagesRoleHydrator'            => 'Planbold\\Hydrator\\PackagesEntityFactory',
            'planbold.entity.accountRoleHydrator'             => 'Planbold\\Hydrator\\AccountEntityFactory',
            'planbold.entity.authLogHydrator'                 => 'Planbold\\Hydrator\\AuthLogEntityFactory',
            'planbold.entity.industryHydrator'                => 'Planbold\\Hydrator\\IndustryEntityFactory',
            'planbold.entity.personaHydrator'                 => 'Planbold\\Hydrator\\PersonaEntityFactory',
            'planbold.entity.jobtypeHydrator'                 => 'Planbold\\Hydrator\\JobtypeEntityFactory',
            'planbold.entity.archetypeEntityFactory'          => 'Planbold\\Hydrator\\ArchetypeEntityFactory',
            'planbold.entity.globalChannelsEntityFactory'     => 'Planbold\\Hydrator\\GlobalChannelsEntityFactory',
            'planbold.entity.globalIndustryEntityFactory'     => 'Planbold\\Hydrator\\GlobalIndustryEntityFactory',
            'planbold.entity.colorWidgetEntityFactory'        => 'Planbold\\Hydrator\\ColorWidgetEntityFactory',
            'planbold.entity.stripeAccountEntityFactory'      => 'Planbold\\Hydrator\\StripeAccountEntityFactory',
            'planbold.entity.stripeCardsEntityFactory'        => 'Planbold\\Hydrator\\StripeCardsEntityFactory',
            'planbold.entity.stripeShippingEntityFactory'     => 'Planbold\\Hydrator\\StripeShippingEntityFactory',
            'planbold.entity.personaDataEntityFactory'        => 'Planbold\\Hydrator\\PersonaDataEntityFactory',
            'planbold.entity.personaFieldsEntityFactory'      => 'Planbold\\Hydrator\\PersonaFieldsEntityFactory',
            'planbold.entity.personaChannelsEntityFactory'    => 'Planbold\\Hydrator\\PersonaChannelsEntityFactory',
            'planbold.entity.motivationsEntityFactory'        => 'Planbold\\Hydrator\\MotivationsEntityFactory',
            'planbold.entity.personaMotivationsEntityFactory' => 'Planbold\\Hydrator\\PersonaMotivationsEntityFactory',
            'planbold.entity.locationsEntityFactory'          => 'Planbold\\Hydrator\\LocationsEntityFactory',
            'planbold.entity.personaKnowledgeEntityFactory'   => 'Planbold\\Hydrator\\PersonaKnowledgeEntityFactory',
            'planbold.entity.surveyEntityFactory'             => 'Planbold\\Hydrator\\SurveyEntityFactory',
            'planbold.entity.surveyRespondentEntityFactory'   => 'Planbold\\Hydrator\\SurveyRespondentEntityFactory',
            'planbold.entity.surveyRespondentDataEntityFactory'     => 'Planbold\\Hydrator\\SurveyRespondentDataEntityFactory',
            'planbold.entity.personaTemplatesEntityFactory'         => 'Planbold\\Hydrator\\PersonaTemplatesEntityFactory',
            'planbold.entity.personaTemplateBodyEntityFactory'      => 'Planbold\\Hydrator\\PersonaTemplateBodyEntityFactory',
            'planbold.entity.personaSectionsEntityFactory'          => 'Planbold\\Hydrator\\PersonaSectionsEntityFactory',
            'planbold.entity.subscribeAccountTemplateEntityFactory' => 'Planbold\\Hydrator\\SubscribeAccountTemplateEntityFactory',
            'planbold.entity.agencyClientEntityFactory'             => 'Planbold\\Hydrator\\AgencyClientEntityFactory',
            'planbold.entity.personaRevisionEntityFactory'          => 'Planbold\\Hydrator\\PersonaRevisionEntityFactory',
            'planbold.entity.surveyQuestionsEntityFactory'          => 'Planbold\\Hydrator\\SurveyQuestionsEntityFactory',
            'planbold.entity.accountFileEntityFactory'              => 'Planbold\\Hydrator\\AccountFileEntityFactory',
            'planbold.entity.superAdminEntityFactory'               => 'Planbold\\Hydrator\\SuperAdminEntityFactory',
            'planbold.entity.headshot'                              => 'Planbold\\Hydrator\\HeadshotEntityFactory',
            'planbold.entity.personaSharedEmailEntityFactory'       => 'Planbold\\Hydrator\\PersonaSharedEmailEntityFactory'
        ),
        'shared' => array(
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            //'elapsedTime'   => 'Lisyn\View\Helper\ElapsedTime',
        ),
    ),
);

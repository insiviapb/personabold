<?php
namespace Planbold;

use Doctrine\DBAL\Types\Type;
use Zend\Mvc\MvcEvent;

class Module
{
    const PLANBOLD_UUID_PATTERN = '([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})';

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * Bootstrap
     *
     * @param MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e)
    {
        $app = $e->getTarget();
        $service = $app->getServiceManager();

        // register UUID to doctrine
        Type::addType('uuid', 'Ramsey\Uuid\Doctrine\UuidType');
        $em = $service->get('Doctrine\\ORM\\EntityManager');
        $em->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('uuid', 'uuid');
        // enable soft-delete
        $filter = $em->getFilters()->enable("soft-deleteable");
    }
}

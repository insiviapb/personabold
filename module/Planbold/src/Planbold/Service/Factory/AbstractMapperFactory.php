<?php

namespace Planbold\Service\Factory;

use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AbstractMapperFactory implements AbstractFactoryInterface
{
    protected $mappers = [];

    const MAPPER_PREFIX = 'planbold.mapper';

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @param string $name
     * @param string $requestedName
     * @return boolean
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        return (strpos($name, self::MAPPER_PREFIX) === 0);
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @param string $name
     * @param string $requestedName
     * @return Container
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        $mapperName = str_replace(self::MAPPER_PREFIX . '.', '', $requestedName);

        if (isset($this->mappers[$mapperName])) {
            return $this->mappers[$mapperName];
        }

        $className = '\Planbold\Mapper\\' . ucfirst($mapperName);
        $mapper = new $className;
        $mapper->setEntityManager($serviceLocator->get('Doctrine\\ORM\\EntityManager'));
        $this->mappers[$mapperName] = $mapper;
        return $mapper;
    }
}

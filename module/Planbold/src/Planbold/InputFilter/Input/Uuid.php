<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Lisyn\InputFilter\Input;

use Zend\InputFilter\Input;
use Zend\Filter\StringTrim;
use Zend\Validator;
use Lisyn;

/**
 * Class for InputFilter Uuid
 *
 * @author Dolly Aswin <dolly.aswin@gmail.com>
 */
class Uuid extends Input
{
    public function __construct($name = 'uuid')
    {
        parent::__construct($name);
        
        $filterChain = $this->getFilterChain()
                            ->attach(new StringTrim());
        $uuidValidator = new Validator\Regex(array('pattern' => '/^' . Planbold\Module::PLANBOLD_UUID_PATTERN . '/'));
        $uuidValidator->setMessage('Uuid is invalid', Validator\Regex::INVALID);
        $validatorChain = $this->getValidatorChain()->attach($uuidValidator, true);
        $this->setRequired(true)
            ->setErrorMessage('Please entry UUID')
            ->setFilterChain($filterChain)
            ->setValidatorChain($validatorChain);
    }
}

<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Planbold\InputFilter\Input;

use Zend\InputFilter\Input;
use Zend\Filter;
use Zend\Validator;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

/**
 * Class for InputFilter UserImage
 * @author Oliver Pasigna <opasigna@gmail.com>
 */
class UserImage extends Input implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    
    public function __construct(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        parent::__construct('userImage');
        
        $config = $serviceLocator->get('Config');
        $uploadConfig = $config['upload'];
        if (isset($_FILES['userImageFile'])) {
            $file = $_FILES['userImageFile']['name'];
        } else {
            $file = $_FILES['userImage']['name'];
        }
        $randNum = rand(1000,10000);
        $renameUpload = new Filter\File\RenameUpload(array(
            'target' => $uploadConfig['dir'].'/'.$randNum.'_'.str_replace(' ','',$file),
            'randomize' => true,
        ));
        $filterChain = $this->getFilterChain()
                            ->attach($renameUpload);
        $extensionValidator = new Validator\File\Extension($uploadConfig['allowed_extensions']);
        $extensionValidator->setMessage(
            $uploadConfig['invalid_extension'],
            Validator\File\Extension::FALSE_EXTENSION
        );
        $validatorChain = $this->getValidatorChain()
                            ->attach($extensionValidator, true);
        $this->setRequired(true)
            ->setFilterChain($filterChain)
            ->setValidatorChain($validatorChain);
    }
}

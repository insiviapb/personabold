<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Planbold\InputFilter\Input;

use Zend\InputFilter\Input;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;

/**
 * Class for Dynamic InputFilter TextDesc
 *
 */
class TextDesc extends Input
{
    public function __construct($name = null, $details)
    {
        parent::__construct($name);
        
        $filterChain = $this->getFilterChain()
                        ->attach(new StringTrim())
                        ->attach(new StripTags());
        $this->setRequired(false)
            ->setErrorMessage("Please input {$details}.")
            ->setFilterChain($filterChain);
    }
}

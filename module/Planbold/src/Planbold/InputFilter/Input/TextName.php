<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Planbold\InputFilter\Input;

use Zend\InputFilter\Input;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;

/**
 * Class for InputFilter TextName
 *
 */
class TextName extends Input
{
    public function __construct($name = 'textName', $errorMessage = null)
    {
        parent::__construct($name);
        
        $filterChain = $this->getFilterChain()
                        ->attach(new StringTrim())
                        ->attach(new StripTags());
        $this->setRequired(true)
            ->setErrorMessage(!empty($errorMessage) ? $errorMessage : 'Please enter Text Name')
            ->setFilterChain($filterChain);
    }
}

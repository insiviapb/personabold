<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Planbold\InputFilter\Input;

use Zend\InputFilter\Input;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;

/**
 * Class for InputFilter LastName
 *
 */
class LastName extends Input
{
    public function __construct($name = 'lastName')
    {
        parent::__construct($name);
        
        $filterChain = $this->getFilterChain()
                        ->attach(new StringTrim())
                        ->attach(new StripTags());
        
        $this->setRequired(true)
            ->setErrorMessage('Please entry Last Name')
            ->setFilterChain($filterChain);
    }
}

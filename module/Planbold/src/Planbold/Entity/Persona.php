<?php
namespace Planbold\Entity;

/**
 *  Persona Entity
 *  @author Aleks Daloso <adaloso@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class Persona implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;

    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->uuid = \Ramsey\Uuid\Uuid::uuid4();
    }


    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Planbold\Entity\Jobtype
     */
    private $jobtype;

    /**
     * @var Planbold\Entity\Archetype
     */
    private $archetype;

    /**
     * @var Planbold\Entity\Industry
     */
    private $industry;

    /**
     * @var Planbold\Entity\User
     */
    private $user;

    /**
     * @var Planbold\Entity\Account
     */
    private $account;

    /**
     * @var string
     */
    private $jobTitle;

    /**
     * @var string
     */
    private $jobDescription;

    /**
     * @var string
     */
    private $headshot;

    /**
     * @var string
     */
    private $age;

    /**
     * @var string
     */
    private $gender;

    /**
     * @var string
     */
    private $location;

    /**
     * @var string
     */
    private $basicLife;

    /**
     * @var integer
     */
    private $isTemplate;
    
    /**
     * Short Code
     * @var type 
     */
    private $shortCode;
    
    /**
     * Public Flag
     * @var type 
     */
    private $publicFlag;
    
    /**
     * @var password 
     */
    private $password;
    
    /**
     * @var type 
     */
    private $archivedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get jobTitle
     *
     * @return string
     */
    public function getJobTitle() {
        return $this->jobTitle;
    }

    /**
     * Set jobDescription
     *
     * @param $jobDescription
     * @return $this
     */
    public function setJobDescription($jobDescription) {
        $this->jobDescription = $jobDescription;
        return $this;
    }

    /**
     * Get jobDescription
     *
     * @return string
     */
    public function getJobDescription() {
        return $this->jobDescription;
    }

    /**
     * Set jobTitle
     *
     * @param $jobTitle
     * @return $this
     */
    public function setJobTitle($jobTitle) {
        $this->jobTitle = $jobTitle;
        return $this;
    }

    /**
     * Get headshot
     *
     * @return string
     */
    public function getHeadshot() {
        return $this->headshot;
    }

    /**
     * Set headshot
     *
     * @param $headshot
     * @return $this
     */
    public function setHeadshot($headshot) {
        $this->headshot = $headshot;
        return $this;
    }

    /**
     * Get age
     *
     * @return string
     */
    public function getAge() {
        return $this->age;
    }

    /**
     * Set Age
     *
     * @param $age
     * @return $this
     */
    public function setAge($age) {
        $this->age = $age;
        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender() {
        return $this->gender;
    }

    /**
     * Set gender
     *
     * @param $gender
     * @return $this
     */
    public function setGender($gender) {
        $this->gender = $gender;
        return $this;
    }

    /**
     * Get Location
     *
     * @return string
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * Set location
     *
     * @param $location
     * @return $this
     */
    public function setLocation($location) {
        $this->location = $location;
        return $this;
    }

    /**
     * Get basicLife
     *
     * @return string
     */
    public function getBasicLife() {
        return $this->basicLife;
    }

    /**
     * Set basicLife
     *
     * @param $basicLife
     * @return $this
     */
    public function setBasicLife($basicLife) {
        $this->basicLife = $basicLife;
        return $this;
    }

    /**
     * Get isTemplate
     *
     * @return string
     */
    public function getIsTemplate() {
        return $this->isTemplate;
    }

    /**
     * Set isTemplate
     *
     * @param $isTemplate
     * @return $this
     */
    public function setIsTemplate($isTemplate) {
        $this->isTemplate = $isTemplate;
        return $this;
    }

    /**
     * Set User
     *
     * @param  \Planbold\Entity\User $user
     *
     * @return \Planbold\Entity\User
     */
    public function setUser(\Planbold\Entity\user $user)    {
        return $this->user = $user;
    }

    /**
     * Get User
     *
     * @return Planbold\Entiry\User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set Account
     * @return the $account
     */
    public function setAccount($account)    {
        return $this->account = $account;
    }

    /**
     * Get Account
     * @return $account
     */
    public function getAccount()    {
        return $this->account;
    }

    /**
     * Set Jobtype
     * @return the $jobtype
     */
    public function setJobtype($jobtype)    {
        return $this->jobtype = $jobtype;
    }

    /**
     * Get jobtype
     * @return $jobtype
     */
    public function getJobtype()    {
        return $this->jobtype;
    }

    /**
     * Set Archetype
     * @return the $archetype
     */
    public function setArchetype($archetype)    {
        return $this->archetype = $archetype;
    }

    /**
     * Get Archetype
     * @return $archetype
     */
    public function getArchetype()    {
        return $this->archetype;
    }

    /**
     * @param $industry
     * @return mixed
     */
    public function setIndustry($industry) {
        return $this->industry = $industry;
    }

    /**
     * @return Planbold\Entity\Industry
     */
    public function getIndustry() {
        return $this->industry;
    }
    
    /**
     * @param $archivedAt
     * @return mixed
     */
    public function setArchivedAt($archivedAt) {
        return $this->archivedAt = $archivedAt;
    }

    /**
     * @return $archivedAt
     */
    public function getArchivedAt() {
        return $this->archivedAt;
    }
    
    /**
     * @param $shortCode
     * @return mixed
     */
    public function setShortCode($shortCode) {
        return $this->shortCode = $shortCode;
    }

    /**
     * @return $shortCode
     */
    public function getShortCode() {
        return $this->shortCode;
    }
    
    /**
     * @param $publicFlag
     * @return mixed
     */
    public function setPublicFlag($publicFlag) {
        return $this->publicFlag = $publicFlag;
    }

    /**
     * @return $publicFlag
     */
    public function getPublicFlag() {
        return $this->publicFlag;
    }
    
    /**
     * @param $password
     * @return mixed
     */
    public function setPassword($password) {
        return $this->password = $password;
    }

    /**
     * @return $password
     */
    public function getPassword() {
        return $this->password;
    }
}
<?php
namespace Planbold\Entity;

/**
 *  Industry Entity
 *  @author Oliver Pasigna <opasigna@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class ColorWidget implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;

    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->uuid = \Ramsey\Uuid\Uuid::uuid4();
    }

    /**
     * @var integer
     */
    private $id;
    
    /**
     * @var type 
     */
    private $jsonData;
    
    /**
     * @var $user
     */
    private $account;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get json data
     * @return string
     */
    public function getJsonData()
    {
        return $this->jsonData;
    }

    /**
     * Set json_data
     * @param string $jsonData
     */
    public function setJsonData($jsonData)
    {
        $this->jsonData = $jsonData;
        return $this;
    }
    
    /**
     * Set Account
     * @return the $account
     */
    public function setAccount($account)    {
        return $this->account = $account;
    }

    /**
     * Get Account
     * @return $account
     */
    public function getAccount() {
        return $this->account;
    }
}
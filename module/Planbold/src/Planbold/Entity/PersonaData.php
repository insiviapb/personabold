<?php
namespace Planbold\Entity;

/**
 *  PersonaData Entity
 *  @author Aleks Daloso <adaloso@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class PersonaData implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;

    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->uuid = \Ramsey\Uuid\Uuid::uuid4();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Planbold\Entity\Persona
     */
    private $persona;

    /**
     * @var \Planbold\Entity\PersonaFields
     */
    private $personaFields;

    /**
     * @var string
     *
     * @ORM\Column(name="data_type", type="string", length=255, nullable=false)
     */
    private $dataType;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=false)
     */
    private $content;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     * @return mixed
     */
    public function setTitle($title)
    {
        return $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param $content
     * @return mixed
     */
    public function setContent($content)
    {
        return $this->content = $content;
    }

    /**
     * @return string
     */
    public function getDataType()
    {
        return $this->dataType;
    }

    /**
     * @param $dataType
     * @return mixed
     */
    public function setDataType($dataType)
    {
        return $this->dataType = $dataType;
    }

    /**
     * @return Persona
     */
    public function getPersona()
    {
        return $this->persona;
    }

    /**
     * @param \Planbold\Entity\Persona $persona
     * @return mixed
     */
    public function setPersona($persona)
    {
        return $this->persona = $persona;
    }

    /**
     * @param \Planbold\Entity\PersonaFields $personaFields
     * @return mixed
     */
    public function setPersonaFields($personaFields)
    {
        return $this->personaFields = $personaFields;
    }

    /**
     * @return \Planbold\Entity\PersonaFields
     */
    public function getPersonaFields()
    {
        return $this->personaFields;
    }
}


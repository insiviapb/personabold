<?php

namespace Planbold\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PublickeyOauth2
 *
 * @ORM\Table(name="PublicKey_OAuth2", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_7355AB8319EB6921", columns={"client_id"})})
 * @ORM\Entity
 */
class PublickeyOauth2
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="publicKey", type="text", nullable=true)
     */
    private $publickey;

    /**
     * @var string
     *
     * @ORM\Column(name="privateKey", type="text", nullable=true)
     */
    private $privatekey;

    /**
     * @var string
     *
     * @ORM\Column(name="encryptionAlgorithm", type="string", length=255, nullable=true)
     */
    private $encryptionalgorithm;

    /**
     * @var \Planbold\Entity\ClientOauth2
     *
     * @ORM\ManyToOne(targetEntity="Planbold\Entity\ClientOauth2")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;


}


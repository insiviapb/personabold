<?php

namespace Planbold\Entity;

/**
 * Interface of UuidAware
 *
 */
interface UuidAwareInterface
{
    public function setUuid(\Ramsey\Uuid\Uuid $uuid);
    
    public function getUuid();
}

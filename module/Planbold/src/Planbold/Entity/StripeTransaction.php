<?php

namespace Planbold\Entity;

use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

/**
 * StripeTransaction
 */
class StripeTransaction implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;
    
    use TimestampableTrait;
    
    use SoftDeleteableTrait;
    
    /**
     * @var integer
     */
    private $id;
    
    /**
     * @var \Planbold\Entity\Account 
     */
    private $account;

    /**
     * @var string
     */
    private $invoiceId;
    
    /**
     * @var string
     */
    private $subscriptionId;

    /**
     * @var string
     */
    private $chargeId;
    
    /**
     * @var string 
     */
    private $planId;
    
    /**
     * @var string 
     */
    private $receiptNumber;
    
    /**
     * @var string 
     */
    private $planName;
    
    /**
     * @var boolean 
     */
    private $closed;
    
    /**
     * @var boolean 
     */
    private $paid;
    
    /**
     * @var decimal 
     */
    private $currency;
    
    /**
     * @var decimal 
     */
    private $discount;
    
    /**
     * @var decimal 
     */
    private $startingBalance;
    
    /**
     * @var decimal 
     */
    private $endingBalance;
    
    /**
     * @var decimal 
     */
    private $amountDue;
    
    /**
     * @var decimal 
     */
    private $subTotal;
    
    /**
     * @var decimal 
     */
    private $total;
    
    /**
     * @var decimal 
     */
    private $taxPercent;
    
    /**
     * @var string
     */
    private $description;
    
    /**
     * @var string 
     */
    private $status;
    
    /**
     * @var periodStart datetime
     */
    private $periodStart;
    
    /**
     * @var periodEnd datetime
     */
    private $periodEnd;
    
    /**
     * @return the $id
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set account
     * @param $account
     * @return $this
     */
    public function setAccount($account)
    {
        $this->account = $account;
        return $this;
    }
    
    /**
     * Get account
     * @return $account
     */
    public function getAccount()
    {
        return $this->account;
    }
    
    /**
     * Set invoiceId
     * @param $invoiceId
     * @return $this
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;
        
        return $this;
    }
    
    /**
     * Get invoice id
     * @return $invoiceId
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }
    
    /**
     * Set subscriptionId
     * @param $subscriptionId
     * @return $this
     */
    public function setSubscriptionId($subscriptionId)
    {
        $this->subscriptionId = $subscriptionId;
        
        return $this;
    }
    
    /**
     * Get subscription id
     * @return $subscriptionId
     */
    public function getSubscriptionId()
    {
        return $this->subscriptionId;
    }
    
    /**
     * Set charge id
     * @param $chargeId
     * @return $this
     */
    public function setChargeId($chargeId)
    {
        $this->chargeId = $chargeId;
        
        return $this;
    }
    
    /**
     * Get charge id
     * @return $chargeId
     */
    public function getChargeId()
    {
        return $this->chargeId;
    }
    
    /**
     * Set receipt number
     * @param $receiptNumber
     * @return $this
     */
    public function setReceiptNumber($receiptNumber)
    {
        $this->receiptNumber = $receiptNumber;
        
        return $this;
    }
    
    /**
     * Get receipt number
     * @return $receiptNumber
     */
    public function getReceiptNumber()
    {
        return $this->receiptNumber;
    }
    
    /**
     * Set plan id
     * @param $planId
     * @return $this
     */
    public function setPlanId($planId)
    {
        $this->planId = $planId;
        
        return $this;
    }
    
    /**
     * Get plan id
     * @return $planId
     */
    public function getPlanId()
    {
        return $this->planId;
    }
    
    /**
     * Set plan name
     * @param $planName
     * @return $this
     */
    public function setPlanName($planName)
    {
        $this->planName = $planName;
        return $this;
    }
    
    /**
     * Get plan name
     * @return $planName
     */
    public function getPlanName()
    {
        return $this->planName;
    }
    
    /**
     * Set closed
     * @param $closed
     * @return $this
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;
        
        return $this;
    }
    
    /**
     * Get closed
     * @return $closed
     */
    public function getClosed()
    {
        return $this->closed;
    }
    
    /**
     * Set paid
     * @param $paid
     * @return $this
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
        
        return $this;
    }
    
    /**
     * Get paid
     * @return $paid
     */
    public function getPaid()
    {
        return $this->paid;
    }
    
    /**
     * Set currency
     * @param $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        
        return $this;
    }
    
    /**
     * Get currency
     * @return $currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    
    /**
     * Set discount
     * @param $discount
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
        
        return $this;
    }
    
    /**
     * Get discount
     * @return $discount
     */
    public function getDiscount()
    {
        return $this->discount;
    }
    
    /**
     * Set starting balance
     * @param $startingBalance
     * @return $this
     */
    public function setStartingBalance($startingBalance)
    {
        $this->startingBalance = $startingBalance;
        
        return $this;
    }
    
    /**
     * Get starting balance
     * @return #startingBalance
     */
    public function getStartingBalance()
    {
        return $this->startingBalance;
    }
    
    /**
     * Set ending balance
     * @param $endingBalance
     * @return $this
     */
    public function setEndingBalance($endingBalance)
    {
        $this->endingBalance = $endingBalance;
        
        return $this;
    }
    
    /**
     * Get ending balance
     * @return $endingBalance
     */
    public function getEndingBalance()
    {
        return $this->endingBalance;
    }
    
    /**
     * Set status
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        
        return $this;
    }
    
    /**
     * Get status
     * @return $status
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Set amount due
     * @param $amountDue
     * @return $this
     */
    public function setAmountDue($amountDue)
    {
        $this->amountDue = $amountDue;
        
        return $this;
    }
    
    /**
     * Get amount Due
     * @return $amountDue
     */
    public function getAmountDue()
    {
        return $this->amountDue;
    }
    
    /**
     * Set subTotal
     * @param $subtotal
     * @return $this
     */
    public function setSubTotal($subtotal)
    {
        $this->subTotal = $subtotal;
        
        return $this;
    }
    
    /**
     * Get subtotal
     * @return $subtotal
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }
    
    /**
     * Set total
     * @param $total
     * @return $this
     */
    public function setTotal($total)
    {
        $this->total = $total;
        
        return $this;
    }
    
    /**
     * Get total
     * @return $total
     */
    public function getTotal()
    {
        return $this->total;
    }
    
    /**
     * Set taxPercent
     * @param $taxPercent
     * @return $this
     */
    public function setTaxPercent($taxPercent)
    {
        $this->taxPercent = $taxPercent;
        
        return $this;
    }
    
    /**
     * Get taxPercent
     * @return $taxPercent
     */
    public function getTaxPercent()
    {
        return $this->taxPercent;
    }
    
    /**
     * Set description
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        
        return $this;
    }
    
    /**
     * Get description
     * @return $description
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Set periodStart
     * @param $periodStart
     * @return $this
     */
    public function setPeriodStart($periodStart)
    {
        $this->periodStart = $periodStart;
        
        return $this;
    }
    
    /**
     * Get periodStart
     * @return $periodStart
     */
    public function getPeriodStart()
    {
        return $this->periodStart;
    }
    
    /**
     * Set periodEnd
     * @param $periodEnd
     * @return $this
     */
    public function setPeriodEnd($periodEnd)
    {
        $this->periodEnd = $periodEnd;
        
        return $this;
    }
    
    /**
     * Get periodEnd
     * @return $periodEnd
     */
    public function getPeriodEnd()
    {
        return $this->periodEnd;
    }
    
    /**
     * @return DateTime|null $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
}
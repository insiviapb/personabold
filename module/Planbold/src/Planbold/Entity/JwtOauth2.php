<?php

namespace Planbold\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * JwtOauth2
 *
 * @ORM\Table(name="Jwt_OAuth2", indexes={@ORM\Index(name="IDX_F220BE7A19EB6921", columns={"client_id"})})
 * @ORM\Entity
 */
class JwtOauth2
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=true)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="publicKey", type="text", nullable=true)
     */
    private $publickey;

    /**
     * @var \Planbold\Entity\ClientOauth2
     *
     * @ORM\ManyToOne(targetEntity="Planbold\Entity\ClientOauth2")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;


}


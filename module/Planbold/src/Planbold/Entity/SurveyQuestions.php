<?php
namespace Planbold\Entity;

/**
 *  SurveyQuestions Entity
 *  @author Aleks Daloso <aleks.daloso@gmail.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class SurveyQuestions implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;

    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->uuid = \Ramsey\Uuid\Uuid::uuid4();
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var type 
     */
    private $description;
    


    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }
    
    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set Description
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

}
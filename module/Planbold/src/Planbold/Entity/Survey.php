<?php
namespace Planbold\Entity;

/**
 *  Survey Entity
 *  @author Aleks Daloso <adaloso@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class Survey implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;

    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->uuid = \Ramsey\Uuid\Uuid::uuid4();
    }


    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $questions;

    /**
     * @var Planbold\Entity\User
     */
    private $user;

    /**
     * @var Planbold\Entity\Persona
     */
    private $personaId;
    
    /**
     * Custom Titles
     * @var type 
     */
    private $customTitles;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getQuestions() {
        return $this->questions;
    }

    /**
     * @param $questions
     * @return $this
     */
    public function setQuestions($questions) {
        $this->questions = $questions;
        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set Message
     *
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Set User
     *
     * @param  \Planbold\Entity\User $user
     *
     * @return \Planbold\Entity\User
     */
    public function setUser(\Planbold\Entity\user $user)    {
        return $this->user = $user;
    }

    /**
     * Get User
     *
     * @return Planbold\Entiry\User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @param  $personaId
     * @return mixed
     */
    public function setPersona($personaId) {
        return $this->personaId = $personaId;
    }

    /**
     * @return $personaId
     */
    public function getPersona() {
        return $this->personaId;
    }
    
    /**
     * @param  $customTitles
     * @return mixed
     */
    public function setCustomTitles($customTitles) {
        return $this->customTitles = $customTitles;
    }

    /**
     * @return $customTitles
     */
    public function getCustomTitles() {
        return $this->customTitles;
    }
}
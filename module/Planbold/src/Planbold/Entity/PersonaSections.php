<?php
namespace Planbold\Entity;

/**
 *  PersonaSections Entity
 *  @author Aleks Daloso <adaloso@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class PersonaSections implements EntityInterface, Timestampable, SoftDeleteable
{

    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $sectionName;

    /**
     * @var string
     */
    private $columnName;

    /**
     * @var integer
     */
    private $position;

    /**
     * @var Persona
     */
    private $personaId;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get sectionName
     * @return string
     */
    public function getSectionName()
    {
        return $this->sectionName;
    }

    /**
     * Set value
     * @param string $sectionName
     */
    public function setSectionName($sectionName)
    {
        $this->sectionName = $sectionName;
        return $this;
    }

    /**
     * Get columnName
     * @return string
     */
    public function getColumnName()
    {
        return $this->columnName;
    }

    /**
     * Set columnName
     * @param $columnName
     * @return $this
     */
    public function setColumnName($columnName)
    {
        $this->columnName = $columnName;
        return $this;
    }

    /**
     * Get position
     * @return int position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set Position
     * @param $position
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * Get Persona
     * @return string
     */
    public function getPersonaId()
    {
        return $this->personaId;
    }

    /**
     * Set Persona
     * @param string $persona
     */
    public function setPersonaId($personaId)
    {
        $this->personaId = $personaId;
        return $this;
    }

}
<?php

namespace Planbold\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ScopeOauth2
 *
 * @ORM\Table(name="Scope_OAuth2")
 * @ORM\Entity
 */
class ScopeOauth2
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="scope", type="text", nullable=true)
     */
    private $scope;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDefault", type="boolean", nullable=true)
     */
    private $isdefault;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Planbold\Entity\AccesstokenOauth2", inversedBy="scope")
     * @ORM\JoinTable(name="accesstokentoscope_oauth2",
     *   joinColumns={
     *     @ORM\JoinColumn(name="scope_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="access_token_id", referencedColumnName="id")
     *   }
     * )
     */
    private $accessToken;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Planbold\Entity\AuthorizationcodeOauth2", inversedBy="scope")
     * @ORM\JoinTable(name="authorizationcodetoscope_oauth2",
     *   joinColumns={
     *     @ORM\JoinColumn(name="scope_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="authorization_code_id", referencedColumnName="id")
     *   }
     * )
     */
    private $authorizationCode;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Planbold\Entity\ClientOauth2", inversedBy="scope")
     * @ORM\JoinTable(name="clienttoscope_oauth2",
     *   joinColumns={
     *     @ORM\JoinColumn(name="scope_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     *   }
     * )
     */
    private $client;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Planbold\Entity\RefreshtokenOauth2", inversedBy="scope")
     * @ORM\JoinTable(name="refreshtokentoscope_oauth2",
     *   joinColumns={
     *     @ORM\JoinColumn(name="scope_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="refresh_token_id", referencedColumnName="id")
     *   }
     * )
     */
    private $refreshToken;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->accessToken = new \Doctrine\Common\Collections\ArrayCollection();
        $this->authorizationCode = new \Doctrine\Common\Collections\ArrayCollection();
        $this->client = new \Doctrine\Common\Collections\ArrayCollection();
        $this->refreshToken = new \Doctrine\Common\Collections\ArrayCollection();
    }

}


<?php

namespace Planbold\Entity;

use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

use ZfcUser\Entity\User as ZfcUserEntity;
use ZF\OAuth2\Doctrine\Entity\UserInterface as ZFOAuth2DoctrineUserInterface;


/**
 * Class User
 * @package Planbold\Entity
 */
class User extends ZfcUserEntity implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface, ZFOAuth2DoctrineUserInterface
{
    use UuidAwareTrait;

    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->uuid = \Ramsey\Uuid\Uuid::uuid4();
    }

    /**
     * @var Collection
     */
    private $client;

    /**
     * @var Collection
     */
    private $accessToken;

    /**
     * @var Collection
     */
    private $authorizationCode;

    /**
     * @var Collection
     */
    private $refreshToken;


    /**
     * @var string
     */
    private $firstName;

    /**
     * @var \Planbold\Entity\Account
     */
    private $account;


    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $activationCode;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="google_id", type="string", length=128, nullable=true)
     */
    private $googleId;

    /**
     * @var string
     *
     * @ORM\Column(name="linkedin_id", type="string", length=128, nullable=true)
     */
    private $linkedInId;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=256, nullable=true)
     */
    private $image;

    /**
     * @var \Planbold\Entity\UserRole
     */
    private $userRole;

    /**
     * @var \DateTime
     */
    private $signupAt;

    /**
     * @var \DateTime
     */
    private $activateAt;

    /**
     * @var Collection
     */
    private $authLogs;

    /**
     * @var persona
     */
    private $persona;

    /**
     * @var \DateTime
     */
    private $inactivateAt;
    
    /**
     * @var imagePath 
     */
    private $imagePath;
    
    /**
     * @var haveAlert 
     */
    private $haveAlert;
    
    /**
     * @var hours 
     */
    private $hours;
    
    /**
     * @var revision date will be use in sending notifications
     */
    private $revisionDate;
    
    /**
     * @return the $firstName
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return the $lastName
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * Get title
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get imagePath
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Set imagePath
     * @param string $imagePath
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;
        return $this;
    }

    /**
     * @return the $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $lastName
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return the $address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return the $activationCode
     */
    public function getActivationCode()
    {
        return $this->activationCode;
    }

    /**
     * @param string $activationCode
     */
    public function setActivationCode($activationCode)
    {
        $this->activationCode = $activationCode;
    }

    /**
     * Get GoogleId
     * @return type
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }

    /**
     * Set Google Id
     * @param type $googleId
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;
    }

    /**
     * Get GoogleId
     * @return type
     */
    public function getLinkedInId()
    {
        return $this->linkedInId;
    }

    /**
     * Set LinkedIn Id
     * @param type $linkedInId
     */
    public function setLinkedInId($linkedInId)
    {
        $this->linkedInId = $linkedInId;
    }
    
    /**
     * Get have Alert
     * @return type
     */
    public function getHaveAlert()
    {
        return $this->haveAlert;
    }

    /**
     * Set Have Alert
     * @param type $haveAlert
     */
    public function setHaveAlert($haveAlert)
    {
        $this->haveAlert = $haveAlert;
    }
    
    /**
     * Get hours
     * @return $hours
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Set Hours
     * @param type $hours
     */
    public function setHours($hours)
    {
        $this->hours = $hours;
    }
    
    /**
     * Get revision date
     * @return $revisionDate
     */
    public function getRevisionDate()
    {
        return $this->revisionDate;
    }

    /**
     * Set revision date
     * @param type $revisionDate
     */
    public function setRevisionDate($revisionDate)
    {
        $this->revisionDate = $revisionDate;
    }
    
    /**
     * Get Image
     * @return type
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set $image
     * @param type $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return \DateTime|null $signupAt
     */
    public function getSignupAt()
    {
        return $this->signupAt;
    }

    /**
     * @param DateTime $signupAt
     */
    public function setSignupAt(\DateTime $signupAt)
    {
        $this->signupAt = $signupAt;
    }

    /**
     * @return DateTime|null $activateAt
     */
    public function getActivateAt()
    {
        return $this->activateAt;
    }

    /**
     * @param DateTime $activateAt
     */
    public function setActivateAt(\DateTime $activateAt)
    {
        $this->activateAt = $activateAt;
    }
    /**
     * @return DateTime|null $inactivateAt
     */
    public function getInactivateAt()
    {
        return $this->inactivateAt;
    }

    /**
     * @param DateTime $inactivateAt
     */
    public function setInactivateAt(\DateTime $inactivateAt)
    {
        $this->inactivateAt = $inactivateAt;
    }

    /**
     * Set userRole
     *
     * @param Planbold\Entity\UserRole $userRole
     *
     * @return $userRole
     */
    public function setUserRole(UserRole $userRole)
    {
        $this->userRole = $userRole;

        return $this;
    }

    /**
     * Get userRole
     *
     * @return Planbold\Entity\UserRole
     */
    public function getUserRole()
    {
        return $this->userRole;
    }

    /**
     * Set account
     *
     * @param Planbold\Entity\Account $account
     *
     * @return $account
     */
    public function setAccount(Account $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return Planbold\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set persona
     * @return the $persona
     */
    public function setPersona($persona)    {
        return $this->persona = $persona;
    }

    /**
     * Get persona
     * @return $persona
     */
    public function getPersona()    {
        return $this->persona;
    }

    /**
     * @return Collection
     */
    public function getAuthLogs()
    {
        return $this->authLogs;
    }

    /**
     * @param Planbold\Entity\AuthLog $authLog
     *
     * @return Planbold\Entity\AuthLog
     */
    public function addAuthLog(AuthLog $authLog)
    {
        $this->authLogs[] = $authLog;
        $authLog->setUser($this);
    }

    /**
     * Set Exchange Array
     *
     * @param array $data
     * @return \Planbold\Entity\User
     */
    public function exchangeArray(array $data)
    {
        foreach ($data as $key => $value) {
            switch ($key) {
                case 'username':
                    $this->setUsername($value);
                    break;
                case 'password':
                    $this->setPassword($value);
                    break;
                case 'email':
                    $this->setEmail($value);
                    break;
                case 'displayName':
                    $this->setDisplayName($value);
                    break;
                case 'state':
                    $this->setState($value);
                    break;
                case 'googleId':
                    $this->setGoogleId($value);
                    break;
                case 'linkedInId':
                    $this->setLinkedInId($value);
                    break;
                case 'image':
                    $this->setImage($value);
                    break;
                default:
                    break;
            }
        }

        return $this;
    }

    /**
     * Get Array Copy
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return array(
            'id'          => $this->getId(),
            'username'    => $this->getUsername(),
            'password'    => $this->getPassword(),
            'email'       => $this->getEmail(),
            'state'       => $this->getState(),
            'displayName' => $this->getDisplayName(), // underscore formatting for openid
            'googleId'    => $this->getGoogleId(),
            'linkedInId'  => $this->getLinkedInId(),
            'image'       => $this->getImage()
        );
    }

    /**
     * @return Collection
     */
    public function getClient()
    {
        return $this->client;
    }

    public function getAccessToken()
    {
        return $this->accessToken;
    }

    public function getAuthorizationCode()
    {
        return $this->authorizationCode;
    }

    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

}


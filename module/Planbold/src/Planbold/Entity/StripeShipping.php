<?php

namespace Planbold\Entity;

use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

/**
 * StripeShipping
 */
class StripeShipping implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;
    
    use TimestampableTrait;
    
    use SoftDeleteableTrait;
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var Stripe Card Id
     */
    private $stripeCardId;
        
    /**
     * @var address1 
     */
    private $address1;
    
    /**
     * @var address2 
     */
    private $address2;
    
    /**
     * @var phone1 
     */
    private $phone1;
    
    /**
     * @var phone2 
     */
    private $phone2;
    
    /**
     * @var city 
     */
    private $city;
    
    /**
     * @var country 
     */
    private $country;
    
    /**
     * @var state 
     */
    private $state;
    
    /**
     * @var stripeAccount 
     */
    private $stripeAccount;
    
    /**
     * @var $email 
     */
    private $email;
    
    /**
     * @var $postalCode 
     */
    private $postalCode;
    
    /**
     * @var $company 
     */
    private $company;
    
    /**
     * @return the $id
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set stripe card id
     * @param $stripeCardId
     * @return $this
     */
    public function setStripeCardId($stripeCardId)
    {
        $this->stripeCardId = $stripeCardId;
        return $this;
    }
    
    /**
     * Get stripe card id
     * @return $stripeCardId
     */
    public function getStripeCardId()
    {
        return $this->stripeCardId;
    }
    
    /**
     * Set address1
     * @param $address1
     * @return $this
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
        return $this;
    }
    
    /**
     * Get address1
     * @return $address1
     */
    public function getAddress1()
    {
        return $this->address1;
    }
    
    /**
     * Set address2
     * @param $address2
     * @return $this
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
        return $this;
    }
    
    /**
     * Get address1
     * @return $address1
     */
    public function getAddress2()
    {
        return $this->address2;
    }
    
    /**
     * Set phone1
     * @param $phone1
     * @return $this
     */
    public function setPhone1($phone1)
    {
        $this->phone1 = $phone1;
        return $this;
    }
    
    /**
     * Get phone1
     * @return $phone1
     */
    public function getPhone1()
    {
        return $this->phone1;
    }
    
    /**
     * Set phone1
     * @param $phone2
     * @return $this
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;
        return $this;
    }
    
    /**
     * Get phone2
     * @return $phone2
     */
    public function getPhone2()
    {
        return $this->phone2;
    }
    
    /**
     * Set city
     * @param $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }
    
    /**
     * Get city
     * @return $city
     */
    public function getCity()
    {
        return $this->city;
    }
    
    /**
     * Set country
     * @param $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }
    
    /**
     * Get country
     * @return $country
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    /**
     * Set state
     * @param $state
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }
    
    /**
     * Get state
     * @return $state
     */
    public function getState()
    {
        return $this->state;
    }
    
    /**
     * Set stripeAccount
     * @param $stripeAccount
     * @return $this
     */
    public function setStripeAccount($stripeAccount)
    {
        $this->stripeAccount = $stripeAccount;
        return $this;
    }
    
    /**
     * Get stripeAccount
     * @return $stripeAccount
     */
    public function getStripeAccount()
    {
        return $this->stripeAccount;
    }
    
    /**
     * Set email
     * @param $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    
    /**
     * Get email
     * @return $email
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * Set company
     * @param $company
     * @return $this
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }
    
    /**
     * Get company
     * @return $company
     */
    public function getCompany()
    {
        return $this->company;
    }
    
    /**
     * Set postal code
     * @param $postalCode
     * @return $this
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }
    
    /**
     * Get postalCode
     * @return $postalCode
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }
    
    /**
     * @return DateTime|null $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
}
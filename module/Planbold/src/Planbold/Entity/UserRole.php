<?php

namespace Planbold\Entity;

/**
 *  UserRole Entity
 *  @author Aleks Daloso <adaloso@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class UserRole implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;

    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;
    
    /**
     * @var string
     */
    private $roleType;

    /**
     * @var Planbold\Entiry\User
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    
    /**
     * Get role type
     * @return string
     */
    public function getRoleType()
    {
        return $this->roleType;
    }

    /**
     * Set role type
     * @param string $roleType
     */
    public function setRoleType($roleType)
    {
        $this->roleType = $roleType;
        return $this;
    }

    /**
     * Set User
     *
     * @param  \Planbold\Entity\User $user
     *
     * @return \Planbold\Entity\User
     */
    public function setUser(\Planbold\Entity\User $user)    {
        return $this->user = $user;
    }

    /**
     * Get User
     *
     * @return \Planbold\Entity\User
     */
    public function getUser()    {
        return $this->user;
    }
}
<?php
/** 
 * Planbold Module
 * 
 * @link      https://bitbucket.org/insivia-lis/planbold
 * @copyright Copyright (c) 2015
 */

namespace Planbold\Entity;

/**
 * Trait for User 
 *
 */
trait UserAwareTrait
{
    /**
     * @var user
     */
    private $user;
    
    /**
     * Set user
     *
     * @param Planbold\Entity\User $user
     *
     * @return Feed
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    
        return $this;
    }
    
    /**
     * Get user
     *
     * @return Plandold\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}

<?php

namespace Planbold\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientOauth2
 *
 * @ORM\Table(name="Client_OAuth2", uniqueConstraints={@ORM\UniqueConstraint(name="idx_clientId_unique", columns={"clientId"})}, indexes={@ORM\Index(name="IDX_A66D48A8A76ED395", columns={"user_id"})})
 * @ORM\Entity
 */
class ClientOauth2
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="clientId", type="string", length=255, nullable=true)
     */
    private $clientid;

    /**
     * @var string
     *
     * @ORM\Column(name="secret", type="string", length=255, nullable=true)
     */
    private $secret;

    /**
     * @var string
     *
     * @ORM\Column(name="redirectUri", type="text", nullable=true)
     */
    private $redirecturi;

    /**
     * @var array
     *
     * @ORM\Column(name="grantType", type="array", nullable=true)
     */
    private $granttype;

    /**
     * @var \Planbold\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Planbold\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Planbold\Entity\ScopeOauth2", mappedBy="client")
     */
    private $scope;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->scope = new \Doctrine\Common\Collections\ArrayCollection();
    }

}


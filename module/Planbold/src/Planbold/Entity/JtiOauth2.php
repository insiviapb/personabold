<?php

namespace Planbold\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * JtiOauth2
 *
 * @ORM\Table(name="Jti_OAuth2", indexes={@ORM\Index(name="IDX_2C13A64519EB6921", columns={"client_id"})})
 * @ORM\Entity
 */
class JtiOauth2
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=true)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="audience", type="string", length=255, nullable=true)
     */
    private $audience;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expires", type="datetime", nullable=true)
     */
    private $expires;

    /**
     * @var string
     *
     * @ORM\Column(name="jti", type="text", nullable=true)
     */
    private $jti;

    /**
     * @var \Planbold\Entity\ClientOauth2
     *
     * @ORM\ManyToOne(targetEntity="Planbold\Entity\ClientOauth2")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;


}


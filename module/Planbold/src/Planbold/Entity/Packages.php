<?php
namespace Planbold\Entity;

/**
 *  Packages Entity
 *  @author Aleks Daloso <adaloso@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class Packages implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;

    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $amount;
    
    /**
     * @var integer 
     */
    private $userCount;

    /**
     * @var integer 
     */
    private $templateCount;
    
    /**
     * @var integer 
     */
    private $clientCount;
    
    /**
     * @var integer 
     */
    private $surveyCount;
    
    /**
     * @var string
     */
    private $description;

    /**
     * @var stripe plancode
     */
    private $stripePlanCode;
    
    /**
     * @var type 
     */
    private $planCode;
    
    /**
     * old plan code
     * @var type 
     */
    private $oldPlanCode;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * set description
     * @param $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get amount
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * set amount
     * @param $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }
    
     /**
     * Get user count
     * @return integer
     */
    public function getUserCount()
    {
        return $this->userCount;
    }

    /**
     * set user count
     * @param $userCount
     */
    public function setUserCount($userCount)
    {
        $this->userCount = $userCount;
        return $this;
    }

    /**
     * Get stripe plancode
     * @return string
     */
    public function getStripePlanCode()
    {
        return $this->stripePlanCode;
    }

    /**
     * Set stripe plancode
     * @param $stripePlanCode
     */
    public function setStripePlanCode($stripePlanCode)
    {
        $this->stripePlanCode = $stripePlanCode;
        return $this;
    }
    
    /**
     * Get stripe plancode
     * @return string
     */
    public function getPlanCode()
    {
        return $this->planCode;
    }

    /**
     * Set stripe plancode
     * @param $stripePlanCode
     */
    public function setPlanCode($planCode)
    {
        $this->planCode = $planCode;
        return $this;
    }
    
    /**
     * Get stripe old plan code
     * @return string
     */
    public function getOldPlanCode()
    {
        return $this->oldPlanCode;
    }

    /**
     * Set stripe old plan code
     * @param $stripeOldPlanCode
     */
    public function setOldPlanCode($planCode)
    {
        $this->oldPlanCode = $planCode;
        return $this;
    }
    
    /**
     * Get template count
     * @return integer
     */
    public function getTemplateCount()
    {
        return $this->templateCount;
    }

    /**
     * Set template count
     * @param $templateCount
     */
    public function setTemplateCount($templateCount)
    {
        $this->templateCount = $templateCount;
        return $this;
    }
    
    /**
     * Get client count
     * @return integer
     */
    public function getClientCount()
    {
        return $this->clientCount;
    }

    /**
     * Set client count
     * @param $clientCount
     */
    public function setClientCount($clientCount)
    {
        $this->clientCount = $clientCount;
        return $this;
    }
    
    /**
     * Get survey count
     * @return string
     */
    public function getSurveyCount()
    {
        return $this->surveyCount;
    }

    /**
     * Set survey count
     * @param $surveyCount
     */
    public function setSurveyCount($surveyCount)
    {
        $this->surveyCount = $surveyCount;
        return $this;
    }
}
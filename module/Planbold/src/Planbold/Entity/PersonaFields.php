<?php
namespace Planbold\Entity;

/**
 *  Persona Fields Entity
 *  @author Aleks Daloso <adaloso@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class PersonaFields implements EntityInterface, Timestampable, SoftDeleteable
{
    use TimestampableTrait;

    use SoftDeleteableTrait;


    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string 
     */
    private $fieldCode;
    
    /**
     * @var string 
     */
    private $type;
    
    /**
     * @var type 
     */
    private $description;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }
    
    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set Description
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
    
    /**
     * Get type
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    /**
     * Get persona field code
     * @return string
     */
    public function getFieldCode()
    {
        return $this->fieldCode;
    }

    /**
     * Set persona field code
     * @param string $fieldCode
     */
    public function setFieldCode($fieldCode)
    {
        $this->fieldCode = $fieldCode;
        return $this;
    }
}
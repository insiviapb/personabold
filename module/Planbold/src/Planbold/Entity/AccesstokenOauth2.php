<?php

namespace Planbold\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccesstokenOauth2
 *
 * @ORM\Table(name="AccessToken_OAuth2", indexes={@ORM\Index(name="IDX_C092BBF419EB6921", columns={"client_id"}), @ORM\Index(name="IDX_C092BBF4A76ED395", columns={"user_id"})})
 * @ORM\Entity
 */
class AccesstokenOauth2
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="accessToken", type="text", nullable=true)
     */
    private $accesstoken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expires", type="datetime", nullable=true)
     */
    private $expires;

    /**
     * @var \Planbold\Entity\ClientOauth2
     *
     * @ORM\ManyToOne(targetEntity="Planbold\Entity\ClientOauth2")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var \Planbold\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Planbold\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Planbold\Entity\ScopeOauth2", mappedBy="accessToken")
     */
    private $scope;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->scope = new \Doctrine\Common\Collections\ArrayCollection();
    }

}


<?php
/** 
 * Planbold Module
 * 
 * @link      https://bitbucket.org/insivia-lis/planbold
 * @copyright Copyright (c) 2015
 */

namespace Planbold\Entity;

/**
 * Trait for uuid 
 *
 */
trait UuidAwareTrait
{
    /**
     * @var uuid
     */
    private $uuid;
    
    
    /**
     * Set uuid
     *
     * @param uuid $uuid
     *
     * @return File
     */
    public function setUuid(\Ramsey\Uuid\Uuid $uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return uuid
     */
    public function getUuid()
    {
        return $this->uuid;
    }
}

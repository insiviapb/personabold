<?php
namespace Planbold\Entity;

/**
 *  Agency Client Entity
 *  @author Oliver Pasigna <opasigna@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class AgencyClient implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;

    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->uuid = \Ramsey\Uuid\Uuid::uuid4();
    }

    /**
     * @var integer
     */
    private $id;


    /**
     * @var integer 
     */
    private $clientId;
    
    /**
     * @var \Planbold\Entity\Account
     */
    private $account;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get client Id
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Set Client Id
     * @param string $clientId
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Set Account
     * @param \Planbold\Entity\Account $account
     * @return mixed
     */
    public function setAccount($account)
    {
        return $this->account = $account;
    }

    /**
     * Get Account
     * @return \Planbold\Entity\Account
     */
    public function getAccount() {
        return $this->account;
    }

}
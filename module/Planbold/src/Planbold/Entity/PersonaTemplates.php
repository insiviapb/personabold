<?php
namespace Planbold\Entity;
/**
 *  PersonaTemplate Entity
 *  @author Oliver Pasigna <opasigna@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class PersonaTemplates implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;
    
    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $templateName;

    /**
     * @var boolean 
     */
    private $defaultFlag;
    
    /**
     * @var TemplatePreviewPath
     */
    private $templatePreviewPath;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->uuid = \Ramsey\Uuid\Uuid::uuid4();
    }
    
    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get templateName
     * @return string
     */
    public function getTemplateName()
    {
        return $this->templateName;
    }

    /**
     * Set templateName
     * @param string $templateName
     */
    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;
        return $this;
    }
    
    /**
     * Get default flag
     * @return string
     */
    public function getDefaultFlag()
    {
        return $this->defaultFlag;
    }

    /**
     * Set default flag
     * @param boolean
     */
    public function setDefaultFlag($defaultFlag)
    {
        $this->defaultFlag = $defaultFlag;
        return $this;
    }
    
    /**
     * Get templatePreviewPath
     * @return string
     */
    public function getTemplatePreviewPath()
    {
        return $this->templatePreviewPath;
    }

    /**
     * Set templatePreviewPath
     * @param string $templatePreviewPath
     */
    public function setTemplatePreviewPath($templatePreviewPath)
    {
        $this->templatePreviewPath = $templatePreviewPath;
        return $this;
    }
}
<?php
namespace Planbold\Entity;

/**
 *  Account Industry Entity
 *  @author Aleks Daloso <adaloso@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class AccountIndustry implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;

    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->uuid = \Ramsey\Uuid\Uuid::uuid4();
    }

    /**
     * @var integer
     */
    private $id;


    /**
     * @var type 
     */
    private $description;
    
    /**
     * @var \Planbold\Entity\Account
     */
    private $account;

    /**
     * @var \Planbold\Entity\Industry
     */
    private $industry;

    /**
     * @var string
     */
    private $industryName;

    /**
     * Archived Date
     * @var type
     */
    private $archivedAt;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set Description
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Set Industry
     *
     * @param \Planbold\Entity\Industry $industry
     * @return mixed
     */
    public function setIndustry($industry)
    {
        return $this->industry = $industry;
    }

    /**
     * @return \Planbold\Entity\Industry
     */
    public  function getIndustry()
    {
        return $this->industry;
    }

    /**
     * Get industryName
     * @return string
     */
    public function getIndustryName()
    {
        return $this->industryName;
    }

    /**
     * Set industryName
     * @param string $name
     */
    public function setIndustryName($name)
    {
        $this->industryName = $name;
        return $this;
    }

    /**
     * Set Account
     *
     * @param \Planbold\Entity\Account $account
     * @return mixed
     */
    public function setAccount($account)
    {
        return $this->account = $account;
    }

    /**
     * Get Account
     * @return \Planbold\Entity\Account
     */
    public function getAccount() {
        return $this->account;
    }

    /**
     * Get archived At
     *
     * @return string
     */
    public function getArchivedAt()
    {
        return $this->archivedAt;
    }

    /**
     * Set ArchivedAt
     *
     * @param $archivedAt
     *
     * @return $this
     */
    public function setArchivedAt($archivedAt)
    {
        $this->archivedAt = $archivedAt;

        return $this;
    }
}
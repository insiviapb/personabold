<?php

namespace Planbold\Entity;

use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

/**
 * AuthLog
 */
class AuthLog implements EntityInterface, Timestampable, SoftDeleteable
{
    use UserAwareTrait;
    
    use TimestampableTrait;
    
    use SoftDeleteableTrait;
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $role;

    /**
     * @var \DateTime
     */
    private $loginTime;

    /**
     * @var \DateTime
     */
    private $logoutTime;

    /**
     * Construct
     */
    public function __construct()
    {
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Role
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Get loginTime
     * 
     * @return the $loginTime
     */
    public function getLoginTime()
    {
        return $this->loginTime;
    }
    
    /**
     * Set loginTime
     * 
     * @param DateTime $loginTime
     */
    public function setLoginTime($loginTime)
    {
        $this->loginTime = $loginTime;
        return $this;
    }
    
    /**
     * Get logoutTime
     * 
     * @return the $logoutTime
     */
    public function getLogoutTime()
    {
        return $this->logoutTime;
    }
    
    /**
     * Set logoutTime
     * 
     * @param DateTime $logoutTime
     */
    public function setLogoutTime($logoutTime)
    {
        $this->logoutTime = $logoutTime;
    }
}

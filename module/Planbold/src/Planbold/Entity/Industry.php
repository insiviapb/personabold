<?php
namespace Planbold\Entity;

/**
 *  Industry Entity
 *  @author Oliver Pasigna <opasigna@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class Industry implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;

    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->uuid = \Ramsey\Uuid\Uuid::uuid4();
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $industryName;

    /**
     * @var string 
     */
    private $phone;
    
    /**
     * @var type 
     */
    private $description;
    
    /**
     * @var $user
     */
    private $account;
    
    /**
     * @var parentIndustry 
     */
    private $parentIndustry;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get industryName
     * @return string
     */
    public function getIndustryName()
    {
        return $this->industryName;
    }

    /**
     * Set industryName
     * @param string $name
     */
    public function setIndustryName($name)
    {
        $this->industryName = $name;
        return $this;
    }
    
    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set Description
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
    
    /**
     * Get phone
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * Set Account
     * @return the $account
     */
    public function setAccount($account)    {
        return $this->account = $account;
    }

    /**
     * Get Account
     * @return $account
     */
    public function getAccount() {
        return $this->account;
    }
    
    /**
     * Set Parent Industry
     * @return the $parentIndustry
     */
    public function setParentIndustry($parentIndustry)    {
        return $this->parentIndustry = $parentIndustry;
    }

    /**
     * Get Parent Industry
     * @return $parentIndustry
     */
    public function getParentIndustry() {
        return $this->parentIndustry;
    }
}
<?php
namespace Planbold\Entity;
/**
 *  PersonaRevision Entity
 *  @author Oliver Pasigna <opasigna@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class PersonaRevision implements EntityInterface, Timestampable, SoftDeleteable
{
    use UuidAwareTrait;
    
    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * @var integer
     */
    private $id;
    
    /**
     * @var accountId 
     */
    private $accountId;
    
    /**
     * @var userId 
     */
    private $userId;
    
    /**
     * @var username 
     */
    private $username;

    /**
     * @var string
     */
    private $personaName;
    
    /**
     * @var personaId 
     */
    private $personaId;
    
    /**
     * @var revision details
     */
    private $revisionDetails;
    
    /**
     * @var content
     */
    private $content;
    
    /**
     * @var revisionId 
     */
    private $revisionId;
    
    /**
     * @var company 
     */
    private $company;
    
    /**
     * @var image 
     */
    private $image;
    
    /**
     * @var personaImage 
     */
    private $personaImage;
    
    /**
     * @var deleteFlag 
     */
    private $deleteFlag;
    
    /**
     * @var createFlag 
     */
    private $createFlag;
    
    /**
     * Construct
     */
    public function __construct()
    {
        $this->uuid = \Ramsey\Uuid\Uuid::uuid4();
    }
    
    
    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get Persona Id
     * @return string
     */
    public function getPersonaId()
    {
        return $this->personaId;
    }

    /**
     * Set personaId
     * @param string $personaId
     */
    public function setPersonaId($personaId)
    {
        $this->personaId = $personaId;
        return $this;
    }
    
    /**
     * Get Persona Name
     * @return string
     */
    public function getPersonaName()
    {
        return $this->personaName;
    }

    /**
     * Set Persona Name
     * @param string $personaName
     */
    public function setPersonaName($personaName)
    {
        $this->personaName = $personaName;
        return $this;
    }
    
    /**
     * Get revision details
     * @return string
     */
    public function getRevisionDetails()
    {
        return $this->revisionDetails;
    }

    /**
     * Set revision details
     * @param string $revisionDetails
     */
    public function setRevisionDetails($revisionDetails)
    {
        $this->revisionDetails = $revisionDetails;
        return $this;
    }
    
    /**
     * Get content
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }
    
    /**
     * Get revision id
     * @return string
     */
    public function getRevisionId()
    {
        return $this->revisionId;
    }

    /**
     * Set revisionId
     * @param string $revisionId
     */
    public function setRevisionId($revisionId)
    {
        $this->revisionId = $revisionId;
        return $this;
    }
    
    /**
     * Get user id
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set userId
     * @param string $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }
    
    /**
     * Get user name
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set username
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }
    
    /**
     * Get account id
     * @return string
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * Set account id
     * @param string $accountId
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
        return $this;
    }
    
    /**
     * Get company id
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set company
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }
    
    /**
     * Set User Image
     * @param type $userImage
     */
    public function setUserImage($userImage)
    {
        $this->image = $userImage;
    }
    
    /**
     * Get User Image
     * @param type $userImage
     * @return type
     */
    public function getUserImage()
    {
        return $this->image;
    }
    
    /**
     * Set Persona Image
     * @param type $personaImage
     */
    public function setPersonaImage($personaImage)
    {
        $this->personaImage = $personaImage;
    }
    
    /**
     * Get Persona Image
     * @param type $personaImage
     * @return type
     */
    public function getPersonaImage()
    {
        return $this->personaImage;
    }
    
    /**
     * Set Delete flag
     * @param type $deleteFlag
     */
    public function setDeleteFlag($deleteFlag)
    {
        $this->deleteFlag = $deleteFlag;
    }
    
    /**
     * Get Delete Flag
     * @param type $deleteFlag
     * @return type
     */
    public function getDeleteFlag()
    {
        return $this->deleteFlag;
    }
    
    /**
     * Set Create flag
     * @param type $createFlag
     */
    public function setCreateFlag($createFlag)
    {
        $this->createFlag = $createFlag;
    }
    
    /**
     * Get Create Flag
     * @param type $createFlag
     * @return type
     */
    public function getCreateFlag()
    {
        return $this->createFlag;
    }
}
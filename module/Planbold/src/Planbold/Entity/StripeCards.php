<?php

namespace Planbold\Entity;

use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

/**
 * StripeCard
 */
class StripeCards implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;
    
    use TimestampableTrait;
    
    use SoftDeleteableTrait;
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var card number
     */
    private $cardNumber;
    
    /**
     * @var card name
     */
    private $cardName;
    
    /**
     * @var expMonth 
     */
    private $expMonth;
    
    /**
     * @var expYear 
     */
    private $expYear;
    
    /**
     * @var cvc 
     */
    private $cvc;
    
    /**
     * @var stripeAccount
     */
    private $stripeAccount;
    
    /**
     *Stripe Card Id
     */
    private $stripeCardId;
    
    /**
     * @var brand
     */
    private $brand;
    
    /**
     * @return the $id
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set card number
     * @param $cardNumber
     * @return $this
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
        return $this;
    }
    
    /**
     * Get card number
     * @return $cardNumber
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }
    
    /**
     * Set card name
     * @param $cardName
     * @return $this
     */
    public function setCardName($cardName)
    {
        $this->cardName = $cardName;
        return $this;
    }
    
    /**
     * Get card name
     * @return $cardName
     */
    public function getCardName()
    {
        return $this->cardName;
    }
    
    /**
     * Set brand
     * @param $brand
     * @return $this
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }
    
    /**
     * Get brand
     * @return $brand
     */
    public function getBrand()
    {
        return $this->brand;
    }
    
    /**
     * Set expiry Month
     * @param $expMonth
     * @return $this
     */
    public function setExpiryMonth($expMonth)
    {
        $this->expMonth = $expMonth;
        return $this;
    }
    
    /**
     * Get expiry Month
     * @return $expMonth
     */
    public function getExpiryMonth()
    {
        return $this->expMonth;
    }
    
    /**
     * Set expiry Year
     * @param $expYear
     * @return $this
     */
    public function setExpiryYear($expYear)
    {
        $this->expYear = $expYear;
        return $this;
    }
    
    /**
     * Get expiry Year
     * @return $expYear
     */
    public function getExpiryYear()
    {
        return $this->expYear;
    }
    
    /**
     * Set cvc
     * @param $cvc
     * @return $this
     */
    public function setCvc($cvc)
    {
        $this->cvc = $cvc;
        return $this;
    }
    
    /**
     * Get cvc
     * @return $cvc
     */
    public function getCvc()
    {
        return $this->cvc;
    }
    
    /**
     * Set stripeAccount
     * @param $stripeAccount
     * @return $this
     */
    public function setStripeAccount($stripeAccount)
    {
        $this->stripeAccount = $stripeAccount;
        return $this;
    }
    
    /**
     * Get stripeAccount
     * @return $stripeAccount
     */
    public function getStripeAccount()
    {
        return $this->stripeAccount;
    }
    
    /**
     * Set stripeCardId
     * @param $stripeCardId
     * @return $this
     */
    public function setStripeCardId($stripeCardId)
    {
        $this->stripeCardId = $stripeCardId;
        return $this;
    }
    
    /**
     * Get stripeCardId
     * @return $stripeCardId
     */
    public function getStripeCardId()
    {
        return $this->stripeCardId;
    }
    
    /**
     * @return DateTime|null $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
}
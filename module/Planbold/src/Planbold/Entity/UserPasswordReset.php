<?php

namespace Planbold\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserPasswordReset
 *
 * @ORM\Table(name="user_password_reset", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_DA84AD0BA76ED395", columns={"user_id"})})
 * @ORM\Entity
 */
class UserPasswordReset
{
    /**
     * @var string
     *
     * @ORM\Column(name="request_key", type="string", length=32, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $requestKey;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="request_time", type="datetime", nullable=false)
     */
    private $requestTime;


}


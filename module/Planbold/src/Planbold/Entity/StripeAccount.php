<?php

namespace Planbold\Entity;

use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

/**
 * StripeAccount
 */
class StripeAccount implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;
    
    use TimestampableTrait;
    
    use SoftDeleteableTrait;
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var account id 
     */
    private $accountId;
    
    /**
     * @var email 
     */
    private $email;
    
    /**
     * @var $subscriptionId 
     */
    private $subscriptionId;
    
    /**
     * @var \Planbold\Entity\Account 
     */
    private $account;
    
    /**
     * @var \Lisyn\Entity\StripeCards 
     */
    private $stripeCards;
    
    /**
     * @var \Lisyn\Entity\StripePlanCode 
     */
    private $stripePlanCode;
    
    /**
     * @var Stripe Status 
     */
    private $stripeStatus;
    
    /**
     * @return the $id
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set subscriptionId
     * @param $subscriptionId
     * @return $this
     */
    public function setSubscriptionId($subscriptionId)
    {
        $this->subscriptionId = $subscriptionId;
        return $this;
    }
    
    /**
     * Get subscriptionId
     * @return $subscriptionId
     */
    public function getSubscriptionId()
    {
        return $this->subscriptionId;
    }
    
    /**
     * Set accountId
     * @param $accountId
     * @return $this
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
        return $this;
    }
    
    /**
     * Get accountId
     * @return $accountId
     */
    public function getAccountId()
    {
        return $this->accountId;
    }
    
    /**
     * Set email
     * @param $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    
    /**
     * Get email
     * @return $email
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * Set account
     * @param $account
     * @return $this
     */
    public function setAccount($account)
    {
        $this->account = $account;
        return $this;
    }
    
    /**
     * Get account
     * @return $publisher
     */
    public function getAccount()
    {
        return $this->account;
    }
    
    /**
     * Set stripeCards
     * @param $stripeCards
     * @return $this
     */
    public function setStripeCards($stripeCards)
    {
        $this->stripeCards = $stripeCards;
        return $this;
    }
    
    /**
     * Get stripeCards
     * @return $stripeCards
     */
    public function getStripeCards()
    {
        return $this->stripeCards;
    }
    
    /**
     * Set stripePlanCode
     * @param $stripePlanCode
     * @return $this
     */
    public function setStripePlanCode($stripePlanCode)
    {
        $this->stripePlanCode = $stripePlanCode;
        return $this;
    }
    
    /**
     * Get stripePlanCode
     * @return $stripePlanCode
     */
    public function getStripePlanCode()
    {
        return $this->stripePlanCode;
    }
    
    /**
     * Set stripeStatus
     * @param $stripeStatus
     * @return $this
     */
    public function setStripeStatus($stripeStatus)
    {
        $this->stripeStatus = $stripeStatus;
        return $this;
    }
    
    /**
     * Get stripeStatus
     * @return $stripeStatus
     */
    public function getStripeStatus()
    {
        return $this->stripeStatus;
    }
    
    /**
     * @return DateTime|null $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
}
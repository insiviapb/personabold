<?php
namespace Planbold\Entity;

/**
 *  Subscribe Account TEmplate Entity
 *  @author Oliver Pasigna <opasigna@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class SubscribeAccountTemplate implements EntityInterface, Timestampable, SoftDeleteable
{
    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->uuid = \Ramsey\Uuid\Uuid::uuid4();
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var Planbold\Mapper\Account
     */
    private $account;
    
    /**
     * @var templateUuid 
     */
    private $templateUuid;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get account
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set account
     * @param Planbold\Entity\Account $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
        return $this;
    }
    
    /**
     * Get templateUuid
     * @return string
     */
    public function getTemplateUuid()
    {
        return $this->templateUuid;
    }

    /**
     * Set template uuid
     * @param $templateUuid
     */
    public function setTemplateUuid($templateUuid)
    {
        $this->templateUuid = $templateUuid;
        return $this;
    }
}
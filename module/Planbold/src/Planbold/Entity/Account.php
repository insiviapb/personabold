<?php
namespace Planbold\Entity;

/**
 *  Account Entity
 *  @author Aleks Daloso <adaloso@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class Account implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;

    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->uuid = \Ramsey\Uuid\Uuid::uuid4();
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;
    
    /**
     * @var string 
     */
    private $logoPath;

    /**
     * @var integer
     */
    private $activateAt;

    /**
     * @var integer
     */
    private $inactivateAt;

    /**
     * @var $user
     */
    private $user;

    /**
     * @var industry 
     */
    private $industry;

    /**
     * @var persona
     */
    private $persona;

    /**
     * @var description 
     */
    private $description;
    
    /**
     * @var agencyClient 
     */
    private $agencyClient;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    
    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
    
    /**
     * Get logo
     * @return string
     */
    public function getLogo()
    {
        return $this->logoPath;
    }

    /**
     * Set logoPath
     * @param string $logoPath
     */
    public function setLogo($logoPath)
    {
        $this->logoPath = $logoPath;
        return $this;
    }

    /**
     * Get activateAt
     * @return integer
     */
    public function getactivateAt()
    {
        return $this->activateAt;
    }

    /**
     * Set activateAt
     * @param string activateAt
     */
    public function setActivateAt($activateAt)
    {
        $this->activateAt = $activateAt;
        return $this;
    }

    /**
     * Get inactivateAt
     * @return integer
     */
    public function getInactivateAt()
    {
        return $this->cancelAt;
    }

    /**
     * Set inactivateAt
     * @param string inactivateAt
     */
    public function setInactivateAt($inactivateAt)
    {
        $this->inactivateAt = $inactivateAt;
        return $this;
    }

    /**
     * Set User
     * @return the $user
     */
    public function setUser($user)    {
        return $this->user = $user;
    }

    /**
     * Get User
     * @return $user
     */
    public function getUser()    {
        return $this->user;
    }
    
     /**
     * Set Industry
     * @return the $industry
     */
    public function setIndustry($industry)    {
        return $this->industry = $industry;
    }

    /**
     * Get Industry
     * @return $industry
     */
    public function getIndustry()    {
        return $this->industry;
    }

    /**
     * Set persona
     * @return the $persona
     */
    public function setPersona($persona)    {
        return $this->persona = $persona;
    }

    /**
     * Get Persona
     * @return $persona
     */
    public function getPersona()    {
        return $this->persona;
    }
    
    /**
     * Get agency client
     * @return string
     */
    public function getAgencyClient()
    {
        return $this->agencyClient;
    }

    /**
     * Set agency client
     * @param \Planbold\Entity\AgencyClient
     */
    public function setAgencyClient($agencyClient)
    {
        $this->agencyClient = $agencyClient;
        return $this;
    }
}
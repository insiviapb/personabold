<?php
namespace Planbold\Entity;

/**
 *  SurveyRespondent Entity
 *  @author Aleks Daloso <adaloso@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class SurveyRespondent implements EntityInterface, Timestampable, SoftDeleteable, UuidAwareInterface
{
    use UuidAwareTrait;

    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->uuid = \Ramsey\Uuid\Uuid::uuid4();
    }


    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var Planbold\Entity\Jobtype
     */
    private $jobtype;

    /**
     * @var Planbold\Entity\Archetype
     */
    private $archetype;

    /**
     * @var Planbold\Entity\Industry
     */
    private $industry;

    /**
     * @var Planbold\Entity\Survey
     */
    private $survey;

    /**
     * @var string
     */
    private $jobTitle;

    /**
     * @var string
     */
    private $jobDescription;


    /**
     * @var string
     */
    private $age;

    /**
     * @var bool
     */
    private $responded;

    /**
     * @var string
     */
    private $gender;

    /**
     * @var string
     */
    private $location;

    /**
     * @var string
     */
    private $basicLife;

    /**
     * @var integer
     */
    private $isTemplate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return bool
     */
    public function getResponded()
    {
        return $this->responded;
    }

    /**
     * @param $responded
     * @return $this
     */
    public function setResponded($responded)
    {
        $this->responded = $responded;
        return $this;
    }
    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get jobTitle
     *
     * @return string
     */
    public function getJobTitle() {
        return $this->jobTitle;
    }

    /**
     * Set jobDescription
     *
     * @param $jobDescription
     * @return $this
     */
    public function setJobDescription($jobDescription) {
        $this->jobDescription = $jobDescription;
        return $this;
    }

    /**
     * Get jobDescription
     *
     * @return string
     */
    public function getJobDescription() {
        return $this->jobDescription;
    }

    /**
     * Set jobTitle
     *
     * @param $jobTitle
     * @return $this
     */
    public function setJobTitle($jobTitle) {
        $this->jobTitle = $jobTitle;
        return $this;
    }

    /**
     * Get age
     *
     * @return string
     */
    public function getAge() {
        return $this->age;
    }

    /**
     * Set Age
     *
     * @param $age
     * @return $this
     */
    public function setAge($age) {
        $this->age = $age;
        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender() {
        return $this->gender;
    }

    /**
     * Set gender
     *
     * @param $gender
     * @return $this
     */
    public function setGender($gender) {
        $this->gender = $gender;
        return $this;
    }

    /**
     * Get Location
     *
     * @return string
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * Set location
     *
     * @param $location
     * @return $this
     */
    public function setLocation($location) {
        $this->location = $location;
        return $this;
    }

    /**
     * Get basicLife
     *
     * @return string
     */
    public function getBasicLife() {
        return $this->basicLife;
    }

    /**
     * Set basicLife
     *
     * @param $basicLife
     * @return $this
     */
    public function setBasicLife($basicLife) {
        $this->basicLife = $basicLife;
        return $this;
    }

    /**
     * Get isTemplate
     *
     * @return string
     */
    public function getIsTemplate() {
        return $this->isTemplate;
    }

    /**
     * Set isTemplate
     *
     * @param $isTemplate
     * @return $this
     */
    public function setIsTemplate($isTemplate) {
        $this->isTemplate = $isTemplate;
        return $this;
    }

    /**
     * Set Jobtype
     * @return the $jobtype
     */
    public function setJobtype($jobtype)    {
        return $this->jobtype = $jobtype;
    }

    /**
     * Get jobtype
     * @return $jobtype
     */
    public function getJobtype()    {
        return $this->jobtype;
    }

    /**
     * Set Archetype
     * @return the $archetype
     */
    public function setArchetype($archetype)    {
        return $this->archetype = $archetype;
    }

    /**
     * Get Archetype
     * @return $archetype
     */
    public function getArchetype()    {
        return $this->archetype;
    }

    /**
     * @param $industry
     * @return mixed
     */
    public function setIndustry($industry) {
        return $this->industry = $industry;
    }

    /**
     * @return Planbold\Entity\Industry
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * @param $survey
     * @return mixed
     */
    public function setSurvey($survey) {
        return $this->survey = $survey;
    }

    /**
     * @return Planbold\Entity\Survey
     */
    public function getSurvey()
    {
        return $this->survey;
    }
}
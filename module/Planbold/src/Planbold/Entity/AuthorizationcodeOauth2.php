<?php

namespace Planbold\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AuthorizationcodeOauth2
 *
 * @ORM\Table(name="AuthorizationCode_OAuth2", indexes={@ORM\Index(name="IDX_7DED2FDD19EB6921", columns={"client_id"}), @ORM\Index(name="IDX_7DED2FDDA76ED395", columns={"user_id"})})
 * @ORM\Entity
 */
class AuthorizationcodeOauth2
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="authorizationCode", type="string", length=255, nullable=true)
     */
    private $authorizationcode;

    /**
     * @var string
     *
     * @ORM\Column(name="redirectUri", type="text", nullable=true)
     */
    private $redirecturi;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expires", type="datetime", nullable=true)
     */
    private $expires;

    /**
     * @var string
     *
     * @ORM\Column(name="idToken", type="text", nullable=true)
     */
    private $idtoken;

    /**
     * @var \Planbold\Entity\ClientOauth2
     *
     * @ORM\ManyToOne(targetEntity="Planbold\Entity\ClientOauth2")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var \Planbold\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Planbold\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Planbold\Entity\ScopeOauth2", mappedBy="authorizationCode")
     */
    private $scope;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->scope = new \Doctrine\Common\Collections\ArrayCollection();
    }

}


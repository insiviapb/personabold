<?php
namespace Planbold\Entity;

/**
 *  PersonaSharedEmail Entity
 *  @author Bill Richards <brichards@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;

class PersonaSharedEmail implements EntityInterface, Timestampable, UuidAwareInterface
{
    use UuidAwareTrait;

    use TimestampableTrait;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->uuid = \Ramsey\Uuid\Uuid::uuid4();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Planbold\Entity\Persona
     */
    private $persona;

    /**
     * @var \Planbold\Entity\PersonaFields
     */
    private $emails;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * @param $emails
     * @return mixed
     */
    public function setEmails($emails)
    {
        return $this->emails = $emails;
    }
    
    /**
     * @return $persona
     */
    public function getPersona()
    {
        return $this->persona;
    }

    /**
     * @param $persona
     * @return mixed
     */
    public function setPersona($persona)
    {
        return $this->persona = $persona;
    }
}


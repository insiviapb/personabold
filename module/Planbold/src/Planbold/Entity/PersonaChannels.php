<?php
namespace Planbold\Entity;

/**
 *  PersonaChannels Entity
 *  @author Aleks Daloso <adaloso@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class PersonaChannels implements EntityInterface, Timestampable, SoftDeleteable
{

    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $value;

    /**
     * @var Persona
     */
    private $personaId;

    /**
     * @var GlobalChannels
     */
    private $globalChannels;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get value
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set value
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    
    /**
     * Get personaId
     * @return string
     */
    public function getPersona()
    {
        return $this->personaId;
    }

    /**
     * Set personaId
     * @param string $personaId
     */
    public function setPersona($personaId)
    {
        $this->personaId = $personaId;
        return $this;
    }
    
    public function getGlobalChannels()
    {
        return $this->globalChannels;
    }

    public function setGlobalChannels($globalChannels)
    {
        $this->globalChannels = $globalChannels;
        return $this;
    }
}
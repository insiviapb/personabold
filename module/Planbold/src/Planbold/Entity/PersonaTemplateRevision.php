<?php
namespace Planbold\Entity;
/**
 *  PersonaTemplateRevision Entity
 *  @author Oliver Pasigna <opasigna@insivia.com>
 */

Use Gedmo\Timestampable\Timestampable;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Gedmo\Timestampable\Traits\Timestampable as TimestampableTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable as SoftDeleteableTrait;

class PersonaTemplateRevision implements EntityInterface, Timestampable, SoftDeleteable
{
    use UuidAwareTrait;
    
    use TimestampableTrait;

    use SoftDeleteableTrait;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $templateName;

    /**
     * @var template body
     */
    private $templateBody;
    
    /**
     * @var templateUuid 
     */
    private $templateUuid;
    
    /**
     * @var published flag 
     */
    private $publishedFlag;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->uuid = \Ramsey\Uuid\Uuid::uuid4();
    }
    
    
    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get templateUuid
     * @return string
     */
    public function getTemplateUuid()
    {
        return $this->templateUuid;
    }

    /**
     * Set templateUuid
     * @param string $templateUuid
     */
    public function setTemplateUuid($templateUuid)
    {
        $this->templateUuid = $templateUuid;
        return $this;
    }
    
    /**
     * Get templateName
     * @return string
     */
    public function getTemplateName()
    {
        return $this->templateName;
    }

    /**
     * Set templateName
     * @param string $templateName
     */
    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;
        return $this;
    }
    
    /**
     * Get template body
     * @return string
     */
    public function getTemplateBody()
    {
        return $this->templateBody;
    }

    /**
     * Set template body
     * @param string $templateBody
     */
    public function setTemplateBody($templateBody)
    {
        $this->templateBody = $templateBody;
        return $this;
    }
    
    /**
     * Get published flag
     * @return string
     */
    public function getPublishedFlag()
    {
        return $this->publishedFlag;
    }

    /**
     * Set published flag
     * @param string $publishedFlag
     */
    public function setPublishedFlag($publishedFlag)
    {
        $this->publishedFlag = $publishedFlag;
        return $this;
    }
}
<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Planbold\Form\Element;

use Zend\Form\Element\Text;

/**
 * Class for Text Element TextName
 *
 */
class TextName extends Text
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct($name);
        $this->setName($name)
            ->setOption('label', 'Text Name');
        
        foreach($options as $key => $value) {
            $this->setAttribute($key, $value);
        }
        
    }
}

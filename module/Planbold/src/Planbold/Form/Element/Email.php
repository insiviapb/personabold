<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Planbold\Form\Element;

use Zend\Form\Element\Email as FormElementEmail;

/**
 * Class for Text Element lastName
 *
 */
class Email extends FormElementEmail
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct();
        $this->setName('email')
            ->setOption('label', 'Email')
            ->setAttribute('placeholder', 'Email');
    }
}

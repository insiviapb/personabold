<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Planbold\Form\Element;

use Zend\Form\Element\Radio;

/**
 * Class for Dynamic Radio
 *
 */
class RadioDesc extends Radio
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct($name);
        $this->setName($name)
             ->setOption('label', 'RadioButton');
    }
}

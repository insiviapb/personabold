<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Planbold\Form\Element;

use Zend\Form\Element\Hidden as FormElementHidden;

/**
 * Class for Text Element lastName
 *
 */
class HiddenUuid extends FormElementHidden
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct();
        $this->setName('uuid');
    }
}

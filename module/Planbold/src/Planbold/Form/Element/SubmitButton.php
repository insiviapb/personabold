<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Planbold\Form\Element;

use Zend\Form\Element\Button;

/**
 * Class for Button Element Submit
 *
 */
class SubmitButton extends Button
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct();
        $this->setName('submit')
            ->setAttribute('type', 'submit');
    }
}

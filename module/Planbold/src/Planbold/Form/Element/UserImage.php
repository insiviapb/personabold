<?php
namespace Planbold\Form\Element;

use Zend\Form\Element\File as FormElementFile;

/**
 * Class for Input Element User Images
 *
 * @author Oliver Pasigna <opasigna@insivia.com>
 */
class UserImage extends FormElementFile
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct();
        $this->setName('userImage')
            ->setOption('label', 'UserImage')
            ->setAttribute('placeholder', 'User Image');
    }
}

<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Planbold\Form\Element;

use Zend\Form\Element\Text;

/**
 * Class for Text Element lastName
 *
 */
class LastName extends Text
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct();
        $this->setName('lastName')
            ->setOption('label', 'Last Name')
            ->setAttribute('placeholder', 'Last Name');
    }
}

<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Planbold\Form\Element;

use Zend\Form\Element\MultiCheckbox;

/**
 * Class for Dynamic MultiCheckbox
 *
 * @author Aleks Daloso <aleks.daloso@gmail.com>
 */
class MultiCheckBoxDesc extends MultiCheckbox
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct($name);
        $this->setValueOptions($options)
            ->setName($name)
            ->setValue(true)
            ->setAttribute('class', $name);
    }
}

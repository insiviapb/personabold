<?php

namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class StripeShipping Mapper with Doctrine support
 * @package Planbold\Mapper
 */
class StripeShipping extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Fetch All StripeAccount
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('ss');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('ss.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('ss.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('ss.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'stripe-shipping-list');
        return $query;
    }

    /**
     * Set Hydrator
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Hydrator
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\StripeShipping');
    }

    /**
     * Find one by array of data
     * @param type $arrData
     * @return type
     */
    public function findOneBy($arrData)
    {
        return $this->getEntityRepository()->findOneBy($arrData);
    }
    
    /**
     * Fetch Shipping
     * @param $stripeAccount
     * @return type
     */
    public function fetchShipping($stripeAccount)
    {
             
        $qb = $this->getEntityRepository()->createQueryBuilder('ss');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->eq('ss.stripeAccount', ':stripeAccount'));
        $qb->setParameter('stripeAccount', $stripeAccount);
        $qb->where($andX);
        $qb->orderBy('ss.id', 'ASC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'stripe-shipping-list');
        return $query->getResult();
    }
}
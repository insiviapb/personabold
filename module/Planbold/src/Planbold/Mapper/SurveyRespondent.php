<?php

namespace Planbold\Mapper;

use Herrera\Json\Exception\Exception;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class SurveyRespondent mapper with Doctrine support
 * @package Planbold\Mapper
 */
class SurveyRespondent extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Fetch Reviews with pagination
     *
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('sr');
        $qb->select('
            sr.id,
            sr.name, 
            sr.jobTitle, 
            sr.email,
            sr.jobDescription, 
            sr.age, 
            sr.gender, 
            sr.location, 
            sr.basicLife,
            sr.responded, 
            sr.uuid, 
            arc.id as archetypeId,
            arc.title as archetype,
            ind.id as industryId,
            ind.industryName as industry');
        $qb->leftJoin('Planbold\Entity\Archetype', 'arc', 'WITH', 'arc.id = sr.archetype');
        $qb->leftJoin('Planbold\Entity\Industry', 'ind', 'WITH', 'ind.id = sr.industry');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('sr.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('sr.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('sr.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'survey-respondent-list');
        return $query;
    }

    public function fetchIndustryGroupBy(array $respondents = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('sr');
        $qb->select('ind.id as industryId, ind.industryName, count(ind.id) as countIndustry');
        $qb->leftJoin('Planbold\Entity\Industry', 'ind', 'WITH', 'ind.id = sr.industry');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->isNull('sr.deletedAt'));
        $andX->add($qb->expr()->isNotNull('sr.industry'));
        $andX->add($qb->expr()->in('sr.id', $respondents));
        $qb->where($andX);
        $qb->groupBy('sr.industry');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'survey-respondent-list');
        return $query;
    }

    public function fetchArchetypeGroupBy(array $respondents = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('sr');
        $qb->select('a.id as archetypeId, a.title as archetypeName, count(a.id) as countArchetype');
        $qb->leftJoin('Planbold\Entity\Archetype', 'a', 'WITH', 'a.id = sr.archetype');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->isNull('sr.deletedAt'));
        $andX->add($qb->expr()->isNotNull('sr.archetype'));
        $andX->add($qb->expr()->in('sr.id', $respondents));
        $qb->where($andX);
        $qb->groupBy('sr.archetype');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'survey-respondent-list');
        return $query;
    }

    public function fetchGenderGroupBy(array $respondents = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('sr');
        $qb->select('sr.gender , count(sr.gender) as countGender');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->isNull('sr.deletedAt'));
        $andX->add($qb->expr()->in('sr.id', $respondents));
        $qb->where($andX);
        $qb->groupBy('sr.gender');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'survey-respondent-list');
        return $query;
    }

    public function fetchAgeGroupBy(array $respondents = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('sr');
        $qb->select('max(sr.age) as maxAge, min(sr.age) as minAge');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->isNull('sr.deletedAt'));
        $andX->add($qb->expr()->in('sr.id', $respondents));
        $qb->where($andX);
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'survey-respondent-list');
        return $query;
    }

    public function fetchJobtypeGroupBy(array $respondents = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('sr');
        $qb->select('j.id as jobtypeId, j.title as jobtypeName, count(j.id) as countJobtype');
        $qb->leftJoin('Planbold\Entity\Jobtype', 'j', 'WITH', 'j.id = sr.jobtype');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->isNull('sr.deletedAt'));
        $andX->add($qb->expr()->isNotNull('sr.jobtype'));
        $andX->add($qb->expr()->in('sr.id', $respondents));
        $qb->where($andX);
        $qb->groupBy('sr.jobtype');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'survey-respondent-list');
        return $query;
    }

    /**
     * Set Hydrator
     *
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get SurveyRespondent By Id
     *
     * @return null|\Planbold\Entity\SurveyRespondent
     */
    public function findOneById($id)
    {
        return $this->getEntityRepository()->findOneBy(array('id' => $id));
    }

    /**
     * Get Hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Find one by array of data
     * @param array $arrData
     * @return array
     */
    public function findOneBy($arrData)
    {
        return $this->getEntityRepository()->findOneBy($arrData);
    }

    /**
     * Find one by array of data
     * @param type $arrData
     * @return array
     */
    public function findBy($arrData)
    {
        return $this->getEntityRepository()->findBy($arrData);
    }

    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\SurveyRespondent');
    }

}
<?php

namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class Persona Revision mapper with Doctrine support
 * @package Planbold\Mapper
 */
class PersonaRevision extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Fetch Reviews with pagination
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('pr');
        $qb->select('p.uuid, pr.company, pr.revisionId, pr.username, pr.image as userImage, pr.personaImage, pr.personaName, pr.deleteFlag, pr.createFlag, pr.revisionDetails, date_format(pr.createdAt, :formatDate) as createdAt, date_format(pr.createdAt, :sformatDate) as revDate');
        $qb->innerJoin('Planbold\Entity\Persona', 'p', 'WITH', 'p.id = pr.personaId');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('pr.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('pr.deletedAt'));
        $qb->setParameter('formatDate', '%M-%d-%Y %H:%i');
        $qb->setParameter('sformatDate', '%b %d, %Y %H:%i %p');
        $qb->where($andX);
        $qb->orderBy('pr.createdAt', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'persona-revision-list');
        return $query;
    }

    /**
     * Set Hydrator
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Hydrator
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\PersonaRevision');
    }

    /**
     * Find one by array of data
     * @param type $arrData
     * @return type
     */
    public function findOneBy($arrData)
    {
        return $this->getEntityRepository()->findOneBy($arrData);
    }
    
    /**
     * fetch All Revision
     * @param array $userIds
     * @return type
     */
    public function fetchAllRevision($userIds)
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('pr');
        $qb->select('pr.company, pr.revisionId, pr.username, pr.image as userImage, pr.personaImage, pr.personaName, pr.deleteFlag, pr.createFlag, pr.revisionDetails, date_format(pr.createdAt, :formatDate) as createdAt, date_format(pr.createdAt, :sformatDate) as revDate');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->isNull('pr.deletedAt'));
        $andX->add($qb->expr()->in('pr.accountId', $userIds));
        $qb->setParameter('formatDate', '%M-%d-%Y %H:%i');
        $qb->setParameter('sformatDate', '%b %d, %Y %H:%i %p');
        $qb->where($andX);
        $qb->orderBy('pr.company, pr.createdAt', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'persona-revision-list');
        return $query->getResult();
    }
    
    /**
     * fetch All Revision With Date
     * @param array $userIds
     * @return type
     */
    public function fetchAllRevisionWithDate($userIds)
    {
        $date = new \DateTime();
        $dateToday  = date_format($date, 'm-d-Y');
        $hour       = date_format($date, 'H');
        $yesterday  = date('m-d-Y', strtotime('-1 day', strtotime($dateToday)));
        
        $qb = $this->getEntityRepository()->createQueryBuilder('pr');
        $qb->select('pr.company, pr.revisionId, pr.username, pr.image as userImage, pr.personaImage, pr.personaName, pr.deleteFlag, pr.createFlag, pr.revisionDetails, date_format(pr.createdAt, :formatDate) as createdAt, date_format(pr.createdAt, :sformatDate) as revDate');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->isNull('pr.deletedAt'));
        $andX->add($qb->expr()->in('pr.accountId', $userIds));
        $qb->setParameter('formatDate', '%M-%d-%Y %H:%i');
        $qb->setParameter('sformatDate', '%b %d, %Y %H:%i %p');
        $qb->where($andX);
        $qb->andWhere('DATE_FORMAT(pr.createdAt, :createdAt) BETWEEN :yesterday AND :today ');
        $qb->setParameter('createdAt', '%m-%d-%Y %H:00');
        $qb->setParameter('yesterday', $yesterday." {$hour}:00");
        $qb->setParameter('today', $dateToday." {$hour}:00");
        $qb->orderBy('pr.company, pr.createdAt', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'persona-revision-list');
        return $query->getResult();
        
    }
}
<?php

namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class StripeTransaction Mapper with Doctrine support
 * @package Planbold\Mapper
 */
class StripeTransaction extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Fetch All StripeAccount
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('st');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('st.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $qb->where($andX);
        $qb->orderBy('st.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'stripe-transaction-list');
        return $query->getResult();
    }

    /**
     * Set Hydrator
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Hydrator
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\StripeTransaction');
    }

    /**
     * Find one by array of data
     * @param type $arrData
     * @return type
     */
    public function findOneBy($arrData)
    {
        return $this->getEntityRepository()->findOneBy($arrData);
    }
}
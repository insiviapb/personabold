<?php
namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class PersonaKnowledge mapper with Doctrine support
 * @package Planbold\Mapper
 */
class PersonaKnowledge extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Fetch Persona Knowledge with pagination
     *
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('k');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('k.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('k.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('k.id', 'ASC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'persona-knowledge-list');
        return $query;
    }

    /**
     * Set Hydrator
     *
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\PersonaKnowledge');
    }

    /**
     * Find one by array of data
     * @param array $arrData
     * @return array
     */
    public function findOneBy($arrData)
    {
        return $this->getEntityRepository()->findOneBy($arrData);
    }
}
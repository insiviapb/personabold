<?php

namespace Planbold\Mapper;

use Doctrine\ORM\EntityManagerInterface;
use Planbold\Entity\EntityInterface;

/**
 * Interface of Entity
 *
 * @author Aleks Daloso <adaloso@insivia.com>
 */
interface MapperInterface
{
    public function setEntityManager(EntityManagerInterface $em);
    
    public function getEntityManager();
    
    public function getEntityRepository();
    
    public function fetchOne($id);
    
    public function fetchAll(array $params);
    
    public function insert(EntityInterface $entity);
    
    public function update(EntityInterface $entity);
    
    public function delete(EntityInterface $entity);
}

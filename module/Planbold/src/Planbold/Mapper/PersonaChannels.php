<?php

namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class PersonaChannels Mapper with Doctrine support
 * @package Planbold\Mapper
 */
class PersonaChannels extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Fetch All PersonaChannels
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('pc');
        $qb->select('pc.id, pc.personaId, gc.id as globalChannelsId, gc.title, gc.parentId, pc.value, pc.createdAt, pc.updatedAt, pc.deletedAt');
        $andX = $qb->expr()->andX();
        $qb->leftJoin('Planbold\Entity\GlobalChannels', 'gc', 'WITH', 'pc.globalChannels = gc.id');
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('pc.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('pc.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('pc.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'persona-channels-list');
        return $query;
    }

    /**
     * Set Hydrator
     *
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\PersonaChannels');
    }

    /**
     * Find one by array of data
     * @param array $arrData
     * @return array
     */
    public function findOneBy($arrData)
    {
        return $this->getEntityRepository()->findOneBy($arrData);
    }
}
<?php
namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class UserRole mapper with Doctrine support
 * @package Planbold\Mapper
 */
class UserRole extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Fetch Reviews with pagination
     *
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('user_role');
        $qb->orderBy('user_role.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'user-role-list');
        return $query;
    }

    /**
     * Set Hydrator
     *
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\UserRole');
    }

    /**
     * Get Role By array params
     */
    public function getRoleBy($params)
    {
        return $this->getEntityRepository()->findOneBy($params);
    }
    
    /**
     * Get Owner Role
     */
    public function getOwnerRole()
    {
        return $this->getRoleBy(array('id' => 3));
    }

    /**
     * Get Manager Role
     */
    public function getManagerRole()
    {
        return $this->getRoleBy(array('id' => 2));
    }

    /**
     * Get Basic User Role
     */
    public function getEmployeeRole()
    {
        return $this->getRoleBy(array('id' => 1));
    }
}
<?php

namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * User Mapper with Doctrine support
 *
 * @author Aleks <aleks.daloso@gmail.com>
 */
class User extends AbstractMapper implements ServiceLocatorAwareInterface
{
    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;
    
    /**
     * Fetch Reviews with pagination
     *
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('user');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('user.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('user.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('user.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'users-list');
        return $query;
    }
    
    /**
     * Fetch user by username
     * 
     * @param string $username
     * @return object
     */
    public function fetchOneByUsername($username)
    {
        return $this->getEntityRepository()->findOneBy(array('username' => $username));
    }
    
    /**
     * Fetch user by email
     *
     * @param string $email
     * @return object
     */
    public function fetchOneByEmail($email)
    {
        return $this->getEntityRepository()->findOneBy(array('email' => $email));
    }
    
    /**
     * Fetch single records with params uuid
     * @param uuid $uuid
     * @return object
     */
    public function fetchOneByUuid($uuid)
    {
        return $this->getEntityRepository()->findOneBy(array('uuid' => $uuid));
    }
    
    /**
     * Set Hydrator
     *
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }
    
    /**
     * Get Hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }
    
    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\User');
    }
    
    /**
     * Get Active Alerts
     * @param type $userId
     * @return type
     */
    public function getActiveAlert()
    {
        $dateCreated  = new \DateTime();
        $dateNow      = date_format($dateCreated, 'mdY');
        $dateHour     = date_format($dateCreated, 'G');
        $qb = $this->getEntityRepository()->createQueryBuilder('user');
        $qb->select('act.id as actId, user.firstName, user.lastName, user.email, user.id, '
                   .'(SELECT GROUP_CONCAT(agc2.clientId) FROM Planbold\Entity\AgencyClient agc2 WHERE agc2.account = user.account) as clientId');
        $qb->innerJoin('Planbold\Entity\Account', 'act', 'WITH', 'act.id = user.account');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->eq('user.haveAlert' , ':haveAlert'));
        $qb->setParameter('haveAlert', true);
        $andX->add($qb->expr()->isNull('user.deletedAt'));
        $qb->where($andX);
        $qb->andWhere('DATE_FORMAT(user.revisionDate, :revisionFormat) != :revisionDate');
        $qb->setParameter('revisionFormat', '%m%d%Y');
        $qb->setParameter('revisionDate', $dateNow);
        $qb->orderBy('user.id', 'DESC');
        $qb->groupBy('user.id');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'active-alert-list');
        return $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}

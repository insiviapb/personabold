<?php

namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class Account mapper with Doctrine support
 * @package Planbold\Mapper
 */
class Account extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Fetch Reviews with pagination
     *
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('act');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('act.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('act.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('act.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'account-list');
        return $query;
    }

    /**
     * Set Hydrator
     *
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\Account');
    }

    /**
     * Find one by array of data
     * @param type $arrData
     * @return type
     */
    public function findOneBy($arrData)
    {
        return $this->getEntityRepository()->findOneBy($arrData);
    }
    
    /**
     * Fetch current packages
     * @param  Planbold\Entity\Account $account
     * @return ZendPaginator
     */
    public function getCurrentPackages($account)
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('act');
        $qb->select('ps.id, ps.amount, ps.templateCount, ps.surveyCount, ps.description, ps.stripePlanCode, ps.planCode, ps.clientCount, ps.userCount');
        $qb->innerJoin('Planbold\Entity\StripeAccount', 'sa', 'WITH', 'sa.account = act.id');
        $qb->innerJoin('Planbold\Entity\Packages', 'ps', 'WITH', 'ps.stripePlanCode = sa.stripePlanCode');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->eq('act.id', ':account'));
        $qb->setParameter('account', $account);
        $qb->where($andX);
        $qb->orderBy('act.id', 'ASC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'publisher-package');
        return $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}
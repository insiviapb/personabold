<?php

namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * AuthLog Mapper with Doctrine support
 *
 * @author Dolly Aswin <dolly.aswin@gmail.com>
 */
class AuthLog extends AbstractMapper implements ServiceLocatorAwareInterface
{
    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;
    
    /**
     * Fetch Reviews with pagination
     *
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('auth_log');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('auth_log.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        
        $qb->where($andX);
        $qb->orderBy('auth_log.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'auth-log-list');
        return $query;
    }
    
    /**
     * Set Hydrator
     *
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }
    
    /**
     * Get Hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }
    
    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\AuthLog');
    }
    
    /**
     * Get Owner Role
     */
    public function getLatestRole(\Planbold\Entity\User $user)
    {
        return $this->getEntityRepository()->findOneBy(
            array(
                'user' => $user->getId()
            ),
            array('id' => 'DESC'),
            1
        );
    }
}

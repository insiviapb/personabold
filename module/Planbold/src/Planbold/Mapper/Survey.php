<?php

namespace Planbold\Mapper;

use Herrera\Json\Exception\Exception;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class Survey mapper with Doctrine support
 * @package Planbold\Mapper
 */
class Survey extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Fetch Survey with pagination
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('s');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('s.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('s.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('s.id', 'ASC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'survey-list');
        return $query;
    }

    /**
     * Set Hydrator
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Respondent By Id
     * @return null|\Planbold\Entity\Respondent
     */
    public function findOneById($id)
    {
        return $this->getEntityRepository()->findOneBy(array('id' => $id));
    }

    /**
     * Get Hydrator
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }


    /**
     * Find one by array of data
     * @param type $arrData
     * @return array
     */
    public function findBy($arrData)
    {
        return $this->getEntityRepository()->findBy($arrData);
    }

    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\Survey');
    }

    /**
     * Find one by array of data
     * @param array $arrData
     * @return array
     */
    public function findOneBy($arrData)
    {
        return $this->getEntityRepository()->findOneBy($arrData);
    }
    
    /**
     * Fetch Survey by Account
     * @param  $params
     * @return ZendPaginator
     */
    public function findSurveyByAccount($account)
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('s');
        $qb->leftJoin('Planbold\Entity\Persona', 'pa', 'WITH', 'pa.id = s.personaId');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->eq('pa.account' , ':account'));
        $qb->setParameter('account', $account );
        $andX->add($qb->expr()->isNull('s.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('s.id', 'ASC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'survey-list');
        return $query->getResult();
    }
}
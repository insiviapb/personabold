<?php

namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class SurveyRespondentData Mapper with Doctrine support
 * @package Planbold\Mapper
 */
class SurveyRespondentData extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Fetch All SurveyRespondentData
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('sd');
        $qb->select('sd.id, sr.id as surveyRespondentId, pf.id as personaFieldId, pf.title as personaFieldTitle, sd.title, sd.dataType, sd.content, sd.uuid, sd.createdAt, sd.updatedAt, sd.deletedAt');
        $qb->leftJoin('Planbold\Entity\PersonaFields', 'pf', 'WITH', 'sd.personaFields = pf.id');
        $qb->leftJoin('Planbold\Entity\SurveyRespondent', 'sr', 'WITH', 'sd.persona = sr.id');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('sd.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('sd.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('sd.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'survey-respondent-data-list');
        return $query;
    }

    /**
     * Fetch All SurveyRespondentData
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAllByRespondents(array $respondents = array())
    {

        $qb = $this->getEntityRepository()->createQueryBuilder('sd');
        $qb->select('sd.id, sr.id as surveyRespondentId, 0 as personaId, pf.id as personaFieldId, pf.title as personaFieldTitle, sd.title, sd.dataType, sd.content, sd.uuid, sd.createdAt, sd.updatedAt, sd.deletedAt');
        $qb->leftJoin('Planbold\Entity\PersonaFields', 'pf', 'WITH', 'sd.personaFields = pf.id');
        $qb->leftJoin('Planbold\Entity\SurveyRespondent', 'sr', 'WITH', 'sd.surveyRespondent = sr.id');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->in('sr.id', $respondents));
        $andX->add($qb->expr()->isNull('sd.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('sd.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'survey-respondent-data-list');

        return $query;
    }


    /**
     * Set Hydrator
     *
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\SurveyRespondentData');
    }

    /**
     * Find one by array of data
     * @param array $arrData
     * @return array
     */
    public function findOneBy($arrData)
    {
        return $this->getEntityRepository()->findOneBy($arrData);
    }
}
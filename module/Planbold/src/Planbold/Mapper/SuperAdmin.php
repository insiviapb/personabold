<?php
namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * SuperAdmin Mapper with Doctrine support
 */
class SuperAdmin extends AbstractMapper implements ServiceLocatorAwareInterface
{
    /**
    * @var Zend\Stdlib\Hydrator\HydratorInterface
    */
    protected $hydrator;

    /**
     * Fetch SuperUser with pagination
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('su');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('su.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $qb->where($andX);
        $qb->orderBy('su.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'super-user-list');
        return $query;
    }
    
    
    /**
    * Set Hydrator
    *
    * @param HydratorInterface $hydrator
    */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
    * Get Hydrator
    *
    * @return HydratorInterface
    */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
    * Get Entity Repository
    */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\SuperAdmin');
    }

    /**
    * Fetch Super User by Id
    * @param int $id
    */
    public function fetchOne($id)
    {
        return $this->getEntityRepository()->findOneBy(array('id' => $id));
    }
    
    /**
    * Fetch Super User by array params
    * @param array $array
    */
    public function findOneBy($array)
    {
        return $this->getEntityRepository()->findOneBy($array);
    }
    
}

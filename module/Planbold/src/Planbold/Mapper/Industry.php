<?php

namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class Industry Mapper with Doctrine support
 * @package Planbold\Mapper
 */
class Industry extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Fetch All Industry
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('ind');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            if ( $param == 'account' ) {
                $andX->add($qb->expr()->in('ind.account', ':account'));
                $qb->setParameter($param, array(0, $value));
            } else {
                $andX->add($qb->expr()->eq('ind.' . $param, ':' . $param));
                $qb->setParameter($param, $value);
            }
            
        }
        $andX->add($qb->expr()->isNull('ind.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('ind.industryName', 'ASC');
        $qb->groupBy('ind.industryName');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'industry-list');
        return $query;
    }
    
    
    /**
     * Fetch All Industry
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAllPersonaIndustry(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('ind');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->in('ind.id' ,':ids'));
        $qb->setParameter('ids', $params);

        $andX->add($qb->expr()->isNull('ind.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('ind.industryName', 'ASC');
        $qb->groupBy('ind.industryName');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'industry-list');
        return $query;
    }

    /**
     * Set Hydrator
     *
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\Industry');
    }

    /**
     * Find one by array of data
     * @param type $arrData
     * @return type
     */
    public function findOneBy($arrData)
    { 
        return $this->getEntityRepository()->findOneBy($arrData);
    }

    /**
     * Get Archetype By Id
     *
     * @return null|\Planbold\Entity\Archetype
     */
    public function findOneById($id)
    {
        return $this->getEntityRepository()->findOneBy(array('id' => $id));
    }
    
    /**
     * Fetch All Industry
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAllWithCompany(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('ind');
        $qb->select('ind.id, ind.uuid, ind.industryName, ind.phone, ind.description, ind.createdAt, act.name as accountName, act.uuid as actUuid');
        $qb->innerJoin('Planbold\Entity\Account', 'act', 'WITH', 'act.id = ind.account');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('ind.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('ind.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('ind.industryName', 'ASC');
        $qb->groupBy('ind.industryName');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'industry-list');
        return $query;
    }
    
    /**
     * Fetch All Parent and SubIndustry
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAllWithSub(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('parent');
        $qb->select('parent.id, parent.uuid, parent.industryName, parent.phone, parent.description, parent.createdAt, act.name as accountName, act.uuid as actUuid,'
                . '  sub.id as subId, sub.uuid as subUuid, sub.industryName as subIndutryName, sub.phone as subPhone, sub.description as subDescription, sub.createdAt as subCreatedAt');
        $qb->innerJoin('Planbold\Entity\Account', 'act', 'WITH', 'act.id = parent.account');
        $qb->leftJoin('Planbold\Entity\Industry', 'sub', 'WITH', 'sub.parentIndustry = parent.id');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('parent.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('parent.deletedAt'));
        $andX->add($qb->expr()->isNull('sub.deletedAt'));
        
        $qb->where($andX);
        $qb->orderBy('parent.industryName, parent.id, act.id', 'DESC');
        $qb->groupBy('parent.industryName');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'parent-sub-industry-list');
        return $query;
    }
    
    public function fetchAllSub($params)
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('ind');
        $qb->select('ind.id as id', 'ind.industryName as industryName', 'ind.phone as phone', 'ind.description as description',
            'ind.uuid as uuid', 'ind.parentIndustry as parentIndustry');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->in('ind.'.$param , ':'.$param));
            $qb->setParameter($param, $value);
        }

        $qb->where($andX);
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'industry-list');
        return $query;
    }
}
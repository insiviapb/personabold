<?php
namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class AccountIndustry Mapper with Doctrine support
 * @package Planbold\Mapper
 */
class AccountIndustry extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Fetch All Industry
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('ind');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('ind.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('ind.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('ind.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'industry-list');
        return $query;
    }

    /**
     * Find one by array of data
     * @param array $arrDat
     * @return type
     */
    public function findOneBy($params)
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('a');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('a.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('a.deletedAt'));
        $qb->where($andX);
        $qb->setMaxResults(1);
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'industry-one-list');
        return $query->getResult();
    }
    
    /**
     * Fetch one by array of data
     * @param type $arrData
     * @return type
     */
    public function fetchOneBy($arrData)
    {
        return $this->getEntityRepository()->findOneBy($arrData);
    }

    /**
     * Set Hydrator
     *
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\AccountIndustry');
    }


}
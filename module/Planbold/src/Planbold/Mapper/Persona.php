<?php

namespace Planbold\Mapper;

use Herrera\Json\Exception\Exception;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class Persona mapper with Doctrine support
 * @package Planbold\Mapper
 */
class Persona extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Fetch Reviews with pagination
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('per');
        $qb->select('per.name, 
            per.jobTitle, 
            per.jobDescription, 
            per.headshot, per.age, 
            per.gender, 
            per.location, 
            per.basicLife, 
            per.isTemplate,
            ind.id as industryId,
            per.uuid, 
            arc.title as archetype, 
            ind.industryName as industry');
        $qb->leftJoin('Planbold\Entity\Archetype', 'arc', 'WITH', 'arc.id = per.archetype');
        $qb->leftJoin('Planbold\Entity\Industry', 'ind', 'WITH', 'ind.id = per.industry');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('per.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('per.deletedAt'));
        $andX->add($qb->expr()->isNull('per.archivedAt'));
        $qb->where($andX);
        $qb->orderBy('per.updatedAt', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'per-list');
        return $query;
    }
    
    /**
     * Set Hydrator
     *
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Persona By Id
     *
     * @return null|\Planbold\Entity\Persona
     */
    public function findOneById($id)
    {
        return $this->getEntityRepository()->findOneBy(array('id' => $id));
    }

    /**
     * Get Hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Find one by array of data
     * @param type $params
     * @return type
     */
    public function findOneBy($params)
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('p');
        $qb->select('p.id, 
            p.shortCode,
            p.publicFlag,
            p.password,
            p.uuid, 
            p.name, 
            p.jobTitle,
            p.gender, 
            p.age, 
            p.jobDescription, 
            p.headshot, 
            act.uuid as actId,
            i.id as industryId, 
            i.industryName, 
            i.description as industryDescription, 
            j.id as jobtypeId, 
            j.title as jobtype, 
            a.id as archetypeId, 
            a.title as archetype, 
            a.shortDescription as archetypeShortDescription,
            a.description as archetypeDescription, 
            p.basicLife, 
            p.createdAt, 
            p.updatedAt, 
            p.deletedAt');
        $qb->leftJoin('Planbold\Entity\Account', 'act', 'WITH', 'act.id = p.account');
        $qb->leftJoin('Planbold\Entity\Industry', 'i', 'WITH', 'p.industry = i.id');
        $qb->leftJoin('Planbold\Entity\Archetype', 'a', 'WITH', 'p.archetype = a.id');
        $qb->leftJoin('Planbold\Entity\Jobtype', 'j', 'WITH', 'p.jobtype = j.id');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('p.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('p.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('p.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'persona-list');
        return $query;
    }

    /**
     * Find one by array of data
     * @param type $arrData
     * @return array
     */
    public function findBy($arrData)
    {
        return $this->getEntityRepository()->findOneBy($arrData);
    }

    /**
     * Find one by uuid
     * @param type $uuid
     * @return array
     */
    public function findOneByUuid($uuid)
    {
        return $this->getEntityRepository()->findOneBy(array('uuid' => $uuid));
    }
    
    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\Persona');
    }

    /**
     * Get Channels
     * @param type $uuid
     * @return type
     */
    public function getChannels($uuid)
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('pa');
        $qb->select('gc.title, pc.value, pa.uuid');
        $qb->innerJoin('Planbold\Entity\PersonaChannels', 'pc', 'WITH', 'pc.personaId = pa.id');
        $qb->innerJoin('Planbold\Entity\GlobalChannels', 'gc', 'WITH', 'gc.id = pc.globalChannels');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->eq('pa.uuid', ':uuid'));
        $qb->setParameter('uuid', $uuid);
        $andX->add($qb->expr()->isNull('pa.deletedAt'));
        $andX->add($qb->expr()->isNull('gc.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('pa.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'persona-channels-list');
        return $query->getResult();
    }
    
    /**
     * Get Knowledge
     * @param type $uuid
     * @return type
     */
    public function getKnowledge($uuid)
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('pa');
        $qb->select('pk.title, pk.value, pa.uuid');
        $qb->innerJoin('Planbold\Entity\PersonaKnowledge', 'pk', 'WITH', 'pk.personaId = pa.id');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->eq('pa.uuid', ':uuid'));
        $qb->setParameter('uuid', $uuid);
        $andX->add($qb->expr()->isNull('pa.deletedAt'));
        $andX->add($qb->expr()->isNull('pk.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('pa.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'persona-knowledge-list');
        return $query->getResult();
    }
    
    /**
     * Get Motivation
     * @param type $uuid
     * @return type
     */
    public function getMotivation($uuid)
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('pa');
        $qb->select('ms.title, pm.value, pa.uuid');
        $qb->innerJoin('Planbold\Entity\PersonaMotivations', 'pm', 'WITH', 'pm.personaId = pa.id');
        $qb->innerJoin('Planbold\Entity\Motivations', 'ms', 'WITH', 'pm.motivations = ms.id');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->eq('pa.uuid', ':uuid'));
        $qb->setParameter('uuid', $uuid);
        $andX->add($qb->expr()->isNull('pa.deletedAt'));
        $andX->add($qb->expr()->isNull('pm.deletedAt'));
        $andX->add($qb->expr()->isNull('ms.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('pa.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'persona-motivation-list');
        return $query->getResult();
    }
    
    /**
     * Get Persona Template Details
     * @param type $personaId
     * @return type
     */
    public function getPersonaTemplateDetails($personaId)
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('pa');
        $qb->select('pa.name as personaName, 
                    pa.age as age,
                    pa.gender as gender,
                    ate.title as archetypeTitle,
                    ate.shortDescription as archeShort,
                    ate.description as archeLong,
                    pa.jobTitle as jobTitle,
                    pa.jobDescription,
                    pa.headshot as personaImage,
                    pa.basicLife,
                    jte.title as jobType,
                    pd.content,
                    act.name as companyName,
                    act.logoPath as companyLogo,
                    IFELSE( pd.personaFields IS NULL, pd.title, pf.title) as personaFieldTitle,
                    iy.industryName');
        $qb->leftJoin('Planbold\Entity\PersonaData', 'pd', 'WITH', 'pa.id = pd.persona');
        $qb->leftJoin('Planbold\Entity\PersonaFields', 'pf', 'WITH', 'pd.personaFields = pf.id');
        $qb->leftJoin('Planbold\Entity\Archetype', 'ate', 'WITH', 'ate.id = pa.archetype');
        $qb->leftJoin('Planbold\Entity\Jobtype', 'jte', 'WITH', 'jte.id = pa.jobtype');
        $qb->leftJoin('Planbold\Entity\Industry', 'iy', 'WITH', 'iy.id = pa.industry');
        $qb->innerJoin('Planbold\Entity\Account', 'act', 'WITH', 'act.id = pa.account');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->eq('pa.id', ':personaId'));
        $qb->setParameter('personaId', $personaId);
        $andX->add($qb->expr()->isNull('pa.deletedAt'));
        $andX->add($qb->expr()->isNull('pd.deletedAt'));
        $andX->add($qb->expr()->isNull('pf.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('pa.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'persona-list');
        return $query->getResult();
    }
    
    /**
     * Find one by user
     * @param type $user
     * @return array
     */
    public function findByUser($user)
    {
        return $this->getEntityRepository()->findOneBy(array('user' => $user, 'archivedAt' => null));
    }
    
    /**
     * Fetch All Persona with pagination
     * @param  array $params
     * @return ZendPaginator
     */
    public function cleanfetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('persona');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('persona.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('persona.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('persona.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'persona-list');
        return $query;
    }
}
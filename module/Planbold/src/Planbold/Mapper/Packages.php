<?php
namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class Packages mapper with Doctrine support
 * @package Planbold\Mapper
 */
class Packages extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Fetch Reviews with pagination
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('ps');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('ps.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('ps.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('ps.id', 'ASC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'packages-list');
        return $query;
    }

    /**
     * Set Hydrator
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Hydrator
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\Packages');
    }

     /**
     * Find one by array of data
     * @param type $arrData
     * @return type
     */
    public function getPackages($arrData)
    {
        return $this->getEntityRepository()->findOneBy($arrData);
    }
}
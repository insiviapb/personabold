<?php

namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class PersonaData Mapper with Doctrine support
 * @package Planbold\Mapper
 */
class PersonaData extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Fetch All Industry
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('pd');
        $qb->select('pd.id, p.id as personaId, pf.id as personaFieldId, pf.title as personaFieldTitle, pd.title, pd.dataType, pd.content, pd.uuid, pd.createdAt, pd.updatedAt, pd.deletedAt');
        $qb->leftJoin('Planbold\Entity\PersonaFields', 'pf', 'WITH', 'pd.personaFields = pf.id');
        $qb->leftJoin('Planbold\Entity\Persona', 'p', 'WITH', 'pd.persona = p.id');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('pd.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('pd.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('pd.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'persona-data-list');
        return $query;
    }

    /**
     * Set Hydrator
     *
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\PersonaData');
    }

    /**
     * Find one by array of data
     * @param array $arrData
     * @return array
     */
    public function findOneBy($arrData)
    {
        return $this->getEntityRepository()->findOneBy($arrData);
    }
}
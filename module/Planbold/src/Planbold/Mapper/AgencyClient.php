<?php

namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class Agency Client mapper with Doctrine support
 * @package Planbold\Mapper
 */
class AgencyClient extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Fetch Reviews with pagination
     *
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('agc');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('agc.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('agc.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('agc.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'agency-client-list');
        return $query;
    }
    
    /**
     * Get all account
     * @param type $account
     * @return type
     */
    public function getAllAccount($account)
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('agc');
        $qb->select('GROUP_CONCAT(agc.clientId) as actId');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->eq('agc.account', ':account'));
        $qb->setParameter('account', $account);
        $andX->add($qb->expr()->isNull('agc.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('agc.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'agency-client-list');
        return $query->getResult();
    }
    
    /**
     * Get all account
     * @param type $account
     * @return type
     */
    public function getAllAccountDetails($account)
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('agc');
        $qb->select('act.uuid, act.name');
        $qb->innerJoin('Planbold\Entity\Account', 'act', 'WITH', 'agc.clientId = act.id');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->eq('agc.account', ':account'));
        $qb->setParameter('account', $account);
        $andX->add($qb->expr()->isNull('agc.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('agc.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'agency-client-list');
        return $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * Set Hydrator
     *
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\AgencyClient');
    }

    /**
     * Find one by array of data
     * @param type $arrData
     * @return type
     */
    public function findOneBy($arrData)
    {
        return $this->getEntityRepository()->findOneBy($arrData);
    }
    
    /**
     * Validate Agency Client Exists
     * @param type $arrData
     * @return type
     */
    public function validateAgencyClientExist($arrData)
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('agc');
        $qb->innerJoin('Planbold\Entity\Account', 'act', 'WITH', 'agc.clientId = act.id');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->eq('act.name', ':name'));
        $qb->setParameter('name', $arrData['client-name']);
        $andX->add($qb->expr()->eq('agc.account', ':account'));
        $qb->setParameter('account', $arrData['account']);
        $andX->add($qb->expr()->isNull('agc.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('agc.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'agency-client-list');
        return $query->getResult();
    }
    
    /**
     * Get All Clients
     * @param type $account
     * @return type
     */
    public function getAllClient($account)
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('agc');
        $qb->select('act.name, act.uuid, act.description, act2.name as actName, act2.uuid as actUuid');
        $qb->innerJoin('Planbold\Entity\Account', 'act', 'WITH', 'agc.clientId = act.id');
        $qb->innerJoin('Planbold\Entity\Account', 'act2', 'WITH', 'act2.id = agc.account');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->eq('agc.account', ':account'));
        $qb->setParameter('account', $account);
        $andX->add($qb->expr()->isNull('agc.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('agc.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'agency-client-list');
        return $query->getResult();
    }
    
}
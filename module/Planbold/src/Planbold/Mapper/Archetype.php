<?php

namespace Planbold\Mapper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator as ZendPaginator;

/**
 * Class Archetype mapper with Doctrine support
 * @package Planbold\Mapper
 */
class Archetype extends AbstractMapper implements ServiceLocatorAwareInterface
{

    /**
     * @var Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * Get Archetype By Id
     *
     * @return null|\Planbold\Entity\Archetype
     */
    public function findOneById($id)
    {
        return $this->getEntityRepository()->findOneBy(array('id' => $id));
    }


    /**
     * Fetch Reviews with pagination
     *
     * @param  array $params
     * @return ZendPaginator
     */
    public function fetchAll(array $params = array())
    {
        $qb = $this->getEntityRepository()->createQueryBuilder('ar');
        $andX = $qb->expr()->andX();
        foreach ($params as $param => $value) {
            $andX->add($qb->expr()->eq('ar.' . $param, ':' . $param));
            $qb->setParameter($param, $value);
        }
        $andX->add($qb->expr()->isNull('ar.deletedAt'));
        $qb->where($andX);
        $qb->orderBy('ar.id', 'DESC');
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true, 600, 'archetype-list');
        return $query;
    }

    /**
     * Set Hydrator
     *
     * @param HydratorInterface $hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Get Entity Repository
     */
    public function getEntityRepository()
    {
        return $this->getEntityManager()->getRepository('Planbold\Entity\Archetype');
    }

    /**
     * Find one by array of data
     * @param type $arrData
     * @return type
     */
    public function findOneBy($arrData)
    {
        return $this->getEntityRepository()->findOneBy($arrData);
    }
}
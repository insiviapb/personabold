<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Planbold\Filter;

use Zend\Filter\FilterInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

/**
 * Filter For UrlFragment
 *
 */
class UrlFragment implements ServiceLocatorAwareInterface, FilterInterface
{
    use ServiceLocatorAwareTrait;
    
    /**
     * (non-PHPdoc)
     * @see \Zend\Filter\FilterInterface::filter()
     */
    public function filter($value)
    {
        $urlFragment = parse_url($value, PHP_URL_FRAGMENT);
        if ($urlFragment !== null) {
            if (strpos($urlFragment, '!') !== false) {
                return $value;
            } else {
                return substr($value, 0, strpos($value, $urlFragment) - 1);
            }
        } else {
            return $value;
        }
    }
}

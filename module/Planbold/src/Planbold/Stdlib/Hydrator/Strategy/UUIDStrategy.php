<?php

namespace Planbold\Stdlib\Hydrator\Strategy;

use Zend\Stdlib\Hydrator\Strategy\StrategyInterface;
use Ramsey\Uuid\UuidFactory;
use Ramsey\Uuid\Uuid;

/**
 * Class ISODateTimeStrategy
 *
 * @package SMR\Stdlib\Hydrator\Strategy
 */
class UUIDStrategy implements StrategyInterface
{
    /**
     * Converts the given value so that it can be extracted by the hydrator.
     *
     * @param  mixed $value The original value.
     * @param  object $object (optional) The original object for context.
     * @return mixed Returns the value that should be extracted.
     * @throws \RuntimeException If object os not a User
     * 
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function extract($value, $object = null)
    {
        if ($value instanceof Uuid) {
            $factory = new UuidFactory();
            return $factory->fromString($value);
        }
            
        return null;
    }

    /**
     * Converts the given value so that it can be hydrated by the hydrator.
     *
     * @param  mixed $value The original value.
     * @param  array $data (optional) The original data for context.
     * @return mixed Returns the value that should be hydrated.
     * @throws \InvalidArgumentException
     * 
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function hydrate($value, array $data = null)
    {
        if (null !== $value) {
            if (!$value instanceof Uuid) {
                throw new \InvalidArgumentException('Expected Uuid object');
            }
        }

        return $value;
    }
}

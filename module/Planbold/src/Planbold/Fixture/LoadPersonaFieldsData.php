<?php
namespace Planbold\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Planbold\Entity\PersonaFields;

class LoadPersonaFieldsData extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 4;
    }
    
    public function load(ObjectManager $manager)
    {
        $fields    = array();
        $fieldsData = array(
            array(
                'title'         => 'Pains/Challenges',
                'description'   => 'Pains/Challenges Management',
                'type'          => 'list',
                'code'          => 'pains'
            ),
            array(
                'title'         => 'Drivers',
                'description'   => 'Drivers Management',
                'type'          => 'list',
                'code'          => 'drivers'
            ),
            array(
                'title'         => 'Goals',
                'description'   => 'Goals Management',
                'type'          => 'list',
                'code'          => 'goals'
            ),
            array(
                'title'         => 'Dislikes',
                'description'   => 'Dislikes Management',
                'type'          => 'list',
                'code'          => 'dislikes'
            ),
            array(
                'title'         => 'Daily Activities',
                'description'   => 'Daily Activities Management',
                'type'          => 'list',
                'code'          => 'daily_activities'
            ),
            array(
                'title'         => 'Channels',
                'description'   => 'Channels Management',
                'type'          => 'channel',
                'code'          => 'channels'
            ),
            array(
                'title'         => 'Motivations',
                'description'   => 'Motivations Management',
                'type'          => 'motivation',
                'code'          => 'motivations'
            ),
            array(
                'title'         => 'Knowledges',
                'description'   => 'Knowledges Management',
                'type'          => 'knowledge',
                'code'          => 'knowledges'
            ),
            array(
                'title'         => 'Age and Gender',
                'description'   => 'Age and Gender Management',
                'type'          => 'list',
                'code'          => 'agegender'
            ),
            array(
                'title'         => 'Archetype',
                'description'   => 'Archetype Management',
                'type'          => 'list',
                'code'          => 'archetype'
            )
        );
        
        foreach ($fieldsData as $data) {
            $personaFields = new PersonaFields();
            $personaFields->setTitle($data['title']);
            $personaFields->setDescription($data['description']);
            $personaFields->setType($data['type']);
            $personaFields->setFieldCode($data['code']);
            $manager->persist($personaFields);
            $fields[] = $personaFields;
        }
        
        $manager->flush();
        foreach ($fields as $key => $field) {
            $this->addReference('persona_fields_' . $key, $field);
        }
    }
}

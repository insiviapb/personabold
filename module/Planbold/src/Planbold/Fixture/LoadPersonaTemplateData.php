<?php
namespace Planbold\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Planbold\Entity\PersonaTemplates;

class LoadPersonaTemplateData extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 9;
    }
    
    public function load(ObjectManager $manager)
    {
        $templates     = array();
        $templatesData = array(
            array(
                'name'         => 'Sample 1',
                'preview_path' => '/img/persona-templates/1.jpg'
            ),
            array(
                'name'         => 'Sample 2',
                'preview_path' => '/img/persona-templates/2.jpg'
            ),
            array(
                'name'         => 'Sample 3',
                'preview_path' => '/img/persona-templates/3.png'
            ),
            array(
                'name'         => 'Sample 4',
                'preview_path' => '/img/persona-templates/4.png'
            ),
            array(
                'name'         => 'Sample 5',
                'preview_path' => '/img/persona-templates/5.png'
            ),
            array(
                'name'         => 'Sample 6',
                'preview_path' => '/img/persona-templates/6.png'
            ),
            array(
                'name'         => 'Sample 7',
                'preview_path' => '/img/persona-templates/7.jpeg'
            ),
            array(
                'name'         => 'Sample 8',
                'preview_path' => '/img/persona-templates/8.jpg'
            ),
            array(
                'name'         => 'Sample 9',
                'preview_path' => '/img/persona-templates/9.jpg'
            ),
            array(
                'name'         => 'Sample 10',
                'preview_path' => '/img/persona-templates/10.png'
            ),
            array(
                'name'         => 'Sample 11',
                'preview_path' => '/img/persona-templates/11.png'
            ),
            array(
                'name'         => 'Sample 12',
                'preview_path' => '/img/persona-templates/12.png'
            ),
        );
        
        foreach ($templatesData as $data) {
            $template = new PersonaTemplates();
            $template->setTemplateName($data['name']);
            $template->setDefaultFlag(0);
            $template->setTemplatePreviewPath($data['preview_path']);
            $manager->persist($template);
            $templates[] = $template;
        }
        
        $manager->flush();
        
        foreach ($templates as $key => $template) {
            $this->addReference('template_' . $key, $template);
        }
    }
}

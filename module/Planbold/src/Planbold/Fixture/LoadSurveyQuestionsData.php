<?php
namespace Planbold\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Planbold\Entity\SurveyQuestions;

class LoadSurveyQuestionsData extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 4;
    }
    
    public function load(ObjectManager $manager)
    {
        $questions    = array();
        $questionsData = array(
            array(
                'title'     => 'age',
                'description'     => 'Age'
            ),
            array(
                'title'     => 'gender',
                'description'     => 'Gender'
            ),
            array(
                'title'     => 'industry',
                'description'     => 'Industry'
            ),
            array(
                'title'     => 'archetype',
                'description'     => 'Archetype'
            ),
            array(
                'title'     => 'jobtype',
                'description'     => 'Job Type'
            ),
            array(
                'title'     => 'jobtitle',
                'description'     => 'Job Title'
            ),
            array(
                'title'     => 'jobdescription',
                'description'     => 'Job Description'
            ),
            array(
                'title'     => 'basiclifedescription',
                'description'     => 'Basic Life Description'
            ),
            array(
                'title'     => 'knowledges',
                'description'     => 'Knowledges'
            ),
            array(
                'title'     => 'motivations',
                'description'     => 'Motivations'
            ),
            array(
                'title'     => 'channels',
                'description'     => 'Channels'
            ),
            array(
                'title'     => 'dailyactivities',
                'description'     => 'Daily Activities'
            ),
            array(
                'title'     => 'dislikes',
                'description'     => 'Dislikes'
            ),
            array(
                'title'     => 'goals',
                'description'     => 'Goals'
            ),
            array(
                'title'     => 'painschallenges',
                'description'     => 'Pains and Challenges'
            ),

        );
        
        foreach ($questionsData as $data) {
            $question = new SurveyQuestions();
            $question->setTitle($data['title']);
            $question->setDescription($data['description']);
            $manager->persist($question);
            $questions[] = $question;
            
        }
        
        $manager->flush();
        foreach ($questions as $key => $question) {
            $this->addReference('survey_questions_' . $key, $question);
        }
    }
}

<?php
namespace Planbold\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Planbold\Entity\GlobalChannels;

class LoadGlobalChannelsData extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 4;
    }
    
    public function load(ObjectManager $manager)
    {
        $channels    = array();
        $channelsData = array(
            array(
                'title'     => 'Social',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Social Paid Advertising',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Search',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Search Paid Advertising',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Traditional Advertising',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Content',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Website',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Mobile',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Print',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'In Store',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Public Relations',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Events',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'E-Mail',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Twitter',
                'parentId'     => '1'
            ),
            array(
                'title'     => 'YouTube',
                'parentId'     => '1'
            ),
            array(
                'title'     => 'Pinterest',
                'parentId'     => '1'
            ),
            array(
                'title'     => 'LinkedIn',
                'parentId'     => '1'
            ),
            array(
                'title'     => 'Google+',
                'parentId'     => '1'
            ),
            array(
                'title'     => 'Facebook',
                'parentId'     => '1'
            ),
            array(
                'title'     => 'SnapChat',
                'parentId'     => '1'
            ),
            array(
                'title'     => 'Instagram',
                'parentId'     => '1'
            ),
            array(
                'title'     => 'Xing',
                'parentId'     => '1'
            ),
            array(
                'title'     => 'Tumblr',
                'parentId'     => '1'
            ),
            array(
                'title'     => 'Twoo',
                'parentId'     => '1'
            ),
            array(
                'title'     => 'Twitter',
                'parentId'     => '2'
            ),
            array(
                'title'     => 'YouTube',
                'parentId'     => '2'
            ),
            array(
                'title'     => 'Pinterest',
                'parentId'     => '2'
            ),
            array(
                'title'     => 'LinkedIn',
                'parentId'     => '2'
            ),
            array(
                'title'     => 'Google+',
                'parentId'     => '2'
            ),
            array(
                'title'     => 'Facebook',
                'parentId'     => '2'
            ),
            array(
                'title'     => 'SnapChat',
                'parentId'     => '2'
            ),
            array(
                'title'     => 'Instagram',
                'parentId'     => '2'
            ),
            array(
                'title'     => 'Xing',
                'parentId'     => '2'
            ),
            array(
                'title'     => 'Tumblr',
                'parentId'     => '2'
            ),
            array(
                'title'     => 'Twoo',
                'parentId'     => '2'
            ),
            array(
                'title'     => 'Onsite Search Optimization',
                'parentId'     => '4'
            ),
            array(
                'title'     => 'Link Building',
                'parentId'     => '4'
            ),
        );
        
        foreach ($channelsData as $data) {
            $channel = new GlobalChannels();
            $channel->setTitle($data['title']);
            $channel->setParentId($data['parentId']);
            $manager->persist($channel);
            $channels[] = $channel;
            
        }
        
        $manager->flush();
        foreach ($channels as $key => $channel) {
            $this->addReference('global_channels_' . $key, $channel);
        }
    }
}

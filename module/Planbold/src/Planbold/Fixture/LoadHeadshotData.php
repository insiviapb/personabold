<?php

namespace Planbold\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Planbold\Entity\Headshot;

class LoadHeadshotData extends AbstractFixture implements OrderedFixtureInterface {

    public function getOrder() {
        return 11;
    }

    public function load(ObjectManager $manager) {
        $headshots = array();
        $headshotsData = array(
            array(
                'name' => 'headshot1',
                'path' => '/img/headshots/1x400.jpg',
                'filter' => 'filter1'
            ),
            array(
                'name' => 'headshot2',
                'path' => '/img/headshots/2x400.jpg',
                'filter' => 'filter2'
            ),
            array(
                'name' => 'headshot3',
                'path' => '/img/headshots/3x400.jpg',
                'filter' => 'filter3'
            ),
            array(
                'name' => 'headshot4',
                'path' => '/img/headshots/4x400.jpg',
                'filter' => 'filter4'
            ),
            array(
                'name' => 'headshot5',
                'path' => '/img/headshots/5x400.jpg',
                'filter' => 'filter5'
            ),
            array(
                'name' => 'headshot6',
                'path' => '/img/headshots/6x400.jpg',
                'filter' => 'filter6'
            ),
            array(
                'name' => 'headshot7',
                'path' => '/img/headshots/7x400.jpg',
                'filter' => 'filter7'
            ),
            array(
                'name' => 'headshot8',
                'path' => '/img/headshots/8x400.jpg',
                'filter' => 'filter8'
            ),
            array(
                'name' => 'headshot9',
                'path' => '/img/headshots/9x400.jpg',
                'filter' => 'filter9'
            ),
        );
        foreach ($headshotsData as $data) {
            $headshot = new Headshot();
            $headshot->setName($data['name']);
            $headshot->setPath($data['path']);
            $headshot->setFilter($data['filter']);

            $manager->persist($headshot);
            $headshots[] = $headshot;
        }

        $manager->flush();

        foreach ($headshots as $key => $headshot) {
            $this->addReference('headshot_' . $key, $headshot);
        }
    }
}

<?php
namespace Planbold\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Planbold\Entity\UserRole;

class LoadUserRoleData extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 3;
    }
    
    public function load(ObjectManager $manager)
    {
        $roles    = array();
        $roleData = array(
            array(
                'roleName'     => 'Basic Role',
                'roleType'     => 'basic'
            ),
            array(
                'roleName'     => 'Manager',
                'roleType'     => 'manager'
            ),
            array(
                'roleName'     => 'Owner',
                'roleType'     => 'owner'
            ),
        );
        
        foreach ($roleData as $data) {
            $role = new UserRole();
            $role->setName($data['roleName']);
            $role->setRoleType($data['roleType']);
            $manager->persist($role);
            $roles[] = $role;
            
        }
        
        $manager->flush();
        foreach ($roles as $key => $role) {
            $this->addReference('role_' . $key, $role);
        }
    }
}

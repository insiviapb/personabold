<?php
namespace Planbold\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Planbold\Entity\User;
use Zend\Crypt\Password\Bcrypt;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 2;
    }
    
    public function load(ObjectManager $manager)
    {
        $users    = array();
        $bcrypt   = new Bcrypt();
        $password = $bcrypt->create('123456');
        
        $account = $this->getReference('account_0');
        $userData = array(
//            array(
//                'username'     => 'aleksd',
//                'email'        => 'aleks.daloso@gmail.com',
//                'display_name' => 'Aleks',
//                'password'     => $password,
//                'firstname'    => 'Aleks',
//                'lastname'     => 'Daloso',
//                'haveAlert'    => false
//            ),
            array(
                'username' => 'andy',
                'email'    => 'andy@example.com',
                'display_name' => 'Andy',
                'password' => $password,
                'firstname'    => 'Andy',
                'lastname'     => 'Halko',
                'haveAlert'    => false
            ),
//            array(
//                'username'     => 'patrick',
//                'email'        => 'patrick@example.com',
//                'display_name' => 'Patrick',
//                'password'     => $password,
//                'firstname'    => 'Patrick',
//                'lastname'     => 'Garcia',
//                'haveAlert'    => false
//            ),
//            array(
//                'username'     => 'derek',
//                'email'        => 'derek@example.com',
//                'display_name' => 'Derek',
//                'password'     => $password,
//                'firstname'    => 'Derek',
//                'lastname'     => 'Ramsey',
//                'haveAlert'    => false
//            )
        );
        
        foreach ($userData as $data) {
            $user = new User();
            $user->setUsername($data['username']);
            $user->setEmail($data['email']);
            $user->setDisplayName($data['display_name']);
            $user->setPassword($password);
            $user->setFirstName($data['firstname']);
            $user->setLastName($data['lastname']);
            $user->setAccount($account);
            $user->setHaveAlert($data['haveAlert']);
            $manager->persist($user);
            $users[] = $user;
        }
        
        $manager->flush();
        
        foreach ($users as $key => $user) {
            $this->addReference('user_' . $key, $user);
        }
    }
}

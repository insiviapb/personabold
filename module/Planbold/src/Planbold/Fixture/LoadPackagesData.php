<?php
namespace Planbold\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Planbold\Entity\Packages;
use Stripe\Stripe;
use Stripe\Plan;

class LoadPackagesData extends AbstractFixture implements OrderedFixtureInterface
{
    //Testing Server
    const SECRET_KEY      = 'sk_test_sqtwwy0hqcWWK2q8ip3bZlDw';
    const PUBLISAHBLE_KEY = 'pk_test_Qz4vImQUVwngju9dn6pgW0sO';
    //Production Server
//    const SECRET_KEY      = 'sk_test_sqtwwy0hqcWWK2q8ip3bZlDw';
//    const PUBLISAHBLE_KEY = 'pk_test_Qz4vImQUVwngju9dn6pgW0sO';
    
    
    public function getOrder()
    {
        return 0;  }
    
    public function load(ObjectManager $manager)
    {
        $packages     = array();
        $packagesData = array(
                            array(
                                'name'   => 'PACKAGE-A',
                                'id'     => 'PACKAGE-A',
                                'amount' => 1200,
                                'userCount'     => 1,
                                'templateCount' => 0,
                                'surveyCount'   => 0,
                                'clientCount'   => 0
                            ),
                            array(
                                'name'   => 'PACKAGE-B',
                                'id'     => 'PACKAGE-B',
                                'amount' => 3900,
                                'userCount'     => 5,
                                'templateCount' => 1,
                                'surveyCount'   => 0,
                                'clientCount'   => 0
                            ),
                            array(
                                'name'   => 'PACKAGE-C',
                                'id'     => 'PACKAGE-C',
                                'amount' => 7400,
                                'userCount'     => 6,
                                'templateCount' => 1,
                                'surveyCount'   => 1,
                                'clientCount'   => 0
                            ),
                            array(
                                'name'   => 'PACKAGE-D',
                                'id'     => 'PACKAGE-D',
                                'amount' => 149000,
                                'userCount'     => 6,
                                'templateCount' => 1,
                                'surveyCount'   => 1,
                                'clientCount'   => 1
                            )
                        );
        
        foreach ( $packagesData as $packagesDta ) {
            
            $this->setAPIKey();
            $stripeData = array("amount"      => $packagesDta['amount'] ,
                                "interval"    => "month",
                                "name"        => $packagesDta['name'],
                                "id"        => $packagesDta['id'],
                                "currency"    => "usd");

            $stripePlan = Plan::create($stripeData);
            $planData   = $stripePlan->__toArray(true);
            $package = new Packages();
            $package->setAmount($planData['amount'] / 100);
            $package->setDescription("Plan for N employee");
            $package->setStripePlanCode($planData['id']);
            $package->setSurveyCount($packagesDta['surveyCount']);
            $package->setTemplateCount($packagesDta['templateCount']);
            $package->setUserCount($packagesDta['userCount']);
            $package->setClientCount($packagesDta['clientCount']);

            $manager->persist($package);
            $packages[] = $package;
        }
        $manager->flush();
        
        foreach ($packages as $key => $package) {
            $this->addReference('package_' . $key, $package);
        }
    }
    
    /**
     * Set API Key
     */
    private function setAPIKey() {
        
        $this->apiKey = self::SECRET_KEY;
        Stripe::setApiKey($this->apiKey);
    }
}

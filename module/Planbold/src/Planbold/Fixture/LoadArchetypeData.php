<?php
namespace Planbold\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Planbold\Entity\Archetype;

class LoadArchetypeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 4;
    }
    
    public function load(ObjectManager $manager)
    {
        $rows    = array();
        $rowData = array(
            array(
                'title'             => 'The Innocent',
                'description'       => "The innocent's core desire is to be free and happy, and their biggest fear is to do something wrong and be punished for it. Think Wall-E or Audrey Hepburn in every one of her films. At their best they are optimistic, honest and enthusiastic - at their worst they are irritating, boring and childish.",
                'shortDescription'  => "aka The Dreamer, The Romantic"
            ),
            array(
                'title'             => 'The Hero',
                'description'       => "The regular guy (or girl) only wants to belong and feel a part of something, and their greatest fear is to be left out or to stand out from the crowd. Think Bilbo Baggins or Homer Simpson. At best they are friendly, empathetic and reliable - at worst they are weak, superficial and suggestible.",
                'shortDescription'  => "aka The Superhero, The Warrior"
            ),
            array(
                'title'             => "The Regular Guy ",
                'description'       => "The regular guy (or girl) only wants to belong and feel a part of something, and their greatest fear is to be left out or to stand out from the crowd. Think Bilbo Baggins or Homer Simpson. At best they are friendly, empathetic and reliable - at worst they are weak, superficial and suggestible.",
                'shortDescription'  => "aka The Everyman, The Good Guy"
            ),
            array(
                'title'             => "The Creator",
                'description'       => "The creator is driven by their desire to produce exceptional and enduring works, and they are most afraid of mediocrity. Think Frida Kahlo or Doc Brown in Back to the Future. At their best they are imaginative, expressive and innovative - at their worst they are self-indulgent, melodramatic and narcissistic.",
                'shortDescription'  => "aka The Artist, The Dreamer"
            ),
        );
        
        foreach ($rowData as $data) {
            $entity = new Archetype();
            $entity->setTitle($data['title']);
            $entity->setShortDescription($data['shortDescription']);
            $entity->setDescription($data['description']);
            $manager->persist($entity);
            $rows[] = $entity;
            
        }
        
        $manager->flush();
        foreach ($rows as $key => $row) {
            $this->addReference('archetype_' . $key, $row);
        }
    }
}

<?php
namespace Planbold\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Planbold\Entity\Account;
use Planbold\Entity\StripeAccount as StripeAccountEntity;
use Planbold\Entity\StripeCards as StripeCards;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Token;

class LoadAccountData extends AbstractFixture implements OrderedFixtureInterface
{
    //Testing Server
    const SECRET_KEY      = 'sk_test_sqtwwy0hqcWWK2q8ip3bZlDw';
    const PUBLISAHBLE_KEY = 'pk_test_Qz4vImQUVwngju9dn6pgW0sO';
    //Production Server
//    const SECRET_KEY      = 'sk_test_sqtwwy0hqcWWK2q8ip3bZlDw';
//    const PUBLISAHBLE_KEY = 'pk_test_Qz4vImQUVwngju9dn6pgW0sO';
    
    public function getOrder()
    {
        return 1;
    }
    
    public function load(ObjectManager $manager)
    {
        $accounts    = array();
        $accountData = array(
            array(
                'name'     => 'Insivia',
                'email'    => 'andy@insivia.com',
                'planId'   => 'PACKAGE-A'
            ),
        );
        foreach ($accountData as $data) {
            $account = new Account();
            $account->setName($data['name']);
            
            $this->setApiKey();
            $customer        = Customer::create(null, $this->apiKey);
            $customer->email = $data['email'];
            $customer        = $customer->save();
            $customer        = Customer::retrieve($customer->id);
            $stripeData      = new StripeAccountEntity();
            $stripeData->setAccount($account);
            $stripeData->setAccountId($customer->id);
            $stripeData->setEmail($customer->email);
            $stripeData->setStripeStatus(true);
            $stripeData->setSubscriptionId($data['planId']);
            $cardData = array(
                            "name"      => 'Testing',
                            "number"    => '4242424242424242',
                            "exp_month" => '12',
                            "exp_year"  => '2019',
                            "cvc"       => '313'
                        );
            $this->token = Token::create(
                        array("card" => $cardData)
            );            
            $customer->sources->create(array("card" => $this->token->id));
            $customer->save();
            $customer->plan = $data['planId'];
            $customer->quantity = 1;
            $customer->save();
            $customer = Customer::retrieve($customer->id);
            $stripeCard = new StripeCards();
            $stripeCard = $this->setStripeService($stripeCard, $cardData, true);
            $stripeCard->setStripeCardId($customer->sources->data[0]['id']);
            $stripeCard->setBrand($customer->sources->data[0]['brand']);
            $stripeCard->setStripeAccount($stripeData);
            
            $manager->persist($stripeCard);
            $manager->persist($stripeData);
            $manager->persist($account);
            $accounts[] = $account;
        }
        
        $manager->flush();
        
        foreach ($accounts as $key => $account) {
            $this->addReference('account_' . $key, $account);
        }
    }
    
    /**
     * Set Stripe Service
     * @param \Planbold\Entity\StripeCards $stripeCards
     * @param type $data
     * @return \Planbold\Entity\StripeCards
     */
    public function setStripeService($stripeCards, $data, $isInsert = false)
    {
        if ($isInsert) {
            $stripeCards->setCardNumber(substr($data['number'], -4));
        }
        $stripeCards->setCardName($data['name']);
        $stripeCards->setExpiryMonth($data['exp_month']);
        $stripeCards->setExpiryYear($data['exp_year']);
        $stripeCards->setCvc($data['cvc']);
        return $stripeCards;
    }
    
    /**
     * Set API Key
     */
    private function setAPIKey() {
        
        $this->apiKey = self::SECRET_KEY;
        Stripe::setApiKey($this->apiKey);
    }
}

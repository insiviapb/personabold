<?php
namespace Planbold\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Planbold\Entity\Motivations;

class LoadMotivationsData extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 4;
    }
    
    public function load(ObjectManager $manager)
    {
        $motivations    = array();
        $motivationsData = array(
            array(
                'title'     => 'Money',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Freedom',
                'parentId'     => '0'
            ),
            array(
                'title'     => "Being Challenged",
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Pride',
                'parentId'     => '0'
            ),
            array(
                'title'     => "Friendship",
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Friends',
                'parentId'     => '0'
            ),
            array(
                'title'     => "Glory",
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Safety',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Success',
                'parentId'     => '0'
            ),
            array(
                'title'     => "Thrill",
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Knowledge',
                'parentId'     => '0'
            ),
            array(
                'title'     => "Love",
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Loyalty',
                'parentId'     => '0'
            ),
            
            
            array(
                'title'     => 'Popularity',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Fame',
                'parentId'     => '0'
            ),
            array(
                'title'     => "Family",
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Children',
                'parentId'     => '0'
            ),
            array(
                'title'     => "Be acknowledged",
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Reduce stress',
                'parentId'     => '0'
            ),
            array(
                'title'     => "Spiritual Belief",
                'parentId'     => '0'
            ),
            array(
                'title'     => 'More Time',
                'parentId'     => '0'
            ),
            array(
                'title'     => 'Health',
                'parentId'     => '0'
            ),
        );
        
        foreach ($motivationsData as $data) {
            $motivation = new Motivations();
            $motivation->setTitle($data['title']);
            $motivation->setParentId($data['parentId']);
            $manager->persist($motivation);
            $motivations[] = $motivation;
            
        }
        
        $manager->flush();
        foreach ($motivations as $key => $motivation) {
            $this->addReference('motivations' . $key, $motivation);
        }
    }
}

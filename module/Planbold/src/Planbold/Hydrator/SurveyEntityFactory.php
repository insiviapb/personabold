<?php
namespace Planbold\Hydrator;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Planbold\Stdlib\Hydrator\Strategy as HydratorStrategy;

/**
 * Hydrator for Doctrine Entity
 *
 * @author Aleks Daloso <adaloso@insivia.com>
 */
class SurveyEntityFactory implements FactoryInterface
{
    /**
     * Create a service for DoctrineObject Hydrator
     *
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $parentServiceLocator = $serviceLocator->getServiceLocator();
        $entityManager = $parentServiceLocator->get('Doctrine\\ORM\\EntityManager');
        $hydrator = new DoctrineObject($entityManager);
        $hydrator->addStrategy('createdAt', new HydratorStrategy\ISODateTimeStrategy);
        $hydrator->addStrategy('deletedAt', new HydratorStrategy\ISODateTimeStrategy);
        $hydrator->addStrategy('updatedAt', new HydratorStrategy\ISODateTimeStrategy);
        return $hydrator;
    }
}

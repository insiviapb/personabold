<?php
namespace Planbold\Hydrator;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Planbold\Stdlib\Hydrator\Strategy as HydratorStrategy;

/**
 * Hydrator for Doctrine Entity
 *
 * @author Aleks Daloso <adaloso@insivia.com>
 */
class UserEntityFactory implements FactoryInterface
{
    /**
     * Create a service for DoctrineObject Hydrator
     *
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $parentServiceLocator = $serviceLocator->getServiceLocator();
        $entityManager = $parentServiceLocator->get('Doctrine\\ORM\\EntityManager');
        $hydrator = new DoctrineObject($entityManager);
        $hydrator->addStrategy('createdAt', new HydratorStrategy\ISODateTimeStrategy);
        $hydrator->addStrategy('signupAt', new HydratorStrategy\ISODateTimeStrategy);
        $hydrator->addStrategy('activateAt', new HydratorStrategy\ISODateTimeStrategy);
        $hydrator->addStrategy('cancelAt', new HydratorStrategy\ISODateTimeStrategy);
        $hydrator->addStrategy('uuid', new HydratorStrategy\UUIDStrategy);
        return $hydrator;
    }
}

/**
 * Created by adaloso on 8/3/16.
 */
module.exports = function(grunt) {
    require('jit-grunt')(grunt);
    pkg: grunt.file.readJSON('package.json'),
        grunt.initConfig({
            pkg: grunt.file.readJSON('package.json'),
             less: {
                 bootstrap: {
                     options: {
                         compress: true,
                         yuicompress: true,
                         optimization: 2
                     },
                     files: {
                         'css/min/settings.min.css': [
                             'css/less/users-profile.less',
                             'css/less/users-management.less',
                             'css/less/industry.less',
                             'css/less/company-profile.less',
                             'css/less/subscription-info.less',
                             'css/less/persona-template.less',
                             'css/less/template-designer.less',
                             'css/less/persona-template-details.less',
                             'css/less/invoice.less',
                             'css/less/persona-fields.less',
                             'css/less/modals.less',
                             'css/less/common.less'
                         ],
                         'css/min/audience.min.css': [
                             'css/less/persona.less',
                             'css/less/share-persona.less',
                             'css/less/survey.less',
                             'css/less/compare.less',
                             'css/less/audience.less',
                             'css/less/persona-client.less',
                             'css/less/export-files.less',
                             'css/less/modals.less'
                         ],
                         'css/min/theme.min.css': [
                             'css/less/theme-management.less',
                             'css/less/modals.less'
                         ],
                         'css/min/static.min.css': [
                            'css/less/static.less'
                         ]
                     }
                 }
             },
            uglify: {
                all: {
                    files : {
                        'js/min/scripts.min.js': [
                            'js/channels.js',
                            'js/motivations.js',
                            'js/knowledge.js',
                            'js/tools.js',
                            'js/persona-js/headshot.js',
                            'js/persona-js/share.js',
                            'js/archetype.js',
                            'js/all.js',
                            'js/scripts.js',
                        ]
                    }
                },
                persona : {
                    files : {
                        'js/min/persona.min.js': [
                            'js/settings/Model/upload-photo.js',
                            'js/persona/Model/get-persona-channels.js',
                            'js/persona/Model/save-persona-section-position.js',
                            'js/persona/Model/get-persona.js',
                            'js/persona/Model/check-packages.js',
                            'js/survey/Model/send-survey.js',
                            'js/persona/Model/add-persona-client.js',
                            'js/persona/Model/save-persona-data.js',
                            'js/persona/Model/get-client-persona.js',
                            'js/persona/Model/export-persona.js',
                            'js/persona/Model/client-files.js',
                            'js/persona/View/persona.js',
                            'js/persona/View/compare.js',
                            'js/persona/View/export-template.js',
                            'js/persona/View/export-template-details.js',
                            'js/persona/View/persona-client.js',
                            'js/persona/View/persona-list-box.js',
                            'js/persona/View/file-management.js',
                            'js/persona/persona.js'
                        ]
                    }
                },
                survey : {
                    files : {
                        'js/min/survey.min.js': [
                            'js/survey/Model/send-survey.js',
                            'js/survey/Model/get-survey-respondents.js',
                            'js/persona/Model/get-persona-fields.js',
                            'js/survey/Model/get-survey-result-by-respondent.js',
                            'js/survey/View/survey-form.js',
                            'js/survey/View/survey-result.js',
                            'js/survey/survey.js',
                        ]
                    }
                },
                settings: {
                    files: {
                        'js/min/settings.min.js': [
                            'js/settings/Model/industry.js',
                            'js/settings/Model/users.js',
                            'js/settings/Model/update-users.js',
                            'js/settings/Model/delete-users.js',
                            'js/settings/Model/upload-photo.js',
                            'js/settings/Model/add-update-cards.js',
                            'js/settings/Model/add-update-billing.js',
                            'js/settings/Model/persona-template.js',
                            'js/settings/Model/publish-persona-template.js',
                            'js/settings/Model/update-subscription.js',
                            'js/settings/Model/update-notifications.js',
                            'js/settings/Model/persona-fields.js',
                            'js/persona/Model/check-packages.js',
                            'js/settings/Model/export-files.js',
                            'js/settings/View/overview.js',
                            'js/settings/View/users.js',
                            'js/settings/View/subscription-info.js',
                            'js/settings/View/user-profile.js',
                            'js/settings/View/industry.js',
                            'js/settings/View/persona-templates.js',
                            'js/settings/View/template-details.js',
                            'js/settings/View/template-descriptions.js',
                            'js/settings/View/alert.js',
                            'js/settings/View/persona-fields.js',
                            'js/settings/View/export-files.js',
                            'js/settings/settings.js'
                        ],
                    }
                },
                widgets: {
                    files: {
                        'js/min/widgets.min.js': [
                            'js/widgets/View/colorpicker.js',
                            'js/widgets/widgets.js'
                        ],
                        'js/min/init-tinymce.min.js': [
                            'js/packages/tinymce/init-tinymce.js'
                        ]
                    }
                },

                theme: {
                    files: {
                        'js/min/theme.min.js': [
                            'js/min/widgets.min.js',
                            'js/theme/Model/colorupdate.js',
                            'js/theme/View/main.js',
                            'js/theme/theme.js'
                        ]
                    }
                },
                audience: {
                    files: {
                        'js/min/audience.min.js': [
                            'js/audience/View/audience-persona.js',
                            'js/audience/Model/get-persona-industry.js',
                            'js/audience/Model/industry-description.js',
                            'js/audience/Model/save-industry-description.js',
                            'js/persona/Model/add-persona-client.js',
                            'js/persona/Model/check-packages.js',
                            'js/persona/Model/get-client-persona.js',
                            'js/persona/View/persona-client.js',
                            'js/audience/audience.js'
                        ]
                    }
                }
            },
            cssmin: {
                user: {
                    files : {
                        'css/min/dist.min.css': [
                            'dist/css/magicsuggest-min.css',
                            'dist/css/bootstrap-colorpicker.min.css',
                            'dist/css/bootstrap-colorpicker-plus.css'
                        ],
                        'css/min/interface.min.css' : [
                            'css/interface.css',
                            'css/min/settings.min.css',
                            'css/min/audience.min.css',
                            'css/min/dist.min.css',
                            'css/min/theme.min.css'
                        ]
                    }
                },
                packages: {
                    files: {
                        'dist/css/bootstrap-colorpicker-plus.min.css': [
                            'dist/css/bootstrap-colorpicker-plus.css'
                        ],
                        'dist/css/bootstrap-colorpicker.min.css': [
                            'dist/css/bootstrap-colorpicker.css'
                        ],
                    }
                }
            },
            watch: {
                user: {
                    files: ['js/user/**/*.js','js/*.js', 'css/*.css', 'css/less/*.less'],
                    tasks: ['uglify', 'cssmin', 'less']
                },
                all: {
                    files: ['js/user/**/*.js','js/*.js', 'css/*.css', 'css/less/*.less'],
                    tasks: ['uglify', 'cssmin', 'less']
                },
                persona: {
                    files: ['js/persona/**/*.js','js/*.js', 'css/*.css'],
                    tasks: ['uglify']
                },
                survey: {
                    files: ['js/survey/**/*.js','js/*.js', 'css/*.css', 'css/less/*.less'],
                    tasks: ['uglify','cssmin', 'less']
                },
                audience: {
                    files: ['js/audience/**/*.js','js/*.js', 'css/*.css'],
                    tasks: ['uglify']
                }
            }
        });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');

    grunt.registerTask('default',['less','uglify', 'cssmin']);
    //grunt.registerTask('default',['less', 'uglify', 'cssmin']);
}


var Knowledges  = Knowledges || {};
Knowledges = {
    knowledgeData: {},
    init: function() {
        
        $.each(personaDataLookUp, function(k, v){
            if ( $.inArray(v.personaFieldTitle, ['Channels', 'Motivations', 'Knowledge']) >= 0 ) {
                if ( v.personaFieldTitle == 'Knowledge' ) {
                    Knowledges.knowledgeData = JSON.parse(v.content);
                }
            }
        });
        if ( typeof knowledgeData == 'undefined' ) {
            Knowledges.knowledgeData = {};
        }

        Knowledges.getPersonaKnowledge('.box-knowledge-slider');
        $("#knowledgeModal").on('hide.bs.modal', function () {
            Knowledges.getPersonaKnowledge('.box-knowledge-slider');
        });
        $('#knowledgeModal').on('show.bs.modal', function() {
            Tools.renderEllipsis();
            $('.modal-knowledge-slider-container').html('');
            _.each (Knowledges.knowledgeData, function(v,k) {
                Knowledges.addSlider(v, '.modal-knowledge-slider-container', true);
            });
            $('#knowledgeModal .delete-slider').on('click', function() {
                var itemId = $(this).siblings('div').attr('data-itemid');
                Knowledges.deleteKnowledge(itemId);
            });
            $('#knowledgeModal .delete-slider').removeClass('hide');
        });
        
        $('.btn-save-knowledge').click(function(){
            Knowledges.saveSlider(); 
            $('#knowledgeModal').modal('hide');
        });
        
        $('button#addKnowledge').click(function() {
            var knowledge = $('input#knowledgeName');
            var uid = (new Date).getTime();

            var _data ={
                id: '',
                data_id: uid,
                type_name: 'knowledge',
                value:  50,
                name: knowledge.val(),
                new: true
            };
            Knowledges.knowledgeData[uid] = _data;
            Knowledges.addSlider(_data, '.modal-knowledge-slider-container', true);

            knowledge.val('');
            $(this).attr('disabled', true);

        }),
        $('input#knowledgeName').on('keyup', function() {
            if($(this).val().length != 0) {
                $('button#addKnowledge').removeAttr('disabled');
            } else {
                $('button#addKnowledge').attr('disabled', true);
            }
        });
    },
    saveSlider: function() {
        
        $.ajax({
            dataType: "json",
            type: "POST",
            url: "/persona/process/savePersonaKnowledge",
            success: function () {
                Knowledges.sliderSaved();
            },
            data: { content            : Knowledges.knowledgeChanges, 
                    personaId          : $('#persona').attr('data-pid'),
                    knowledgeFieldId   : knowledgeFieldId,
                    content            : Knowledges.knowledgeData,
                    revisionId         : revisionId }
        });
    },
    sliderSaved: function(){
        All.progress(100, $('#progressBar') );
    },
    getPersonaKnowledge: function(loadOnElements) {
        
        var personaKnowledge = {};
        try {
            personaKnowledge = JSON.parse(Knowledges.knowledgeData);
        } catch(e) {
            personaKnowledge = Knowledges.knowledgeData;
        }
        $(loadOnElements).html('');
        var _data = new Array();

        $.each( personaKnowledge, function(k, v) {
            var knowledge = {
                id: v.id,
                value: v.value,
                name: v.name,
                type_name: 'knowledge',
                data_id: v.id,
                new: true
            };
            var editable = false;

            _data.push(knowledge);

            if(loadOnElements.indexOf('modal',1) > 0) {
                editable = true;
            }
            Knowledges.addSlider(knowledge, loadOnElements, editable);
        });
        Knowledges.personaKnowledges = _data;
    },
    addSlider: function(data, sliderContainer, isEditable) {
        var uniqueID = (new Date).getTime().toString();

        var _status = (isEditable) ? '' : 'non-editable';
        var dataType = data.type_name; // motivation, channel, knowledge
        var dataId = data.data_id;
        var sliderUniqueId = dataType +'Slider' + uniqueID;
        if(data.new) {
            var sliderTpl = _.template($('#slider-template').html());
            $(sliderContainer).append( sliderTpl({
                id: dataId,
                typeName: dataType,
                sliderUniqueId: sliderUniqueId,
                dataId: dataId,
                value: data.value,
                name: data.name,
                dataStatus: _status,
                parentId: (typeof data.parentId != 'undefined' ? data.parentId : 0)
            }));
        } else { //DELETE A SLIDER
            var itemSlider = $(sliderContainer + ' #' + sliderUniqueId); // eg: $('.slider-container channelSlider123')
            itemSlider.parent().hide();
            return false;
        }
        // eg: $('container #channelSlider1')
        var elSlider = $(sliderContainer + ' #' + sliderUniqueId); // eg: $('.slider-container channelSlider123')
        elSlider.slider({
            min: 0,
            max: 100,
            value: elSlider.attr('data-value'),
            step: 1,
            range: "min",
            stop: function (event, ui) {
                var $slider = $(this);
                
                if ( typeof Knowledges.knowledgeData[$slider.data('id')] != 'undefined' ) {
                    Knowledges.knowledgeData[$slider.data('id')].value = ui.value;
                }
            },
            create: function( event, ui ) {
                var $slider = $(this);

                var randomBg = '#' + Math.random().toString(16).slice(2, 8).toUpperCase();
                if($slider.hasClass('non-editable')) {
                    $slider.slider('disable');
                }
                $slider.find('.ui-widget-header').css('background',randomBg);
                if(!$slider.attr('data-itemId').length) { //prevent saving if slider loads from the Channel Sections
                    if ( typeof Knowledges.knowledgeData[$slider.data('id')] != 'undefined' ) {
                        Knowledges.knowledgeData[$slider.data('id')].value= 50;
                    }
                }

            }
        });
    },
    deleteKnowledge: function(itemId) {
        $('#slider-'+itemId).remove();
        delete Knowledges.knowledgeData[itemId];
    }
}
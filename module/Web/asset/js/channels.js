var Channels  = Channels || {};

Channels = {
    personaChannels: '',
    personaDataChanges: {},
    init: function () {
        $.each(personaDataLookUp, function(k, v){
            if ( $.inArray(v.personaFieldTitle, ['Channels', 'Motivations', 'Knowledge']) >= 0 ) {
                if ( v.personaFieldTitle == 'Channels' ) {
                    pchannels = JSON.parse(v.content);
                }
            }
        });
        
        if ( typeof pchannels == 'undefined' ) {
            pchannels = {};
        }
       
        Channels.getPersonaChannels('.box-channels-slider');
        $('#channelsModal').on('shown.bs.modal', function () {
            Tools.renderEllipsis();
            Channels.renderChannelsList(channelData);
        })
        $('.btn-save-channel').click(function(){
            pchannels = [];
            $.each (Channels.personaDataChanges, function(k,v) {
                pchannels.push(v);
            });
            pchannels = JSON.stringify(pchannels);
            Channels.saveChannelSlider();
            $("#channelsModal").modal('hide');
            Channels.getPersonaChannels('.box-channels-slider');
        });
        $.each(pchannels, function(k,v) {
            Channels.personaDataChanges[v.id] = v;
        });
    },
    renderChannelsList: function (response) {

        response = JSON.parse(response);
        $('#channels-list').html('');

        if (Object.keys(response).length != 0)
        {
            var channelsTemplate = _.template($('#global-channels-template').html());
            var channelItemsTemplate = _.template($('#global-channels-item-template').html());

            $.each(response, function (k, v) {
                
                var channelParentId = v.id;
                $('#channels-list').append(channelsTemplate({
                    id: v.id,
                    title: v.title,
                    hasChild : (typeof v.child != 'undefined' && Object.keys(v.child).length > 0) ? true : false
                }));
                
                if (typeof v.child != 'undefined' && Object.keys(v.child).length > 0) {

                    var collapseChannelID = "collapseChannel" + channelParentId;
                    var channelParent     = $('#channel-parent' + channelParentId);
                    channelParent.after("<div class='collapse' id='" + collapseChannelID + "'>");

                    $.each(v.child, function (m, n) {
                        channelParent.next('#' + collapseChannelID).append(channelItemsTemplate({
                            id: n.id,
                            title: n.title,
                            parent_id: n.parentId
                        }));
                    });
                }
            });

            $(".channels-slider-container").html('');
            if(Object.keys(Channels.personaChannels).length != 0) {
                $.each(Channels.personaChannels, function(key, val) {
                    Channels.addChannelSlider(val, ".channels-slider-container", true);

                    // checked the existing persona's channels
                    if($('#checkBoxChannel'+val.globalChannelsId).length) {
                        $('#checkBoxChannel'+val.globalChannelsId).prop( "checked", true );
                        if(typeof val.parentId != "undefined" && val.parentId != 0) {
                            $('#collapseChannel' + val.parentId).collapse('show');
                        }
                    }
                })
            }

            // Add a slider when a channel list item checked
            $('input[type="checkbox"].checkBoxChannel').click(function(){
                var _isChecked = $(this).prop('checked'),
                    _data      = {};
                if(_isChecked) { // add slider  a
                    Channels.personaDataChanges[$(this).val()] = {
                        id: $(this).val(),
                        globalChannelsId: $(this).val(),
                        value:  50,
                        title: $(this).data('title'),
                        parentId : $(this).closest('label').closest('.list-group-item').data('parent'),
                        new: true
                    };
                    _data = Channels.personaDataChanges[$(this).val()];
                    
                } else {
                    
                    var _data = {
                        id: '',
                        globalChannelsId: $(this).val(),
                        parentId : $(this).closest('label').closest('.list-group-item.channel-child').data('parent'),
                        title: $(this).data('title'),
                        new: false
                    };
                    
                    delete (Channels.personaDataChanges[$(this).val()]);
                }
                
                Channels.addChannelSlider( _data, ".channels-slider-container", true);
            });
        }
    },
    addChannelSlider: function (channelData, sliderContainer, isEditable) {
        
        var _status = (isEditable) ? '' : 'non-editable';
        if (channelData.new) {
            var channelSliderTpl = _.template($('#slider-template').html());
            $(sliderContainer).append(channelSliderTpl({
                id: channelData.id,
                typeName: "channel",
                dataId: channelData.globalChannelsId,
                value: channelData.value,
                sliderUniqueId: 'channelSlider' + channelData.globalChannelsId,
                name: (typeof channelData.name != 'undefined') ? channelData.name : channelData.title,
                dataStatus: _status,
                parent_id: channelData.parentId
            }));
        } else { //DELETE A SLIDER
            var channelSlider = $(sliderContainer + ' #channelSlider' + channelData.globalChannelsId);
            channelSlider.parent().hide();
            return false;
        }

        // eg: $('container #channelSlider1')
        var elChannelSlider = $(sliderContainer + ' #channelSlider' + channelData.globalChannelsId);
        elChannelSlider.slider({
            min: 0,
            max: 100,
            value: elChannelSlider.attr('data-value'),
            step: 1,
            range: "min",
            stop: function (event, ui) {
                var $slider = $(this);
                Channels.personaDataChanges[$slider.data('id')].value= ui.value;
            },
            create: function (event, ui) {
                var $slider = $(this);

                var randomBg = '#' + Math.random().toString(16).slice(2, 8).toUpperCase();
                if ($slider.hasClass('non-editable')) {
                    $slider.slider('disable');
                }
                $slider.find('.ui-widget-header').css('background', randomBg);
                if (!$slider.attr('data-itemId').length) { //prevent saving if slider loads from the Channel Sections
                    Channels.personaDataChanges[$slider.data('id')].value= 50;
                }

            }
        });
    },
    saveChannelSlider: function () {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: "/persona/process/savePersonaChannel",
            success: function (response) {
                console.log(response);
            },
            data: { content         : Channels.personaDataChanges, 
                    personaId       : $('#persona').attr('data-pid'),
                    channelFieldId  : channelFieldId,
                    revisionId      : revisionId }
        });
    },
    getPersonaChannels: function (loadOnElement) {
        
        try {
            personaChannels = JSON.parse(pchannels);
        } catch(e) {
            personaChannels = pchannels;
        }
        _.each(Channels.personaChannelsUpdate, function (v, k) {
            personaChannels.push(v);
        });

        $(loadOnElement).html('');
        var _data = new Array();
        $.each(personaChannels, function (k, v) {
            var channelsData = {
                id: v.id,
                globalChannelsId: v.globalChannelsId,
                value: v.value,
                name: typeof v.name != 'undefined' ? v.name : v.title,
                parentId: v.parentId,
                new : true
            }
            _data.push(channelsData);
            Channels.addChannelSlider(channelsData, loadOnElement, false);
        });

        Channels.personaChannels = _data;
    },
    personaChannelsUpdate: [],
}
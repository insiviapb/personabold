var achtype = 0;
Archetype = {
    displayArchetype: function() {
        $('.archetype-description').html($('select[name="archtype"] option:selected').data('description'));
        if ( achtype == 0 ) {
            $('select[name="archtype"]').change(function(){
                $('.archetype-description').show();
                $('.archetype-description').html($('select[name="archtype"] option:selected').data('description'));
            });
            achtype = 1;
        }
    },
    saveArchetype: function() {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: "/persona/process/saveArchetypeData",
            success: function(response) {
                if (response.success) {
                    $('#personaInfo2Modal').modal('hide');
                    All.removeLoader();
                    personaPage.personaView.updateDisplayPersonaInfo();
                } else {
                    alert('An error occured. Please try again later.');
                }
            },
            data: {
                persona_id     : $('#persona').attr('data-pid'),
                revisionId     : revisionId,
                industry       : $('select[name="industry"]').val(),
                jobtype        : $('select[name="jobtype"]').val(),
                archetype      : $('select[name="archtype"]').val(),
                jobTitle       : $('input[name="jobTitle"]').val(),
                basicLife      : $('textarea[name="basicLife"]').val(),
                jobDescription : $('textarea[name="jobDescription"]').val()
            },
            error: function() {
                alert('An error occured. Please try again later.');
            }
        });
    }
}
var Motivations     = Motivations || {};
Motivations = {
    personaMotivations: '',
    personaMotivationDataChanges: {},
    init: function(){
        $.each(personaDataLookUp, function(k, v){
            if ( $.inArray(v.personaFieldTitle, ['Channels', 'Motivations', 'Knowledge']) >= 0 ) {
                if ( v.personaFieldTitle == 'Motivations' ) {
                    pmotivationData = JSON.parse(v.content);
                }
            }
        });
        if ( typeof pmotivationData == 'undefined' ) {
            pmotivationData = {};
        }
        Motivations.getPersonaMotivations('.box-motivations-slider');
        $('#motivationsModal').on('shown.bs.modal', function () {
            Tools.renderEllipsis();
            Motivations.renderMotivationsList(motivationData);
            $('#motivationsModal .delete-slider').on('click', function() {
                var itemId = $(this).siblings('div').attr('data-itemid');
                Motivations.deleteMotivation(itemId);
            });
            $('#motivationsModal .delete-slider').removeClass('hide');
        });
        
        $('.btn-save-motivation').click(function(){
            pmotivationData = [];
            $.each (Motivations.personaMotivationDataChanges, function(k,v) {
                pmotivationData.push(v);
            });
            pmotivationData = JSON.stringify(pmotivationData);
            Motivations.saveMotivationSlider();
            $("#motivationsModal").modal('hide');
            Motivations.getPersonaMotivations('.box-motivations-slider');
        });
        
        $('#txt-motivation').on('keypress', function(el) {
            
            if ( el.keyCode == 13 ) {
                Motivations.addCustomMotivation($(el.currentTarget).val());
                $(el.currentTarget).val('');
            }
        });
        $.each(pmotivationData, function(k,v) {
            Motivations.personaMotivationDataChanges[v.id] = v;
        });
    },
    renderMotivationsList: function(response) {
        response = JSON.parse(response);
        $('#motivations-list .listing').html('');

        if(Object.keys(response).length != 0){
            $.each(response, function(k,v) {
                if(v.parentId == '0') {
                    var parentId = v.id;
                    var motivationsTemplate = _.template($('#motivations-template').html());
                    $('#motivations-list .listing').append( motivationsTemplate({
                        id: v.id,
                        title: v.title
                    }));
                }
                var collapseMotivationID = "collapseMotivation" + parentId;
                var motivationParent = $('#motivation-parent' + parentId);
                motivationParent.after("<div class='collapse' id='" + collapseMotivationID + "'>");

                $.each(response, function(k,v) {
                    var itemsTemplate = _.template($('#motivations-item-template').html());
                    if(v.parentId == parentId) {
                        motivationParent.next('#'+collapseMotivationID).append( itemsTemplate({
                            id: v.id,
                            title: v.title,
                            parent_id: v.parentId
                        }));
                    }
                });
                if($("#" + collapseMotivationID).children().length == 0 ){
                    motivationParent.find('i').remove();
                }
            });

            $(".motivations-slider-container").html('');
            if(Object.keys(Motivations.personaMotivations).length != 0) {
                $.each(Motivations.personaMotivations, function(key, val) {
                    Motivations.addMotivationSlider(val, ".motivations-slider-container", true);

                    // checked the existing persona's motivations
                    if($('#checkBoxMotivation'+val.motivation_id).length) {
                        $('#checkBoxMotivation'+val.motivation_id).prop( "checked", true );
                        if(typeof val.parent_id != "undefined" && val.parent_id != 0) {
                            $('#collapseMotivation' + val.parent_id).collapse('show');
                        }
                    }
                })
            }

            // Add a slider when a motivation list item checked
            $('input[type="checkbox"].checkBoxMotivation').click(function(){

                var _isChecked = $(this).prop('checked');
                if(_isChecked) { // add slider
                    Motivations.personaMotivationDataChanges[$(this).val()] = {
                        id: $(this).val(),
                        motivation_id: $(this).val(),
                        value:  50,
                        name: $(this).data('title'),
                        new: true
                    };
                    _data = Motivations.personaMotivationDataChanges[$(this).val()];
                } else { // delete slider
                    _data = {
                        id: '',
                        motivation_id: $(this).val(),
                        new: false
                    };
                    
                    delete (Motivations.personaMotivationDataChanges[$(this).val()]);
                }
                Motivations.addMotivationSlider(_data, ".motivations-slider-container", true);
            });
        }
    },
    addMotivationSlider: function(motivationData, sliderContainer, isEditable) {
        
        var _status = (isEditable) ? '' : 'non-editable';
        if(motivationData.new) {
            var sliderTpl = _.template($('#slider-template').html());
            $(sliderContainer).append( sliderTpl({
                id: motivationData.id,
                typeName: 'motivation',
                dataId: motivationData.motivation_id,
                sliderUniqueId: 'motivationSlider' + motivationData.motivation_id,
                value: motivationData.value,
                name: motivationData.name,
                dataStatus: _status
            }));
        } else { //DELETE A SLIDER
            var motivationSlider = $(sliderContainer + ' #motivationSlider' + motivationData.motivation_id);
            motivationSlider.parent().hide();
            return false;
        }
        // eg: $('container #motivationSlider1')
        var elSlider = $(sliderContainer + ' #motivationSlider' + motivationData.motivation_id);
        elSlider.slider({
            min: 0,
            max: 100,
            value: elSlider.attr('data-value'),
            step: 1,
            range: "min",
            stop: function (event, ui) {
                var $slider = $(this);
                Motivations.personaMotivationDataChanges[$slider.data('id')].value = ui.value;
            },
            create: function( event, ui ) {
                var $slider = $(this);
                var randomBg = '#' + Math.random().toString(16).slice(2, 8).toUpperCase();
                if($slider.hasClass('non-editable')) {
                    $slider.slider('disable');
                }
                $slider.find('.ui-widget-header').css('background',randomBg);
                if(!$slider.attr('data-itemId').length) { //prevent saving if slider loads from the Sections
                    Motivations.personaMotivationDataChanges[$slider.data('id')].value= 50;
                }
            }
        });
    },
    saveMotivationSlider: function() {
        
        $.ajax({
            dataType: "json",
            type: "POST",
            url: "/persona/process/savePersonaMotivation",
            success: function (response) {
                Motivations.personaMotivationSaved(response);
            },
            data: { content            : Motivations.personaMotivationDataChanges, 
                    personaId          : $('#persona').attr('data-pid'),
                    motivationFieldId  : motivationFieldId,
                    revisionId         : revisionId }
        });
        
    },
    getPersonaMotivations: function(loadOnElement) {
        
        try {
            personaMotivations = JSON.parse(pmotivationData);
        } catch(e) {
            personaMotivations = pmotivationData;
        }

        if(Object.keys(Motivations.personaMotivationDataChanges).length != 0 ) {
            personaMotivations = [];
            _.each(Motivations.personaMotivationDataChanges, function (v, k) {
                personaMotivations.push(v);
            });
        }

        
        $(loadOnElement).html('');
        var _data = new Array();
        $.each(personaMotivations, function (k, v) {
            var ppmotivationsData = {
                id: v.id,
                motivation_id: v.motivation_id,
                value: v.value,
                name: v.name,
                parent_id: v.parent_id,
                new: true
            }

            _data.push(ppmotivationsData);
            Motivations.addMotivationSlider(ppmotivationsData, loadOnElement, false);
        });
        Motivations.personaMotivations = _data;
    },
    personaMotivationSaved: function( response ){
        All.progress(100, $('#progressBar') );
    },
    addCustomMotivation: function(val) {
        var motivationId = Motivations.makeid();
        Motivations.personaMotivationDataChanges[motivationId] = {
            id: motivationId,
            motivation_id: motivationId,
            value  :  50,
            name   : val,
            new    : true,
            custom : true
        };
        _data = Motivations.personaMotivationDataChanges[motivationId];
        console.log(1);
        Motivations.addMotivationSlider(_data, ".motivations-slider-container", true);
    },
    deleteMotivation: function(itemId) {
        $('#slider-'+itemId).remove();
        delete Motivations.personaMotivationDataChanges[itemId];
    },
    makeid: function()
    {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < 5; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
}
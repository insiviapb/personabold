$('#frmUserRegister').validate({
    onkeyup: function(element) { $(element).valid(); },
    onfocusout: function(element) { $(element).valid(); },
    rules: {
        firstName : "required",
        lastName  : "required",
        companyName  : "required",
        email: {
            required : true,
            email    : true,
        },
        password: {
            required  : true,
            minlength : 5
        },
        passwordVerify: {
            required  : true,
            minlength : 5,
            equalTo: "#password"
        }
    },
    messages: {
        firstName: {
            required: "Please enter your firstname",
        },
        lastName: {
            required: "Please enter your lastname",
        },
        companyName: {
            required: "Please enter your company name",
        },
        password: {
            required: "Please provide a password",
            minlength: "Your password must be at least 5 characters long"
        },
        passwordVerify: {
            required: "Please provide a password",
            minlength: "Your password must be at least 5 characters long",
            equalTo: "Please enter the same password as above"
        },
        email: {
            required: "Please enter email address",
            email: "Please enter a valid email address",
            notEqualTo : 'Please enter email not the same value as username.'
        },
    },
    errorElement: 'label',
    errorPlacement: function (error, element) {
        error.insertAfter(element.parent());

    }
});
$('#firstName, #lastName').change(function(){

    $('input[name="username"]').val($('#firstName').val().replace(/@.*/g, '')+$('#lastName').val().replace(/@.*/g, ''));

});
var pchannels = {};
var All = All || {};
All = {
    feedoffset: 0,
    personas_sort: '',
    disable_fields: true,
    currentChannel: '',
    currentBoxId: 0,
    allPersonaId: '',
    image_index:0,
    appendId:'',
    hasUpdated: false,
    agegender: {},
    persona: function(){
        
        SharePersona.init();
        
        //set default age and gender
        All.agegender = {
            age    : persona_age,
            gender : persona_gender 
        };
        
        $('.btn-save-age-gender').click(function() {
            All.renderLoader($(this));
            All.savePersonaDetails($('#personaInfoModal'), All.agegender);
        });
        
        $('.btn-save-persona-information').click(function(){
            All.renderLoader($(this));
            Archetype.saveArchetype();
        });
        
        $("#sendSurveyModal").on('hidden', function(){
            $(this).data('modal', null);
        }).on('shown.bs.modal', function () {//render AGE slider ui Jquery
            var modal  = $(this);
            modal.find('section').html($('#sendSurveyFormContainer').html());
            var ms = $('.persona-modal #msSurveySendTo').magicSuggest({
                placeholder: 'Enter valid email address..',
                vtype: 'email',
                hideTrigger: true
            });

            $(ms).on('selectionchange', function(){
                if(this.isValid()) {
                    $('#sendSurvey').removeAttr('disabled');
                } else {
                    $('#sendSurvey').attr('disabled', true);
                }
            });
        });

        $('[data-tooltip="tooltip"]').tooltip();

        if($('.headshot').length !=  0) {
            Headshots.process('.headshot');
        }

        //Trigger a start up tour when adding new persona
        if ( $('#persona.persona-index').length > 0 ) {
            if($('#persona').attr('data-pid').length == 0) {
                Tools.renderPersonaTour();
            }
        }
        if($('#persona').attr('data-pid').length != 0) {
            All.allPersonaId = $('#persona').attr('data-pid');
        }

        //render AGE slider ui Jquery
        All.renderAgeSlider('#age-slider', persona_age, false);
        //Render Gender Pie graph
        All.loadGenderPie('.canvas-gender', persona_gender);
        All.renderMsLocation('#msLocation');

        // prevent all modals to pop up
        $('.persona-modal').on('show.bs.modal', function() {
            return All.validateFullName();
        });

        
        Channels.init();
        Motivations.init();
        Knowledges.init();
        
        $('.persona-process').click(function() {
            All.renderLoader($(this));
        })

        $("#personaInfoModal").on('shown.bs.modal', function () {//render AGE slider ui Jquery
            All.renderAgeSlider('#age-slider-modal', persona_age, true);
            All.renderGenderSlider('#gender-slider-modal', persona_gender);
            All.renderMsLocation('#msLocationModals', false);
        });
        
        $('#personaInfo2Modal').on('shown.bs.modal', function() {
            Archetype.displayArchetype();
        });

        $('#uploadHeadshotModal').on('show.bs.modal', function(e) {
            var _el = $(this);
            var personaId = $('#persona').attr('data-pid');

            _el.find('#upload-persona-id').val(personaId);
        });

        $('#personaBoxModal').on('show.bs.modal', function(e) {
            Tools.renderEllipsis();
            var _el = $(this),
               relatedTarget =$(e.relatedTarget);
               boxFieldId   = relatedTarget.data('id');
               isCustom     = relatedTarget.data('iscustom');
               editTemplate = _.template($('#edit-personadata-template').html()),
               dataId       = relatedTarget.closest('.persona-section-box').data('id');
               
            All.appendId = isCustom ? 'custom': '';
            All.currentBoxId = boxFieldId;
            
            if(boxFieldId.length == 0 ) {
                relatedTarget.parent().parent().find('.custom-field-name').tooltip('show');
                return false;
            }
            //display all delete buttons
            
            var modalTitle = (!isCustom) ? relatedTarget.data('title') : 'Custom Box: Edit Title or List' ;
            $('#field' + All.appendId + boxFieldId + 'p' + All.allPersonaId + ' .persona-main-box-content').find('.custom-field').each(function(v){
                _el.find('.box-modal .persona-main-box-content .fixed-field').append(editTemplate({value : $(this).text()}));
            });
            _el.find('.box-modal .persona-main-box-content .fixed-field').attr('data-field-id', boxFieldId);
            _el.find('.modal-title').html("<b>" + modalTitle + "</b>");
            _el.find('.persona-main-box-content').attr('data-iscustom', isCustom);
            _el.find('.persona-main-box-content').attr('data-id', dataId);
            
            if(isCustom) {
                _el.find('.persona-main-box').attr('data-id', boxFieldId);

            }

        }).on('click', '.delete-persona-data', function() {
            $(this).closest('li').remove();
        }).on('hidden.bs.modal', function (e) {
            All.hasUpdate($("#field"+ All.appendId + All.currentBoxId  + 'p' + All.allPersonaId));
        }).on('hide.bs.modal', function (e) {
            var element = $("#field" + All.appendId + All.currentBoxId  + 'p' + All.allPersonaId).position();
            $(window).scrollTop(element.top);
            $(this).find('.box-modal .persona-main-box-content .fixed-field').html('');

        });
        $(document).on('click', '.editable', function(e){

            All.validateFullName($(this));

            if( !$(this).hasClass('editing') && $(this).find('select').length !== 1){
                var contents = $(this).html();
                var height = $(this).height();
                $(this).addClass('econtentsditing');
            }
        }).on('blur', '.editable', function(){
            var contents = $(this).val();
            if(contents.length == 0 && $(this).hasClass('persona-name')) {
                return false;
            }

            if($(this).hasClass('persona-name')) {
                $('.selectable > button').removeAttr("disabled").removeClass('is_disabled');
                $('.editable').removeAttr("disabled").removeClass('is_disabled');
                $('input.custom-field-name').removeAttr("disabled").removeClass('is_disabled');
            }
            // save data base on data-field
            All.progress(50, $('#progressBar') );
            All.savePersona($(this), contents);

            $(this).html( contents );
            $(this).removeClass('editing');

            switch( $(this).attr('data-field') ){
                case 'name':
                    $('.personabox-add span').html(' About ' + contents);
                    break;
            }
        }).on('click', '.selectable > button', function(){
            All.validateFullName($(this));
        }).on('change', 'select.selectable', function() {
            var el = $(this);
            var contents = el.val();
            All.updateSelectableDescription(el);
            All.savePersona(el, contents);
            All.progress(50, $('#progressBar') );
        }).on('click', '.addPersona', function(){
            if(!All.validateFullName()) {
                return false;
            }
            var personaBoxTemplate = _.template($('#persona-data-template').html());
            var newEl =  $('.persona_data' + All.allPersonaId).prepend(personaBoxTemplate({id: "", personaId:"", type: "", sectionName:"", title: "", type: 'list', content: "<ul></ul>"}));
            // enable edit title
            newEl.find('input.custom-field-name').attr('disabled',false);
            setTimeout(function(){
                newEl.find('.new').removeClass('new');
            },0);
        }).on('shown.bs.modal', '#personaBoxModal',  function(){
            if ( $(this).find('ul').find('#add-new-list-item').length == 0 ) {
                $(this).find('ul').append('<li id="add-new-list-item">Click to add new list item here...</li>');
                $('#add-new-list-item').click();
                $('#add-new-list-item > input.custom-field').focus();
            }
        }).on('mouseenter', '.persona-sidebar-box', function() {
            $(this).find('.button-tool-container .gear-btn').tooltip('show');
        }).on('mouseleave', '.persona-sidebar-box', function() {
            $(this).find('.button-tool-container .gear-btn').tooltip('hide');
        }).on('blur', 'li:last-child .custom-field', function(){
            if ( $.trim($(this).val()).length != 0 && $(this).closest('ul').find('#add-new-list-item').length != 0 ||
                 $(this).closest('ul').find('li:last-child input.custom-field').length > 0 && $.trim($(this).closest('ul').find('li:last-child input.custom-field').val()).length != 0 ) {
                $(this).closest('ul').append('<li id="add-new-list-item">Click to add new list item here...</li>');
                $('#add-new-list-item').click();
                $('#add-new-list-item > input.custom-field').focus();
            }
        }).on('click', '#personaBoxModal .persona-main-box-content .fixed-field li', function(e){
            var _boxTitle = $(this).parent().parent().prev().find('.custom-field-name');
            if(_boxTitle.length !== 0 && _boxTitle.val().trim().length == 0 ) { // field is from custom
                $('[data-toggle="tooltip"]').tooltip();
                _boxTitle.tooltip('show');
                return false;
            } else { // field is from fixed
                if(!All.validateFullName($(this))) {
                    return false
                }
            }
            if( $(this).attr('id') == 'add-new-list-item' ){
                $(this).attr('id', '');
            }
            if( !$(this).hasClass('editing') ){
                if ($(this).closest('ul').find('li:last-child input.custom-field').length == 0 ||
                    $(this).closest('ul').find('li:last-child input.custom-field').length > 0 && $(this).closest('ul').find('li:last-child input.custom-field').val().length != 0) {
                    $(this).addClass('editing');

                        $(this).html( '<input class="custom-field" placeholder="Add new list item" type="text"/>' );

                    $(this).find('input').focus();
                }
            }

        }).on('click', 'input.custom-field-name', function(e){
            if($('#persona-name').val().length == 0) {
                $(this).attr('disabled','disabled');
                All.errorfullName();
                return false;
            }

            if( !$(this).hasClass('editing') ){
                $(this).addClass('editing');
            }

        }).on('blur', 'input.custom-field-name', function(){
            var contents = $(this).val();
            var _parent = $(this).parent().parent();
            if( contents != '' ){
                All.progress(50, $('#progressBar') );

                var bid = _parent.attr('data-id');
                if( bid == '' ){
                    var uid = (new Date).getTime();
                    _parent.attr('id', uid );
                }
                $('.persona_data ' + All.allPersonaId + ' .persona-main-box ul li input').attr('disabled', true);

                if(!contents.trim().length == 0 ) {
                    All.progress(50, $('#progressBar'));
                    All.savePersonaData('title', contents, _parent.attr('data-id'), uid);
                }
                
                $(this).html( contents );
                $(this).removeClass('editing');
            }else{
                $(this).html( $(this).find('input').attr('data-org') );
                $(this).removeClass('editing');
            }


        }).on('click','#archivePersona', function() {
            All.deletePersona();
        });
        if($.trim($('#persona select.selectpicker').val()).length > 0) {
            All.updateSelectableDescription($('#persona select.selectpicker'));
        }
        
        $('.persona-section-box .button-tool-container .fa.fa-trash').click(function(){
            $('#archive-custom-data').attr('data-custom-id', $(this).data('id'));
        })
        
        $('#confirmArchivePersonaData').on('hide.bs.modal', function(){
            $('#archive-custom-data').removeAttr('data-custom-id');
        });
        
        $('#archive-custom-data').click(function(){
            var personaDataId = $(this).data('custom-id');
            $.ajax({
                dataType: "json",
                type: "POST",
                url: "/persona/process/delete-persona-data",
                success: function(response) {
                    if (response.success) {
                        window.location.reload();
                    } else {
                        alert('An error occured. Please try again later.');
                    }
                },
                data: {
                    persona_id       : $('#persona').attr('data-pid'),
                    persona_data_id  : personaDataId,
                    revisionId       : revisionId,
                    uid              : (new Date).getTime()
                },
                error: function() {
                    alert('An error occured. Please try again later.');
                }
            });
        });
        
        $('.questionItem').each(function(){
          $(this).after('<input type="text" placeholder="Input Custom Title" class="custom-titles" data-value="'+$(this).val()+'" />');
        });
        
        $('.navbar-left .btn-share-persona').removeClass('hide');
    },
    hasUpdate: function(el) {
        el.addClass('new');
        setTimeout(function(){
            el.removeClass('new');
        },1000);
    },
    renderLoader: function(loadTo, remove)
    {
        remove = typeof remove !== 'undefined' ? remove : false;
        if(!remove) {
            loadTo.append('<div class="loader" ><img src="/img/ajax-loader.gif" alt=""></div>');
        } else {
            loadTo.find('.loader').remove();
        }
    },
    removeLoader: function() {
        $('.loader').remove();
    },
    renderGenderSlider: function(elSlider, personaGender) {
        var _el = $(elSlider),
            _male = 50,
            _female = 50;
        if(personaGender.length || typeof personaGender == "object") {
            if(typeof personaGender == "string") {
                personaGender = JSON.parse(personaGender);
            }
            _male = personaGender.male;
            _female = personaGender.female;
        }
        _el.slider({
            value: _male,
            min: 0,
            max: 100,
            step: 1,
            range: "min",
            slide: function( event, ui ) {
                _el.siblings('.male').html(ui.value + '%');
                _el.siblings('.female').html((100 - parseInt(ui.value)) + '%' );
            },
            stop: function(event, ui) {
                _male = ui.value;
                _female = 100 - parseInt(ui.value);
                persona_gender = JSON.stringify({"male": _male, "female": _female});
                if(All.validateFullName($(this))) {
                    All.agegender['gender'] = JSON.stringify({'male': _male, 'female': _female});
                    All.progress(50, $('#progressBar') );
                    All.loadGenderPie('.canvas-gender-modal', persona_gender);
                }
            },
            change: function(event, ui) {
            },
            create: function(event, ui) {
                All.loadGenderPie('.canvas-gender-modal', personaGender);
                _el.siblings('.male').html(_male + '%');
                _el.siblings('.female').html((100 - parseInt(_male)) + '%' );
            }
        });
    },
    loadGenderPie: function(el, personaGender) {
        var _el = $(el),
        _male = 50,
        _female = 50,
        _elCanvasId = (new Date).getTime();

        if(personaGender.length || typeof personaGender == "object") {
            if(typeof personaGender == "string") {
                personaGender = JSON.parse(personaGender);
            }
            _male = personaGender.male;
            _female = personaGender.female;
        }


        _el.find('canvas').remove();
        _el.append("<canvas id='" + _elCanvasId + "'><canvas>");
        var ctx = $('#'+ _elCanvasId)[0].getContext("2d");

        var data = [{
            value: _male,
            color: "#46BFBD",
            highlight: "#5AD3D1",
            label: "Male"
        }, {
            value: _female,
            color: "#F7464A",
            highlight: "#FF5A5E",
            label: "Female"
        }];
        var options = {
            // String - Template string for single tooltips
            tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= value + ' %' %>",
            // String - Template string for multiple tooltips
            multiTooltipTemplate: "<%= value + ' %' %>",
        };

        var myNewChart = new Chart(ctx).Pie(data, options);
        _el.next('.genderLegendDiv').html(myNewChart.generateLegend());
    },
    renderAgeSlider: function(el, personaAge, isEditable) {
        /**
         * AGE slider ui Jquery
         */
        var _el = $(el),
            _min = 0,
            _max = 100;
        if(personaAge.length || typeof personaAge == "object") {
            if(typeof personaAge == "string") {
                personaAge = JSON.parse(personaAge);
            }

            _min = personaAge.min;
            _max = personaAge.max;
        }
        _el.slider({
            range:true,
            min: 0,
            max: 100,
            values: [ _min , _max],
            slide: function( event, ui ) {
                if(_el.siblings('.age-min').length != 0 && _el.siblings('.age-max').length !=0 ) {
                    _el.siblings('.age-min').html(ui.values[ 0 ]);
                    _el.siblings('.age-max').html(ui.values[ 1 ]);
                }
            },
            stop: function(event, ui ) {
                persona_age = JSON.stringify({"min": ui.values[ 0 ], "max": ui.values[ 1 ]});
                if(All.validateFullName($(this))) {
                    All.agegender['age'] = JSON.stringify({"min": ui.values[ 0 ], "max": ui.values[ 1 ]});
                    All.progress(50, $('#progressBar') );
                }
            },
            change: function(event, ui) {
                _el.siblings('.age-min').html(ui.values[ 0 ]);
                _el.siblings('.age-max').html(ui.values[ 1 ]);
            },
            create: function(event, ui) {
                if(_el.hasClass('non-editable')) {
                    _el.slider('disable');
                }
            }
        });
        _el.siblings('.age-min').html(_el.slider( "values", 0 ));
        _el.siblings('.age-max').html(_el.slider( "values", 1 ));
    },
    renderPersonaFields: function(response, targetContainer, personaId) {
        personaId = typeof personaId !== 'undefined' ? personaId : $('#persona').attr('data-pid');
        if(Object.keys(response).length != 0 && response.hasOwnProperty('personaFields')){
            $('.persona-main-box').show();
            var personaFixedFieldsTemplate = _.template($('#persona-fixed-data-template').html());
            targetContainer.html('');
            $.each(response.personaFields, function(k, v) {
                if ( v.type == 'list' ) {
                    targetContainer.append(personaFixedFieldsTemplate({
                        id:     v.id,
                        personaId: personaId,
                        title:  v.title
                    }));
                }
            });
        }
    },
    renderPersonaData: function (response, isClearList) {
        isClearList = typeof isClearList !== 'undefined' ? isClearList : false;
        
        if ( typeof response.data != 'undefined' ) {
            response = response.data;
        }
        
        // clear the lists
        if(isClearList) {
            $('.persona-main-box ').find('ul').html('');
        }
        if(Object.keys(response).length != 0){
            $('.persona-main-box').show();

            $.each(response, function(k, v) {
                var persona_field_id = v.personaFieldId,
                    boxid = v.id,
                    items = (v.content !== null) ? JSON.parse(v.content) : '';

                if ( $('#personaBoxModal').hasClass('in') ) {
                    editPersonaDataTemplate = _.template($('#display-personadata-template').html());
                } else {
                    editPersonaDataTemplate = _.template($('#display-personadata-template').html());
                }
                if(persona_field_id == null) {
                    var personaBoxContent = $('.persona-main-box#fieldcustom' +  v.id + 'p' + v.personaId);

                    if(typeof(items) == 'object') {
                        $.each(items, function(k,v) {
                            personaBoxContent.find('ul').append(editPersonaDataTemplate({value: v.item}));
                        });
                    }
                } else {
                    
                    if ( $('.persona-main-box#field' + persona_field_id + 'p' + v.personaId).length == 0 ) {
                        $('.persona-main-box#field' + persona_field_id + 'p').attr('id', 'field' + persona_field_id + 'p' + v.personaId);
                    }
                    var _field = $('.persona-main-box#field' + persona_field_id + 'p' + v.personaId);
                    
                    if(typeof(items) == 'object') {
                        $.each(items, function (k, v) {
                            if(v.hasOwnProperty('item')) {
                                _field.find('ul').append(editPersonaDataTemplate({value: v.item}));
                            } else {
                                _field.find('ul').append(editPersonaDataTemplate({value: v}));
                            }

                            _field.attr('data-id', boxid);
                        });
                    }
                }
            });
            $( ".persona-section-box" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" );
        }

        $('[data-toggle="tooltip"]').tooltip();
    },
    renderMsLocation: function(el, is_disabled) {
        is_disabled = typeof is_disabled !== 'undefined' ? is_disabled : true;
        var _location = [
            {"id":0, "name":"Paris"},
            {"id":1, "name":"New York"},
            {"id":2, "name":"Cleveland"},
            {"id":3, "name":"London"}
        ];

        var msLocation = $(el).magicSuggest({
            placeholder: "Type some location",
            maxSelection: null ,
            allowFreeEntries: !1,
            noSuggestionText: "No result matching the term <i>{{query}}</i>",
            data: _location,
            disabled: is_disabled,
        });
    },
    validateFullName: function(el) {
        //disable slider
        if($('#persona-name').val().length == 0) {

            var  _el = el;
            // disabling input elements
            if(typeof(_el) != 'undefined') {
                _el = el.not('#persona-name');
                _el.attr('disabled','disabled').addClass('is_disabled');
            }

            All.errorfullName('#persona-name');
            return false;
        }
        return true;
    },
    errorfullName: function(el) {
        var fullName =$(el);
        fullName.tooltip('show');
        fullName.addClass('error_dashed');
        fullName.focus();
    },
    savePersonaData: function(field, contents, persona_data_id, uid) {
        uid = (typeof uid !== 'undefined')? uid : null;
        persona_field_id = (field == 'content' && uid != 0)? uid : null;
        $.ajax({
            dataType: "json",
            type: "POST",
            url: "/persona/process/savePersonaData",
            success: All.personaDataSaved,
            data: {
                content          : contents,
                field            : field,
                persona_field_id : persona_field_id,
                persona_id       : $('#persona').attr('data-pid'),
                uid              : uid,
                persona_data_id  : persona_data_id,
                revisionId       : revisionId
            }
        });
    },
    deletePersona: function() {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: "/persona/process/savePersona",
            success: function() {
                location.href = '/audience'
            },
            data: {
                content    : true,
                field      : 'delete',
                persona_id : $('#persona').attr('data-pid'),
                revisionId : revisionId
            },
        });
    },
    savePersona: function(el, contents) {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: "/persona/process/savePersona",
            success: All.dataSaved,
            data: {
                content    : contents,
                field      : el.attr('data-field'),
                persona_id : $('#persona').attr('data-pid'),
                revisionId : revisionId
            },
        });
    },
    savePersonaDetails: function(modalObj, content) {
        
        $.ajax({
            dataType: "json",
            type: "POST",
            url: "/persona/process/savePersonaDetails",
            success: function(response) {
                All.dataSaved(response);
                All.renderAgeSlider('#age-slider', persona_age, false);
                All.loadGenderPie('.canvas-gender', persona_gender);
                All.removeLoader();
                modalObj.modal('hide');
            },
            data: {
                content    : content,
                persona_id : $('#persona').attr('data-pid'),
                revisionId : revisionId
            },
        });
    },
    personaDataSaved: function( response ){
        if(Object.keys(response).length != 0 && response.success && response.hasOwnProperty('persona_data')) {
            var persona_data = response.persona_data,
                 _customField = $('#' + persona_data.uid),
                persona_data_id = persona_data.persona_data_id;

            if( _customField.length ){
                _customField.attr('data-id', persona_data_id);
                _customField.attr('id', "fieldcustom" + persona_data_id + 'p' + All.allPersonaId);
                _customField.find('i.gear-btn').attr('data-id', persona_data_id);
                _customField.find('i').removeClass('hide');
            }

            var _fixedField = $('.persona-main-box#field' + persona_data.persona_field_id + 'p' + persona_data.persona_id);
            if(_fixedField.length != 0) {
                var box_id = persona_data.persona_data_id;
                _fixedField.attr('data-id', box_id);
            } else {
                var _fixedField = $('#'+persona_data.uid);
                if ( _fixedField.length != 0 ) {
                    var box_id = persona_data.persona_data_id;
                    _fixedField.attr('data-id', box_id);
                }
            }
        }
        All.progress(100, $('#progressBar') );
        All.hasUpdated = true;

    },
    personaSaveCounter: 0,
    dataSaved: function(response) {

        if(Object.keys(response.success).length != 0 && response.success.hasOwnProperty('persona_id') && $('#persona').attr('data-pid').length == 0) {
            $('#persona').attr('data-pid', response.success.persona_id);
            $('#persona').attr('data-uuid', response.success.uuid);
            window.history.replaceState(null, null, "/persona/" + response.success.uuid);
            $('.navbar-righ.persona-options').removeClass('hide');

            if ( All.personaSaveCounter == 0 ) {
                personaPage.exportTemplatesView.personaUuid = response.success.uuid; // for export template
                $('.btn-compare').attr('href',(($('.btn-compare').attr('href')).replace('persona%5B%5D=0', 'persona%5B%5D='+response.success.uuid)));
                All.personaSaveCounter++;
            }

            if($('.headshot').length != 0) {
                All.savePersona($('.headshot'), headshots[All.image_index]);
            }
            
            if ( $('.page-navigation .pull-left .navbar-left > li').length <= 0 ) {
                var navTemplate = _.template($('#left-nav-template').html());
                $('.page-navigation .pull-left .navbar-left').html(navTemplate({uuid : response.success.uuid}))
            }
        }
        All.progress(100, $('#progressBar') );
    },
    progress: function(percent, $element) {
        $element.find('div').width(1);
        $element.show();
        var progressBarWidth = percent * $element.width() / 100;
        $element.find('div').animate({ width: progressBarWidth }, 500, function(){
            $element.fadeOut(8000);
        }).html("<span>" + percent + "% Saved.</span>");

        $('#archive').show();
    },

    updateSelectableDescription: function (el) {
        if((typeof el == 'object' && el.length > 1)) {
            $.each(el, function(k, v) {
                All.updateSelectableDescription($(el[k]));
            });
        } else {
            var description = el.find(':selected').data('description');
            $('.' + el.data('field') + '-description').html(description).show();
        }

    },
    getPersonaData: function(personaId) {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: "/persona/process/getPersonaData",
            success: All.renderPersonaData,
            data: {
                id: personaId
            }
        });
        
        //remove ajax loader
        All.removeLoader();
    },

    /**
     * Display Slider
     * @param data
     * @param sliderContainer
     * @param isEditable
     * @returns {boolean}
     */
    addSlider: function(data, sliderContainer, isEditable) {
       
        var uniqueID = (new Date).getTime().toString();

        var _status = (isEditable) ? '' : 'non-editable';
        var dataType = data.type_name; // motivation, channel, knowledge
        var dataId = data.data_id;
        var sliderUniqueId = dataType +'Slider' + uniqueID;
        if(data.new) {
            var sliderTpl = _.template($('#slider-template').html());
            $(sliderContainer).append( sliderTpl({
                id: data.id,
                typeName: dataType,
                sliderUniqueId: sliderUniqueId,
                dataId: dataId,
                value: data.value,
                name: data.name,
                dataStatus: _status
            }));
        } else { //DELETE A SLIDER
            var itemSlider = $(sliderContainer + ' #' + sliderUniqueId); // eg: $('.slider-container channelSlider123')
            itemSlider.parent().hide();
            All.saveSlider(itemSlider, 50, true); // set value to true to delete slider
            return false;
        }
        // eg: $('container #channelSlider1')
        var elSlider = $(sliderContainer + ' #' + sliderUniqueId); // eg: $('.slider-container channelSlider123')
        elSlider.slider({
            min: 0,
            max: 100,
            value: elSlider.attr('data-value'),
            step: 1,
            range: "min",
            stop: function (event, ui) {
                var $slider = $(this);
                All.saveSlider($slider, ui.value);
            },
            create: function( event, ui ) {
                var $slider = $(this);

                var randomBg = '#' + Math.random().toString(16).slice(2, 8).toUpperCase();
                if($slider.hasClass('non-editable')) {
                    $slider.slider('disable');
                }
                $slider.find('.ui-widget-header').css('background',randomBg);
            }
        });
    },
    validateEmail: function(email)
    {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
};
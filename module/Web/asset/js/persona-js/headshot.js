var Headshots = {
    process: function (el) {
        var bg          = $(el).find('img').attr('src'),
            imgHeadshot = '';
        if( bg == '' ){
            if ( typeof headshots != 'undefined' ) {
                $(el).attr('style', 'background:url("' + headshots[0] + '") no-repeat center');
                imgHeadshot = headshots[0];
            }
        } else {
            $(el).attr('style', 'background:url("' + bg + '") no-repeat center');
            imgHeadshot = bg;
        }
        $('.modal-headshot').attr('src',imgHeadshot);
        $(el).hover(function(){
            $('.persona-nav').fadeIn();
        }, function(){
            $('.persona-nav').fadeOut();
        });
        
        $('#uploadHeadshotModal').on('shown.bs.modal', function() {
            
            $('.file-ready').remove();
            currentHeadshot = $('.persona-image-container').data('image');
            
            $('.browse-headshot-library').html('');
            $('.filter-headshot .filter-container').html('');
            $('#persona-headshot').val('');
            $('.filter-headshot .filter-container').append('<li data-filter="all">All</li>');
            headshotTemplate = _.template($('#headshot-template').html());
            arrHeadshot = [];
            _.each (JSON.parse(headshots), function(v,k) {
                hdata = v;
                hdata.active = '';
                
                if ( v.path == currentHeadshot ) {
                    hdata.active= 'active';
                } 
               $('.browse-headshot-library').append(headshotTemplate(hdata));
               
               if ( typeof arrHeadshot[v.filter] == 'undefined' ) {
                   arrHeadshot[v.filter] = [];
                   $('.filter-headshot .filter-container').append('<li data-filter="'+v.filter+'">'+v.filter+'</li>');
               }
               arrHeadshot[v.filter].push(v);
            });
            $('.browse-headshot-library .headshot').click(function() {
                $('.browse-headshot-library .headshot').removeClass('active');
                $(this).addClass('active');
                $('#persona-headshot').val($(this).data('image'));
            });
            $('.filter-headshot .filter-container > li').click(function() {
                
                $('.filter-headshot .filter-container > li').removeClass('active');
                $(this).addClass('active');
                $('.browse-headshot-library').html('');
                if ( $(this).data('filter') == 'all' ) {
                    data = JSON.parse(headshots);
                } else {
                    data = arrHeadshot[$(this).data('filter')];
                }
                
                if ( Object.keys(JSON.parse(JSON.stringify(data))).length ) {
                    _.each (JSON.parse(JSON.stringify(data)), function(v,k) {
                        hdata        = v;
                        hdata.active = '';
                        if ( hdata.path == currentHeadshot ) {
                            hdata.active = 'active';
                        }
                        $('.browse-headshot-library').append(headshotTemplate(hdata));
                     });
                 }
                 $('.browse-headshot-library .headshot').click(function() {
                    $('.browse-headshot-library .headshot').removeClass('active');
                    $(this).addClass('active');
                });
            });
        });
        
        $('.btn-save-headshot').click(function() {
            All.renderLoader($(this));
            $.ajax({
                dataType: "json",
                type: "POST",
                url: "/persona/process/savePersona",
                success: function() {
                    $('div.persona-sidebar-box.headshot').attr('style', 'background:url("'+$('#persona-headshot').val()+'") no-repeat center');
                    $('#persona-headshot').val('');
                    $('#uploadHeadshotModal').modal('hide');
                },
                data: {
                    content    : $('#persona-headshot').val(),
                    field      : 'headshot',
                    persona_id : $('#persona').attr('data-pid'),
                    revisionId : revisionId
                },
            });
        });
    }
};
SharePersona = {
    init: function() {
        
        $('#sharePersonaModal').on('shown.bs.modal', function(){
            var sharedUrl = window.location.origin +'/share/'+shareData.shortCode;
            $('#txt-shared').val(sharedUrl);
            $('.btn-open-window').attr('href', sharedUrl);
            
            var ms = $('#sharePersonaModal #shareEmail').magicSuggest({
                placeholder: 'Enter valid email address',
                vtype: 'email',
                hideTrigger: true
            });
            
            $('#switch-toggle').prop('checked', false);
            if ( shareData.publicFlag ) {
                $('#switch-toggle').prop('checked', true);
            }
            $('#chk-enable-password').prop('checked', false);
            if ( shareData.password != null ) {
                $('#txt-password').val(shareData.password);
                $('#chk-enable-password').prop('checked', true);
                $('#txt-password').removeClass('hide');
                $('#btn-add-pw').removeClass('hide');
            }
            
            $('#chk-enable-password').change(function(){
                if ( $(this).is(':checked') ) {
                    $('.btn-add-pw').removeClass('hide');
                    $('#txt-password').removeClass('hide');
                } else {
                    $('#txt-password').addClass('hide');
                    $('.btn-add-pw').addClass('hide');
                }
            });
        });
        
        $('#switch-toggle').change(function() {
           $.ajax({
                dataType: "json",
                type: "POST",
                url: "/persona/process/enable-sharing",
                success: function(response) {
                    if ( !response.success) {
                        shareData.publicFlag = $('#switch-toggle').is(':checked') ;
                        alert('An error occured. Please try again later.');
                    }
                },
                data: {
                    enableSharing : ( $(this).is(':checked') ? 1 : 0 ),
                    persona_id    : $('#persona').attr('data-pid')
                },
                error: function() {
                    alert('An error occured. Please try again later.');
                }
            });
        });
        
        $('#sharePersonaModal .btn-share-persona').click(function(){
            
            var emails    = $('#shareEmail').magicSuggest().getValue(),
                shareData = 'persona_id='+$('#persona').attr('data-pid')+'&emails='+emails.join(',');
            
            for( i = 0; i < (emails).length ; i++ ) {
                if (!All.validateEmail(emails[i])) {
                    alert('Some of the email is invalid. Please enter valid emails.');
                    return;
                }
            }
            
            $.ajax({
                dataType: "json",
                type: "POST",
                url: "/persona/process/share-persona",
                success: function(response) {
                    if (response.success) {
                        alert('Persona has been successfully shared.');
                    } else {
                        alert('An error occured. Please try again later.');
                    }
                },
                data: shareData,
                error: function() {
                    alert('An error occured. Please try again later.');
                }
            });
        });
        
        $('#sharePersonaModal .btn-add-pw').click(function() {
            
            var shareData = $('#frm-secure-persona').serialize()+'&persona_id='+$('#persona').attr('data-pid')+'&havePassword='+($('.enable-share #chk-enable-password').is(':checked') ? 1 : 0);
            
            $.ajax({
                dataType: "json",
                type: "POST",
                url: "/persona/process/enable-persona-password",
                success: function(response) {
                    if (response.success) {
                        shareData.password = $('#txt-password').val();
                        alert('Password has been successfully saved.');
                    } else {
                        alert('An error occured. Please try again later.');
                    }
                },
                data: shareData,
                error: function() {
                    alert('An error occured. Please try again later.');
                }
            });
        });
        
        $('#sharePersonaModal').on('hide.bs.modal', function() {
            $('.btn-open-window').attr('href', '#');
        });
    }
};
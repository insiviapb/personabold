var Audience = Backbone.View.extend({
    initialize: function (data) {
        // persona view
        this.personaModel   = data.personaModel;
        this.audiencePersonaView        = data.audiencePersonaView;

        // industry description
        this.industryDescriptionModel = data.industryDescriptionModel;
        this.saveIndustryDescriptionModel = data.saveIndustryDescriptionModel;

        // persona client
        this.addPersonaClientModel = data.addPersonaClientModel;
        this.getClientPersonaModel = data.getClientPersonaModel;
        this.checkPackagesModel    = data.checkPackagesModel;
        this.personaClientView     = data.personaClientView;
        
        if ( $('.audience-persona').length > 0 ) {
            this.renderAudiencePersona();
        }
        
        if ( $('#audience').length > 0 ) {
            this.renderAudienceClient();
        }

    },

    renderAudiencePersona: function() {
        this.personaModel = new this.personaModel;
        this.saveIndustryDescriptionModel = new this.saveIndustryDescriptionModel;
        this.industryDescriptionModel = new this.industryDescriptionModel;
        this.audiencePersonaView  = new this.audiencePersonaView({
            el                          : $('.audience-persona'),
            personaModel                : this.personaModel,
            personaTemplate             : $('#persona-box-template').html(),
            industryDescriptionModel    : this.industryDescriptionModel,
            saveIndustryDescriptionModel: this.saveIndustryDescriptionModel,
            industryDescriptionTemplate : $('#industry-description-template').html(),
            newIndustryDescriptionTemplate : $('#new-industry-description-template').html(),
            industryAddPersonaTemplate : $('#industry-add-persona-template').html()
        });
    },
    renderAudienceClient: function() {
        this.addPersonaClientModel = new this.addPersonaClientModel;
        this.getClientPersonaModel = new this.getClientPersonaModel;
        this.checkPackagesModel    = new this.checkPackagesModel;

        this.personaClientView     = new this.personaClientView({
           el                      : $('#audience'),
           personaClientTemplate   : $('#persona-client-template').html(),
           clientListMainTemplate  : $('#client-list-main').html(),
           addClientTemplate       : $('#persona-add-client').html(),
           activityMainTemplate    : $('#persona-revision-main').html(),
           activityListTemplate    : $('#persona-revision-list').html(),
           activityCompanyTemplate : $('#activity-company-list').html(),
           addPersonaClientModel   : this.addPersonaClientModel,
           getClientPersonaModel   : this.getClientPersonaModel,
           checkPackagesModel      : this.checkPackagesModel,
        });
    }
});
var audiencePage = new Audience({
    el                          : $('.audience-section'),
    audiencePersonaView         : AudiencePersonaView,
    personaModel                : PersonaModel,
    industryDescriptionModel    : IndustryDescriptionModel,
    saveIndustryDescriptionModel: SaveIndustryDescriptionModel,
    personaClientView           : PersonaClientsView,
    addPersonaClientModel       : AddPersonaClientModel,
    getClientPersonaModel       : GetClientPersonaModel,
    checkPackagesModel          : CheckPackagesModel
});
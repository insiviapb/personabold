AudiencePersonaView = Backbone.View.extend({
    events: {
        'click .filter-industry'          : 'filterPersonByIndustry',
        'blur #industry-description-text' : 'saveIndustryDescription',
        'click .add-persona-group'        : 'addPersonaGroup'
    },
    initialize: function (data) {
        this.personaModel                   = data.personaModel;
        this.personaTemplate                = data.personaTemplate;
        this.industryDescriptionTemplate    = data.industryDescriptionTemplate;
        this.newIndustryDescriptionTemplate = data.newIndustryDescriptionTemplate;
        this.industryAddPersonaTemplate     = data.industryAddPersonaTemplate;
        this.industryDescriptionModel       = data.industryDescriptionModel;
        this.saveIndustryDescriptionModel   = data.saveIndustryDescriptionModel;
        this.personaModel.bind('change', this.displayPersona, this);
        this.industryDescriptionModel.bind('change', this.displayIndustryDescription, this);
    },
    saveIndustryDescription: function(el) {
        var currentTarget    = this.$el.find(el.currentTarget);
        var industry_id      = currentTarget.data('id');
        var content          = currentTarget.val();
        var original_content = currentTarget.data('original');
        if(content != original_content) {
            All.progress(100, $('#progressBar') );
            this.processIndustry = this.saveIndustryDescriptionModel.fetch({
                data: {
                    industry_id: industry_id,
                    description: content
                },
                type: 'POST',
                beforeSend: function() {
                    if(typeof audiencePage.audiencePersonaView.processIndustry != 'undefined' && audiencePage.audiencePersonaView.processIndustry != null) {
                        audiencePage.audiencePersonaView.processIndustry.abort();
                    }
                }
            });
        }

    },
    displayIndustryDescription: function() {
        var result = this.industryDescriptionModel.toJSON();
        var industryDescTemplate  = _.template(this.industryDescriptionTemplate);
        var newIndustryDescTemplate  = _.template(this.newIndustryDescriptionTemplate);
        var industryDescContainer = $('#industry-description');
        industryDescContainer.html('');
        if(result.success) {
            industryDescContainer.append(industryDescTemplate(result.industry));
        } else {
            industryDescContainer.append(newIndustryDescTemplate(result.industry));
        }
    },
    displayPersona: function() {
        var result = this.personaModel.toJSON();
        if(result.success) {
            var personBoxTemplate = _.template(this.personaTemplate);
            var addPersonaTemplate = _.template(this.industryAddPersonaTemplate);
            var personaContainer = this.$el.find('#persona-container');
            personaContainer.html('');
            if(Object.keys(result.persona).length != 0) {
                _.each(result.persona, function (v, k) {
                    personaContainer.append(personBoxTemplate(v));
                });
                personaContainer.append(addPersonaTemplate());
            } else {
                personaContainer.append('<i class="text-muted">There are no persona yet. <a href="/persona">Add now</a>.</i>');
            }

        }
    },
    getIndustryDescription: function(el) {
        this.processIndustry = this.industryDescriptionModel.fetch({
             data: {
                industry: this.$el.find(el.currentTarget).data('id')
             },
            type: 'POST',
            beforeSend: function () {
                if(typeof audiencePage.audiencePersonaView.processIndustry != 'undefined' && audiencePage.audiencePersonaView.processIndustry != null) {
                    audiencePage.audiencePersonaView.processIndustry.abort();
                }
            }
        });
    },
    filterPersonByIndustry: function(el) {
        this.processPersona = this.personaModel.fetch({
            data: {
                industry: this.$el.find(el.currentTarget).data('id')
            },
            type: 'POST',
            beforeSend: function () {
                if(typeof audiencePage.audiencePersonaView.processPersona != 'undefined' && audiencePage.audiencePersonaView.processPersona != null) {
                    audiencePage.audiencePersonaView.processPersona.abort();
                }
            }
        });
        audiencePage.audiencePersonaView.getIndustryDescription(el);
    },
    addPersonaGroup: function() {
        if ( haveCards ) {
            window.location = '/persona';
        } else {
            $('#notifStripeCardsModal').modal('show');
        }
    }
});
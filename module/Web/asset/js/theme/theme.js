Theme = Backbone.View.extend({
    initialize: function (data) {
        this.mainView         = data.mainView;
        this.colorUpdate      = data.colorUpdate;

        if ( $('.color-management').length > 0 ) {
            this.mainView = new this.mainView({
                el            : $('.theme-management'),
                colorTitle    : ['#app-color', '#primary-color', '#secondary-color', '#tertiary-color'],
                colorUpdate   : this.colorUpdate,
                accountColors : accountColors
            });
        }
    }
});
if ( $('.theme-management').length > 0 ) {
    theme = new Theme({
        el               : $('.theme-management'),
        mainView         : MainView,
        colorUpdate      : new ColorUpdateModel()
    });
}

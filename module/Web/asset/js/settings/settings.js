var Settings = Backbone.View.extend({
    initialize: function (data) {
        // company profile
        this.uploadPhotoModel = data.uploadPhotoModel;
        this.overview         = data.overviewView;
        // industry view
        this.industryModel    = data.industryModel;
        this.industry         = data.industryView;
        // users view
        this.usersModel       = data.usersModel;
        this.updateUserModel  = data.updateUserModel;
        this.deleteUserModel  = data.deleteUserModel;
        this.users            = data.usersView;
        // user profile view
        this.userProfileView    = data.userProfileView;
        this.checkPackagesModel = data.checkPackagesModel;
        // account subscription info
        this.subscriptionInfo        = data.subscriptionInfoView;
        this.addUpdateCardsModel     = data.addUpdateCardsModel;
        this.addUpdateBillingModel   = data.addUpdateBillingModel;
        this.updateSubscriptionModel = data.updateSubscriptionModel;
        // for persona template view
        this.personaTemplateView   = data.personaTemplateView;
        // for persona template details
        this.personaTemplateModel  = data.personaTemplateModel;
        this.templateDetailsView   = data.templateDetailsView;
        this.publishPersonaModel   = data.publishPersonaModel;
        // persona template integration 
        this.templateDescriptionView = data.templateDescriptionView;
        // settings alert
        this.updateNotificationsView  = data.updateNotificationsView;
        this.updateNotificationsModel = data.updateNotificationsModel;
        // settings of persona fields
        this.personaFieldsView   = data.personaFieldsView;
        this.personaFieldsModel   = data.personaFieldsModel;
        // export pdf
        this.exportFilesView  = data.exportFilesView;
        this.exportFilesModel = data.exportFilesModel;
        
        if ( $('.settings-company').length > 0 ) {
            this.renderCompany();
        }
        if ( $('.setting-industries').length > 0 ) {
            this.renderIndustry();
        }
        if ( $('.settings-users').length > 0 ) {
            this.renderUsers();
        }
        if ( $('.settings-user-profile').length > 0 ) {
            this.renderUserProfile();
        }
        if ( $('.settings-subscription-info').length > 0 ) {
            this.renderSubscriptionInfo();
        }
        if ( $('.settings-persona').length > 0 ) {
            this.renderSettingsPersona();
        }
        if ( $('.settings-design-template').length > 0 ) {
            this.renderTemplateDetails();
        }
        if ( $('.template-descriptions-content').length > 0 ) {
            this.renderTemplateIntegration();
        }
        if ( $('.settings-alert').length > 0 ) {
            this.renderAlert();
        }
        if ( $('.setting-persona-fields').length > 0 ) {
            this.renderPersonaFields();
        }
        if ( $('.setting-export-files').length > 0 ) {
            this.renderExportFilesList();
        }
    },
    renderCompany: function() {
        this.uploadPhotoModel = new this.uploadPhotoModel;
        this.overviewView     = new this.overview({
            el               : $('.settings-company'),
            uploadPhotoModel : this.uploadPhotoModel
        });
    },
    renderIndustry: function() {
        this.industryModel = new this.industryModel;
        this.industryView  = new this.industry({
            el                  : $('.setting-industries'),
            industryModel       : this.industryModel,
            industryTemplate    : $('#industry-listing-template').html(),
            addIndustryTemplate : $('#add-industry-template').html(),
            subIndustryTemplate : $('#sub_industry_listing').html()
        });
    },
    renderUsers: function() {
        this.usersModel      = new this.usersModel;
        this.updateUserModel = new this.updateUserModel;
        this.deleteUserModel = new this.deleteUserModel;
        this.checkPackagesModel = new this.checkPackagesModel;
        this.usersView  = new this.users({
            el                 : $('.settings-users'),
            usersModel         : this.usersModel,
            updateUserModel    : this.updateUserModel,
            deleteUserModel    : this.deleteUserModel,
            checkPackagesModel : this.checkPackagesModel,
            usersTemplate      : $('#user-listing-template').html()
        });
    },
    renderUserProfile: function() {
        this.userProfileView  = new this.userProfileView({
            el : $('.settings-user-profile')
        });
    },
    renderSubscriptionInfo: function() {
        this.addUpdateCardsModel     = new this.addUpdateCardsModel;
        this.addUpdateBillingModel   = new this.addUpdateBillingModel;
        this.updateSubscriptionModel = new this.updateSubscriptionModel;
        this.subscriptionInfoView    = new this.subscriptionInfo({
            el                      : $('.settings-subscription-info'),
            addUpdateCardsModel     : this.addUpdateCardsModel,
            addUpdateBillingModel   : this.addUpdateBillingModel,
            updateSubscriptionModel : this.updateSubscriptionModel
        });
    },
    renderSettingsPersona: function() {
        this.personaTemplateView  = new this.personaTemplateView({
            el                   : $('.settings-persona')
        });
    },
    renderTemplateDetails: function() {
        this.personaTemplateModel = new this.personaTemplateModel;
        this.publishPersonaModel  = new this.publishPersonaModel;
        this.templateDetailsView  = new this.templateDetailsView({
            el                   : $('.settings-design-template'),
            personaTemplateModel : this.personaTemplateModel,
            publishPersonaModel  : this.publishPersonaModel
        });
    },
    renderTemplateIntegration: function() {
        this.templateDescriptionView = new this.templateDescriptionView({
            el : $('.template-descriptions-content'),
        });
    },
    renderAlert: function() {
        this.updateNotificationsModel = new this.updateNotificationsModel;
        this.updateNotificationsView = new this.updateNotificationsView({
            el                       : $('.settings-alert'),
            updateNotificationsModel : this.updateNotificationsModel
        })
    },
    renderPersonaFields: function() {
        this.personaFieldsModel  = new this.personaFieldsModel;
        this.personaFieldsView   = new this.personaFieldsView({
            el                    : $('.setting-persona-fields'),
            personaFieldsModel    : this.personaFieldsModel,
            personaFieldsTemplate : $('#persona-fields-table-template').html()
        })
    },
    renderExportFilesList: function() {
        this.exportFilesModel   = new this.exportFilesModel;
        this.exportFilesView    = new this.exportFilesView({
            el                  : $('.setting-export-files'),
            exportFilesModel    : this.exportFilesModel,
            exportFilesTemplate : $('#export-files-table-template').html()
        });
    }
});
var settingsPage = new Settings({
    el                       : $('.settings-details'),
    industryView             : IndustryView,
    industryModel            : IndustryModel,
    usersView                : UsersView,
    usersModel               : UsersModel,
    updateUserModel          : UpdateUsersModel,
    deleteUserModel          : DeleteUserModel,
    overviewView             : Overview,
    uploadPhotoModel         : UploadPhotoModel,
    addUpdateCardsModel      : AddUpdateCardsModel,
    addUpdateBillingModel    : AddUpdateBillingModel,
    personaTemplateModel     : PersonaTemplateModel,
    publishPersonaModel      : PublishPersonaTemplateModel,
    updateSubscriptionModel  : UpdateSubscriptionModel,
    checkPackagesModel       : CheckPackagesModel,
    updateNotificationsModel : UpdateNotificationsModel,
    personaFieldsModel       : PersonaFieldsModel,
    exportFilesModel         : ExportFilesModel,
    personaTemplateView      : PersonaTemplateView,
    subscriptionInfoView     : SubscriptionInfoView,
    userProfileView          : UserProfileView,
    templateDetailsView      : TemplateDetailsView,
    templateDescriptionView  : TemplateDescriptionView,
    updateNotificationsView  : UpdateNotificationsView,
    personaFieldsView        : PersonaFieldsView,
    exportFilesView          : ExportFilesView
});
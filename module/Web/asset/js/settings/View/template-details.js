TemplateDetailsView = Backbone.View.extend({
    events: {
        'click .temp-action .btn-cancel'          : 'cancelTemplate',
        'click .temp-action .btn-update-template' : 'saveTemplate',
        'click .btn-preview-pdf'                  : 'previewPersonaPDF',
        'click .btn-published-persona'            : 'publishedPersona'
    },
    initialize: function (data) {

        if ( typeof templateUuid == 'undefined' || $.trim(templateUuid).length == 0 ) {
            this.cancelTemplate;
        }
        this.personaTemplateModel = data.personaTemplateModel;
        this.publishPersonaModel  = data.publishPersonaModel;
        this.listenTo(this.publishPersonaModel, 'change', this.checkPersona);
        this.listenTo(this.personaTemplateModel, 'change', this.checkResult);
    },
    cancelTemplate: function (evt) {
        window.location = '/settings/persona-templates';
    },
    saveTemplate: function (evt) {
        
        var templateData = 'css='+escape($('.custom-css').val())+'&data='+escape($('.custom-body').val()) + '&templateName=' + $('#template-name').val() + '&uuid=' + ( typeof(templateUuid) != 'undefined' ? templateUuid : '0' );

        this.processPersonaTemplates = this.personaTemplateModel.fetch({
            data: templateData,
            type: 'POST',
            beforeSend: function () {
                if (typeof settingsPage.templateDetailsView.processPersonaTemplates != 'undefined' && settingsPage.templateDetailsView.processPersonaTemplates != null) {
                    settingsPage.templateDetailsView.processPersonaTemplates.abort();
                }
            },
            error: function () {
                alert('An error occured. Please refresh the page and try again.');
            }
        });
    },
    checkResult: function(){
        var templateModel = this.personaTemplateModel.toJSON();
        if ( Object.keys(templateModel).length > 0 && templateModel.success == true ) {
            alert('Template Design has been successfully saved.');
            if ( typeof templateModel.templateUuid != 'undefined' ) {
                window.location = '/settings/persona-templates/details/'+ templateModel.templateUuid;
            } else {
                location.reload();
            }
        }
    },
    publishedPersona: function() {
        
        if ( templateUuid == 0 ) {
            alert('Please save the template and then published thanks.');
            return;
        }
        this.processPublishPersonaModel = this.publishPersonaModel.fetch({
            data: { uuid : templateUuid },
            type: 'POST',
            beforeSend: function () {
                if (typeof settingsPage.templateDetailsView.processPublishPersonaModel != 'undefined' && settingsPage.templateDetailsView.processPublishPersonaModel != null) {
                    settingsPage.templateDetailsView.processPublishPersonaModel.abort();
                }
            },
            error: function () {
                alert('An error occured. Please refresh the page and try again.');
            }
        });
    },
    previewPersonaPDF: function() {
        window.location = '/settings/persona-templates/process/preview-persona-pdf/'+templateUuid;
    },
    checkPersona: function() {
        var publishData = this.publishPersonaModel.toJSON();
        if ( publishData.success ) {
            alert('Latest update has been published.');
        } else {
            alert(publishData.message);
        }
    }
});
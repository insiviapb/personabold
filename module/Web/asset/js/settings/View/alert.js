UpdateNotificationsView = Backbone.View.extend({
    events: {
        'click #have-alert'          : 'showHideAlert',
        'click .btn-update-schedule' : 'updateSchedule'
    },
    initialize: function(data) {
        this.updateNotificationsModel = data.updateNotificationsModel;
        this.listenTo(this.updateNotificationsModel, 'change', this.notificationResult);
    },
    notificationResult: function() {
        var notifResult = this.updateNotificationsModel.toJSON();
        if ( notifResult.success ) {
            alert('Alert update successfully.');
        }
    },
    showHideAlert: function(el) {
        if ( this.$el.find(el.currentTarget).is(':checked') ) {
            this.$el.find('.alert-config').removeClass('hide');
        } else {
            this.$el.find('.alert-config').addClass('hide');
        }
    },
    updateSchedule: function() {
        
        var isChecked = this.$el.find('#have-alert').is(':checked') ? 1 : 0;
        
        this.processNotification = this.updateNotificationsModel.fetch({
            data: this.$el.find('#frm-alert').serialize() + '&haveAlert=' + isChecked,
            type: 'POST',
            beforeSend: function () {
                if (typeof settingsPage.updateNotificationsView.processNotification != 'undefined' && settingsPage.updateNotificationsView.processNotification != null) {
                    settingsPage.updateNotificationsView.processNotification.abort();
                }
            },
            error: function() {}
        });
        return false;
    }
});
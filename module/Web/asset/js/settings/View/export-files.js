ExportFilesView = Backbone.View.extend({
    events: {
        'click #btn-add-files' : 'displayModalExportFiles',
    },
    initialize: function(data) {
        this.exportFilesModel    = data.exportFilesModel;
        this.exportFilesTemplate = data.exportFilesTemplate;
        this.exportFilesModel.bind('change', this.displayExportFiles, this);
    },
    executeSave: function(el) {
        this.processExportFiles = this.personaFieldsModel.fetch({
            data: this.$el.find('#exportFilesForm').serialize(),
            type: 'POST',
            beforeSend: function () {
                if (typeof settingsPage.exportFilesView.processExportFiles != 'undefined' && settingsPage.exportFilesView.processExportFiles != null) {
                    settingsPage.exportFilesView.processExportFiles.abort();
                }
            },
        });
    },
    displayExportFields: function() {
        this.$el.find('#uploadFiles')[0].reset();
        var result = this.personaFieldsModel.toJSON();
        if ( result.success ) {
            this.$el.find('#addExportFiles').modal('hide');
            this.$el.find('#addExportFiles input[type=text], textarea').val("");
            var exportFileTemplate  = _.template(this.exportFilesTemplate),
                tableExportFiles    = this.$el.find('#table-account-files tbody');
                tableExportFiles.html('');
            
            _.each(result.exportfiles, function(v,k){
                tableExportFiles.append(exportFileTemplate(v));
            });
        }
    },
    displayModalExportFiles: function() {
        this.$el.find('#uploadFiles')[0].reset();
        this.displayAddExportFiles();
    },
    displayAddExportFiles: function() {
        this.$el.find('#uploadExportFileModal').modal('show');
    },
    appendAndDisplayPersonaFields: function(el) {
        this.$el.find('#personaFieldsForm')[0].reset();
        var id    = this.$el.find(el.currentTarget).parent('tr').data('id'),
            desc  = this.$el.find(el.currentTarget).parent('tr').data('description'),
            details = this.$el.find(el.currentTarget).parent('tr').data('val').split('|');
        this.$el.find('#personaFieldsForm #title').attr('readonly', 'readonly').val(details[0]);
        this.$el.find('#personaFieldsForm #type').val(details[1]);
        this.$el.find('#personaFieldsForm #description').val(desc);
        this.$el.find('#personaFieldsForm #id').val(id);
        
        this.displayAddPersonaFields();
    },
    deletePersonaFields: function(el) {
        this.processPersonaFields = this.personaFieldsModel.fetch({
            data: { action: 'delete',
                    id  : this.$el.find(el.currentTarget).data('id')},
            type: 'POST',
            beforeSend: function () {
                if (typeof settingsPage.personaFieldsView.processPersonaFields != 'undefined' && settingsPage.personaFieldsView.processPersonaFields != null) {
                    settingsPage.personaFieldsView.processPersonaFields.abort();
                }
            },
        });
    }
});
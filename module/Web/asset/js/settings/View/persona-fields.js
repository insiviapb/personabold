PersonaFieldsView = Backbone.View.extend({
    events: {
        'click #btn-add-persona-fields' : 'displayModalPersonaFields',
        'click #savePersonaFields'      : 'executeSave',
        'click .update-persona'         : 'appendAndDisplayPersonaFields',
        'click .delete-persona'         : 'deletePersonaFields',
    },
    initialize: function(data) {
        this.personaFieldsModel    = data.personaFieldsModel;
        this.personaFieldsTemplate = data.personaFieldsTemplate;
        
        this.personaFieldsModel.bind('change', this.displayPersonaFields, this);
    },
    executeSave: function(el) {
        this.processPersonaFields = this.personaFieldsModel.fetch({
            data: this.$el.find('#personaFieldsForm').serialize(),
            type: 'POST',
            beforeSend: function () {
                if (typeof settingsPage.personaFieldsView.processPersonaFields != 'undefined' && settingsPage.personaFieldsView.processPersonaFields != null) {
                    settingsPage.personaFieldsView.processPersonaFields.abort();
                }
            },
        });
    },
    displayPersonaFields: function() {
        this.$el.find('#personaFieldsForm')[0].reset();
        var result = this.personaFieldsModel.toJSON();
        if ( result.success ) {
            this.$el.find('#addPersonaFields').modal('hide');
            this.$el.find('#addPersonaFields input[type=text], textarea').val("");
            var personaTemplate   = _.template(this.personaFieldsTemplate),
                tablePersonaFields = this.$el.find('#table-persona-fields tbody');
                tablePersonaFields.html('');
            
            _.each(result.personaFields, function(v,k){
                tablePersonaFields.append(personaTemplate(v));
            });
        } else {
            alert(result.message);
        }
    },
    displayModalPersonaFields: function() {
        this.$el.find('#personaFieldsForm')[0].reset();
        this.$el.find('#personaFieldsForm #title').removeAttr('readonly');
        this.$el.find('#personaFieldsForm #field_code').removeAttr('readonly');
        this.displayAddPersonaFields();
    },
    displayAddPersonaFields: function() {
        this.$el.find('#addPersonaFields').modal('show');
    },
    appendAndDisplayPersonaFields: function(el) {
        this.$el.find('#personaFieldsForm')[0].reset();
        var id    = this.$el.find(el.currentTarget).parent('tr').data('id'),
            desc  = this.$el.find(el.currentTarget).parent('tr').data('description'),
            details = this.$el.find(el.currentTarget).parent('tr').data('val').split('|');
        this.$el.find('#personaFieldsForm #field_code').attr('readonly', 'readonly').val(details[0]);
        this.$el.find('#personaFieldsForm #title').attr('readonly', 'readonly').val(details[1]);
        this.$el.find('#personaFieldsForm #type').val(details[2]);
        this.$el.find('#personaFieldsForm #description').val(desc);
        this.$el.find('#personaFieldsForm #id').val(id);
        
        this.displayAddPersonaFields();
    },
    deletePersonaFields: function(el) {
        this.processPersonaFields = this.personaFieldsModel.fetch({
            data: { action: 'delete',
                    id  : this.$el.find(el.currentTarget).data('id')},
            type: 'POST',
            beforeSend: function () {
                if (typeof settingsPage.personaFieldsView.processPersonaFields != 'undefined' && settingsPage.personaFieldsView.processPersonaFields != null) {
                    settingsPage.personaFieldsView.processPersonaFields.abort();
                }
            },
        });
    }
});
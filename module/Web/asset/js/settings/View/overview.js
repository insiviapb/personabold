Overview = Backbone.View.extend({
    events: {
        'click #btn-upload' : 'uploadPhoto'
    },
    initialize: function(data) {
        this.uploadPhotoModel = data.uploadPhotoModel;
        this.listenTo(this.uploadPhotoModel, 'change', this.changePhoto);
        this.initializeUploader();
    },
    initializeUploader: function() {
        $('#uploadLogo').fileupload({
            url: '/settings/process/upload-photo',
            dataType: 'json',
            done: function (e, data) {
                if (data.result.success) {
                    $('.profile-big > img').attr('src', data.result.userImage);
                    location.reload();
                }
            },
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    },
    uploadPhoto: function() {
        this.$el.find('#userImage').click();
    },
    changePhoto: function() {
        var uploadPhoto = this.uploadPhotoModel.toJSON();
    }
});
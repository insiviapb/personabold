UserProfileView = Backbone.View.extend({
    events: {
        'click #btn-upload' : 'uploadPhoto'
    },
    initialize: function(data) {
        this.initializeUploader();
    },
    initializeUploader: function() {
        $('#uploadPhoto').fileupload({
            url: '/settings/process/upload-photo',
            dataType: 'json',
            done: function (e, data) {
                if (data.result.success) {
                    $('.profile-big > img').attr('src', data.result.userImage);
                    location.reload();
                }
            },
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    },
    uploadPhoto: function() {
        this.$el.find('#userImage').click();
    },
});
PersonaTemplateView = Backbone.View.extend({
    events: {
        'click .gallery-template .templates' : 'templatePreview',
        'click .btn-add-persona'             : 'addPersona'
    },
    templatePreview: function(evt) {
        window.location = '/settings/persona-templates/details/' + this.$el.find(evt.currentTarget).data('uuid');
    },
    addPersona: function(evt) {
        window.location = '/settings/persona-templates/details/0';
    }
});
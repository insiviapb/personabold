UsersView = Backbone.View.extend({
    events: {
        'click #btn-create'                   : 'inviteUsers',
        'click .edit-users'                   : 'editUsersModal',
        'click .delete-users'                 : 'deleteUsersModal',
        'click #updateUsers #close'           : 'closeUUModal',
        'click #updateUsers #saveUsers'       : 'updateUsers',
        'click #confirmUserDelete #close'     : 'closeDeleteConfirm',
        'click #confirmUserDelete #saveUsers' : 'deleteUsers',
        'click button[data-search]'           : 'displayInfo'
    },
    initialize: function(data) {
        this.usersModel         = data.usersModel;
        this.updateUserModel    = data.updateUserModel;
        this.usersTemplate      = data.usersTemplate;
        this.deleteUserModel    = data.deleteUserModel;
        this.checkPackagesModel = data.checkPackagesModel;
        this.roleDescription    = roleDescription;
        this.usersListingModel  = new Backbone.Model();
        this.listenTo(this.checkPackagesModel, 'change', this.packageResult);
        this.listenTo(this.usersModel, 'change', this.updateUsersTable);
        this.listenTo(this.updateUserModel, 'change', this.updateUserTable);
        this.listenTo(this.usersListingModel, 'change', this.render);
        this.usersListingModel.set(allusers);
    },
    render: function() {
        var usersListing  = this.usersListingModel.toJSON(),
            usersTemplate = _.template(this.usersTemplate),
            self         = this.$el;
        
        self.find('.user-settings-list').html('');
        _.each( usersListing, function(v, k) {
            if ( v.image == null ) {
                v.image = '/img/user-icon.png';
            }
            
            self.find('.user-settings-list').append(usersTemplate(v));
        });
    },
    displayInfo: function(el) {
        var searchData = this.$el.find(el.currentTarget).data('search');
        
        this.$el.find('.role-description > button').removeClass('btn-selected');
        this.$el.find(el.currentTarget).addClass('btn-selected');
        this.$el.find('.role-description > pre').html('');
        this.$el.find('.role-description > pre').html(this.roleDescription[searchData]);
    },
    closeDeleteConfirm: function() {
        this.$el.find('#confirmUserDelete').modal('hide');
    },
    deleteUsers: function() {
        this.processUsersDelete = this.deleteUserModel.fetch({
            data: this.$el.find('#userDeleteForm').serialize(),
            type: 'POST',
            beforeSend: function () {
                if (typeof settingsPage.usersView.processUsersDelete != 'undefined' && settingsPage.usersView.processUsersDelete != null) {
                    settingsPage.usersView.processUsersDelete.abort();
                }
            },
            error: function() {
                alert('An error occured. Please try again later.');
            }
        });
    },
    editUsersModal: function(el) {
        this.$el.find('#updateUsers').modal('show');
        this.$el.find("#edit-role option[value='"+this.$el.find(el.currentTarget).data('role')+"']").attr("selected", true);
        this.$el.find('#edit-uuid').val(this.$el.find(el.currentTarget).data('uuid'));
        this.$el.find('#updateUsers').find('label[name=email]').html(this.$el.find(el.currentTarget).data('email'));
    },
    updateUsers: function() {
        this.processUsersUpdate = this.updateUserModel.fetch({
            data: this.$el.find('#userForm').serialize()+'&role='+this.$el.find('#edit-role').val(),
            type: 'POST',
            beforeSend: function () {
                if (typeof settingsPage.usersView.processUsersUpdate != 'undefined' && settingsPage.usersView.processUsersUpdate != null) {
                    settingsPage.usersView.processUsersUpdate.abort();
                }
            },
            error: function() {
                alert('An error occured. Please try again later.');
            }
        });
    },
    closeUUModal: function() {
        this.$el.find('#updateUsers').modal('hide');
    },
    deleteUsersModal: function(el) {
        this.$el.find('#confirmUserDelete #uuid').val(this.$el.find(el.currentTarget).data('uuid'));
        this.$el.find('#confirmUserDelete').modal('show');
    },
    updateUserTable: function() {
        this.$el.find('#updateUsers').modal('hide');
        var users         = this.updateUserModel.toJSON(),
            usersTemplate = _.template(this.usersTemplate);
        this.updateTable(users, usersTemplate);
    },
    updateUsersTable: function() {
        this.$el.find('#invite-user')[0].reset();
        var users         = this.usersModel.toJSON(),
            usersTemplate = _.template(this.usersTemplate);
    
        if ( users.success ) {
            this.updateTable(users, usersTemplate);
        } else {
            alert(users.message);
        }
            
    },
    updateTable: function(users, template) {
        usersTable = this.$el.find('#table-users tbody');
        if ( !users.success ) {
            alert(users.message);
            return;
        }
        usersTable.html('');
        _.each(users.data, function(v,k){
            usersTable.append(template(v));
        });
    },
    inviteUsers: function() {
        this.processCheckPackages = this.checkPackagesModel.fetch({
            data: {
                type : 'users'
            },
            type: 'POST',
            beforeSend: function () {
                if (typeof settingsPage.usersView.processCheckPackages != 'undefined' && settingsPage.usersView.processCheckPackages != null) {
                    settingsPage.usersView.processCheckPackages.abort();
                }
            },
            error: function () {}
        });
    },
    packageResult: function() {
        var result = this.checkPackagesModel.toJSON();
        if ( result.success ) {
            this.processUserPackages();
            
        } else if (result.success == false) {
            // currrent packages
            $('#notifAddUserModal .packages.current').find('.package-label').html(result.data.current.planCode);
            $('#notifAddUserModal .packages.current').find('.package-amt').html(result.data.current.amount);
            $('#notifAddUserModal .packages.current').find('.package-survey').html(result.data.current.surveyCount);
            $('#notifAddUserModal .packages.current').find('.package-persona').html(result.data.current.personaCount);
            $('#notifAddUserModal .packages.current').find('.package-template').html(result.data.current.templateCount);
            // next packages
            $('#notifAddUserModal .packages.next').find('.package-label').html(result.data.next.planCode);
            $('#notifAddUserModal .packages.next').find('.package-amt').html(result.data.next.amount);
            $('#notifAddUserModal .packages.next').find('.package-survey').html(result.data.next.surveyCount);
            $('#notifAddUserModal .packages.next').find('.package-persona').html(result.data.next.personaCount);
            $('#notifAddUserModal .packages.next').find('.package-template').html(result.data.next.templateCount);
            $('#notifAddUserModal').modal('show');
            $('#notifAddUserModal .btn-add-user').click(function() {
                $('#notifAddUserModal').modal('hide');
                settingsPage.usersView.processUserPackages();
            });
            $('#notifAddUserModal .btn-cancel-user').click(function() {
                $('#notifAddUserModal').modal('hide');
            });
        }
        this.checkPackagesModel.clear();
    },
    processUserPackages: function() {
        this.processUsers = this.usersModel.fetch({
            data: this.$el.find('#invite-user').serialize()+'&role='+this.$el.find('#role').val(),
            type: 'POST',
            beforeSend: function () {
                if (typeof settingsPage.usersView.processUsers != 'undefined' && settingsPage.usersView.processUsers != null) {
                    settingsPage.usersView.processUsers.abort();
                }
            },
            error: function() {
                alert('An error occured. Please try again later.');
            }
        });
    }
});
IndustryView = Backbone.View.extend({
    events: {
        'click #btn-add-industry' : 'displayModalIndustry',
        'click .saveindustry'      : 'executeSave',
        'click .update-industry'   : 'appendAndDisplayIndustry',
        'click .update-sub-industry'   : 'appendAndDisplaySubIndustry',
        'click .delete-industry'   : 'deleteIndustry',
        'keydown #phone'           : 'maskPhone',
        'click #addIndustry .close' : 'clearValidationMessage',
        'click #addSubIndustry .close' : 'clearValidationMessage',
        'click .add-sub-industry' : 'renderSubIndustryModal',
        'click .toggle-sub-industry' : 'toggleSubIndustry'
    },
    initialize: function(data) {
        this.industryModel       = data.industryModel;
        this.indTemplate         = data.industryTemplate;
        this.addIndustryTemplate = data.addIndustryTemplate;
        this.subIndustryTemplate = data.subIndustryTemplate;
        this.industryListModel   = new Backbone.Model();
        this.subIndustryModel    = new Backbone.Model();
        
        var options = '';
        if ( typeof allClient != 'undefined' ) {
            _.each( allClient, function(v, k){
               options += '<option value="'+v.uuid+'">'+v.name+'</option>'; 
            });
        }
        this.$el.find('#sct-client').html(options);
        this.listenTo(this.industryListModel, 'change', this.render);
        this.listenTo(this.subIndustryModel, 'change', this.render);
        this.industryModel.bind('change', this.displayIndustry, this);
        
        if ( Object.keys(allIndustry).length > 0 ) {
            this.industryListModel.set(allIndustry);
            if ( Object.keys(allSubIndustries).length > 0 ) {
                this.subIndustryModel.set(allSubIndustries);
            }
        } else {
            this.render();
        }
    },
    executeSave: function(el) {
        $('.error-message-industry').hide().html('');
        var id = $(el.currentTarget).attr('id').split('-');
        if (!$('#'+id[1]).val()) {
            $('.error-message-industry').show().html('Industry Name is required.');
            return false;
        }
        var serialized_form = this.$el.find('#'+id[1]+'Form').serialize();
        this.processIndustry = this.industryModel.fetch({
            data: serialized_form,
            type: 'POST',
            beforeSend: function () {
                if (typeof settingsPage.industryView.processIndustry != 'undefined' && settingsPage.industryView.processIndustry != null) {
                    settingsPage.industryView.processIndustry.abort();
                }
            },
            success: function (data) {
                if(!data.attributes.success) {
                    $('.error-message-industry').show().html(data.attributes.message);
                } else {
                    location.reload();
                }
            }
        });
    },
    displayIndustry: function() {
        var result = this.industryModel.toJSON();
        if ( result.success ) {
            this.$el.find('#addIndustry').modal('hide');
            this.$el.find('#addIndustry input[type=text], textarea').val("");
            this.industryListModel.clear();
            this.industryListModel.set(result.industries);
        }
    },
    displayModalIndustry: function() {
        this.$el.find('#industryForm')[0].reset();
        this.displayAddIndustry();
    },
    displayAddIndustry: function() {
        this.$el.find('#addIndustry').modal('show');
    },
    appendAndDisplayIndustry: function(el) {
        this.$el.find('#industryForm')[0].reset();
        var uuid     = this.$el.find(el.currentTarget).closest('tr').data('uuid'),
            indCount = this.$el.find(el.currentTarget).closest('tr').data('count'),
            details  = this.industryListModel.toJSON()[indCount]; 
        this.$el.find('#sct-client option[value="'+ details.accountName +'"]').prop('selected', true);
        this.$el.find('#industryForm #industry').val(details.industryName);
        this.$el.find('#industryForm #phone').val(details.phone);
        this.$el.find('#industryForm #description').val(details.description);
        this.$el.find('#industryForm #uuid').val(uuid);
        
        this.displayAddIndustry();
    },
    appendAndDisplaySubIndustry: function(el) {
        this.$el.find('#sub_industryForm')[0].reset();
        var uuid     = this.$el.find(el.currentTarget).closest('tr').data('uuid'),
            indCount = this.$el.find(el.currentTarget).closest('tr').data('count'),
            details  = this.subIndustryModel.toJSON()[indCount];
        
        this.$el.find('#sub_industryForm #sub_industry').val(details.industryName);
        this.$el.find('#sub_industryForm #phone').val(details.phone);
        this.$el.find('#sub_industryForm #sub_description').val(details.description);
        this.$el.find('#sub_industryForm #uuid').val(details.uuid);
        this.$el.find('#sub_industryForm #parent_id').val(details.parentIndustry);

        this.displaySubIndustryModal();
    },
    deleteIndustry: function(el) {
        this.processIndustry = this.industryModel.fetch({
            data: { action: 'delete',
                    uuid  : this.$el.find(el.currentTarget).data('uuid')},
            type: 'POST',
            beforeSend: function () {
                if (typeof settingsPage.industryView.processIndustry != 'undefined' && settingsPage.industryView.processIndustry != null) {
                    settingsPage.industryView.processIndustry.abort();
                }
            },
            success: function (data) {
                if(data.attributes.success) {
                    location.reload();
                }
            }
        });
    },
    render: function() {
        $("#phone").mask("999-999-9999");
        $("#sub_phone").mask("999-999-9999");
        
        var industryList   = this.industryListModel.toJSON();
            indTemplate    = _.template(this.indTemplate),
            self           = this.$el,
            addIndTemplate = _.template(this.addIndustryTemplate),
            subIndTemplate = _.template(this.subIndustryTemplate),
            indCount       = 0;
        
        self.find('.industry-container').html('');
        self.find('.industry-container').append(addIndTemplate({}));
        _.each( industryList, function(v, k) {
            v.indCount = indCount;
            self.find('.industry-container').append(indTemplate(v));
            indCount++;
        });
        
        //rendering of sub Industries
        var subIndustryList = this.subIndustryModel.toJSON();
        console.log(Object.keys(subIndustryList));
        var counter = 0;
        _.each(subIndustryList, function (v,k) {
            $.inArray(counter, Object.keys(subIndustryList))
            {
                self.find('#delete_'+v.parentIndustry).hide();
                self.find('#table-industry tbody .parent_' + v.parentIndustry).after(subIndTemplate({subList: v, counter : counter}));
                self.find('#table-industry tbody .parent_' + v.parentIndustry+' .toggle-sub-industry').show();
                counter++;
            }
        });
        
    },
    clearValidationMessage : function () {
        $('.error-message-industry').hide().html('');
    },
    renderSubIndustryModal : function (e) {
        var parent_id = $(e.currentTarget).data('parentid');
        var uuid = $(e.currentTarget).data('uuid');
        this.$el.find('#addSubIndustry #sub_industryForm')[0].reset();
        this.$el.find('#addSubIndustry #parent_id').val(parent_id);

        this.displaySubIndustryModal();
    },
    displaySubIndustryModal :function () {
        this.$el.find('#addSubIndustry').modal('show');
    },
    toggleSubIndustry : function (e) {
        var target = this.$el.find(e.currentTarget).data('id');
        this.$el.find('.sublist-'+target).slideToggle('slow');
    }
});
TemplateDescriptionView = Backbone.View.extend({
    events: {
        'click .persona-template-content > li' : 'viewIntegration'
    },
    initialize: function(data) {
        personaTemplate = _.template($('#personacreation').html());
        this.renderTemplate(personaTemplate);
    },
    viewIntegration: function(evt) {
        personaTemplate = _.template($('#'+this.$el.find(evt.currentTarget).attr('template')).html());
        this.renderTemplate(personaTemplate);
    },
    renderTemplate: function(personaTemplate) {
        this.$el.find('.template-implementations').html('');
        this.$el.find('.template-implementations').html(personaTemplate);
    }
});
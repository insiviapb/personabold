SubscriptionInfoView = Backbone.View.extend({
    updatedPlan: '',
    events: {
        'click .edit-card-button button'    : 'showEditCards',
        'click .edit-billing-button button' : 'showBilling',
        'click .btn-card-cancel'            : 'cancelEditCards',
        'click .btn-billing-cancel'         : 'cancelBilling',
        'click .btn-submit-card'            : 'submitCards',
        'click .btn-submit-billing'         : 'submitBilling',
        'click .package-data'               : 'confirmPackageData',
        'click #notifUpgradeSubscription .btn-success': 'updateSubscription',
        'click #notifUpgradeSubscription .btn-cancel' : 'closeSubscription',
        'click #notifAddSubscription .btn-success'    : 'updateSubscription',
        'click #notifAddSubscription .btn-cancel'     : 'closeSubscription',
    },
    initialize: function(data) {
        
        this.inputPostal();
        //this.controlKey();
        this.addUpdateCardsModel     = data.addUpdateCardsModel;
        this.addUpdateBillingModel   = data.addUpdateBillingModel;
        this.updateSubscriptionModel = data.updateSubscriptionModel;
        this.listenTo(this.addUpdateCardsModel, 'change', this.reflectCardChanges);
        this.listenTo(this.addUpdateBillingModel, 'change', this.reflectBillingChanges);
        this.listenTo(this.updateSubscriptionModel, 'change', this.reflectSubscription);
        this.addValidation();
    },
    reflectCardChanges: function() {
        var result = this.addUpdateCardsModel.toJSON();
        if ( result.success ) {
            location.reload();
        } else {
            alert('An error occured. Please refresh the page and try again.');
        }
    },
    reflectBillingChanges: function() {
        var result = this.addUpdateBillingModel.toJSON();
        if ( result.success ) {
            location.reload();
        } else {
            alert('An error occured. Please refresh the page and try again.');
        }
    },
    inputPostal: function() {
        
        $('input[name=postal_code]').on('keypress', function(e) {
            
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                 // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                 // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) && e.keyCode != 0 ) {
                e.preventDefault();
            }
        });
    },
    controlKey: function() {
        
        $('input[name=number], input[name=cvc], input[name=exp_month], input[name=exp_year]').on('keypress', function(e) {
        
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                 // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                 // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    },
    submitBilling: function(evt) {
        
        if ($('#billingForm').valid()) {
            // append loader
            this.$el.find(evt.currentTarget).append('&nbsp;&nbsp;<img src="/img/ajax-loader.gif">');
            
            this.processBilling = this.addUpdateBillingModel.fetch({
                data: this.$el.find('#billingForm').serialize(),
                type: 'POST',
                beforeSend: function () {
                    if (typeof settingsPage.subscriptionInfoView.processBilling != 'undefined' && settingsPage.subscriptionInfoView.processBilling != null) {
                        settingsPage.subscriptionInfoView.processBilling.abort();
                    }
                },
                error: function() {
                    alert('An error occured. Please refresh the page and try again.');
                }
            });
        }
    },
    submitCards: function(evt) {
        
        if ($('#creditCard').valid()) {
            // append loader
            this.$el.find(evt.currentTarget).append('&nbsp;&nbsp;<img src="/img/ajax-loader.gif">');
            
            this.processCards = this.addUpdateCardsModel.fetch({
                data: this.$el.find('#creditCard').serialize(),
                type: 'POST',
                beforeSend: function () {
                    if (typeof settingsPage.subscriptionInfoView.processCards != 'undefined' && settingsPage.subscriptionInfoView.processCards != null) {
                        settingsPage.subscriptionInfoView.processCards.abort();
                    }
                },
                error: function() {
                    alert('An error occured. Please refresh the page and try again.');
                }
            });
        }
    },
    addValidation: function() {
        if ( $('#creditCard').length != 0 ) {
            
             $('#creditCard').validate({
                rules: {
                    number : {
                        required : true,
                        minlength : 16
                    },
                    cvc  : {
                        required: true,
                        minlength : 3
                    },
                    exp_month: {
                        required : true,
                        maxlength : 2,
                        min: 1,
                        max: 12
                    },
                    exp_year: {
                        required  : true,
                        minlength : 4
                    },
                },
                messages: {
                    number: {
                        required: "Please provide card number.",
                        minlength: "Please input valid card number."
                    },
                    cvc: {
                        required: "Please provide your cvc",
                        minlength: "Please input valid cvc."
                    },
                    exp_month: {
                        required: "Please provide month.",
                        minlength: "Please input valid month.",
                        min: "Please input valid cvc.",
                        max: "Please input valid cvc."
                    },
                    exp_year: {
                        required: "Please provide year.",
                        minlength: "Please input valid year."
                    },
                },
                errorElement: 'label'
            });
        }
        if ( $('#billingForm').length != 0 ) {
            
            $('#billingForm').validate({
                rules: {
                    shipping_name: {
                        required : true,
                        minlength : 3
                    },
                    email: {
                        required: true,
                        minlength : 3,
                        email: true
                    },
                    company: {
                        required : true
                    },
                    address1: {
                        required  : true,
                        minlength : 3
                    },
                    address2: {
                        required  : false,
                        minlength : 3
                    },
                    postal_code: {
                        required  : true,
                        minlength : 4
                    },
                    phone1: {
                        required: true
                    },
                    phone2: {
                        required: false
                    },
                    city: {
                        required: true
                    },
                    country: {
                        required: true
                    },
                    state: {
                        required: true,
                        minlength:3
                    }
                },
                messages: {
                    shipping_name: {
                        required: "Please provide your name.",
                        minlength: "Please input valid name."
                    },
                    email: {
                        required: "Please provide email.",
                        minlength: "Please input valid email.",
                        email: "Please input valid email."
                    },
                    company: {
                        required: "Please provide company."
                    },
                    address1: {
                        required: "Please provide address.",
                        minlength: "Please input valid address."
                    },
                    address2: {
                        minlength: "Please input valid address."
                    },
                    postal_code: {
                        required: "Please provide postal code.",
                        minlength: "Please input valid postal code."
                    },
                    phone1: {
                        required: "Please provide phone."
                    },
                    city: {
                        required: "Please provide city.",
                        minlength: "Please input valid city."
                    },
                    state: {
                        required: "Please provide state.",
                        minlength: "Please input valid state."
                    },
                    country: {
                        required: "Please provide country."
                    }
                },
                errorElement: 'label'
            });
        }
    },
    showEditCards: function() {
        this.$el.find('.edit-card-button').addClass('hide');
        this.$el.find('.edit-card-details').addClass('hide');
        this.$el.find('.edit-card-form').removeClass('hide');
    },
    showBilling: function() {
        this.$el.find('.edit-billing-button').addClass('hide');
        this.$el.find('.edit-billing-details').addClass('hide');
        this.$el.find('.edit-billing-form').removeClass('hide');
    },
    cancelEditCards: function() {
        this.$el.find('.edit-card-button').removeClass('hide');
        this.$el.find('.edit-card-details').removeClass('hide');
        this.$el.find('.edit-card-form').addClass('hide');
    },
    cancelBilling: function() {
        this.$el.find('.edit-billing-button').removeClass('hide');
        this.$el.find('.edit-billing-details').removeClass('hide');
        this.$el.find('.edit-billing-form').addClass('hide');
    },
    confirmPackageData: function(evt) {
        
        if ( !isCreditCardExist ) {
            alert('Please complete first the credit card details.');
            return;
        }
        
        var packageName = this.$el.find(evt.currentTarget).find('#package-name').val();
        if ( packageName != currPackages.stripePlanCode && typeof currPackages.stripePlanCode != 'undefined' ) {
            this.updatedPlan = packageName;
            nextPackages     = allPackages[packageName];
            this.$el.find('.packages.current').find('.package-label').html(currPackages.planCode);
            this.$el.find('.packages.current').find('.package-amt').html('$'+(currPackages.amount).toFixed(2));
            this.$el.find('.packages.current').find('.package-user').html(currPackages.userCount == 1 ? '1 User' : ( currPackages.userCount <= 5 ? '5 Users' : 'Unlimited Users'));
            this.$el.find('.packages.next').find('.package-client').html(currPackages.clientCount == 0 ? '' : 'Unlimited Client Groups');
            this.$el.find('.packages.current').find('.package-template').html(currPackages.templateCount == 0 ? '1 Export Template' : 'Full Export Template Library');
            this.$el.find('.packages.current').find('.package-survey').html(currPackages.surveyCount == 0 ? '' : 'Persona Survey System');
            this.$el.find('.packages.current').find('.package-persona').html('Unlimited Personas');
            // next packages
            this.$el.find('.packages.next').find('.package-label').html(nextPackages.planCode);
            this.$el.find('.packages.next').find('.package-amt').html(nextPackages.amount);
            this.$el.find('.packages.next').find('.package-user').html(nextPackages.userCount == 1 ? '1 User' : ( nextPackages.userCount == 5 ? '5 Users' : 'Unlimited Users'));
            this.$el.find('.packages.next').find('.package-client').html(nextPackages.clientCount == 0 ? '' : 'Unlimited Client Groups');
            this.$el.find('.packages.next').find('.package-template').html(nextPackages.templateCount == 0 ? '1 Export Template' : 'Full Export Template Library');
            this.$el.find('.packages.next').find('.package-survey').html(nextPackages.surveyCount == 0 ? '' : 'Persona Survey System');
            this.$el.find('.packages.next').find('.package-persona').html('Unlimited Personas');
            this.$el.find('#notifUpgradeSubscription').modal('show');
        } else {
            this.updatedPlan = packageName;
            nextPackages     = allPackages[packageName];
            // next packages
            this.$el.find('.packages.next').find('.package-label').html(nextPackages.planCode);
            this.$el.find('.packages.next').find('.package-amt').html(nextPackages.amount);
            this.$el.find('.packages.next').find('.package-user').html(nextPackages.userCount == 1 ? '1 User' : ( nextPackages.userCount == 5 ? '5 Users' : 'Unlimited Users'));
            this.$el.find('.packages.next').find('.package-client').html(nextPackages.clientCount == 0 ? '' : 'Unlimited Client Groups');
            this.$el.find('.packages.next').find('.package-template').html(nextPackages.templateCount == 0 ? '1 Export Template' : 'Full Export Template Library');
            this.$el.find('.packages.next').find('.package-survey').html(nextPackages.surveyCount == 0 ? '' : 'Persona Survey System');
            this.$el.find('.packages.next').find('.package-persona').html('Unlimited Personas');
            this.$el.find('#notifAddSubscription').modal('show');
        }
    },
    updateSubscription: function(evt) {
        self = this;
        this.$el.find(evt.currentTarget).append('&nbsp;&nbsp;<img src="/img/ajax-loader.gif">');
        this.processSubscription = this.updateSubscriptionModel.fetch({
            data: { planCode : self.updatedPlan },
            type: 'POST',
            beforeSend: function () {
                if (typeof settingsPage.subscriptionInfoView.processSubscription != 'undefined' && settingsPage.subscriptionInfoView.processSubscription != null) {
                    settingsPage.subscriptionInfoView.processSubscription.abort();
                }
            },
            error: function() {
                self.find(evt.currentTarget).html('Proceed');
                alert('An error occured. Please refresh the page and try again.');
            }
        });
    },
    closeSubscription: function() {
        $('#notifUpgradeSubscription').modal('hide');
        $('#notifAddSubscription').modal('hide');
    },
    reflectSubscription: function() {
        var result = this.updateSubscriptionModel.toJSON();
        if ( result.success ) {
            alert('Subscription successfully updated');
            window.location.reload();
        }
    }
});
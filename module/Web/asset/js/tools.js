var Tools = Tools || {};
Tools = {
    renderPersonaTour: function() {
        // Instance the tour
        var tour = new Tour({
            steps: [
                {
                    placement: 'top',
                    element: "#persona",
                    title: "Welcome to User Persona Page!",
                    content: "Where you can modify or create a new persona"
                },
                {
                    element: ".persona-name-box",
                    title: "Persona Name",
                    content: "You must name your persona before we start."
                },
                {
                    element: ".headshot-tool",
                    title: "Persona's Headshot",
                    content: "You change your persona's headshot image by clicking on <i class='fa fa-chevron-left'></i> " +
                                "and <i class='fa fa-chevron-right'></i> to browse our system images.<br> " +
                                "You can also upload your own image by clicking on <i class='fa fa-cog'>.</i>"
                },
                {
                    element: ".age-gender-location",
                    title: "The Age, Gender and Location",
                    content: "Set the minimum age, gender and location."
                },
                {
                    element: ".pesona-info-box",
                    title: "Set Archetype, Industry etc",
                    content: "Set the persona details like Industry, Job, Archetype and etc."
                },

                {
                    placement: 'top',
                    element: ".persona_data",
                    title: "Persona Data",
                    content: "Add some list to this box by clicking this icon <i class='fa fa-cog'></i> of this box."
                },

                {
                    placement: 'bottom',
                    element: ".addPersona",
                    title: "Custom Persona Data Box",
                    content: "You can add your own Persona Data box where you can have your own title and click this icon <i class='fa fa-cog'></i> to add your lists."
                },
                {
                    placement: 'left',
                    element: ".box-channels-slider",
                    title: "Channels Section",
                    content: "You may start adding lists of channels by click on the <i class='fa fa-cog'></i> icon. Here you can add or remove channels and change values using the slider tool."
                },
                {
                    placement: 'left',
                    element: ".box-motivations-slider",
                    title: "Motivation Section",
                    content: "You may start adding lists of motivations by click on the <i class='fa fa-cog'></i> icon. Here you can add or remove channels and change values using the slider tool."
                },
                {
                    placement: 'left',
                    element: ".box-knowledge-slider",
                    title: "Knowledge Section",
                    content: "You start add knowledge by clicking on the <i class='fa fa-cog'></i> icon."
                },
        ]});

        // Initialize the tour
        tour.init();

        // Start the tour
        tour.restart(true);
    },
    renderEllipsis: function() {
            
        var showChar = 180;
        var ellipsestext = "...";
        var moretext = "Hide info";
        var lesstext = "Read more info";
        $('.more').each(function() {
            var content = $(this).html();

            if(content.length > showChar) {
                var c = content.substr(0, showChar);
                var h = content.substr(showChar-1, content.length - showChar);
                var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
                $(this).html(html);
            }
        });

        $(".morelink").click(function(){
            if($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
        
    }
}
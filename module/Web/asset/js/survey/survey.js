var Survey = Backbone.View.extend({
    initialize: function(data) {
        this.surveyFormView = data.surveyFormView;
        this.surveyResult = data.surveyResultView;

        this.getSurveyRespondentsModel = data.getSurveyRespondentsModel;
        this.getSurveyResultByRespondentModel = data.getSurveyResultByRespondentModel;

        if($('.survey-form').length > 0 ) {
            this.renderSurveyForm();
        }

        if($('.survey-result').length > 0) {
            this.renderSurveyResultView();
        }
    },
    renderSurveyResultView: function() {
        this.getSurveyRespondentsModel = new this.getSurveyRespondentsModel;
        this.getSurveyResultByRespondentModel = new this.getSurveyResultByRespondentModel;
        
        this.surveyResultView = new this.surveyResult({
            el                                : $('.survey-result'),
            getSurveyRespondentsModel         : this.getSurveyRespondentsModel,
            getSurveyResultByRespondentModel  : this.getSurveyResultByRespondentModel,
            listCountTemplate                 : $('#list-count').html()
        });
    },
    renderSurveyForm: function() {
        this.surveyFormView  = new this.surveyFormView({
            el             : $('.survey-form'),
        });
    },

});

var surveyPage = new Survey({

    surveyFormView                   : SurveyFormView,
    surveyResultView                 : SurveyResultView,
    getSurveyRespondentsModel        : GetSurveyRespondentsModel,
    getSurveyResultByRespondentModel : GetSurveyResultByRespondentModel,
});

$('form input').on('keypress', function(e) {
    return e.which !== 13;
});

SurveyFormView = Backbone.View.extend({
    events: {
        'mouseenter .persona-main-box': 'addPersonaFieldItem',
        'mouseleave .persona-main-box': 'removePersonaFieldItem',
        'click #add-new-list-item'    : 'addListInput'
    },
    initialize: function(data) {

    },
    addPersonaFieldItem: function(el) {
        var currentEl = this.$el.find(el.currentTarget);
        if(currentEl.find('ul').find('#add-new-list-item').length == 0) {
            currentEl.find('ul').append('<li id="add-new-list-item" class="list-item" data-persona-field="'+ currentEl.find('ul').data('field-id') + '">Click to add new list item here...</li>');
        }
    },
    removePersonaFieldItem: function(el) {
        var currentEl = this.$el.find(el.currentTarget);
        if(currentEl.find('#add-new-list-item').length != 0) {
            currentEl.find('#add-new-list-item').remove();
        }

        if(currentEl.find('input').length != 0 && currentEl.find('input').val().length == 0) {
            var liParent = currentEl.find('input').parent();
            currentEl.find('input').remove();
            liParent.remove();
        }
    },
    addListInput: function(el){
        var currentEl = this.$el.find(el.currentTarget);
        if( currentEl.attr('id') == 'add-new-list-item' ){
            currentEl.attr('id', '');
        }
        if(currentEl.find('ul').find('input.custom-field').length == 0) {
            currentEl.html( '<input name="personaFieldlist[fieldId'+ currentEl.data('persona-field') +'][]" class="custom-field" placeholder="Add new list item" type="text"/>' );
        }
    }

});
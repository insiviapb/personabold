SurveyResultView = Backbone.View.extend({
    events: {
        'click .respondent-list a.respondent' : 'filterSurveyResult'
    },
    initialize: function(data) {



        this.listCountTemplate = data.listCountTemplate;

        this.getSurveyRespondentsModel = data.getSurveyRespondentsModel;
        this.getSurveyResultByRespondentModel = data.getSurveyResultByRespondentModel;

        this.listenTo(this.getSurveyResultByRespondentModel, 'change', this.render);
        this.clearDataList = false;

        var respondentIds = [];
        $(".respondent").each(function() {
            if($(this).attr('data-id').length != 0) {
                respondentIds.push($(this).attr('data-id'));
            }
        });

        this.respondents = respondentIds;
        this.allRespondentIds = respondentIds;

        this.fetchSurveyResultByRespondents();

    },
    render: function() {

        var surveyResult = this.getSurveyResultByRespondentModel.toJSON();
        if(surveyResult.success && surveyResult.hasOwnProperty('data')) {
            $.each(surveyResult.data, function(k, v) {
                switch (k) {
                    case 'age':
                        surveyPage.surveyResultView.renderAge(v);
                        break;
                    case 'genders':
                        surveyPage.surveyResultView.renderGender(v);
                        break;
                    case 'industries':
                        surveyPage.surveyResultView.renderIndustry(v);
                        break;
                    case 'archetypes':
                        surveyPage.surveyResultView.renderArchetype(v);
                        break;
                    case 'jobtypes':
                        surveyPage.surveyResultView.renderJobtype(v);
                        break;
                    case 'surveyData':
                        surveyPage.surveyResultView.renderSurveyRespondentsData(v)
                        break;
                    default:
                        break;
                }
            });
        }
    },

    filterSurveyResult: function(el) {
        var currentEl = this.$el.find(el.currentTarget);
        var ulParent = currentEl.parent().parent();
        ulParent.find('.active').removeClass('active');

        currentEl.parent().addClass('active');

        var respondents = [];
        this.clearDataList = true;
        if(currentEl.attr('data-id').length != 0) {

            respondents.push(currentEl.attr('data-id'));
            $('#resultOf').html(currentEl.html());
        } else {
            respondents = this.allRespondentIds;
            $('#resultOf').html("All");
        }
        this.respondents = respondents;
        this.fetchSurveyResultByRespondents();
    },
    renderIndustry: function(data) {
        var currentEl = this.$el;
        var targetEl = currentEl.find('.industries').find('ul');
        var listCountTemplate = _.template(this.listCountTemplate);
        if(data.length != 0) {
            targetEl.html('');
            $.each(data, function (k, v) {
                targetEl.append(listCountTemplate({
                    title        : v.industryName,
                    count        : v.countIndustry,
                }));
            });

            if(data.length == 1) {
                targetEl.find('.count').hide();
            }
        }
    },
    renderAge: function(data) {
        var max = data[0].maxAge,
            min = data[0].minAge;
            ageData = {min: min, max:max};
        if(max == min) {
            ageData = {min: 0, max:max};
        }

        All.renderAgeSlider('#age-slider', ageData, false);
    },
    renderGender: function(data) {
        var currentEl = this.$el;
        var targetEl = currentEl.find('.genders').find('ul');
        var listCountTemplate = _.template(this.listCountTemplate);
        if(data.length != 0) {
            targetEl.html('');
            $.each(data, function (k, v) {
                targetEl.append(listCountTemplate({
                    title        : v.gender,
                    count        : v.countGender,
                }));
            });

            if(data.length == 1) {
                targetEl.find('.count').hide();
            }
        }
    },
    renderJobtype: function(data) {
        var currentEl = this.$el;
        var targetEl = currentEl.find('.jobtypes').find('ul');
        var listCountTemplate = _.template(this.listCountTemplate);
        if(data.length != 0) {
            targetEl.html('');
            $.each(data, function (k, v) {
                targetEl.append(listCountTemplate({
                    title        : v.jobtypeName,
                    count        : v.countJobtype,
                }));
            });

            if(data.length == 1) {
                targetEl.find('.count').hide();
            }
        }
    },
    renderArchetype: function(data) {
        var currentEl = this.$el;
        var targetEl = currentEl.find('.archetypes').find('ul');
        var listCountTemplate = _.template(this.listCountTemplate);
        if(data.length != 0) {
            targetEl.html('');
            $.each(data, function (k, v) {
                targetEl.append(listCountTemplate({
                    title        : v.archetypeName,
                    count        : v.countArchetype,
                }));
            });

            if(data.length == 1) {
                targetEl.find('.count').hide();
            }
        }

    },
    renderSurveyRespondentsData: function (data) {
        if(data.length != 0) {
            All.renderPersonaData(data, this.clearDataList);
        }

    },

    fetchSurveyResultByRespondents: function() {
        this.processModel = this.getSurveyResultByRespondentModel.fetch({
            data: {
                respondents: this.respondents
            },
            type: 'POST',
            error: function() {
                alert('An error occured. Please refresh the page.');
            }
        });
    }
});
ColorPicker = Backbone.View.extend({
    prepare: function(data) {
        _.each (data, function(v, k){
            var appColor = $(v);
            appColor.colorpickerplus();
            appColor.on('changeColor', function (e, color) {
                if (color == null) {
                    $('.color-fill-icon', $(this)).addClass('colorpicker-color');
                } else {
                    $('.color-fill-icon', $(this)).removeClass('colorpicker-color');
                    $('.color-fill-icon', $(this)).css('background-color', color);
                    $('input[name='+$(this).attr('id')+']').val(color);
                }
            });
        });
    },
    setcolors: function(colors) {
        _this = this;
        _.each(colors, function(v, k) {
            _this.updatecolors(k,v);
        });
    },
    updatecolors: function(id, color) {
        $('.color-fill-icon', $('#'+id)).removeClass('colorpicker-color');
        $('.color-fill-icon', $('#'+id)).css('background-color', color);
        $('input[name='+id+']').val(color);
    }
});
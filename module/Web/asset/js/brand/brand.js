Brand = Backbone.View.extend({
    initialize: function (data) {
        this.mainView         = data.mainView;
        this.uploadView       = data.uploadView;
        this.colorUpdate      = data.colorUpdate;
        this.uploadPhotoModel = data.uploadPhotoModel;
        if ( $('.company-details').length > 0 ) {
            this.uploadView = new this.uploadView({
                el               : $('.company-details'),
                uploadPhotoModel : this.uploadPhotoModel
            });
        }
        if ( $('.color-management').length > 0 ) {
            this.mainView = new this.mainView({
                el            : $('.brand-management'),
                colorTitle    : ['#app-color', '#primary-color', '#secondary-color', '#tertiary-color'],
                colorUpdate   : this.colorUpdate,
                accountColors : accountColors
            });
        }
    }
});
if ( $('.brand-management').length > 0 ) {
    brand = new Brand({
        el               : $('.brand-management'),
        mainView         : MainView,
        uploadView       : UploadView,
        uploadPhotoModel : UploadPhotoModel,
        colorUpdate      : new ColorUpdateModel()
    });
}

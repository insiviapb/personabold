MainView = Backbone.View.extend({
    events: {
        'click #btn-update-colors': 'updateColors',
        'mouseover #upload-space': 'showUploadButton',
        'mouseleave #upload-space': 'hideUploadButton'
    },
    initialize: function (data) {
        this.colorTitle    = data.colorTitle;
        this.colorUpdate   = data.colorUpdate;
        this.accountColors = data.accountColors;
        this.render();
    },
    render: function () {
        widgets.colorpicker.prepare(this.colorTitle);
        widgets.colorpicker.setcolors(this.accountColors);
    },
    updateColors: function () {
        this.processColors = this.colorUpdate.fetch({
            data: this.$el.find('#form-color').serialize(),
            type: 'POST',
            beforeSend: function () {
                if (typeof brand.mainView.processColors != 'undefined' && brand.mainView.processColors != null) {
                    brand.mainView.processColors.abort();
                }
            },
            success:function() {
                location.reload();
            }
        });
    },
    showUploadButton: function() {
        $('#upload-space input, #upload-space span, #upload-space button').css('opacity','1');
    },
    hideUploadButton: function() {
        $('#upload-space input, #upload-space span, #upload-space button').css('opacity','0.1');
    }
});
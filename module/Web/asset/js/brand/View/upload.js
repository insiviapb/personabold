UploadView = Backbone.View.extend({
    events: {
        'click #btn-upload' : 'uploadPhoto'
    },
    initialize: function(data) {
        this.uploadPhotoModel = data.uploadPhotoModel;
        this.listenTo(this.uploadPhotoModel, 'change', this.changePhoto);
        this.initializeUploader();
    },
    initializeUploader: function() {
        $('#uploadLogo').fileupload({
            url: '/settings/process/upload-photo',
            dataType: 'json',
            done: function (e, data) {
                if (data.result.success) {
                    location.reload();
                }
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $(this).closest('#uploadLogo').find('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                        );
            }, 
            change: function (e, data) {
                $(this).closest('#uploadLogo').find('#progress').css('opacity',1);
                $(this).closest('#uploadLogo').find('#progress .progress-bar').css('width', '0%');
            },
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    },
    uploadPhoto: function() {
        this.$el.find('#userImage').click();
    },
});
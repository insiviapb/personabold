var Persona = Backbone.View.extend({
    initialize: function(data) {
        
        this.sendSurveyModel         = data.sendSurveyModel;
        this.uploadPhotoModel        = data.uploadPhotoModel;
        this.getPersonaChannelsModel = data.getPersonaChannelsModel;
        this.savePersonaSectionPositionModel = data.savePersonaSectionPositionModel;
        this.savePersonaDataModel    = data.savePersonaDataModel;

        // for persona list data view
        this.personaListBoxView      = data.personaListBoxView;

        //for persona view
        this.persona                 = data.personaView;
        this.checkPackagesModel      = data.checkPackagesModel;  
        
        // for persona compare  
        this.compare                 = data.compareView;

        //for exporting persona template
        this.exportTemplatesView       = data.exportTemplatesView;
        this.exportTemplateDetailsView = data.exportTemplateDetailsView;
        this.exportPersonaModel        = data.exportPersonaModel;
        //for persona client
        this.personaClientView       = data.personaClientView;
        this.addPersonaClientModel   = data.addPersonaClientModel;
        this.getClientPersonaModel   = data.getClientPersonaModel;

        //file management
        this.fileManagementView      = data.fileManagementView;
        this.clientFilesModel        = data.clientFilesModel;
        
        if($('.persona-list-box').length > 0) {
            this.renderPersonaListBox();
        }

        if ( $('#persona').length > 0 && !$('#persona').hasClass('personaCompare') ) {
            this.renderPersona();
        }
        if( $('.personaCompare').length > 0 ) {
            this.renderPersonaCompare();
        }
        if ( $('#export-persona-modal').length > 0 ) {
            this.renderExportPersonaTemplates();
        }
        if ( $('#exportPersonaDetailsModal').length > 0 ) {
            this.renderExportPersonaTemplateDetails();
        }
        if ( $('.audience-persona').length > 0 ) {
            this.renderPersonaMain();
        }
        if ( $('#persona-client').length > 0 ) {
            this.renderPersonaClient();
        }
        
        if ( $('.file-management').length > 0 ) {
            this.renderFileMngt();
        }
    },

    renderPersonaListBox: function () {
        this.savePersonaDataModel = new this.savePersonaDataModel;
        this.personaListBoxView = new this.personaListBoxView({
            el: $('.persona-list-box'),
            savePersonaDataModel: this.savePersonaDataModel,
            editPersonaListItemTemplate   : $('#edit-personadata-template').html()
        })
    },
    renderPersonaMain: function() {
        this.personaMainPageView = new this.personaMainPageView({
            el : $('.audience-persona')
        });
    },
    renderPersona: function() {
        this.sendSurveyModel = new this.sendSurveyModel;
        this.savePersonaSectionPositionModel = new this.savePersonaSectionPositionModel;
        this.checkPackagesModel              = new this.checkPackagesModel;
        this.personaView  = new this.persona({
            el                  : $('#persona'),
            sendSurveyModel     : this.sendSurveyModel,
            checkPackagesModel  : this.checkPackagesModel,
            savePersonaSectionPositionModel: this.savePersonaSectionPositionModel
        });
    },
    renderPersonaBoxes: function() {
        this.renderPersonaDetailsView = new this.renderPersonaDetailsView({
           el   : $('#persona') 
        });
    },
    renderPersonaCompare: function() {
        this.getPersonaChannelsModel = new this.getPersonaChannelsModel;
        this.compareView  = new this.compare({
            el                     : $('.personaCompare'),
            getPersonaChannelsModel: this.getPersonaChannelsModel
        });
    },
    renderExportPersonaTemplates: function() {
        this.exportTemplatesView = new this.exportTemplatesView({
            el   : $('#export-persona-modal')
        });
    },
    renderExportPersonaTemplateDetails: function(){
        this.exportPersonaModel = new this.exportPersonaModel;
        this.exportTemplateDetailsView = new this.exportTemplateDetailsView({
            exportModel : this.exportPersonaModel,
            el          : $('#exportPersonaDetailsModal')
        });
    },
    renderPersonaClient: function() {
        this.addPersonaClientModel = new this.addPersonaClientModel;
        this.getClientPersonaModel = new this.getClientPersonaModel;
        this.checkPackagesModel    = new this.checkPackagesModel;

        this.personaClientView     = new this.personaClientView({
           el                      : $('#persona-client'),
           personaClientTemplate   : $('#persona-client-template').html(),
           clientListMainTemplate  : $('#client-list-main').html(),
           addClientTemplate       : $('#persona-add-client').html(),
           activityMainTemplate    : $('#persona-revision-main').html(),
           activityListTemplate    : $('#persona-revision-list').html(),
           activityCompanyTemplate : $('#activity-company-list').html(),
           addPersonaClientModel   : this.addPersonaClientModel,
           getClientPersonaModel   : this.getClientPersonaModel,
           checkPackagesModel      : this.checkPackagesModel,
        });
    },
    renderFileMngt: function() {
        this.clientFilesModel   = new this.clientFilesModel();
        this.fileManagementView = new this.fileManagementView({
           el                        : $('.file-management'),
           clientFilesModel          : this.clientFilesModel,
           clientFileListingTemplate : $('#client-file-listing-template').html(),
           addFileTemplate           : $('#client-add-file-template').html()
        });
    }
});

var personaPage = new Persona({
    el                              : $('.audience-section'),
    uploadPhotoModel                : UploadPhotoModel,
    getPersonaChannelsModel         : GetPersonaChannelsModel,
    sendSurveyModel                 : SendSurveyModel,
    checkPackagesModel              : CheckPackagesModel,
    savePersonaSectionPositionModel : SavePersonaSectionPositionModel,
    addPersonaClientModel           : AddPersonaClientModel,
    getClientPersonaModel           : GetClientPersonaModel,
    exportPersonaModel              : ExportPersonaModel,
    personaView                     : PersonaView,
    compareView                     : CompareView,
    exportTemplatesView             : ExportTemplatesView,
    exportTemplateDetailsView       : ExportTemplateDetailsView,
    personaClientView               : PersonaClientsView,
    savePersonaDataModel            : SavePersonaDataModel,
    personaListBoxView              : PersonaListBoxView,
    fileManagementView              : FileManagementView,
    clientFilesModel                : ClientFilesModel
});
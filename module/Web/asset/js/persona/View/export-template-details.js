ExportTemplateDetailsView = Backbone.View.extend({
    events: {
        'click .generate-preview' : 'generatePreview',
        'click .download-pdf'     : 'downloadPDF'
    },
    initialize: function(data) {
        
        if ( typeof personaUuid != 'undefined' ) {
            this.personaUuid = personaUuid;
        }
        this.exportModel  = data.exportModel;
        this.exportIframeTemplate = _.template($('#export-iframe').html());
        this.exportLoaderTemplate = _.template($('#export-loader').html());
        this.listenTo(this.exportModel, 'change', this.exportResult);
    },
    render: function() {},
    exportResult: function() {
        var exportData = this.exportModel.toJSON();
        if ( exportData.success ) {
            this.$el.find('.export-content').html(this.exportIframeTemplate(exportData));
            this.$el.find('#view-full-preview').removeClass('hide').attr('href', exportData.pdfPreview);
            this.$el.find('.download-pdf').removeClass('hide');
        }
    },
    generatePreview: function(evt) {
        
        var details  = 'pageSize=' + $('.slct-page-size').val();
            details += '&orientation=' + $('.slct-orientation').val(),
            details += '&personaUuid=' + personaUuid,
            details += '&isMultiple=' + isMultiple;
        self =this;
        
        this.templateUuid = this.$el.find('.input-form #uuid').val();
        this.$el.find('#view-full-preview').addClass('hide');
        this.$el.find('.download-pdf').addClass('hide')
        this.processExportTemplate = this.exportModel.fetch({
            data: details + '&' + this.$el.find('#frmExportTemplate').serialize(),
            type: 'POST',
            beforeSend: function () {
                self.$el.find('.export-content').html( _.template($('#export-loader').html()));
                if (typeof personaPage.exportTemplateDetailsView.processExportTemplate != 'undefined' && personaPage.exportTemplateDetailsView.processExportTemplate != null) {
                    personaPage.exportTemplateDetailsView.processExportTemplate.abort();
                }
            },
            error: function () {
                self.$el.find('.export-content').html( _.template($('#export-error').html()));
            }
        });
    },
    downloadPDF: function() {
        var isMul = isMultiple ? '1' : '0';
        window.open( templateUrl + '/' + this.personaUuid + '/' + this.templateUuid + '/'+ isMul, '_blank'); 
    }
});
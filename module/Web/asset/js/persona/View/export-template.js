ExportTemplatesView = Backbone.View.extend({
    events: {
        'click .templates' : 'exportTemplates'
    },
    initialize: function(data) {
        if ( typeof personaTemplateData != 'undefined' ) {
            this.render();
        }
        if ( typeof personaUuid != 'undefined' ) {
            this.personaUuid = personaUuid;
        }
    },
    render: function() {
        this.personaTemplate = personaTemplateData;
             personaTemplate = _.template($('#persona-export-templates').html());
        this.$el.find('.gallery-template').html('');
        _.each(this.personaTemplate, function(v,k) {
            $('#export-persona-modal .gallery-template').append(personaTemplate(v));
        });
    },
    exportTemplates: function(evt) {
        var templateUuid = this.$el.find(evt.currentTarget).data('uuid');
        var isMul        = 0;  // is multiple
        if ( isMultiple ) {
            this.personaUuid = '';
            self = this;
            _.each(personaIds, function(v, k){
               self.personaUuid += v +':';
            });
            isMul       = 1;
            this.personaUuid = this.personaUuid.substring(0, this.personaUuid.length - 1);
        }
        if ( typeof templateUrl != 'undefined' ) {
            var templateUuid = this.$el.find(evt.currentTarget).data('uuid');
            $('#exportPersonaDetailsModal').modal('show');
            $('#exportPersonaDetailsModal #uuid').val(templateUuid);
        }
    }
});
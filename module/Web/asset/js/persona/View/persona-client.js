PersonaClientsView = Backbone.View.extend({
    currClientUuid: '',
    icons: {
        'channels'       : '<i class="fa fa-television"></i>',
        'motivation'     : '<i class="fa fa-thumbs-up"></i>',
        'knowledge'      : '<i class="fa fa-lightbulb-o"></i>',
        'personadata'    : '<i class="fa fa-star"></i>',
        'personadetails' : '<i class="fa fa-user"></i>',
        'archetype'      : '<i class="fa fa-user"></i>',
        'age'            : '<i class="fa fa-birthday-cake"></i>'
    },
    events: {
        'click .add-client'        : 'addClientPage',
        'click .btn-add-client'    : 'addClient',
        'click .btn-update-client' : 'updateClient',
        'click .navi-list > li'    : 'renderClientPersona',
        'click .persona-dashboard' : 'renderActivityList'
    },
    initialize: function (data) {
        this.personaClientTemplate   = data.personaClientTemplate;
        this.addClientTemplate       = data.addClientTemplate;
        this.clientListMainTemplate  = data.clientListMainTemplate;
        this.activityCompanyTemplate = data.activityCompanyTemplate;
        this.activityMainTemplate    = data.activityMainTemplate;
        this.activityListTemplate    = data.activityListTemplate;
        this.addPersonaClientModel   = data.addPersonaClientModel;
        this.checkPackagesModel      = data.checkPackagesModel;
        this.listenTo(this.checkPackagesModel, 'change', this.packageResult);
        
        this.clientPersonaModel = new Backbone.Model(); // client persona model which contain all persona data

        //model for left nav
        this.clientModel = new Backbone.Model();
        this.listenTo(this.clientModel, 'change', this.renderClientList);
        //set model for client in left nav
        if (Object.keys(allclient).length > 0) {
            this.clientModel.set(allclient);
        }

        // set model and activity list in dashboard
        this.personaRevisionModel = new Backbone.Model();
        if ($('.activity-list').length > 0) {
            if (Object.keys(personarevision).length > 0) {
                this.personaRevisionModel.set(personarevision);
            }
            this.renderActivityList();
        }
        this.listenTo(this.addPersonaClientModel, 'change', this.render);
    },
    renderClientList: function () {
        var clientModel  = this.clientModel.toJSON(),
            naviList     = this.$el.find('.navi-list'),
            counter      = 0,
            naviTemplate = _.template('<li class="client-listing <%= activeDetails %>" title="<%= description %>" data-name="<%= name %>" data-uuid="<%= uuid %>"><%= name %></li>');
            
        naviList.html('');
        _.each(clientModel, function (v, k) {
            if ( counter == 0 ){
                var companyDetails = {
                    name        : v.actName,
                    uuid        : v.actUuid,
                    description : v.description,
                    activeDetails : (v.actUuid == clientUuid) ? 'active' : ''
                };
                naviList.append(naviTemplate(companyDetails));
            }
            v.activeDetails = (v.uuid == clientUuid) ? 'active' : '';
            naviList.append(naviTemplate(v));
            counter++;

        });
    },
    render: function () {
        this.$el.find('.btn-add-client').html('Add Client');
        var addClient = this.addPersonaClientModel.toJSON();

        if (typeof addClient.success != 'undefined') {
            if (addClient.success) {
                
                if ( addClient.action == 'add' ) {
                    alert("Client has successfully been added.");
                    this.$el.find('#frm-add-client').find("input[type=text], textarea").val("");

                    //reset client data in left nav
                    this.clientModel.clear();
                    this.clientModel.set(addClient.data);
                } else {
                    alert("Client Details has successfully been updated.");
                }

            } else {
                alert(addClient.message);
            }
            this.addPersonaClientModel.clear();
        }
    },
    addClientPage: function () {
        template = _.template(this.addClientTemplate);
        this.$el.find('.client-main').html(template());
    },
    addClient: function (evt) {
        this.processCheckPackages = this.checkPackagesModel.fetch({
            data: {
                type : 'client'
            },
            type: 'POST',
            beforeSend: function () {
                if (typeof personaPage.personaClientView.processCheckPackages != 'undefined' && personaPage.personaClientView.processCheckPackages != null) {
                    personaPage.personaClientView.processCheckPackages.abort();
                }
            },
            error: function () {}
        });
    },
    updateClient: function (evt) {
        this.processAddClient = this.addPersonaClientModel.fetch({
            type: "POST",
            data: this.$el.find('#frm-add-client').serialize()+'&action=update',
            beforeSend: function () {
                if (typeof personaPage.personaClientView.processAddClient != 'undefined' && personaPage.personaClientView.processAddClient != null) {
                    personaPage.personaClientView.processAddClient.abort();
                }
            },
            error: function () {
                this.$el.find('.btn-add-client').html('Add Client');
            }
        });
    },
    packageResult: function() {
        
        var result = this.checkPackagesModel.toJSON();
        if ( result.success ) {
            this.processClientPackages();
        } else if (result.success == false) {
            // currrent packages
            $('#notifAddClientModal .packages.current').find('.package-label').html(result.data.current.planCode);
            $('#notifAddClientModal .packages.current').find('.package-amt').html(result.data.current.amount);
            $('#notifAddClientModal .packages.current').find('.package-survey').html(result.data.current.surveyCount);
            $('#notifAddClientModal .packages.current').find('.package-persona').html(result.data.current.personaCount);
            $('#notifAddClientModal .packages.current').find('.package-template').html(result.data.current.templateCount);
            // next packages
            $('#notifAddClientModal .packages.next').find('.package-label').html(result.data.next.planCode);
            $('#notifAddClientModal .packages.next').find('.package-amt').html(result.data.next.amount);
            $('#notifAddClientModal .packages.next').find('.package-survey').html(result.data.next.surveyCount);
            $('#notifAddClientModal .packages.next').find('.package-persona').html(result.data.next.personaCount);
            $('#notifAddClientModal .packages.next').find('.package-template').html(result.data.next.templateCount);
            $('#notifAddClientModal').modal('show');
            $('#notifAddClientModal .btn-success').click(function() {
                $('#notifAddClientModal').modal('hide');
                this.processClientPackages();
            });
            $('#notifAddClientModal .btn-cancel').click(function() {
                $('#notifAddClientModal').modal('hide');
            });
            this.checkPackagesModel.clear();
        }
    },
    processClientPackages: function() {
        this.processAddClient = this.addPersonaClientModel.fetch({
            type: "POST",
            data: this.$el.find('#frm-add-client').serialize()+'&action=add',
            beforeSend: function () {
                if (typeof personaPage.personaClientView.processAddClient != 'undefined' && personaPage.personaClientView.processAddClient != null) {
                    personaPage.personaClientView.processAddClient.abort();
                }
            },
            error: function () {
                this.$el.find('.btn-add-client').html('Add Client');
            }
        });
    },
    renderClientPersona: function (evt) {
        window.location = '/audience/'+this.$el.find(evt.currentTarget).data('uuid');
    },
    renderPersona: function (personaData) {

        var clientPersonaData = _.template(this.personaClientTemplate);
        this.$el.find('.client-main').find('#persona-container').html('');
        self = this.$el;
        _.each(personaData[this.currClientUuid], function (v, k) {
            self.find('.client-main').find('#persona-container').append(clientPersonaData(v));
        });

        this.clientPersonaModel.set(personaData);
    },
    renderActivityList: function () {
        var personaRevisions = this.personaRevisionModel.toJSON(),
                activityMainTemplate    = _.template(this.activityMainTemplate),
                activityListTemplate    = _.template(this.activityListTemplate),
                activityCompanyTemplate = _.template(this.activityCompanyTemplate)
                self                    = this,
                company                 = '';

        this.$el.find('.client-main').html(activityMainTemplate());
        
        _.each(personaRevisions, function (v, k) {
            
            revisionDetails = v.revisionDetails;
            _.each(self.icons, function(v,k){
                revisionDetails = revisionDetails.replace('['+k+']', v);
            });
            v.revisionDetails = revisionDetails;
            
            if (v.company != company) {
                self.$el.find('.client-main').find('.activity-list').append(activityCompanyTemplate({ clientName : v.company}));
                company = v.company;
            }
            v.createdAt = $.timeago(v.createdAt);
            self.$el.find('.client-main').find('.activity-list').append(activityListTemplate(v));
        });
    }
});
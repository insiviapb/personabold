FileManagementView = Backbone.View.extend({
    events: {
        'click #btn-upload'    : 'uploadFile',
        'click .btn-save-file' : 'saveFile',
        'shown.bs.modal #uploadExportFileModal': 'showFileModal'
    },
    initialize: function(data) {
        this.clientFilesModel        = data.clientFilesModel;
        this.clientFilesListingModel = new Backbone.Model();
        this.clientFLTemplate        = data.clientFileListingTemplate;
        this.addFileTemplate         = data.addFileTemplate;
        this.initializeUploader();
        this.listenTo(this.clientFilesModel, 'change', this.reload);
        this.listenTo(this.clientFilesListingModel, 'change', this.renderClientFilesListing);        
        if ( Object.keys(clientFiles).length > 0 ) {
            this.clientFilesListingModel.set(clientFiles);
        } else {
            this.renderClientFilesListing();
        }
    },
    initializeUploader: function() {
        $('#uploadFiles').fileupload({
            url: '/persona/process/upload-files',
            dataType: 'json',
            done: function (e, data) {
                if (data.result.success) {
                   $('#upload-space #files').html(data.result.file);
                   $('#txt-files').val(data.result.file);
                }
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $(this).closest('#uploadFiles').find('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                        );
            }, 
            change: function (e, data) {
                $(this).closest('#uploadFiles').find('#progress').css('opacity',1);
                $(this).closest('#uploadFiles').find('#progress .progress-bar').css('width', '0%');
            },
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    },
    render: function() {},
    uploadFile: function() {
        $('#uploadFiles #userImage').click();
    },
    saveFile: function() {
        this.processClientFiles = this.clientFilesModel.fetch({
            data: this.$el.find('#uploadFiles').serialize(),
            type: 'POST',
            beforeSend: function () {
                if (typeof personaPage.fileManagementView.processClientFiles != 'undefined' && personaPage.fileManagementView.processClientFiles != null) {
                    personaPage.fileManagementView.processClientFiles.abort();
                }
            },
            error: function () {}
        });
    },
    reload: function() {
        var result = this.clientFilesModel.toJSON();
        if ( result.success ) {
            location.reload();
        }
    },
    showFileModal: function() {
        $('#uploadFiles')[0].reset();
    },
    renderClientFilesListing: function() {
        var clientFileListing = this.clientFilesListingModel.toJSON(),
            self              = this.$el,
            clientFLTemplate  = _.template(this.clientFLTemplate),
            addFileTemplate   = _.template(this.addFileTemplate);
        this.$el.find('.file-mangement-container').html('');
        // adding of file
        this.$el.find('.file-management-container').append(addFileTemplate({}));
        _.each( clientFileListing, function(v, k) {
            self.find('.file-management-container').append(clientFLTemplate(v));
        });
    }
});
CompareView = Backbone.View.extend({
    currentPersonaUuid: '',
    events: {
        'click .persona-compare'              : 'addPersonaCompare',
        'click .btn-export-compare'           : 'exportCompare'
    },
    initialize: function(data) {
        this.render();
    },
    render: function() {
        this.renderAgeSliders();
        this.renderPieGender();
        this.renderPersonaMotivations();
        this.renderPersonaKnowledges();
        this.renderPersonaDataList();
    },
    addPersonaCompare: function(el) {
        // persona's uuid
        var persona_uuid = this.$el.find(el.currentTarget).data('uuid');
        if(persona_uuid.length != 0) {
            currentUrl = location.href;
            location.href = currentUrl + '&persona[]=' + persona_uuid;
        }
    },
    renderAgeSliders: function() {
        $('.age-slider').each(function() {
            if(typeof $(this).data('value') == 'object') {
                All.renderAgeSlider($(this), $(this).data('value'), false);
            }
        });

    },
    renderPieGender: function(el) {
        $('.gender-pie').each(function() {
            if(typeof $(this).data('value') == 'object') {
                All.loadGenderPie($(this).find('.canvas-gender'), $(this).data('value'), false);
            }
        });
    },
    reloadCompare: function(el) {
        if (this.currentPersonaUuid.length != 0) {

        }
    },

    renderPersonaChannels: function(response){
        console.log(response);
        return;
        var i = 0;
        $('.persona-channel-slider').each(function() {

            var uniqueClass = (new Date).getTime().toString() + i;
            $(this).addClass(uniqueClass);

            var channel = {
                id: $(this).data('id'),
                data_id: $(this).data('channelid'),
                type_name: 'channel',
                value: $(this).data('value'),
                name: $(this).data('name'),
                new: true
            };
            i++;
            Knowledges.addSlider(channel, "." + uniqueClass, false);
        });
    },
    renderPersonaMotivations: function(el){
        var i = 0;
        $('.persona-motivation-slider').each(function() {

            var uniqueClass = (new Date).getTime().toString() + i;
            $(this).addClass(uniqueClass);

            var motiv = {
                id: $(this).data('id'),
                data_id: $(this).data('motivationid'),
                value: $(this).data('value'),
                type_name: 'motivation',
                name: $(this).data('name'),
                new: true
            }
            i++
            Knowledges.addSlider(motiv, "." + uniqueClass, false);
        });
    },
    renderPersonaDataList: function() {
        $('.persona-fields').each(function() {
            All.renderPersonaFields(personaFields, $(this), $(this).data('persona-id'))
        });
        if(typeof(personaDataLists) == 'object') {
            $.each(personaDataLists, function (k,v) {

                var dataList = [];
                $.each(v, function(key,val){
                    var personaId = val.personaId
                    if ( val.dataType == 'list' ) {;
                        dataList.push(val);
                    }

                    // render persona channels
                    if(val.dataType == 'channel') {
                        var channelContent = JSON.parse(val.content);
                        $.each(channelContent, function(key,val) {
                            var channel = {
                                id: val.id,
                                data_id: val.globalChannelsId,
                                type_name: 'channel',
                                value:  val.value,
                                name:  val.title,
                                new: true
                            };
                            All.addSlider(channel, ".box-channels-slider-persona-"+personaId, false);
                        });
                    }

                    // render persona motivations
                    if(val.dataType == 'motivation') {
                        var motivContent = JSON.parse(val.content);

                        $.each(motivContent, function(key,val) {
                            var motiv = {
                                id: val.id,
                                data_id: val.motivation_id,
                                type_name: 'channel',
                                value:  val.value,
                                name:  val.name,
                                new: true
                            };
                            All.addSlider(motiv, ".box-motivations-slider-persona-"+personaId, false);
                        });
                    }
                });
                if(Object.keys(dataList).length != 0) {
                    All.renderPersonaData(dataList);
                }
            });
        }
    },
    renderPersonaKnowledges: function(el){
        $('.persona-knowledge-slider').each(function() {

            var uniqueClass = (new Date).getTime().toString();
            $(this).addClass(uniqueClass);

            var motiv = {
                id: $(this).data('id'),
                data_id: $(this).data('knowledgeid'),
                value: $(this).data('value'),
                type_name: 'knowledge',
                name: $(this).data('name'),
                new: true
            }

            Knowledges.addSlider(motiv, "." + uniqueClass, false);
        });
    },
    exportCompare: function() {
        $('#exportPersonaModal').modal('show')
    }
});
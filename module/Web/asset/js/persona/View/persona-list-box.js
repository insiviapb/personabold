/**
 * Created by adaloso on 11/17/16.
 */
PersonaListBoxView = Backbone.View.extend({
    events: {
        'click .saveData': 'savePersonaData'
    },
    initialize: function(data) {
        this.savePersonaDataModel = data.savePersonaDataModel;
        this.editPersonaListItemTemplate = data.editPersonaListItemTemplate;
        this.savePersonaDataModel.bind('change', this.personaDataSaved, this);
    },
    savePersonaData: function() {
        All.progress(50, $('#progressBar') );
        var personaMainBox = this.$el.find('.persona-main-box-content'),
            isCustom = false;
        if(personaMainBox.attr('data-iscustom').length != 0) {
            isCustom = true;
        }
        var _list = this.$el.find('ul li'),
            i = 0,
            data_id  = (personaMainBox.attr('data-id').length != 0)? personaMainBox.attr('data-id') : null,
            data = {},
            persona_field_id = 0,
            persona_id = $('#persona').attr('data-pid'),
            editPersonaListItemTemplate   = _.template(this.editPersonaListItemTemplate)

        if(_list.length == 0) {
            return;
        }

        if(this.$el.find('ul').hasClass('fixed-field')) {
            persona_field_id = this.$el.find('ul').attr('data-field-id');
            this.$el.find('ul').find('li input').attr('disabled', true);
        }

        //empty the view list box
        if(isCustom) {
            var listViewBox = $('.persona-main-box-content#fieldcustom' + persona_field_id + 'p' + persona_id);
        } else {
            var listViewBox = $('.persona-main-box-content#field' + persona_field_id + 'p' + persona_id);
        }

        listViewBox.find('ul').html('');

        _list.each(function(){
            if ( typeof $(this).find('input').val() != 'undefined' ) {
                if($(this).find('input').val().trim() != '') {
                    var _value = $(this).find('input').val();
                    data[i] = {'item':  _value};
                    listViewBox.find('ul').append( editPersonaListItemTemplate({value: _value}));
                }
                i++;
            }
        });

        this.processModel = personaPage.personaListBoxView.savePersonaDataModel.fetch({
            type: "POST",
            data: {
                content          : JSON.stringify(data),
                field            : 'content',
                persona_field_id : persona_field_id,
                persona_id       : persona_id,
                persona_data_id  : data_id,
                revisionId       : revisionId,
                is_custom         : isCustom,
            },
            beforeSend: function () {
                if (typeof personaPage.personaListBoxView.processModel != 'undefined' && personaPage.personaListBoxView.processModel != null) {
                    personaPage.personaListBoxView.processModel.abort();
                }
            },
        });

        All.progress(100, $('#progressBar') );
    },
    personaDataSaved: function( response ){
        if(Object.keys(response).length != 0 && response.success && response.hasOwnProperty('persona_data')) {
            var persona_data = response.persona_data,
                _customField = $('#' + persona_data.uid),
                persona_data_id = persona_data.persona_data_id;

            if( _customField.length ){
                _customField.attr('data-id', persona_data_id);
                _customField.attr('id', "fieldcustom" + persona_data_id + 'p' + All.allPersonaId);
                _customField.find('i.gear-btn').attr('data-id', persona_data_id);
            }

            var _fixedField = $('.persona-main-box#field' + persona_data.persona_field_id + 'p' + persona_data.persona_id);
            if(_fixedField.length != 0) {
                var box_id = persona_data.persona_data_id;
                _fixedField.attr('data-id', box_id);
            }
        }
        
        // clear persona data 
        this.savePersonaDataModel.clear();
        $('#personaBoxModal').modal('hide');
        //display the list to the viewer
        All.getPersonaData($('#persona').attr('data-pid'));
    },
});
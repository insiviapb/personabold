PersonaView = Backbone.View.extend({
    events: {
        'click #btn-upload'       : 'uploadPhoto',
        'click #sendSurvey'       : 'sendSurvey',
        'click .btn-survey'       : 'openSurveyModal',
        'click .revision-history' : 'reloadPage',
        'click .show-instruction' : 'showInstruction'
    },
    initialize: function(data) {
        this.sendSurveyModel = data.sendSurveyModel;
        this.savePersonaSectionPositionModel = data.savePersonaSectionPositionModel;
        this.checkPackagesModel              = data.checkPackagesModel;

        this.initializeUploader();
        this.listenTo(this.sendSurveyModel, 'change', this.surveySent);
        this.listenTo(this.checkPackagesModel, 'change', this.packageResult);
        
        this.renderSections();
        this.render();
    },
    reloadPage: function(el) {
        window.location = '/persona/revision/' +this.$el.find(el.currentTarget).data("revid");
    },
    render: function() {

        //render persona text informations in modal
        this.renderPersonaInfoText();

        //render arcehtype selectable in modal
        if(typeof archetypeData !== 'undefined') {
            this.renderArcehtypeSelectable(JSON.parse(archetypeData));
        }

        //render jobtype selectable in modal
        if(typeof jobTypeData !== 'undefined') {
            this.renderJobtypeSelectable(JSON.parse(jobTypeData));
        }

        //render industries selectable in modal
        if(typeof industryData != 'undefined') {
            this.renderIndustrySelectable(JSON.parse(industryData));
        }


        //Set the height for persona sections in modal
        this.$el.find('.persona-section').height(this.$el.find('#persona-container').height());

        //Render persona_data
        this.renderPersonaDataContent();
        this.$el.find(".persona-section").sortable({
            connectWith: ".persona-section",
            opacity: 0.8,
            handle: ".section-mover",
            containment: "#persona-container",
            placeholder: "persona-section-placeholder",
            stop: function (event, ui) {

                //set the current position of the element to be moved
                var currentPosition = {
                    column:$(this).data('column'),
                    items: $(this).sortable('serialize')
                };
                
                //set the new position of the element to be moved
                var newPosition = {
                    column: ui.item.parent().data('column'),
                    items: ui.item.parent().sortable('serialize')
                };

                var data = new Array();
                data.push(newPosition);
                if(currentPosition.column != newPosition.column) {
                    data.push(currentPosition);
                }

                this.processModel = personaPage.personaView.savePersonaSectionPositionModel.fetch({
                    type: "POST",
                    data: {
                        data: data,
                        persona_uuid: $('#persona').attr('data-uuid'),
                    },
                    beforeSend: function () {
                        if (typeof personaPage.personaView.processModel != 'undefined' && personaPage.personaView.processModel != null) {
                            personaPage.personaView.processModel.abort();
                        }
                    },
                });

            }
        });
        
    },
    renderSections: function() {
        this.$el.find(".section-items").each(function() {
            if($('#section-' + $(this).data('name')).length != 0) {
                var _template = _.template($('#section-' + $(this).data('name')).html());
                $(this).append(_template({}));
            }

        });
    },
    renderPersonaInfoText: function() {
        var hasValue = false;

        var jobtitleText = this.$el.find('#personaJobTitle').find('.persona-value').html();
        if(jobtitleText.length != 0) {
            this.$el.find('#personaInfo2Modal').find('.persona-title').val(jobtitleText);
            hasValue = true;
        }

        var jobDescription = this.$el.find('#personaJobDescription').find('.persona-value').html();
        if(jobDescription != 0) {
            this.$el.find('#personaInfo2Modal').find('.persona-jobdesc').val(jobDescription);
            hasValue = true;
        }

        var basicLife = this.$el.find('#personaBasicLife').find('.persona-value').html();
        if(basicLife.length != 0) {
            this.$el.find('#personaInfo2Modal').find('.persona-basic-life').val(basicLife);
            hasValue = true;
        }

        return hasValue;
    },
    renderPersonaDataContent: function(){
        if(typeof persona_data !== 'undefined') {
            All.renderPersonaData(persona_data);
        }
        $( ".persona-section-box" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" );
    },
    renderJobtypeSelectable: function(data) {
        //render the jobtype
        if(typeof data == "object") {
            var _selOptGroup = this.$el.find('.select-jobtype optgroup');
            _selOptGroup.html();
            $.each(data, function(k,v) {
                var jobtypeOptTemplate = _.template($('#jobtype-option-template').html());
                _selOptGroup.append(jobtypeOptTemplate({
                    id: v.id,
                    jobTypename: v.title,
                }));
            });
            this.$el.find('.select-jobtype').selectpicker('refresh');
            if(this.$el.find('#personaJobtype').find('input[name=id]').val().length != 0) {
                this.$el.find('#personaInfo2Modal').find('.select-jobtype').selectpicker("val", this.$el.find('#personaJobtype').find('input[name=id]').val());
            }
        }
    },
    renderIndustrySelectable: function(data) {
        // render the industries
        if(typeof data == "object") {
            var _selOptGroup = this.$el.find('.select-industry optgroup'),
                indCount     = 0;
            _selOptGroup.html();
            $.each(data, function(k,v) {
                var indOptTemplate = _.template($('#industry-option-template').html());
                _selOptGroup.append(indOptTemplate({
                    id: v.id,
                    industryName: v.industryName,
                    indCount    : indCount
                }));
                indCount++;
            });
            this.$el.find('.select-industry ').selectpicker('refresh');
            if(this.$el.find('#personaIndustry').find('input[name=id]').val().length != 0) {
                this.$el.find('#personaInfo2Modal').find('.select-industry ').selectpicker("val", this.$el.find('#personaIndustry').find('input[name=id]').val());
            }
        }
    },
    renderArcehtypeSelectable: function(data) {
        //render the archetype
        if(typeof data == "object") {
        var _selOptGroup = this.$el.find('.select-archetype optgroup')
            _selOptGroup.html();
            $.each(data, function(k,v) {
                var archeOptTemplate = _.template($('#archetype-option-template').html());
                _selOptGroup.append(archeOptTemplate({
                    id: v.id,
                    shortDescription: v.shortDescription,
                    description: v.description,
                    title: v.title
                }));
            });
            this.$el.find('.select-archetype').selectpicker('refresh');
            if(this.$el.find('#personaArchetype').find('input[name=id]').val().length != 0) {
                this.$el.find('#personaInfo2Modal').find('.select-archetype').selectpicker("val", this.$el.find('#personaArchetype').find('input[name=id]').val());
                All.updateSelectableDescription(this.$el.find('.select-archetype'));
            }
        }
    },

    updateDisplayPersonaInfo: function() {
        //display selected archetype
        var archetypeName = this.$el.find('#personaInfo2Modal').find('.persona-archetype ').find('.filter-option').html();
        var archetypeDesc = $('.persona-archetype ').find('.selected-description').html();
        var personaArchetype = $('#personaArchetype');
        personaArchetype.css('display', 'block')
        personaArchetype.find('.persona-value').html(archetypeName);
        personaArchetype.find('.selected-description').html(archetypeDesc);

        //display selected industry
        var industryName = this.$el.find('#personaInfo2Modal').find('.persona-industry ').find('.filter-option').html(),
            industryPos  = this.$el.find('#personaInfo2Modal').find('.select-industry option:selected').data('position'),
            industryDesc = typeof JSON.parse(industryData)[industryPos] != 'undefined' ? JSON.parse(industryData)[industryPos].description : '';
        var personaIndustry = $('#personaIndustry');
        personaIndustry.css('display','block');
        
        personaIndustry.find('.persona-value').html(industryName);
        
        if ( industryDesc.length > 0 ) {
            personaIndustry.find('.industry-description').html(industryDesc);
        }
        //display selected job industry
        //display selected industry
        var jobTypeName = this.$el.find('#personaInfo2Modal').find('.persona-jobtype ').find('.filter-option').html();
        var personaJobType = $('#personaJobtype');
        personaJobType.css('display','block');
        personaJobType.find('.persona-value').html(jobTypeName);

        //display input basic life
        $('#personaBasicLife').css('display','block').find('.persona-value').html(this.$el.find('#personaInfo2Modal').find('.persona-basic-life').val());

        //display input job title
        $('#personaJobTitle').css('display','block').find('.persona-value').html(this.$el.find('#personaInfo2Modal').find('.persona-title').val());

        //display input job description
        $('#personaJobDescription').css('display','block').find('.persona-value').html(this.$el.find('#personaInfo2Modal').find('.persona-jobdesc').val());

    },

    initializeUploader: function() {
        var personaId = (this.$el.attr('data-pid').length != 0) ? this.$el.attr('data-pid') : '';
            $('#uploadPhoto').fileupload({
            url: '/settings/process/upload-photo',
            dataType: 'json',
            done: function (e, data) {
                if (data.result.success) {
                    $('#uploadHeadshotModal').find('#upload-space').attr('style', 'background:url("'+data.result.filename+'")');
                }
                $('#persona-headshot').val(data.result.filename);
                $('form#upload-photo').append('<span class="file-ready">File is ready.</span>');
                return false;
            },
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    },
    uploadPhoto: function() {
        this.$el.find('#userImage').click();
    },

    surveySent: function() {
        var result = this.sendSurveyModel.toJSON();
        if(result.success) {
            $('.persona-modal #sendSurveyForm').before("<div class='alert alert-success' role='alert'>" + result.message + "</div>")

        } else {
            $('.persona-modal #sendSurveyForm').before("<div class='alert alert-warning' role='alert'>" + result.message + "</div>");
        }

        $('.persona-modal #sendSurvey')
            .removeClass('btn-success')
            .addClass('btn-default')
            .html('Close')
            .on('click', function() {
                location.reload();
            });

    },

    sendSurvey: function() {
        var msSendto = $('.persona-modal #msSurveySendTo').magicSuggest().getValue();
        
        for( i = 0; i < (msSendto).length ; i++ ) {
            if (!All.validateEmail(msSendto[i])) {
                alert('Some of the email is invalid. Please enter valid emails.');
                return;
            }
        }
        
        var questions = $( ".persona-modal .questionItem:checked" ).map(function() {
            return $(this).val();
        }).get();

        $('#sendsurvey-modal').find('.alert').remove();
        
        customTitles = {};

        $('.custom-titles').each(function(){
            customTitles[$(this).data('value')] = $(this).val();
        });
        
        this.processModel = this.sendSurveyModel.fetch({
            type: "POST",
            data: {
                sendTo       : msSendto,
                questions    : JSON.stringify(questions),
                message      : $('.persona-modal #surveyMessage').val(),
                persona_id   : this.$el.attr('data-pid'),
                customTitles : customTitles
            },
            beforeSend: function () {
                if (typeof personaPage.personaView.processModel != 'undefined' && personaPage.personaView.processModel != null) {
                    personaPage.personaView.processModel.abort();
                }
            },
            error: function(collection,response) {
                $('.persona-modal #sendSurveyForm').before("<div class='alert alert-warning' role='alert'>An error occured. Please try again later.</div>");
            },
        });

    },
    openSurveyModal: function() {
        this.processCheckPackages = this.checkPackagesModel.fetch({
            data: {
                type : 'survey'
            },
            type: 'POST',
            beforeSend: function () {
                if (typeof personaPage.personaView.processCheckPackages != 'undefined' && personaPage.personaView.processCheckPackages != null) {
                    personaPage.personaView.processCheckPackages.abort();
                }
            },
            error: function () {}
        });
    },
    packageResult: function() {
        var result = this.checkPackagesModel.toJSON();
        if ( result.success ) {
            $('#sendSurveyModal').modal('show');
        } else if (result.success == false) {
            // currrent packages
            this.$el.find('.packages.current').find('.package-label').html(result.data.current.planCode);
            this.$el.find('.packages.current').find('.package-amt').html(result.data.current.amount);
            this.$el.find('.packages.current').find('.package-survey').html(result.data.current.surveyCount);
            this.$el.find('.packages.current').find('.package-persona').html(result.data.current.personaCount);
            this.$el.find('.packages.current').find('.package-template').html(result.data.current.templateCount);
            // next packages
            this.$el.find('.packages.next').find('.package-label').html(result.data.next.planCode);
            this.$el.find('.packages.next').find('.package-amt').html(result.data.next.amount);
            this.$el.find('.packages.next').find('.package-survey').html(result.data.next.surveyCount);
            this.$el.find('.packages.next').find('.package-persona').html(result.data.next.personaCount);
            this.$el.find('.packages.next').find('.package-template').html(result.data.next.templateCount);
            $('#notifSendSurveySubscModal').modal('show');
            $('#notifSendSurveySubscModal .btn-success').click(function() {
                $('#notifSendSurveySubscModal').modal('hide');
                $('#sendSurveyModal').modal('show');
            });
            $('#notifSendSurveySubscModal .btn-cancel').click(function() {
                $('#notifSendSurveySubscModal').modal('hide');
            });
            this.checkPackagesModel.clear();
        }
    },
    showInstruction: function(el) {
        var target  = this.$el.find(el.currentTarget).data('target'),
            fieldId = this.$el.find(el.currentTarget).data('id');
    
        this.$el.find(target + ' .instruction-details').html(personaInstructions[fieldId]);
    }
});
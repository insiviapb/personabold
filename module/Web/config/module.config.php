<?php
return array(
    'router' => array(
        'routes' => array(
            'signout' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/signout',
                    'defaults' => array(
                        'controller' => 'zfcuser',
                        'action'     => 'logout',
                    ),
                ),
            ),
            'forgot' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/forgot',
                    'defaults' => array(
                        'controller' => 'goalioforgotpassword_forgot',
                        'action'     => 'forgot',
                    ),
                ),
            ),
        ),
    ), //router
    'controllers' => array(
        'invokables' => array(
        ),
    ), // controller
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'template_map' => array(
            'zfc-user/user/login' => __DIR__ . '/../view/zfc-user/user/login.phtml',
            'layout/web'  => __DIR__ . '/../view/layout/layout.phtml',
        ),
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                __DIR__ . '/../asset',
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'web.email.transport'       => 'Insivia\Service\Email\Transport\SmtpFactory',
            'zfcuser_redirect_callback' => 'Web\Service\Factory\RedirectCallbackFactory'
        ),
        'invokables' => array(
            'planbold.role' => 'Web\Service\Role'
        ),
        'aliases' => array(
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'roleSwitcher'       => 'Web\View\Helper\RoleSwitcher',
            'validatePermission' => 'Web\View\Helper\ValidatePermission',
            'validateUser'       => 'Web\View\Helper\ValidateSuperUser',
            'gravatar'           => 'Web\View\Helper\Gravatar',
        ),
    ),
    'module_layouts' => array(
        'Web'          => 'layout/web',
        'Audience'     => 'layout/user',
        'User'         => 'layout/user',
        'Application'  => 'layout/web',
        'Dashboard'    => 'layout/user',
        'Brand'        => 'layout/user',
        'StaticPage'   => 'layout/user'
    ),
);
<?php

namespace Web\Service;

use Insivia\Service\Role as InsiviaRole;

class Role extends InsiviaRole
{
    /**
     * @var \Planbold\Entity\User
     */
    protected $user;

    protected $userCurrentRole;

    /**
     * Get User Current Role
     * @return string
     */
    public function getUserCurrentRole()
    {
        if ($this->userCurrentRole === null) {
            $zfcUserAuthService = $this->getServiceLocator()->get('zfcuser_auth_service');
            $identity = $zfcUserAuthService->getIdentity();
            if ($identity === null) {
                return;
            }
            $this->userCurrentRole = $identity->getUserRole()->getId();
        }

        return $this->userCurrentRole;
    }

    /**
     * Set User
     *
     * @param \Planbold\Entity\User $user
     * @return User
     */
    public function setUser(\Planbold\Entity\User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        if ($this->user !== null) {
            return $this->user;
        }

        $zfcUserAuthService = $this->getServiceLocator()->get('zfcuser_auth_service');
        $identity = $zfcUserAuthService->getIdentity();

        if ($identity !== null) {
            $userMapper = $this->getServiceLocator()->get('planbold.mapper.user');
            $this->user = $userMapper->fetchOne($identity);
        }

        return $this->user;
    }
}

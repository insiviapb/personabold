<?php
namespace Web\Service\Email\Transport;

use Insivia\Service\Email\Transport\SmtpFactory as InsiviaSmtpFactory;

/**
 * Mail Transport Object
 *
 * @author Aleks Daloso <adaloso@insivia.com>
 */
class SmtpFactory extends InsiviaSmtpFactory
{

}

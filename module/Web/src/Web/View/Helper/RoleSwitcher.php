<?php
namespace Web\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class RoleSwitcher extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    
    /**
     * Route Action 
     * 
     * @var string action
     */
    protected $action;

    public function __invoke()
    {
        $sm = $this->getServiceLocator()->getServiceLocator();
        $viewHelperManager = $sm->get('ViewHelperManager');
        $urlHelper = $viewHelperManager->get('url');
        $form = $this->getForm();
        $form->setAttribute('action', $urlHelper->__invoke($this->getAction()));
        $formHelper = $viewHelperManager->get('form');
        return $formHelper->__invoke($form);
    }
    
    /**
     * Get route action
     */
    public function getAction()
    {
        if ($this->action === null) {
            $sm = $this->getServiceLocator()->getServiceLocator();
            $config = $sm->get('Config');
            $this->action = $config['role_switcher']['action'];
        }
        
        return $this->action;
    }
    
    /**
     * Set route action
     * 
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }
    
    /**
     * Get Role Switcher Form
     * 
     * @return \Zend\Form\Form
     */
    public function getForm()
    {
        $sm = $this->getServiceLocator()->getServiceLocator();
        $roleService    = $sm->get('planbold.role');
        $zfcAuthService = $sm->get('zfcuser_auth_service');
        $user = $zfcAuthService->getIdentity();
        $switcher = array();
        
        $consumer = $user->getConsumer();
        if ($consumer !== null) {
            $switcher['consumer'] = 'Consumer/' . $consumer->getFirstName();
        }
        
        $publisher = $user->getPublisher();
        if ($publisher !== null) {
            $switcher['publisher'] = 'Publisher/' . $publisher->getCompany();
        }
        
        $form = new \Zend\Form\Form('roleSwitcher');
        $form->setAttribute("action", '/test/asfa');
        $form->add(array(
            'name' => 'role',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'onchange' => 'this.form.submit();'
            )
        ));
        $form->get('role')
            ->setValueOptions($switcher)
            ->setValue($roleService->getCurrentRole());
        return $form;
    }
}

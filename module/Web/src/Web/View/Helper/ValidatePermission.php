<?php
namespace Web\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

/**
 * check permission
 * 
 * @author opasigna@insivia.com
 */
class ValidatePermission extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    
    /**
     * Check Permission
     * @return string
     */
    public function __invoke($actionType = null)
    {
        $sm           = $sm = $this->getServiceLocator()->getServiceLocator();
        $roleService  = $sm->get('planbold.role');
        $result       = 0;
        if (($actionType == 'edit' && $roleService->getUserCurrentRole() >= 2) ||
            ($actionType == 'manage' && $roleService->getUserCurrentRole() == 3)){
            $result   = 1;
        }
        return $result;
    }
}

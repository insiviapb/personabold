<?php
namespace Web\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

/**
 * check super user
 * @author brichards@insivia.com
 */
class ValidateSuperUser extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    
    /**
     * Check Super User
     * @return string
     */
    public function __invoke()
    {
        $sm               = $sm = $this->getServiceLocator()->getServiceLocator();
        $zfcUserService   = $sm->get('zfcuser_auth_service');
        $identity         = $zfcUserService->getIdentity();
        $settingsService  = $sm->get('settings.service');
        $result           = $settingsService->checkSuperUser($identity);
        
        if ( $result !== null ) {
            return true;
        }
        return false;
    }
}

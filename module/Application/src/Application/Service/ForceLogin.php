<?php

namespace Application\Service;

use Zend\Authentication\Adapter\AdapterInterface as Adapter;
use Zend\Authentication\Result as ZendResult;

class ForceLogin implements Adapter
{
    /**
     * @var UserInterface
     */
    protected $user;

    /**
     * @param $user
     */
    function __construct($user = null)
    {
        $this->user = $user;
    }

    public function authenticate()
    {
        return new ZendResult(
            ZendResult::SUCCESS,
            $this->user->getId()
        );
    }
}
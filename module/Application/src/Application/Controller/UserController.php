<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class UserController extends AbstractActionController
{
    public function emailNotificationsAction()
    {
        $sm             = $this->getServiceLocator();
        $settingService = $sm->get('settings.service');
        $settingService->processEmailNotifications();
        return new JsonModel(array('success' => true));
    }
    
    /**
     * migrating old subscription plan to new subscription plan
     * @return JsonModel
     */
    public function migrateSubscriptionAction()
    {
        $sm             = $this->getServiceLocator();
        $paymentService = $sm->get('payment.service');
        $paymentService->migrateSubscription();
        return new JsonModel(array('success' => true));
    }
    
    /**
     * Stripe Web Hooks
     */
    public function stripeWebHookAction()
    {
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        $sm             = $this->getServiceLocator();
        $config         = $sm->get('Config');
        $personaService = $sm->get('persona.service');
        \Stripe\Stripe::setApiKey($config['stripe']['secret_key']);

        // Retrieve the request's body and parse it as JSON
        $input     = @file_get_contents("php://input");
        $eventJson = json_decode($input);
        $personaService->processWebHook($eventJson);
        
        // Do something with $eventJson
        http_response_code(200); // PHP 5.4 or greater

    }
}

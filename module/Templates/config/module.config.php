<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
        )
    ),
    'controllers' => array(
        'invokables' => array(
        ),
        'factories' => array(
        ),
    ),
    'service_manager' => array(
        'factories' => array(
        ),
        'invokables' => array(
            'templates.service'         => 'Templates\\Service\\Templates',
            'templateformatter.service' => 'Templates\\Service\\TemplateFormatter',
        )
    ),
    'view_helpers' => array(
        'invokables' => array(
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                __DIR__ . '/../asset',
            ),
        ),
    ),
);

<?php

namespace Templates\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class Templates implements ServiceLocatorAwareInterface {

    use ServiceLocatorAwareTrait;

    /**
     * @var personaTemplate 
     */
    protected $personaTemplateMapper;

    /**
     * @var personaTemplateRevision 
     */
    protected $personaTemplateRevisionMapper;

    /**
     * @var personaTemplatePublished 
     */
    protected $personaTemplatePublishedMapper;

    /**
     * @var personaMapper 
     */
    protected $personaMapper;
    
    /**
     * @var subscribeAccountTemplateMapper 
     */
    protected $subscribeAccountTemplateMapper;
    
    /**
     * @var personaDataMapper 
     */
    protected $personaDataMapper;
    
    /**
     * Get Service Object
     * @param type $serviceName
     * @return type
     */
    private function getService($serviceName) {
        return $this->getServiceLocator()->get($serviceName);
    }

    /**
     * Get Persona Mapper
     * @return type
     */
    private function getPersonaMapper() {
        if (!isset($this->personaMapper)) {
            $this->personaMapper = $this->getService('planbold.mapper.persona');
        }
        return $this->personaMapper;
    }

    /**
     * Get Persona Template Mapper
     * @return type
     */
    private function getPersonaTemplateMapper() {
        if (!isset($this->personaTemplateMapper)) {
            $this->personaTemplateMapper = $this->getService('planbold.mapper.personaTemplates');
        }
        return $this->personaTemplateMapper;
    }

    /**
     * Get Persona Template Revision Mapper
     * @return type
     */
    private function getPersonaTemplateRevisionMapper() {
        if (!isset($this->personaTemplateRevisionMapper)) {
            $this->personaTemplateRevisionMapper = $this->getService('planbold.mapper.personaTemplateRevision');
        }
        return $this->personaTemplateRevisionMapper;
    }

    /**
     * Get Persona Template Published Mapper
     * @return type
     */
    private function getPersonaTemplatePublishedMapper() {
        if (!isset($this->personaTemplatePublishedMapper)) {
            $this->personaTemplatePublishedMapper = $this->getService('planbold.mapper.personaTemplatePublished');
        }
        return $this->personaTemplatePublishedMapper;
    }
    
    /**
     * Get Subscribe Account Template Mapper
     * @return type
     */
    private function getSATMapper() {
         if (!isset($this->subscribeAccountTemplateMapper)) {
            $this->subscribeAccountTemplateMapper = $this->getService('planbold.mapper.subscribeAccountTemplate');
        }
        return $this->subscribeAccountTemplateMapper;
    }

    /**
     * Get PersonaTemplate
     * @return type
     */
    public function getPersonaTemplates() {
        $personaTemplate = $this->getPersonaTemplateMapper()->fetchAll();
        return $personaTemplate->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
    
    /**
     * Get PublishedPersonaTemplate
     * @return type
     */
    public function getPublishedPersonaTemplates() {
        $personaTemplate = $this->getPersonaTemplatePublishedMapper()->fetchAll();
        return $personaTemplate->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * @return PersonaDataMapperInterface|type
     */
    public function getPersonaDataMapper()
    {
        if (null === $this->personaDataMapper) {
            $this->personaDataMapper = $this->getService('planbold.mapper.personaData');
        }

        return $this->personaDataMapper;
    }
    
    /**
     * Get latest Revision
     * @param type $uuid
     * @return type
     */
    public function getLatestRevision($uuid) {
        $templateRevision = $this->getPersonaTemplateRevisionMapper()->getLatestRevision($uuid);
        if (count($templateRevision) > 0) {
            return $this->formatPersonaTemplate($templateRevision);
        }
        return array();
    }

    /**
     * Return formatted Persona Template Revision
     * @param type $templateRevision
     * @return type
     */
    public function formatPersonaTemplate($templateRevision) {
        return array('templateName' => $templateRevision->getTemplateName(),
            'templateBody' => $templateRevision->getTemplateBody(),
            'templateId' => $templateRevision->getId());
    }

    /**
     * 
     * @param type $data
     * @return boolean
     */
    public function addUpdateTemplate($data) 
    {
        $em = $this->getService('Doctrine\\ORM\\EntityManager');
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        $new = false;
        
        try {
            if (!isset($data['uuid']) || strlen(trim($data['uuid'])) == 0 || ($data['uuid'] == "0" )) {
                $template = new \Planbold\Entity\PersonaTemplates();
                $template->setTemplateName($data['templateName']);
                $template->setTemplatePreviewPath('');
                
                $personaTemplates = $this->getPersonaTemplateMapper()->fetchAll()->getResult();
                
                if ( count($personaTemplates) > 0 ) {
                    $template->setDefaultFlag(false);
                } else {
                    $template->setDefaultFlag(true);
                }
                
                $em->persist($template);
                $new = true;
            }
            
            $templateRevision = new \Planbold\Entity\PersonaTemplateRevision();
            $templateRevision->setPublishedFlag(false);
            $templateRevision->setTemplateBody(json_encode(array('data' => $data['data'], 'css' =>  $data['css'])));
            $templateRevision->setTemplateName($data['templateName']);

            if ($new) {
                $templateRevision->setTemplateUuid($template->getUuid());
            } else {
                $templateRevision->setTemplateUuid($data['uuid']);
            }

            $em->persist($templateRevision);
            $em->flush();
            $em->getConnection()->commit();

            // update template image
            $this->updateTemplateImage($templateRevision);
            
            if ($new) {
                return array('success' => true, 'templateUuid' => $template->getUuid());
            }
            return array('success' => true);
        } catch (\Exception $e) {
           
            $em->getConnection()->rollback(); var_dump($e->getMessage());exit;
            return array('success' => false);
        }
    }
    
    /**
     * Update Image Template Revision
     * @param type $templateRevision
     * @return \Templates\Service\JsonModel
     */
    public function updateTemplateImage($templateRevision)
    {
        try {
            
            $date = new \DateTime();
            $dateFormatted = date_format($date, 'M-d-y h:i:s');
            $templateService = $this->getService('templates.service');
            $authService     = $this->getService('zfcuser_auth_service');
            $config          = $this->getService('Config');
            $currentUser     = $authService->getIdentity();
            $result          = $templateService->processPersonaData($currentUser, $templateRevision->getTemplateUuid());
            
            
            $output          = $this->serviceLocator->get('mvlabssnappy.image.service')->getOutputFromHtml($result);
            
            $fileName = md5($templateRevision->getTemplateUuid() . $dateFormatted);
            
            $imageOutput     = './module/User/asset/files/'.$fileName.'.jpg';
            file_put_contents($imageOutput, $output);
            
            $template = $this->getPersonaTemplateMapper()->findOneBy(array('uuid' => $templateRevision->getTemplateUuid()));
            $template->setTemplatePreviewPath($config['planbold']['server_url'].'/files/'.$fileName.'.jpg');
            $this->getPersonaTemplateMapper()->update($template);
        } catch (\Exception $ex) {
            var_dump($ex->getMessage());exit;
        }
    }

    /**
     * Published Persona Templates
     * @param type $data
     * @return type
     */
    public function publishedPersonaTemplates($data) 
    {
        if (!isset($data['uuid']) || strlen(trim($data['uuid'])) == 0 || ($data['uuid'] == "0" )) {
            return array('success' => false);
        }

        $templateRevision = $this->getPersonaTemplateRevisionMapper()->getLatestRevision($data['uuid']);
        $templatePublish  = $this->getPersonaTemplatePublishedMapper()->findOneByUuid($data['uuid']);
        $personaTemplates = $this->getPersonaTemplateMapper()->findOneBy(array('uuid' => $data['uuid']));
        if ((count($templateRevision) > 0 && count($templatePublish) > 0 ) &&
                $templatePublish->getTemplateRevId() == $templateRevision->getId()) {
            return array('success' => false, 'message' => 'Already published the latest revision.');
        }
        $em = $this->getService('Doctrine\\ORM\\EntityManager');
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {

            $templateRevision->setPublishedFlag(true);
            $em->persist($templateRevision);

            if (count($templatePublish) > 0) {
                $em->remove($templatePublish);
            }

            $templatePublishNew = new \Planbold\Entity\PersonaTemplatePublished();
            $templatePublishNew->setTemplateRevId($templateRevision->getId());
            $templatePublishNew->setTemplateName($templateRevision->getTemplateName());
            $templatePublishNew->setTemplateBody($templateRevision->getTemplateBody());
            $templatePublishNew->setTemplateUuid($templateRevision->getTemplateUuid());
            $templatePublishNew->setTemplatePreviewPath($personaTemplates->getTemplatePreviewPath());
            $em->persist($templatePublishNew);
            $em->flush();
            $em->getConnection()->commit();
            return array('success' => true);
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            return array('success' => false, 'message' => 'An error occured, please reload and try again later.');
        }
    }

    /**
     * Process Persona Data
     * @param type $user
     * @param type $templateUuid
     * @return boolean
     */
    public function processPersonaData($user, $templateUuid) 
    {
        $personaData = $this->getPersonaMapper()->findByUser($user->getId());
        if (count($personaData) == 0) {
            return false;
        }
        $personaDetails    = $this->getPersonaMapper()->getPersonaTemplateDetails($personaData->getId());
        $personaData       = $this->getPersonaDataMapper()->fetchAll(array('persona' => $personaData))->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
       
        foreach ( $personaData as $details ) {
            if ( $details['dataType'] == 'knowledge' ) {
                $knowledge  = json_decode($details['content'], true);
            } else if ( $details['dataType'] == 'motivation' ) {
                $motivation = json_decode($details['content'], true);
            } else if ( $details['dataType'] == 'channel' ) {
                $channels = json_decode($details['content'], true);
            } 
        }
        
        $personaTemplate   = $this->getPersonaTemplateRevisionMapper()->getLatestRevision($templateUuid);
        $templateBody      = isset(json_decode($personaTemplate->getTemplateBody())->data) ? json_decode($personaTemplate->getTemplateBody())->data : '';
        $templateCustomCss = isset(json_decode($personaTemplate->getTemplateBody())->css) ? json_decode($personaTemplate->getTemplateBody())->css : '';
        $templateFormatter = $this->getService('templateformatter.service');
        $templateFormatter->setPersonaData($personaDetails);
        $templateFormatter->setPersonaTemplate($templateBody);
        $templateFormatter->setCustomCss($templateCustomCss);
        $templateFormatter->setChannels($channels);
        $templateFormatter->setKnowledge($knowledge);
        $templateFormatter->setMotivation($motivation);
        $templateFormatter->processTemplate();  // start pushing persona data on the template.
        return $templateFormatter->getUpdatedTemplate();
    }
    
    /**
     * Process Individual Client Persona Data
     * @param type $personaUuid
     * @param type $templateUuid
     * @param type $isMultiple
     * @return boolean
     */
    public function processSinglePersonaData($personaUuid, $templateUuid, $isMultiple) 
    {
        $newTemplate = '';
        $arrPersonaUuid = array();
        if ( $isMultiple ) {
            $arrPersonaUuid = explode(':', $personaUuid);
        } else {
            array_push($arrPersonaUuid, $personaUuid);
        }
        
        $personaTemplateRev = $this->getPersonaTemplateRevisionMapper()->getLatestRevision($templateUuid);
        $personaTemplate    = $this->getPersonaTemplateMapper()->findOneBy(array('uuid' => $templateUuid));
        
        if ( $personaTemplate->getDefaultFlag() === false ) {
            $authService    = $this->getService('zfcuser_auth_service');
            $paymentService = $this->getService('payment.service');
            $currentUser    = $authService->getIdentity();
            $sat            = $this->getSATMapper()->findOneBy(array('account' => $currentUser->getAccount()));
          
            // check if already subscribe to unli template
            if ( count($sat) == 0 ) {
                $sat = new \Planbold\Entity\SubscribeAccountTemplate();
                $sat->setAccount($currentUser->getAccount());
                $sat->setTemplateUuid($templateUuid);
                $this->getSATMapper()->insert($sat);
            }
            $paymentService->processSubscription($currentUser);
        }
        
        foreach ( $arrPersonaUuid as $personaUuid) {
            
            $personaData       = $this->getPersonaMapper()->findOneByUuid($personaUuid);
            $personaDetails    = $this->getPersonaMapper()->getPersonaTemplateDetails($personaData->getId());
            $personaData       = $this->getPersonaDataMapper()->fetchAll(array('persona' => $personaData))->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
       
            foreach ( $personaData as $details ) {
                if ( $details['dataType'] == 'knowledge' ) {
                    $knowledge  = json_decode($details['content'], true);
                } else if ( $details['dataType'] == 'motivation' ) {
                    $motivation = json_decode($details['content'], true);
                } else if ( $details['dataType'] == 'channel' ) {
                    $channels = json_decode($details['content'], true);
                } 
            }

            $personaTemplate   = $this->getPersonaTemplateRevisionMapper()->getLatestRevision($templateUuid);
            $templateBody      = isset(json_decode($personaTemplate->getTemplateBody())->data) ? json_decode($personaTemplate->getTemplateBody())->data : '';
            $templateCustomCss = isset(json_decode($personaTemplate->getTemplateBody())->css) ? json_decode($personaTemplate->getTemplateBody())->css : '';
            $templateFormatter = $this->getService('templateformatter.service');
            $templateFormatter->setPersonaData($personaDetails);
            $templateFormatter->setPersonaTemplate($templateBody);
            $templateFormatter->setCustomCss($templateCustomCss);
            $templateFormatter->setChannels($channels);
            $templateFormatter->setKnowledge($knowledge);
            $templateFormatter->setMotivation($motivation);
            $templateFormatter->processTemplate();  // start pushing persona data on the template.
            $newTemplate      .= $templateFormatter->getUpdatedTemplate();

        }
        return $newTemplate;
    }

    /**
     * Format Persona Details
     * @param type $details
     */
    public function formatPersonaDetails($details)
    {
        if ( count($details) > 0 ) {
            foreach ( $details as $personaKey => $personaDetails ) {
                if ( isset($personaDetails['content']) ) {
                    $arrDetails = (array)json_decode($personaDetails['content']);
                        unset($personaDetails[$personaKey]);
                        reset($details);
                }
            }
        }
    }
}

<?php
namespace Templates\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class TemplateFormatter implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const PERSONA_NAME               = '##PERSONA-NAME';
    
    const AGE_TYPE_CHART             = '##AGE-TYPE-CHART';
    
    const AGE_TYPE_TEXT              = '##AGE-TYPE-TEXT';
    
    const GENDER_TYPE_CHART          = '##GENDER-TYPE-CHART';
    
    const GENDER_TYPE_TEXT           = '##GENDER-TYPE-TEXT';
    
    const ARCHETYPE_TITLE            = '##ARCHETYPE-TITLE';
    
    const SHORT_DESCRIPTION          = '##SHORT-DESCRIPTION';
    
    const LONG_DESCRIPTION           = '##LONG-DESCRIPTION';
    
    const JOB_TITLE                  = '##JOB-TITLE';
    
    const JOB_TYPE                   = '##JOB-TYPE';

    const JOB_DESCRIPTION            = '##JOB-DESCRIPTION';
    
    const PROFILE_PHOTO              = '##PROFILE-PHOTO';
    
    const BASIC_LIFE                 = '##BASIC-LIFE';
    
    const COMPANY_NAME               = '##COMPANY-NAME';
    
    const COMPANY_LOGO               = '##COMPANY-LOGO';
    
    const INDUSTRY_NAME              = '##INDUSTRY-NAME';
    
    const PERSONA_FIELD_TITLE        = '##PERSONA-FIELD';
    
    const PERSONA_CHANNELS           = '##PERSONA-CHANNELS';
    
    const PERSONA_KNOWLEDGES         = '##PERSONA-KNOWLEDGES';
    
    const PERSONA_MOTIVATIONS        = '##PERSONA-MOTIVATIONS';
    
    const PERSONA_CHART_CHANNELS     = '##PERSONA-CHART-CHANNELS';
    
    const PERSONA_CHART_KNOWLEDGES   = '##PERSONA-CHART-KNOWLEDGES';
    
    const PERSONA_CHART_MOTIVATIONS  = '##PERSONA-CHART-MOTIVATIONS';
    
    /**
     * @var personaData 
     */
    protected $personaData;
    
    /**
     * @var personaTemplate 
     */
    protected $personaTemplate;
    
    /**
     * @var updatedTemplate 
     */
    protected $updatedTemplate = '';
    
    /**
     * @var personaName 
     */
    protected $personaName;
    
    /**
     * @var age 
     */
    protected $age;
    
    /**
     * @var gender 
     */
    protected $gender;
    
    /**
     * @var archetype
     */
    protected $archetype;
    
    /**
     * @var shortDescription 
     */
    protected $shortDescription;
    
    /*
     * @var longDescription 
     */
    protected $longDescription;
    
    /**
     * @var jobTitle 
     */
    protected $jobTitle;
    
    /**
     * @var jobType 
     */
    protected $jobType;
    
    /**
     * @var jobDescription 
     */
    protected $jobDescription;
    
    /**
     * @var profilePhoto 
     */
    protected $profilePhoto;
    
    /**
     * @var basicLife 
     */
    protected $basicLife;
    
    /**
     * @var companyLogo 
     */
    protected $companyLogo;
    
    /**
     * @var companyName 
     */
    protected $companyName;
    
    /**
     * @var industryName 
     */
    protected $industryName;
    
    /**
     * @var personaSection 
     */
    protected $personaSection = array();
    
    /**
     * @var channels 
     */
    protected $channels = array();
    
    /**
     * @var motivations 
     */
    protected $motivations = array();
    
    /**
     * @var knowledge
     */
    protected $knowledge = array();

    /**
     * @var customCss 
     */
    protected $customCss = '';
    
    /**
     * Set Persona Data
     * @param type $personaData
     */
    public function setPersonaData($personaData)
    {
        $this->personaData = $personaData;
    }
    
    /**
     * Set Persona Template
     * @param type $personaTemplate
     */
    public function setPersonaTemplate($personaTemplate)
    {
        $this->personaTemplate = $personaTemplate;
    }
    
    /**
     * @param type $motivation
     */
    public function setMotivation($motivation)
    {
        $this->motivations = $motivation;
    }
    
    /* 
     * @param type $channels
     */
    public function setChannels($channels)
    {
        $this->channels = $channels;
    }
    
    /**
     * @param type $knowledge
     */
    public function setKnowledge($knowledge)
    {
        $this->knowledge = $knowledge;
    }
    
    /**
     * Set Persona Custom Css
     * @param type $customCss
     */
    public function setCustomCss($customCss)
    {
        $this->customCss = $customCss;
    }
    
    /**
     * Get Updated Template
     * @return type
     */
    public function getUpdatedTemplate()
    {
        return $this->updatedTemplate;
    }
    
    /**
     * process Template - pushing persona data to template
     */
    public function processTemplate()
    {
        // extracting of persona data passing to templateformatter objects
        $this->extractData();
        // binding both persona template and persona data
        $this->bindPersonaTemplateData();
    }
    
    /**
     * Extracting of persona data
     */
    private function extractData()
    {
        if ( isset($this->personaData) ) {
            
            $counter = 0;
            
            foreach ( $this->personaData as $personaData) {
                
                if ( $counter == 0 ) {
                    $this->personaName      = $personaData['personaName'];
                    $this->age              = (array)json_decode($personaData['age']);
                    $this->gender           = (array)json_decode($personaData['gender']);
                    $this->archetype        = $personaData['archetypeTitle'];
                    $this->shortDescription = $personaData['archeShort'];
                    $this->longDescription  = $personaData['archeLong'];
                    $this->jobTitle         = $personaData['jobTitle'];
                    $this->jobType          = $personaData['jobType'];
                    $this->jobDescription   = $personaData['jobDescription'];
                    $this->profilePhoto     = $personaData['personaImage'];
                    $this->basicLife        = $personaData['basicLife'];
                    $this->companyLogo      = $personaData['companyLogo'];
                    $this->companyName      = $personaData['companyName'];
                    $this->industryName     = $personaData['industryName'];
                }
                
                array_push($this->personaSection, array('personaField' => $personaData['personaFieldTitle'],
                                                        'content'      => (array)json_decode($personaData['content'])));
                $counter++;
            }
        }
    }
    
    /**
     * bind Persona Template Data
     */
    private function bindPersonaTemplateData()
    {
        $this->prependDefaultCss();
        $this->prependCustomCss();
        $this->replacePersonaName();
        $this->replaceAge();
        $this->replaceProfilePhoto();
        $this->replaceGender();
        $this->replaceArchetype();
        $this->replaceLongDescription();
        $this->replaceShortDescription();
        $this->replaceIndustryName();
        $this->replaceJobTitle();
        $this->replaceJobType();
        $this->replaceJobDescription();
        $this->replaceBasicLife();
        $this->replaceCompanyLogo();
        $this->replaceCompanyName();
        $this->replacePersonaFields();
        $this->replaceChannels();
        $this->replaceKnowledge();
        $this->replaceMotivations();
    }
    
    /**
     * Replace Persona Name
     */
    private function replacePersonaName()
    {
        preg_match_all('/'.self::PERSONA_NAME.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data ) {
            
            $personaNameHtml = "<p class='{$data[1]}'>{$this->personaName}</p>";
            $newTemplate     = str_replace($data[0], $personaNameHtml, $newTemplate);
        }
        $this->updatedTemplate = $newTemplate;
        
    }
    
    /**
     * Replace Persona Profile Photo
     */
    private function replaceProfilePhoto()
    {
        preg_match_all('/'.self::PROFILE_PHOTO.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data ) {
            $photoHtml    = "<img alt='profile photo' class='{$data[1]}' src='{$this->profilePhoto}' style='border-radius: 14px;
                                height:auto;
                                padding: 10px;
                                width: 100%;'/>";
            $newTemplate  = str_replace($data[0], $photoHtml, $newTemplate);
        }
        
        $this->updatedTemplate = $newTemplate;
    }
    
    /**
     * Replace Persona Age
     */
    private function replaceAge()
    {
        preg_match_all('/'.self::AGE_TYPE_CHART.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data ) {
            $ageHtml = '';
            foreach ( $this->age as $ageType => $ageValue ) {
                $ageHtml .= "<label class='age-type'>{$ageType}</label><div class='chart-range {$data[1]}' ><div style='width:{$ageValue}%'></div></div>";
            }
            $newTemplate = str_replace($data[0], $ageHtml, $newTemplate);
        }
        preg_match_all('/'.self::AGE_TYPE_TEXT.'_?(\w*-?\w*)/i', $this->personaTemplate, $resultText, PREG_SET_ORDER);
        foreach ( $resultText as $data ) {
            $ageHtml = '';
            foreach ( $this->age as $ageType => $ageValue ) {
                $ageHtml .= "<label class='age-type'>{$ageType}</label><p class='{$data[1]}' >{$ageValue}%</p>";
            }
            $newTemplate     = str_replace($data[0], $ageHtml, $newTemplate);
        }
        $this->updatedTemplate = $newTemplate;
    }
    
    /**
     * Replace Persona Gender
     */
    private function replaceGender()
    {
        preg_match_all('/'.self::GENDER_TYPE_CHART.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data ) {
            $genderHtml = '';
            foreach ( $this->gender as $genderType => $genderValue ) {
                $genderHtml .= "<label class='gender-type'>{$genderType}</label><div class='chart-range {$data[1]}' ><div style='width:{$genderValue}%'></div></div>";
            }
            $newTemplate = str_replace($data[0], $genderHtml, $newTemplate);
        }
        preg_match_all('/'.self::GENDER_TYPE_TEXT.'_?(\w*-?\w*)/i', $this->personaTemplate, $resultText, PREG_SET_ORDER);
        foreach ( $resultText as $data ) {
            $genderHtml = '';
            foreach ( $this->gender as $genderType => $genderValue ) {
                $genderHtml .= "<label class='gender-type'>{$genderType}</label><p class='{$data[1]}' >{$genderValue}%</p>";
            }
            $newTemplate     = str_replace($data[0], $genderHtml, $newTemplate);
        }
        $this->updatedTemplate = $newTemplate;
    }
    
    /**
     *  Replace Persona Archetype
     */
    private function replaceArchetype()
    {
        preg_match_all('/'.self::ARCHETYPE_TITLE.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data ) {
            $archeHtml   = "<p class='{$data[1]}'>{$this->archetype}</p>";
            $newTemplate = str_replace($data[0], $archeHtml, $newTemplate);
        }
        $this->updatedTemplate = $newTemplate;
    }
    
    /**
     *  Replace Short Description
     */
    private function replaceShortDescription()
    {
        preg_match_all('/'.self::SHORT_DESCRIPTION.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data ) {
            $shortDescHtml = "<p class='{$data[1]}'>{$this->shortDescription}</p>";
            $newTemplate   = str_replace($data[0], $shortDescHtml, $newTemplate);
        }
        $this->updatedTemplate = $newTemplate;
    }
    
    /**
     *  Replace Long Description
     */
    private function replaceLongDescription()
    {
        preg_match_all('/'.self::LONG_DESCRIPTION.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data ) {
            $longDescHtml = "<p class='{$data[1]}'>{$this->longDescription}</p>";
            $newTemplate  = str_replace($data[0], $longDescHtml, $newTemplate);
        }
        $this->updatedTemplate = $newTemplate;
    }
    
    /**
     *  Replace Job Title
     */
    private function replaceJobTitle()
    {
        preg_match_all('/'.self::JOB_TITLE.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data ) {
            $jobTitleHtml = "<p class='{$data[1]}'>{$this->jobTitle}</p>";
            $newTemplate     = str_replace($data[0], $jobTitleHtml, $newTemplate);
        }
        $this->updatedTemplate = $newTemplate;
    }
    
    /**
     *  Replace Job Type
     */
    private function replaceJobType()
    {
        preg_match_all('/'.self::JOB_TYPE.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data ) {
            $jobTypeHtml = "<p class='{$data[1]}'>{$this->jobType}</p>";
            $newTemplate = str_replace($data[0], $jobTypeHtml, $newTemplate);
        }
        $this->updatedTemplate = $newTemplate;
    }
    
    /**
     *  Replace Job Description
     */
    private function replaceJobDescription()
    {
        preg_match_all('/'.self::JOB_DESCRIPTION.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data ) {
            $jobHtml     = "<p class='{$data[1]}'>{$this->jobDescription}</p>";
            $newTemplate = str_replace($data[0], $jobHtml, $newTemplate);
        }
        $this->updatedTemplate = $newTemplate;
    }
    
    /**
     * Replace Basic Life Details
     */
    private function replaceBasicLife()
    {
        preg_match_all('/'.self::BASIC_LIFE.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data ) {
            $basicLifeHtml = "<p class='{$data[1]}'>{$this->basicLife}</p>";
            $newTemplate   = str_replace($data[0], $basicLifeHtml, $newTemplate);
        }
        $this->updatedTemplate = $newTemplate;
    }
    
    /**
     * Replace Industry Name
     */
    private function replaceIndustryName()
    {
        preg_match_all('/'.self::INDUSTRY_NAME.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data ) {
            $industryHtml = "<p class='{$data[1]}'>{$this->industryName}</p>";
            $newTemplate   = str_replace($data[0], $industryHtml, $newTemplate);
        }
        $this->updatedTemplate = $newTemplate;
    }
    
    /**
     * Replace Company Name
     */
    private function replaceCompanyName()
    {
        preg_match_all('/'.self::COMPANY_NAME.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data ) {
            $companyHtml = "<p class='{$data[1]}'>{$this->companyName}</p>";
            $newTemplate = str_replace($data[0], $companyHtml, $newTemplate);
        }
        $this->updatedTemplate = $newTemplate;
    }
    
    /**
     * Replace Company Logo
     */
    private function replaceCompanyLogo()
    {
        preg_match_all('/'.self::COMPANY_LOGO.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data ) {
            $companyHtml = "<img class='{$data[1]}' src='{$this->companyLogo}' />";
            $newTemplate = str_replace($data[0], $companyHtml, $newTemplate);
        }
        $this->updatedTemplate = $newTemplate;
    }
    
    /**
     * Replace Persona Fields
     */
    private function replacePersonaFields()
    {
        preg_match_all('/'.self::PERSONA_FIELD_TITLE.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data) {
            $sectionHtml = '';
            foreach ( $this->personaSection as $personaData ) {
                if ( !in_array(strtolower($personaData['personaField']), array('motivations', 'knowledge', 'channels', 'agegender'))) {
                    $sectionHtml .= "<div class='{$data[1]}'>";
                    $sectionHtml .= "<ul class='persona-div'>";
                    $sectionHtml .= "    <li class='main'>{$personaData['personaField']}</li>";
                    foreach ($personaData['content'] as $content) {
                        $sectionHtml .= "<li class='sub'>{$content->item}</li>";
                    }
                    $sectionHtml .= "</ul>";
                    $sectionHtml .= "</div>";
                }
            }
             $newTemplate = str_replace($data[0], $sectionHtml, $newTemplate);
        }
        
        $this->updatedTemplate = $newTemplate;
    }

    /**
     * Replace Channels in the template
     */
    private function replaceChannels()
    {
        preg_match_all('/'.self::PERSONA_CHANNELS.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data) {
            $sectionHtml = '';
            if ( count($this->channels) > 0 ) {
                $sectionHtml .= "<h4>CHANNELS</h4>";
            }
            $sectionHtml .= "<div class='{$data[1]}'>";
            $sectionHtml .= "<ul class='channels-div'>";
            foreach ( $this->channels as $personaData ) {
                $sectionHtml .= " <li class='main'>{$personaData['title']} : {$personaData['value']}%</li>";
            }
            $sectionHtml .= "</ul>";
            $sectionHtml .= "</div>";
            $newTemplate = str_replace($data[0], $sectionHtml, $newTemplate);
        }
        preg_match_all('/'.self::PERSONA_CHART_CHANNELS.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        
        foreach ( $result as $data) {
            $sectionHtml = '';
            if ( count($this->channels) > 0 ) {
                $sectionHtml .= '<h4>CHANNELS</h4>';
            }
            $sectionHtml .= "<div class='{$data[1]}'>";
            $sectionHtml .= "<ul class='channels-list'>";
            foreach ( $this->channels as $personaData ) {
                $sectionHtml .= " <li class='main'><label class='channels-type'>{$personaData['title']}</label><div class='chart-range ' ><div style='width:{$personaData['value']}%'></div></div></li>";
            }
            $sectionHtml .= "</ul>";
            $sectionHtml .= "</div>";
            $newTemplate = str_replace($data[0], $sectionHtml, $newTemplate);
        } 
        $this->updatedTemplate = $newTemplate;
    }
    
    /**
     * Replace Knowledge data in the template
     */
    private function replaceKnowledge()
    {
        preg_match_all('/'.self::PERSONA_KNOWLEDGES.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data) {
            $sectionHtml = '';
            if ( count($this->knowledge) > 0 ) {
                $sectionHtml .= '<h4>KNOWLEDGE</h4>';
            }
            $sectionHtml .= "<div class='{$data[1]}'>";
            $sectionHtml .= "<ul class='knowledge-div'>";
            foreach ( $this->knowledge as $personaData ) {
                $sectionHtml .= "<li class='main'>{$personaData['title']} : {$personaData['value']}%</li>";
            }
            $sectionHtml .= "</ul>";
            $sectionHtml .= "</div>";
           
            $newTemplate = str_replace($data[0], $sectionHtml, $newTemplate);
        }
        preg_match_all('/'.self::PERSONA_CHART_KNOWLEDGES.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        foreach ( $result as $data) {
            $sectionHtml = '';
            if ( count($this->knowledge) > 0 ) {
                $sectionHtml .= '<h4>KNOWLEDGE</h4>';
            }
            $sectionHtml .= "<div class='{$data[1]}'>";
            $sectionHtml .= "<ul class='knowledge-list'>";
            foreach ( $this->knowledge as $personaData ) {
                $sectionHtml .= " <li class='main'><label class='knowledge-type'>{$personaData['name']}</label><div class='chart-range ' ><div style='width:{$personaData['value']}%'></div></div></li>";
            }
            $sectionHtml .= "</ul>";
            $sectionHtml .= "</div>";
            $newTemplate = str_replace($data[0], $sectionHtml, $newTemplate);
        }
        $this->updatedTemplate = $newTemplate;
        
    }
    
    /**
     * Replace Motivations in the template
     */
    private function replaceMotivations()
    {
        preg_match_all('/'.self::PERSONA_MOTIVATIONS.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        $newTemplate = $this->updatedTemplate;
        foreach ( $result as $data) {
            $sectionHtml = '';
            if ( count($this->motivations) > 0 ) {
                $sectionHtml .= '<h4>MOTIVATIONS</h4>';
            }
            $sectionHtml .= "<div class='{$data[1]}'>";
            $sectionHtml .= "<ul class='motivations-div'>";
            foreach ( $this->motivations as $personaData ) {
                $sectionHtml .= "<li class='main'>{$personaData['title']} : {$personaData['value']}%</li>";
            }
            $sectionHtml .= "</ul>";
            $sectionHtml .= "</div>";
            $newTemplate = str_replace($data[0], $sectionHtml, $newTemplate);
        }
        preg_match_all('/'.self::PERSONA_CHART_MOTIVATIONS.'_?(\w*-?\w*)/i', $this->personaTemplate, $result, PREG_SET_ORDER);
        
        foreach ( $result as $data) {
            $sectionHtml = '';
            if ( count($this->motivations) > 0 ) {
                $sectionHtml .= '<h4>MOTIVATIONS</h4>';
            }
            $sectionHtml .= "<div class='{$data[1]}'>";
            $sectionHtml .= "<ul class='motivations-list'>";
            foreach ( $this->motivations as $personaData ) {
                $sectionHtml .= " <li class='main'><label class='motivation-type'>{$personaData['name']}</label><div class='chart-range ' ><div style='width:{$personaData['value']}%'></div></div></li>";
            }
            $sectionHtml .= "</ul>";
            $sectionHtml .= "</div>";
            $newTemplate = str_replace($data[0], $sectionHtml, $newTemplate);
        }
        $this->updatedTemplate = $newTemplate;
    }

    /**
     * Append Default Css
     */
    public function prependDefaultCss()
    {
        $this->updatedTemplate = '
            <style>
            .chart-range {
                border: 1px solid #89c64c;
                border-radius: 2px;
                height: 12px;
                width: 90%;
                background: #56723c;
            }
            .chart-range > div {
                background: #89c64c none repeat scroll 0 0;
                border-radius: 2px;
                height: 100%;
            }
            .channels-div, .knowledge-div, .motivations-div, .channels-list,
            .knowledge-list, .motivations-list, .persona-div {
                list-style:none;
                margin-left: -40px;
            }
            </style>'.$this->personaTemplate;
    }
    
    /**
     * Append Custom CSS
     */
    public function prependCustomCss()
    {
        $this->updatedTemplate = '<style>'.$this->customCss.'</style>'.$this->updatedTemplate;
    }
}
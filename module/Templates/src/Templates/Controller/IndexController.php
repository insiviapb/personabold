<?php

namespace Templates\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController {

    public function indexAction() 
    {
        return new ViewModel();
    }

    /**
     * validate login
     * @return boolean
     */
    public function validateLogin() 
    {
        $user = $this->getServiceLocator()->get('user.service');
        //validate session
        if (!$user->validateLogin()) {
            return $this->redirect()->toRoute('signin', array(), array('query' =>
                        array('redirect' => $this->getRequest()->getRequestUri())));
        }
        return true;
    }

    /**
     * Validate Processes that called by ajax
     * @return boolean
     */
    public function validateProcesses() 
    {
        $response = $this->getResponse();
        if (!$this->getRequest()->isXmlHttpRequest()) {
            // give empty response
            $response->setStatusCode(200);
            $response->setContent("");
            return $response;
        }

        // validate login session
        if ($this->validateLogin() !== true) {
            return $this->validateLogin();
        }

        return true;
    }

    /**
     * ShortCut for getServiceLocator
     * @param type $serviceName
     * @return type
     */
    public function getService($serviceName) 
    {
        return $this->getServiceLocator()->get($serviceName);
    }

}

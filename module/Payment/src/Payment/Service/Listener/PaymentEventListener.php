<?php
/**
 * Image EventListener
 *
 * @link
 * @copyright Copyright (c) 2015
 */

namespace Payment\Service\Listener;

use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\ViewModel;
use Zend\Mail\Message;

/**
 * Payment Event Listener
 * @author Oliver Pasigna <opasigna@insivia.com>
 * @SuppressWarnings(PHPMD)
 */
class PaymentEventListener implements ListenerAggregateInterface, ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    
    /**
     * List of listeners
     *
     * @var \Zend\Stdlib\CallbackHandler[]
     */
    protected $listeners = [];
    
    /**
     * Attach listeners to an event manager
     *
     * @param  EventManagerInterface $events
     * @return void
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('formatPreviousPaymentDetails.post', [$this, 'addPaymentLogs'], 500);
        $this->listeners[] = $events->attach('addUpdateCreditCard.post', [$this, 'notifyAdmin'], 400);
    }
    
    /**
     * Detach listeners from an event manager
     * @param  EventManagerInterface $events
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    
    /**
     * Add Payment Logs
     * @param EventInterface $event
     */
    public function addPaymentLogs(EventInterface $event)
    {
        $em = $this->getServiceLocator()->get('Doctrine\\ORM\\EntityManager');
        try {
            $payment = $event->getParam('formattedPayment');
            if (count($payment) != 0) {
                $mapper            = $this->getServiceLocator()->get('planbold.mapper.stripeTransaction');
                $stripeTransaction = $mapper->fetchAll(array('account' => $payment[0]['accountId']));
                if (count($stripeTransaction) != count($payment)) {
                    $diff    = count($payment) - count($stripeTransaction);
                    $counter = 0;
                    $em->getConnection()->beginTransaction();
                    foreach ( $payment as $value ) {

                        if ($counter != $diff) {
                            $stripeTransaction = new \Planbold\Entity\StripeTransaction();
                            $stripeTransaction->setAccount($value['accountId']);
                            $stripeTransaction->setInvoiceId($value['id']);
                            $stripeTransaction->setSubscriptionId($value['subscriptionId']);
                            $stripeTransaction->setChargeId($value['chargeId']);
                            $stripeTransaction->setReceiptNumber($value['receiptNumber']);
                            $stripeTransaction->setPlanId($value['planId']);
                            $stripeTransaction->setPlanName($value['planName']);
                            $stripeTransaction->setClosed($value['closed']);
                            $stripeTransaction->setPaid($value['paid']);
                            $stripeTransaction->setCurrency($value['currency']);
                            $stripeTransaction->setDiscount($value['discount']);
                            $stripeTransaction->setStartingBalance($value['startingBalance']);
                            $stripeTransaction->setEndingBalance($value['endingBalance']);
                            $stripeTransaction->setAmountDue($value['amount']);
                            $stripeTransaction->setSubTotal($value['subtotal']);
                            $stripeTransaction->setTotal($value['total']);
                            $stripeTransaction->setTaxPercent($value['taxPercent']);
                            $stripeTransaction->setDescription($value['description']);
                            $stripeTransaction->setPeriodStart(new \DateTime($value['periodStart']));
                            $stripeTransaction->setPeriodEnd(new \DateTime($value['periodEnd']));
                            $stripeTransaction->setClosed(true);
                            $em->persist($stripeTransaction);
                        }
                        $counter++;
                    }
                    $em->flush();
                    $em->getConnection()->commit();
                }
            }
        } catch (\Exception $e) {
            var_dump($e->getMessage());exit;
            $em->getConnection()->rollback();
            $event->stopPropagation(true);
        }
    }

    public function notifyAdmin()
    {
        $sm = $event->getTarget()->getServiceLocator();
        $user = $event->getParam('user');

        $view = new ViewModel(array('user' => $user));
        $view->setTemplate('email/paymentsetup-notification.phtml');
        $html = $sm->get('view_manager')
            ->getRenderer()
            ->render($view);

        $htmlMimePart = new MimePart($html);
        $htmlMimePart->setType('text/html');
        $mimeMessage = new MimeMessage();
        $mimeMessage->addPart($htmlMimePart);

        $message = $sm->get('admin.notification.payment.setup');
        $message->setBody($mimeMessage);

        $mail = $sm->get('web.email.transport');
        $mail->send($message);
    }
}

<?php

namespace Payment\Service;

use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Token;
use Stripe\Subscription;
use Stripe\Invoice;
use Stripe\Charge;
use Stripe\Transfer;
use Stripe\Account;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;

class StripePayment implements ServiceLocatorAwareInterface, EventManagerAwareInterface
{
    use ServiceLocatorAwareTrait;

    use EventManagerAwareTrait;
    
    /**
     * API Stripe
     * @var type 
     */
    protected $stripe;
    
    /**
     * API Key
     * @var type 
     */
    protected $apiKey;
    
    /**
     * Customer
     * @var type 
     */
    protected $customer;
    
    /**
     * Card Token
     * @var type 
     */
    protected $token;
   
    /**
     * Set API Key
     */
    private function setAPIKey() {
        
        $config       = $this->getServiceLocator()->get('Config');
        $this->apiKey = $config['stripe']['secret_key'];
        Stripe::setApiKey($this->apiKey);
    }
    
    /**
     * Transfer Payment
     * @param type $accountId
     * @param type $artId
     * @param type $paymentAmount
     */
    public function transferStripe($accountId, $artId, $paymentAmount)
    {
        $this->setAPIKey();
        $transfer = Transfer::create(array(
                        "amount"      => ($paymentAmount * 100),
                        "currency"    => "usd",
                        "destination" => $accountId,
                        "description" => "Voice Talent Payment for audio conversion. Content Id {$artId}"
                    ));
        return $transfer;
        
    }
    
    /**
     * Charge Stripe Account
     * @param type $customerId
     * @param type $artId
     * @param type $paymentAmount
     * @return type
     */
    public function chargeStripe($customerId, $artId, $paymentAmount)
    {
        $this->setAPIKey();
        $charge = Charge::create(array(
            'amount'      => ($paymentAmount * 100),
            'currency'    => 'usd',
            "customer"    =>  $customerId,
            "description" => "Voice Talent Payment for Content Id {$artId}",
        ));
        return $charge;
    }
    
    /**
     * Create Stripe Customer
     * @param type $email
     * @return type
     */
    public function createCustomer($email)
    {
        $this->setAPIKey();
        $this->customer = Customer::create(null, $this->apiKey);
        $this->customer->email = $email;
        $customer = $this->customer->save();
        $customer = Customer::retrieve($customer->id);
        return $customer;
    }
    
    /**
     * Create Customer Account
     * @param type $email
     * @return type
     */
    public function createAccount($email)
    {
        $this->setAPIKey();
        $account = Account::create(array(
            "managed" => false,
            "country" => "US",
            "email"   => $email
        ));
        return $account;
    }
    
    /**
     * Add Card Number
     * @param type $customerKey
     * @param type $data
     * @return type
     */
    public function addCard($customerKey, $data)
    {
        try {
            $this->setAPIKey();
            $this->token = Token::create(
                array("card" => array(
                      "name"      => $data['card_name'],
                      "number"    => $data['number'],
                      "exp_month" => $data['exp_month'],
                      "exp_year"  => $data['exp_year'],
                      "cvc"       => $data['cvc']
                ))
            );
            $this->customer = Customer::retrieve($customerKey);
            $this->customer->sources->create(array("card" => $this->token->id));
            $this->customer->save();
            $this->customer = Customer::retrieve($customerKey);
            $data['stripeCardId'] = $this->customer->sources->data[0]['id'];
            $data['brand']        = $this->customer->sources->data[0]['brand'];
            return array('success' => true, 'stripeDetails' => $data);
        } catch(\Exception $e) {
            return array('success' => false, 'message' => $e->getMessage());
        }
    }
    
    /**
     * Add Card Number
     * @param type $customerKey
     * @param type $data
     * @return type
     */
    public function updateCard($customerKey, $cardId, $data)
    {
        try {
            $this->setAPIKey();
            $this->customer  = Customer::retrieve($customerKey);
            $card            = $this->customer->sources->retrieve($cardId);
            $card->name      = $data['card_name'];
            $card->exp_month = $data['exp_month'];
            $card->exp_year  = $data['exp_year'];
            $card->save();
            return array('success' => true, 'stripeDetails' => $data);
        } catch(\Exception $e) {
            return array('success' => false, 'message' => $e->getMessage());
        }
    }
    
    /**
     * Update Billing Info in Stripe
     * @param type $customerKey
     * @param type $data
     * @return type
     */
    public function updateBilling($customerKey, $cardId, $data)
    {
        try{
            $this->setAPIKey();
            $this->customer         = Customer::retrieve($customerKey);
            $card                   = $this->customer->sources->retrieve($cardId);
            $card->address_line1    = $data['address1'];
            $card->address_line2    = $data['address2'];
            $card->address_city     = $data['city'];
            $card->address_country  = $data['country'];
            $card->address_zip      = $data['postal_code'];
            $card->save();
            return array('success' => true, 'stripeDetails' => $data);
        } catch (\Exception $ex) {
            return array('success' => false, 'message' => $ex->getMessage());
        }
    }
    
    /**
     * Add/Update Subscription
     * @param type $customerKey
     * @param type $planId
     * @return type
     */
    public function addUpdateSubscription($customerKey, $planId)
    {
        try{
            $this->setAPIKey();
            $this->customer = Customer::retrieve($customerKey);
            $this->customer->plan = $planId;
            $this->customer->save();
            $this->customer = Customer::retrieve($customerKey);
            $subscriptionId = $this->customer->subscriptions->data[0]['id'];
            return array('success'        => true, 
                         'subscriptionId' => $subscriptionId);
        } catch (\Exception $ex) {
            return array('success' => false, 
                         'message' => $ex->getMessage());
        }
    }
    
    /**
     * Process Subscription by Quantity
     * @param type $customerKey
     * @param type $planId
     * @return type
     */
    public function processSubscription($customerKey, $planId, $quantity)
    {
        try{
            $this->setAPIKey();
            $this->customer = Customer::retrieve($customerKey);
            $this->customer->plan = $planId;
            $this->customer->quantity = $quantity;
            $this->customer->save();
            $this->customer = Customer::retrieve($customerKey);
            $subscriptionId = $this->customer->subscriptions->data[0]['id'];
            return array('success'        => true, 
                         'subscriptionId' => $subscriptionId);
        } catch (\Exception $ex) {
            return array('success' => false, 
                         'message' => $ex->getMessage());
        }
    }
    
    /**
     * cancel subscription
     * @param type $subscriptionId
     * @return type
     */
    public function cancelSubscription($subscriptionId)
    {
        try{
            $this->setAPIKey();
            $plan = Subscription::retrieve($subscriptionId);
            $plan->cancel();
            return array('success'        => true);
        } catch (\Exception $ex) {
            return array('success' => false, 
                         'message' => $ex->getMessage());
        }
    }
    
    /**
     * @return Load Current Customer Invoice /Paid
     */
    public function loadPreviousCustomerInvoice($customerKey)
    {
        try{
            $this->setAPIKey();
            $invoiceData = Invoice::all(array('customer' => $customerKey));
            return $invoiceData;
        } catch (\Exception $ex) {
            return array('success' => false, 
                         'message' => $ex->getMessage());
        }
    }
    
    public function loadPreviousCustomerPayment($customerKey)
    {
        try{
            $this->setAPIKey();
            $invoiceData = Charge::all(array('customer' => $customerKey));
            return $invoiceData;
        } catch (\Exception $ex) {
            return array('success' => false, 
                         'message' => $ex->getMessage());
        }
    }
    
     /**
      * 
      * @return load Upcoming Customer Invoice /Pending
      */
    public function loadPendingCustomerInvoice($customerKey)
    {
        try{
            
            $this->setAPIKey();
            $invoiceData = Invoice::upcoming(array('customer' => $customerKey));
            return $invoiceData;
        } catch (\Exception $ex) {
            return array('success' => false, 
                         'message' => $ex->getMessage());
        }
    }
    
    /**
     * @return all paid invoices
     */
    public function loadAllPaidInvoices()
    {
        try{
            $this->setAPIKey();
            $invoiceData = Invoice::all();
            return $invoiceData;
        } catch (\Exception $ex) {
            return array('success' => false, 
                         'message' => $ex->getMessage());
        }
    }
    
    /**
     * return all pending invoices of the lisyn app
     * @return type
     */
    public function loadAllPendingInvoices()
    {
        try{
            $this->setAPIKey();
            $invoiceData = Invoice::upcoming();
            return $invoiceData;
        } catch (\Exception $ex) {
            return array('success' => false, 
                         'message' => $ex->getMessage());
        }
    }
    
    /**
     * Load specific invoice of customer
     * @param type $invoiceId
     * @return type
     */
    public function loadInvoiceDetails($invoiceId)
    {
        try{
            $this->setAPIKey();
            $invoiceData = Invoice::retrieve($invoiceId);
            return $invoiceData;
        } catch (\Exception $ex) {
            return array('success' => false, 
                         'message' => $ex->getMessage());
        }
    }
    
    /**
     * Load specific charge of customer
     * @param type $chargeId
     * @return type
     */
    public function loadChargeDetails($chargeId)
    {
        try{
            $this->setAPIKey();
            $chargeData = Charge::retrieve($chargeId);
            return $chargeData;
        } catch (\Exception $ex) {
            return array('success' => false, 
                         'message' => $ex->getMessage());
        }
    }
}
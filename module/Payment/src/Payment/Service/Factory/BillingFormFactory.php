<?php
namespace Payment\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Payment\Form\Billing;
/**
 * Billing Form Factory
 *
 * @author Oliver Pasigna <opasigna@insivia.com>
 */
class BillingFormFactory implements FactoryInterface
{
    /**
     * Create a service for DoctrineObject Hydrator
     *
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $form = new Billing('billingForm');
        return $form;
    }
}

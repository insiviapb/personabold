<?php
namespace Payment\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Payment\Form\CreditCard;
/**
 * Credit Card Form Factory
 *
 * @author Oliver Pasigna <opasigna@insivia.com>
 */
class CreditCardFormFactory implements FactoryInterface
{
    /**
     * Create a service for DoctrineObject Hydrator
     *
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $form = new CreditCard('creditCard');
        return $form;
    }
}

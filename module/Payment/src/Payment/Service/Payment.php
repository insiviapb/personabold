<?php

namespace Payment\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;

class Payment implements ServiceLocatorAwareInterface, EventManagerAwareInterface
{
    use ServiceLocatorAwareTrait;

    use EventManagerAwareTrait;
    
    /**
     * @var PublisherPackageMapperInterface
     */
    protected $publisherPackagesMapper;
    
    /**
     * @var PackagesMapper
     */
    protected $packagesMapper;
    
    /**
     * @var StripeAccountMapper 
     */
    protected $stripeAccountMapper;
    
    /**
     * @var StripeAccountMapper 
     */
    protected $stripeCardsMapper;
    
    /**
     * @var stripeAccount 
     */
    protected $stripeAccount;
    
    /**
     * @var stripeShipping 
     */
    protected $stripeShippingMapper;
    
    /**
     * @var VTStripeAccountMapper 
     */
    protected $vtStripeAccountMapper;
    
    /**
     * @var vtStripeAccountMapper 
     */
    protected $vtStripeCardsMapper;
    
    /**
     * @var vtStripeAccount 
     */
    protected $vtStripeAccount;
    
    /**
     * @var vtStripeShipping 
     */
    protected $vtStripeShippingMapper;
    
    /**
     * @var vtStripeTransactionMapper 
     */
    protected $vtStripeTransactionMapper;
    
    /**
     * @var accountMapper 
     */
    protected $accountMapper;


    /**
     * getPublisherPackagesMapper
     * @return MapperInterface
     */
    public function getPublisherPackagesMapper()
    {
        if (null === $this->publisherPackagesMapper) {
            $this->publisherPackagesMapper = $this->getServiceLocator()->get('planbold.mapper.publisherPackages');
        }
        return $this->publisherPackagesMapper;
    }
    
    /**
     * getStripeAccountMapper
     * @return MapperInterface
     */
    public function getStripeAccountMapper()
    {
        if (null === $this->stripeAccountMapper) {
            $this->stripeAccountMapper = $this->getServiceLocator()->get('planbold.mapper.stripeAccount');
        }
        return $this->stripeAccountMapper;
    }
    
    /**
     * getVTStripeTransactionMapper
     * @return MapperInterface
     */
    public function getVTStripeTransactionMapper()
    {
        if (null === $this->vtStripeTransactionMapper) {
            $this->vtStripeTransactionMapper = $this->getServiceLocator()->get('planbold.mapper.vTStripeTransaction');
        }
        return $this->vtStripeTransactionMapper;
    }
    
    /**
     * getStripeCardsMapper
     * @return MapperInterface
     */
    public function getStripeCardsMapper()
    {
        if (null === $this->stripeCardsMapper) {
            $this->stripeCardsMapper = $this->getServiceLocator()->get('planbold.mapper.stripeCards');
        }
        return $this->stripeCardsMapper;
    }
    
    /**
     * getStripeShippingMapper
     * @return MapperInterface
     */
    public function getStripeShippingMapper()
    {
        if (null === $this->stripeShippingMapper) {
            $this->stripeShippingMapper = $this->getServiceLocator()->get('planbold.mapper.stripeShipping');
        }
        return $this->stripeShippingMapper;
    }
    
    /**
     * getVTSripeAccountMapper
     * @return MapperInterface
     */
    public function getVTStripeAccountMapper()
    {
        if (null === $this->vtStripeAccountMapper) {
            $this->vtStripeAccountMapper = $this->getServiceLocator()->get('planbold.mapper.vTStripeAccount');
        }
        return $this->vtStripeAccountMapper;
    }
    
    /**
     * getVTStripeCardsMapper
     * @return MapperInterface
     */
    public function getVTStripeCardsMapper()
    {
        if (null === $this->vtStripeCardsMapper) {
            $this->vtStripeCardsMapper = $this->getServiceLocator()->get('planbold.mapper.vTStripeCards');
        }
        return $this->vtStripeCardsMapper;
    }
    
    /**
     * getVTStripeShippingMapper
     * @return MapperInterface
     */
    public function getVTStripeShippingMapper()
    {
        if (null === $this->vtStripeShippingMapper) {
            $this->vtStripeShippingMapper = $this->getServiceLocator()->get('planbold.mapper.vTStripeShipping');
        }
        return $this->vtStripeShippingMapper;
    }

    /**
     * setPublisherPackagesMapper
     * @param PublisherPackagesMapperInterface $publisherPackagesMapper
     * @return User
     */
    public function setPublisherPackagesMapper(\Planbold\Mapper\MapperInterface $publisherPackagesMapper)
    {
        $this->publisherPackagesMapper = $publisherPackagesMapper;
        return $this;
    }
    
    /**
     * getPackagesMapper
     * @return MapperInterface
     */
    public function getPackagesMapper()
    {
        if (null === $this->packagesMapper) {
            $this->packagesMapper = $this->getServiceLocator()->get('planbold.mapper.packages');
        }
        return $this->packagesMapper;
    }
    
    /**
     * getAccountMapper
     * @return MapperInterface
     */
    public function getAccountMapper()
    {
        if (null === $this->accountMapper) {
            $this->accountMapper = $this->getServiceLocator()->get('planbold.mapper.account');
        }
        return $this->accountMapper;
    }
    
    /**
     * Get current package
     * @return type
     */
    public function getCurrentPackage()
    {
        $sm             = $this->getServiceLocator();
        $authService    = $sm->get('zfcuser_auth_service');
        $currentUser    = $authService->getIdentity();
        $account        = $currentUser->getAccount();
        $currPackages   = $this->getAccountMapper()->getCurrentPackages($account);
        if ( count($currPackages) > 0 ) {
            return $currPackages[0];
        }
        return $currPackages;
    }
    
    /**
     * Get all packages
     * @return type
     */
    public function getAllPackages()
    {
        $sm          = $this->getServiceLocator();
        $allPackages = $this->getPackagesMapper()->fetchAll()->getResult();
        return $allPackages;
    }
    
    /**
     * Get Stripe Cards
     * @return type
     */
    public function getStripeCards()
    {
        $sm             = $this->getServiceLocator();
        $roleService    = $sm->get('planbold.role');
        $authService    = $sm->get('zfcuser_auth_service');
        $currentUser    = $authService->getIdentity();
        $account        = $currentUser->getAccount();
        $this->stripeAccount = $this->getStripeAccountMapper()->fetchOneBy(array('account' => $account));   
        $stripeCards    = $this->getStripeCardsMapper()->fetchPackages($this->stripeAccount);
        return $stripeCards;
    }
    
    /**
     * Get VT Stripe Cards
     * @return type
     */
    public function getVTStripeCards()
    {
        $sm             = $this->getServiceLocator();
        $authService = $sm->get('zfcuser_auth_service');
        $currentUser = $authService->getIdentity();
        $this->vtStripeAccount = $this->getVTStripeAccountMapper()->fetchOneBy(array('voiceTalent' => $currentUser->getVoiceTalent()));
        $stripeCards    = $this->getVTStripeCardsMapper()->fetchPackages($this->vtStripeAccount);
        return $stripeCards;
    }

    /**
     * Get Stripe Shipping
     * @return type
     */
    public function getStripeShipping()
    {
        $sm             = $this->getServiceLocator();
        $authService = $sm->get('zfcuser_auth_service');
        $currentUser = $authService->getIdentity();
        $account        = $currentUser->getAccount();
        $this->stripeAccount = $this->getStripeAccountMapper()->fetchOneBy(array('account' => $account));   
        $stripeShipping = $this->getStripeShippingMapper()->fetchShipping($this->stripeAccount);
        return $stripeShipping;
    }
    
    /**
     * Get VT Stripe Shipping
     * @return type
     */
    public function getVTStripeShipping()
    {
        $sm             = $this->getServiceLocator();
        $authService = $sm->get('zfcuser_auth_service');
        $currentUser = $authService->getIdentity();
        $this->vtStripeAccount = $this->getVTStripeAccountMapper()->fetchOneBy(array('voiceTalent' => $currentUser->getVoiceTalent()));   
        $stripeShipping = $this->getVTStripeShippingMapper()->fetchShipping($this->vtStripeAccount);
        return $stripeShipping;
    }
    
    /**
     * Get Next Package
     * @param type $id
     * @return type
     */
    public function getNextPackage($id = null)
    {
        $accountPackages   = $this->getAccountMapper();
        $packages          = $this->getPackagesMapper();
        $sm                = $this->getServiceLocator();
        $roleService       = $sm->get('planbold.role');
        $publisher         = $roleService->getPublisher();
        if (empty($id)) {
            $currPackages = $accountPackages->getCurrentPackages($publisher);
            $id = $currPackages[0]->getPackages()->getId();
        }
        $currPackages   = $packages->findOneById((int)$id + 1);
        return $currPackages;
    }
    
    /**
     * Add/Update Credit Cards
     * @param type $data
     * @return type
     */
    public function addUpdateCreditCard($data)
    {
        $sm            = $this->getServiceLocator();
        $stripeService = $sm->get('stripe.service');
        $stripeCards   = $this->getStripeCards();
        if (count($stripeCards) == 0) {
            // add
            $result = $stripeService->addCard($this->stripeAccount->getAccountId(), $data);
            if ($result['success']) {
                $stripeCard = new \Planbold\Entity\StripeCards();
                $stripeCard = $this->setStripeService($stripeCard, $data, true);
                $stripeCard->setStripeCardId($result['stripeDetails']['stripeCardId']);
                $stripeCard->setBrand($result['stripeDetails']['brand']);
                $stripeCard->setStripeAccount($this->stripeAccount);
                $this->getStripeCardsMapper()->insert($stripeCard);

                
                $authService         = $sm->get('zfcuser_auth_service');
                $currentUser         = $authService->getIdentity();
                
                //notify admin
                $this->getEventManager()->trigger(
                    __FUNCTION__.'.post',
                    $this,
                    array('user' => $currentUser)
                );

                return array('success' => true);
            } else {
                return $result;
            }
        } else {
            $result = $stripeService->updateCard($this->stripeAccount->getAccountId(), $stripeCards[0]->getStripeCardId(), $data);
            if ($result['success']) {
                $stripeCard = $this->setStripeService($stripeCards[0], $data, false);
                $this->getStripeCardsMapper()->update($stripeCard);
                return array('success' => true);
            } else {
                return $result;
            }
        }
        return $result;
    }
    
    /**
     * updateStripeSubscription
     * @param type $user
     * @param type $packages
     * @throws \Exception
     */
private function updateStripeSubscription($user, $packages)
    {
        $stripeAccountMapper = $this->getServiceLocator()->get('planbold.mapper.stripeAccount');
        $stripeService       = $this->getServiceLocator()->get('stripe.service');
        //Subscribe to stripe payment
        $stripeAccount = $stripeAccountMapper->fetchOneBy(array('account' => $user->getAccount()->getId()));
        $result        = $stripeService->addUpdateSubscription($stripeAccount->getAccountId(), $packages->getStripePlanCode());
        if (!$result['success']) {
            throw new \Exception($result);
        }
        $stripeAccount->setSubscriptionId($result['subscriptionId']);
        $stripeAccount->setStripePlanCode($packages->getStripePlanCode());
        $stripeAccountMapper->update($stripeAccount);
    }    
    
    /**
     * Add/Update Voice Talent Credit Cards
     * @param type $data
     * @return type
     */
    public function addUpdateVTCreditCard($data)
    {
        $sm            = $this->getServiceLocator();
        $stripeService = $sm->get('stripe.service');
        $stripeCards   = $this->getVTStripeCards();
        if (count($stripeCards) == 0) {
            // add
            $result = $stripeService->addCard($this->stripeAccount->getAccountId(), $data);
            if ($result['success']) {
                $stripeCard = new \Planbold\Entity\VTStripeCards();
                $stripeCard = $this->setStripeService($stripeCard, $data, true);
                $stripeCard->setStripeCardId($result['stripeDetails']['stripeCardId']);
                $stripeCard->setBrand($result['stripeDetails']['brand']);
                $stripeCard->setStripeAccount($this->stripeAccount);
                $this->getVTStripeCardsMapper()->insert($stripeCard);
                return array('success' => true);
            } else {
                return $result;
            }
        } else {
            $result = $stripeService->updateCard($this->vtStripeAccount->getAccountId(), $stripeCards[0]->getStripeCardId(), $data);
            if ($result['success']) {
                $stripeCard = $this->setStripeService($stripeCards[0], $data, false);
                $this->getVTStripeCardsMapper()->update($stripeCard);
                return array('success' => true);
            } else {
                return $result;
            }
        }
        return $result;
    }
    
    /**
     * Add/Update Billing
     * @param type $data
     * @return type
     */
    public function addUpdateBilling($data)
    {
        $sm             = $this->getServiceLocator(); 
        $stripeService  = $sm->get('stripe.service');
        $stripeCards    = $this->getStripeCards();
        $stripeShipping = $this->getStripeShipping();
        if (count($stripeShipping) == 0) {
            // add
            $result = $stripeService->updateBilling($this->stripeAccount->getAccountId(), $stripeCards[0]->getStripeCardId(), $data);
            if ($result['success']) {
                $stripeShipping = new \Planbold\Entity\StripeShipping();
                $stripeShipping = $this->setStripeShipping($stripeShipping, $result['stripeDetails']);
                $stripeShipping->setStripeAccount($this->stripeAccount);
                $stripeShipping->setStripeCardId($stripeCards[0]->getStripeCardId());
                $this->getStripeShippingMapper()->insert($stripeShipping);
                return array('success' => true);
            } else {
                return $result;
            }
        } else {
            $result = $stripeService->updateBilling($this->stripeAccount->getAccountId(),$stripeCards[0]->getStripeCardId(), $data);
            if ($result['success']) {
                $stripeShipping = $this->setStripeShipping($stripeShipping[0], $result['stripeDetails']);
                $this->getStripeShippingMapper()->update($stripeShipping);
                return array('success' => true);
            } else {
                return $result;
            }
        }
        return $result;
    }
    
    /**
     * Add/Update VoiceTalent Billing
     * @param type $data
     * @return type
     */
    public function addUpdateVTBilling($data)
    {
        $sm             = $this->getServiceLocator(); 
        $stripeService  = $sm->get('stripe.service');
        $stripeCards    = $this->getVTStripeCards();
        $stripeShipping = $this->getVTStripeShipping();
        if (count($stripeShipping) == 0) {
            // add
            $result = $stripeService->updateBilling($this->vtStripeAccount->getAccountId(), $stripeCards[0]->getStripeCardId(), $data);
            if ($result['success']) {
                $stripeShipping = new \Planbold\Entity\VTStripeShipping();
                $stripeShipping = $this->setStripeShipping($stripeShipping, $result['stripeDetails']);
                $stripeShipping->setStripeAccount($this->vtStripeAccount);
                $stripeShipping->setStripeCardId($stripeCards[0]->getStripeCardId());
                $this->getVTStripeShippingMapper()->insert($stripeShipping);
                return array('success' => true);
            } else {
                return $result;
            }
        } else {
            $result = $stripeService->updateBilling($this->vtStripeAccount->getAccountId(),$stripeCards[0]->getStripeCardId(), $data);
            if ($result['success']) {
                $stripeShipping = $this->setStripeShipping($stripeShipping[0], $result['stripeDetails']);
                $this->getVTStripeShippingMapper()->update($stripeShipping);
                return array('success' => true);
            } else {
                return $result;
            }
        }
        return $result;
    }
    
    /**
     * Set Stripe Service
     * @param \Planbold\Entity\StripeCards $stripeCards
     * @param type $data
     * @return \Planbold\Entity\StripeCards
     */
    public function setStripeService($stripeCards, $data, $isInsert = false)
    {
        if ($isInsert) {
            $stripeCards->setCardNumber(substr($data['number'], -4));
        }
        $stripeCards->setCardName($data['card_name']);
        $stripeCards->setExpiryMonth($data['exp_month']);
        $stripeCards->setExpiryYear($data['exp_year']);
        $stripeCards->setCvc($data['cvc']);
        return $stripeCards;
    }
    
    /**
     * Set Stripe Shipping
     * @param \Planbold\Entity\StripeShipping $stripeShipping
     * @param type $data
     * @return \Planbold\Entity\StripeShipping
     */
    public function setStripeShipping($stripeShipping, $data)
    {
        $stripeShipping->setEmail($data['email']);
        $stripeShipping->setCompany($data['company']);
        $stripeShipping->setAddress1($data['address1']);
        $stripeShipping->setAddress2($data['address2']);
        $stripeShipping->setCity($data['city']);
        $stripeShipping->setState($data['state']);
        $stripeShipping->setCountry($data['country']);
        $stripeShipping->setPostalCode($data['postal_code']);
        $stripeShipping->setPhone1($data['phone1']);
        $stripeShipping->setPhone2($data['phone2']);
        return $stripeShipping;
    }
    
    /**
     * Cancel Subscription
     * @return $stripeSubscription data
     */
    public function cancelSubscription()
    {
        $sm                  = $this->getServiceLocator();
        $roleService         = $sm->get('planbold.role');
        $stripeService       = $sm->get('stripe.service');
        $publisher           = $roleService->getPublisher();
        $this->stripeAccount = $this->getStripeAccountMapper()->fetchOneBy(array('publisher' => $publisher));
        $stripeSubscription  = $stripeService->cancelSubscription($this->stripeAccount->getSubscriptionId());
        if ($stripeSubscription['success']) {
            $publisherPackages = $this->getPublisherPackagesMapper()->fetchOneBy(array('publisher' => $publisher));
            $publisherPackages->setStatus(false);
            $this->getPublisherPackagesMapper()->update($publisherPackages);
        }
        
        return $stripeSubscription;
    }
    
    /**
     * Cancel Subscription
     * @return $stripeSubscription data
     */
    public function reactivateSubscription()
    {
        $sm                  = $this->getServiceLocator();
        $roleService         = $sm->get('planbold.role');
        $stripeService       = $sm->get('stripe.service');
        $publisher           = $roleService->getPublisher();
        $currentPackage      = $this->getCurrentPackage();
        $this->stripeAccount = $this->getStripeAccountMapper()->fetchOneBy(array('publisher' => $publisher));
        $result              = $stripeService->addUpdateSubscription($this->stripeAccount->getAccountId(), $currentPackage->getPackages()->getStripePlanCode());
        if (!$result['success']) {
            throw new \Exception($result);
        }
        $this->stripeAccount->setSubscriptionId($result['subscriptionId']);
        $currentPackage->setStatus(true);
        $this->getStripeAccountMapper()->update($this->stripeAccount);
        $this->getPublisherPackagesMapper()->update($currentPackage);
        return $result;
    }
    
    /**
     * Get Pending Payment
     * @return type
     */
    public function getPendingPayment()
    {
        $sm                  = $this->getServiceLocator();
        $stripeService       = $sm->get('stripe.service');
        $authService         = $sm->get('zfcuser_auth_service');
        $currentUser         = $authService->getIdentity();
        $this->stripeAccount = $this->getStripeAccountMapper()->fetchOneBy(array('account' => $currentUser->getAccount()));
        $stripePayment       = $stripeService->loadPendingCustomerInvoice($this->stripeAccount->getAccountId());
     
        if (isset($stripePayment['success']) && $stripePayment['success'] == false) {
            return array();
        }
        return $this->formatPendingPaymentDetails($stripePayment);
    }
    
    /**
     * Get Previous Payment
     * @return type
     */
    public function getPreviousPayment()
    {
        $sm                  = $this->getServiceLocator();
        $stripeService       = $sm->get('stripe.service');
        $authService         = $sm->get('zfcuser_auth_service');
        $currentUser         = $authService->getIdentity();
        $this->stripeAccount = $this->getStripeAccountMapper()->fetchOneBy(array('account' => $currentUser->getAccount()));
        $stripePayment       = $stripeService->loadPreviousCustomerPayment($this->stripeAccount->getAccountId());
        if (isset($stripePayment['success']) && $stripePayment['success'] == false) {
            return array();
        }
        return $this->formatPreviousPaymentDetails($stripePayment, $currentUser->getAccount()->getId());
    }
    
    /**
     * Return Invoice Specific Details
     * @param type $invoiceId
     * @return type
     */
    public function getInvoiceDetails($invoiceId)
    {
        $sm             = $this->getServiceLocator();
        $stripeService  = $sm->get('stripe.service');
        $stripePayment  = $stripeService->loadInvoiceDetails($invoiceId);
        if (isset($stripePayment['success']) && $stripePayment['success'] == false) {
             $stripePayment  = $stripeService->loadChargeDetails($invoiceId);
        }
        return $this->formatInvoiceDetails($stripePayment);
    }
    
    /**
     * Format Invoice Details
     * @param type $paymentDetails
     * @return array
     */
    public function formatInvoiceDetails($paymentDetails)
    {
        $arrFormattedPayment = array();
        $paymentDetails = $paymentDetails->__toArray(true);
        $arrFormattedPayment['id']            = $paymentDetails['id'];
        $arrFormattedPayment['amountDue']     = isset($paymentDetails['amount']) ? number_format(abs($paymentDetails['amount']/100), 2) : number_format(abs($paymentDetails['amount_due']/100), 2);
        $arrFormattedPayment['total']         = isset($paymentDetails['total'])  ? number_format(abs($paymentDetails['total']/100), 2) : number_format(abs($paymentDetails['amount']/100), 2);
        $arrFormattedPayment['receiptNumber'] = $paymentDetails['receipt_number'];
        $arrFormattedPayment['startBalance']  = number_format($paymentDetails['starting_balance']/100, 2);
        $arrFormattedPayment['endBalance']    = number_format($paymentDetails['starting_balance']/100, 2);
        $arrFormattedPayment['paid']          = $paymentDetails['paid'];
        $arrFormattedPayment['subTotal']      = isset($paymentDetails['subtotal']) ? number_format($paymentDetails['subtotal']/100,2) : number_format($paymentDetails['amount']/100, 2);
        $arrFormattedPayment['billDate']      = isset($paymentDetails['created']) ? $this->invoiceDate($paymentDetails['created']) : $this->invoiceDate($paymentDetails['date']);
        $arrFormattedPayment['startDuration'] = isset($paymentDetails['created']) ? $this->invoiceDate($paymentDetails['created']) : $this->invoiceDate($paymentDetails['lines']['data'][0]['period']['start']);
        $arrFormattedPayment['endDuration']   = isset($paymentDetails['created']) ? $this->invoiceDate($paymentDetails['created']) : $this->invoiceDate($paymentDetails['lines']['data'][0]['period']['end']);
        $arrFormattedPayment['details']       = isset($paymentDetails['description']) ? $paymentDetails['description'] : $this->invoiceDescription($paymentDetails['lines']['data'][0]);
        $arrFormattedPayment['currency']      = strtoupper($paymentDetails['currency']);
        return $arrFormattedPayment;
    }
    
    /**
     * Format Previous Payment Details
     * @param type $paymentDetails
     * @return array
     */
    public function formatPendingPaymentDetails($paymentDetails)
    {
        $arrFormattedPayment = array();
        $paymentDetails = $paymentDetails->__toArray(true);
        foreach($paymentDetails['lines']['data'] as $value) {
            $formattedPayment = array();
            $formattedPayment['amount']      = number_format(abs($value['amount']/100), 2);
            $formattedPayment['description'] = isset($value['description']) ? $value['description'] : $this->invoiceDescription($value);
            $formattedPayment['id']          = $value['id'];
            $formattedPayment['billDate']    = $this->invoiceDate($value['period']['start']);
            array_push($arrFormattedPayment, $formattedPayment);
        }
        return $arrFormattedPayment;
    }
    
    /**
     * Format Previous Payment Details
     * @param type $paymentDetails
     * @return array
     */
    public function formatPreviousPaymentDetails($paymentDetails, $accountId)
    {
        $arrFormattedPayment = array();
        $paymentDetails = $paymentDetails->__toArray(true);
        foreach($paymentDetails['data'] as $value) {
            $formattedPayment = array();
            $formattedPayment['accountId']         = $accountId;
            $formattedPayment['id']              = $value['invoice'];
            $formattedPayment['subscriptionId']  = isset($value['subscription']) ? $value['subscription'] : '';
            $formattedPayment['chargeId']        = isset($value['charge']) ? $value['charge'] : $value['id'];
            $formattedPayment['receiptNumber']   = isset($value['receipt_number']) ? $value['receipt_number'] : '';
            $formattedPayment['planId']          = isset($value['lines']) ? $value['lines']['data'][0]['plan']['id'] : '';
            $formattedPayment['planName']        = isset($value['lines']) ? $value['lines']['data'][0]['plan']['name'] : '';
            $formattedPayment['closed']          = isset($value['closed']) ? $value['closed'] : '';
            $formattedPayment['paid']            = isset($value['paid']) ? $value['paid'] : '';
            $formattedPayment['currency']        = isset($value['currency']) ? $value['currency'] : '';
            $formattedPayment['discount']        = number_format(abs((isset($value['discount']) ? $value['discount'] : 0)/100), 2);
            $formattedPayment['startingBalance'] = number_format(abs((isset($value['starting_balance']) ? $value['starting_balance'] : 0)/100), 2);
            $formattedPayment['endingBalance']   = number_format(abs((isset($value['ending_balance']) ? $value['ending_balance'] : 0 )/100), 2);
            $formattedPayment['amount']          = isset($value['amount_due']) ? number_format(abs($value['amount_due']/100), 2) : number_format(abs($value['amount']/100), 2);
            $formattedPayment['subtotal']        = isset($value['amount_due']) ? number_format(abs($value['amount_due']/100), 2) : number_format(abs((isset($value['subtotal']) ? $value['subtotal'] : 0)/100), 2);
            $formattedPayment['total']           = isset($value['total']) ? number_format(abs($value['total']/100), 2) : number_format(abs($value['amount']/100), 2);
            $formattedPayment['taxPercent']      = isset($value['tax_percent']) ? $value['tax_percent'] : '';
            $formattedPayment['description']     = isset($value['description']) ? $value['description'] : $this->invoiceDescription((isset($value['lines']) ? $value['lines']['data'][0] : 0));
            $formattedPayment['periodStart']     = isset($value['created']) ? $this->setInvoiceDate($value['created']) : $this->setInvoiceDate($value['lines']['data'][0]['period']['start']);
            $formattedPayment['periodEnd']       = isset($value['created']) ? $this->setInvoiceDate($value['created']) : $this->setInvoiceDate($value['lines']['data'][0]['period']['end']);
            $formattedPayment['billDate']        = $this->invoiceDate($value['created']);
            $formattedPayment['status']          = $value['status'];
            
            array_push($arrFormattedPayment, $formattedPayment);
        }
        // backup data
        $this->getEventManager()->trigger(
            __FUNCTION__.'.post',
            $this,
            array('formattedPayment' => $arrFormattedPayment)
        );
        return $arrFormattedPayment;
    }
    
    /**
     * Return Invoice Description
     * @param type $invoiceItem
     * @return string
     */
    public function invoiceDescription($invoiceItem)
    {
        return 'Subscription to '. $invoiceItem['plan']['name'];
    }
    
    /**
     * InvoiceDate
     * @param type $invoiceItem
     * @return type
     */
    public function invoiceDate($invoiceItem)
    {
        $date = new \DateTime();
        $date->setTimestamp($invoiceItem);
        return $date->format('M d, Y');
    }
    
    /**
     * InvoiceDate
     * @param type $invoiceItem
     * @return type
     */
    public function setInvoiceDate($invoiceItem)
    {
        $date = new \DateTime();
        $date->setTimestamp($invoiceItem);
        return $date->format('Y-m-d H:i:s');
    }
    
    /**
     * Get VoiceTalent Payment Details
     * @param type $voiceTalent
     */
    public function getVoiceTalentPaymentDetails($voiceTalent)
    {
       return $this->getVTStripeTransactionMapper()->getDetails($voiceTalent);
    }
    
    /**
     * Get VoiceTalentPending Payment Details
     * @param type $voiceTalent
     * @return type
     */
    public function getVoiceTalentPendingPaymentDetails($voiceTalent)
    {
        return $this->getVTStripeTransactionMapper()->getPendingDetails($voiceTalent);
    }
    
    /**
     * Get All Registered Packages
     * @return type
     */
    public function getPackages()
    {
        return $this->getPackagesMapper()->fetchAll(array())->getResult();
    }
    
    /**
     * Validate if subscription is allowed in service
     * @param type $user
     * @param type $data
     * @return array
     */
    public function validateSubscription($user, $data)
    {
        $isAllowed = false;
        
        if ( $this->getUserFreeTrial() != 0 ) {
            return array('success' => true);
        }
        
        $currentPackage = $this->getAccountMapper()->getCurrentPackages($user->getAccount());
        if ( isset($data['type']) && $data['type'] == 'client' ) {
            if ( count($currentPackage) != 0 && $currentPackage[0]['clientCount'] == 1) {
               return array('success' => true);
            }
            $nextPackages = $this->getPackagesMapper()->getPackages(array(
                'clientCount' => 1
            ));
        }
        if ( isset($data['type']) && $data['type'] == 'survey' ) {
            if ( count($currentPackage) != 0 && $currentPackage[0]['surveyCount'] == 1) {
               return array('success' => true);
            }
            $nextPackages = $this->getPackagesMapper()->getPackages(array(
                'clientCount' => 0,
                'surveyCount' => 1
            ));
        }
        if ( isset($data['type']) && $data['type'] == 'templates' ) {
            if ( count($currentPackage) != 0 && $currentPackage[0]['templateCount'] > 0) {
               return array('success' => true);
            }
            $nextPackages = $this->getPackagesMapper()->getPackages(array(
                'templateCount' => 1,
                'userCount'     => 5
            ));
        }
        if  ( isset($data['type']) && $data['type'] == 'users' ) {
            
            // get all users in a specific account
            $userMapper          = $this->getServiceLocator()->get('planbold.mapper.user');
            $users               = $userMapper->fetchAll(array('account' => $user->getAccount()))->getResult();
            
            if ( count($currentPackage) != 0 && ( ($currentPackage[0]['userCount'] == 1 && count($users) == 0) || 
                                                  ($currentPackage[0]['userCount'] == 5 && count($users) < 5) ||
                                                  ($currentPackage[0]['userCount'] == 6))) {
               return array('success' => true);
            }
            if ( $currentPackage[0]['userCount'] == 1 ) {
                $nextPackages = $this->getPackagesMapper()->getPackages(array(
                    'templateCount' => 1,
                    'userCount'     => 5
                ));
            } else if ( $currentPackage[0]['userCount'] == 5 ) {
                $nextPackages = $this->getPackagesMapper()->getPackages(array(
                    'clientCount' => 0,
                    'surveyCount' => 1
                ));
            } else {
                $nextPackages = $this->getPackagesMapper()->getPackages(array(
                    'clientCount' => 1
                ));
            }
        }
        
        $result = array(
            'current' => array(
                'amount'        => '$'.number_format($currentPackage[0]['amount'], 2),
                'userCount'     => $currentPackage[0]['userCount'] == 1 ? 'Unlimited persona group' : 'Unlimited persona group',
                'templateCount' => $currentPackage[0]['templateCount'] == 0 ? '1 default persona templates' : 'Unlimited persona templates',
                'surveyCount'   => $currentPackage[0]['surveyCount'] == 0 ? 'No surveys' : 'Unlimited surveys',
                'planCode'      => $currentPackage[0]['stripePlanCode'],
                'clientCount'   => $currentPackage[0]['clientCount']
            ),
            'next'    => array (
                'amount'        => '$'.number_format($nextPackages->getAmount(), 2),
                'userCount'     => $nextPackages->getUserCount() == 1 ? 'Unlimited persona group' : 'Unlimited persona group',
                'templateCount' => $nextPackages->getTemplateCount() == 0 ? '1 default persona templates' : 'Unlimited persona templates',
                'surveyCount'   => $nextPackages->getSurveyCount() == 0 ? 'No surveys' : 'Unlimited surveys',
                'planCode'      => $nextPackages->getStripePlanCode()
            )
        );
        
        return array( 'success' => false, 'data' => $result);
    }
    
    /**
     * Process Subscription
     * @param type $user
     * @param type $data
     */
    public function processSubscription($user)
    {
        $clientCount         = 0;
        $sm                  = $this->getServiceLocator();
        
        // get stripe account details
        $stripeAccountMapper = $sm->get('planbold.mapper.stripeAccount');
        $stripeAccount       = $stripeAccountMapper->findOneBy(array('account' => $user->getAccount()));
        
        // get all survey by accountId
        $surveyMapper        = $sm->get('planbold.mapper.survey');
        $survey              = $surveyMapper->findSurveyByAccount($user->getAccount());
        $surveyCount         = count($survey);
        
        // get all account that subscribe to unli template
        $satMapper           = $sm->get('planbold.mapper.subscribeAccountTemplate');
        $sat                 = $satMapper->findOneBy(array('account' => $user->getAccount()));
        
        // get all users in a specific account
        $userMapper          = $sm->get('planbold.mapper.user');
        $users               = $userMapper->fetchAll(array('account' => $user->getAccount()))->getResult();
        $userCount           = 1;
        if ( count($users) == 1 ) {
            $userCount = 1;
        }  else if ( count($users) < 5 ) {
            $userCount = 5;
        } else {
            $userCount = 6;
        }
        
        // get all clients in specific account
        $clientMapper         = $sm->get('planbold.mapper.agencyClient');
        $clients              = $clientMapper->fetchAll(array('account' => $user->getAccount()))->getResult();
        
        // get all packages
        $packageMapper = $sm->get('planbold.mapper.packages');
        
        $packageData = array(
            'userCount'     => $userCount,  
            'templateCount' => count($sat),
            'surveyCount'   => $surveyCount,
            'clientCount'   => count($clients)
        );
        $packageData = $this->preparePackageData($packageData);

        $packages = $packageMapper->getPackages($packageData);
        // begin transaction
        $em = $this->getServiceLocator()->get('Doctrine\\ORM\\EntityManager');
        $em->getConnection()->beginTransaction();
        
        try {
            if ( ($stripeAccount->getSubscriptionId() == null || strlen(trim($stripeAccount->getSubscriptionId())) == 0) ||
                 ( $packages->getStripePlanCode() != $stripeAccount->getStripePlanCode() ) ) { // for normal packages A, B and C and no subscriptionId yet
                
                $stripeAccount->setStripePlanCode($packages->getStripePlanCode());
                $em->persist($stripeAccount);
               
            }
            if ( $clientCount > 0 ) { // for agency packages update subscription
                $packages = $packageMapper->getPackages($packageData);
                $stripeAccount->setStripePlanCode($packages->getStripePlanCode());
                $em->persist($stripeAccount);
            } 
            
            $em->flush();
            $em->getConnection()->commit();
            
            if ( count($this->getStripeCards()) > 0 ) {
                $this->updateStripeSubscription($user, $packages); // add/update packages in stripe // registering subscription

            }
        } catch(\Exception $e) {
            $em->getConnection()->rollback();
        }
    }
    
    /**
     * Prepare Package Data for query
     * @param type $packageData
     * @return array
     */
    public function preparePackageData($packageData)
    {
        $retPackageData = array();
        
        if ( $packageData['clientCount'] >= 1) {  // for agency if 1 agency package is activated. 0 = false
            $retPackageData = array(
                'clientCount' => 1
            );
        } else if ( $packageData['surveyCount'] > 0 || $packageData['userCount'] > 5 ) {  // survey packages
            $retPackageData = array(
                'clientCount' => 0,
                'surveyCount' => 1
            );
        } else if ( $packageData['templateCount'] > 0 && $packageData['userCount'] < 5 ) { // template library package
            $retPackageData = array(
                'templateCount' => 1,
                'userCount'     => 5
            );
        } else {
            $retPackageData = array( // default
                'userCount' => 1
            );
        }
        return $retPackageData;
    }
    
    /**
     * Update existing subscription base on user's selection
     * @param type $user
     * @param type $data
     */
    public function updateSubscription($user, $data)
    {
        $package = $this->getPackagesMapper()->fetchOneBy(array('stripePlanCode' => $data['planCode']));
        $result = $this->updateStripeSubscription($user, $package);
        return array('success' => true);
    }
    
    /**
     * get user free trial
     * @return type
     */
    public function getUserFreeTrial()
    {   
        $authService = $this->getServiceLocator()->get('zfcuser_auth_service');
        $currentUser = $authService->getIdentity();
        $dateCreated = date_format($currentUser->getCreatedAt(), 'Ymd');
        $dateToday   = date_format(new \DateTime(), 'Ymd');
        $expiration  = ( $dateToday - $dateCreated ) >= 15 ? 0 : 15 - ( $dateToday - $dateCreated );
        return $expiration;
    }
    
    /**
     * Migrate Subscription plans from old to new
     */
    public function migrateSubscription()
    {
        $packages      = $this->getPackagesMapper()->fetchAll()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $arrPackages   = array();
        $arrPlanCode   = array();
        foreach ( $packages as $package ) {
            $arrPackages[$package['oldPlanCode']] = $package['stripePlanCode'];
            array_push($arrPlanCode, $package['stripePlanCode']);
        }
        $stripeAccount = $this->getStripeAccountMapper()->findAllSubscription();
        foreach ($stripeAccount as $accountDetails) {
            if ( !in_array($accountDetails['stripePlanCode'], $arrPlanCode) ) {
                $this->processSubscriptionMigration($accountDetails, $arrPackages[$accountDetails['stripePlanCode']]);
            }
        }
    }
    
    /**
     * process stripe account details
     * @param type $stripeAccountDetails
     */
    public function processSubscriptionMigration($stripeAccountDetails, $planId)
    {
        $sm            = $this->getServiceLocator();
        $stripeAccount = $this->getStripeAccountMapper()->findOneBy(array('id' => $stripeAccountDetails['id']));
        
        if ( count($stripeAccount) != 0 ) {
            if ( !empty($stripeAccountDetails['subscriptionId']) ) {
                $stripeService  = $sm->get('stripe.service');
                $subscriptionDetails = $stripeService->addUpdateSubscription($stripeAccountDetails['accountId'], $planId);
                $stripeAccount->setSubscriptionId($subscriptionDetails['subscriptionId']);
            }
            $stripeAccount->setStripePlanCode($planId);
            $this->getStripeAccountMapper()->update($stripeAccount);
        }
    }
}
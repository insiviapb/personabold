<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Payment\InputFilter;

use Zend\InputFilter\InputFilter;
use Lisyn\InputFilter\Input;

/**
 * Class for CreditCard FormFilter
 *
 * @author Oliver Pasigna <opasigna@insivia.com>
 */
class CreditCardFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(new Input\TextName('number', 'Please provide card-number.'));
        $this->add(new Input\TextName('cvc', 'Please provide cvc.'));
        $this->add(new Input\TextName('exp-month', 'Please provide expiry month.'));
        $this->add(new Input\TextName('exp-month', 'Please provide expiry year.'));
    }
}

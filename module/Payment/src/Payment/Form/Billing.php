<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Payment\Form;

use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Form\Form;
use Planbold\Form\Element;

/**
 * Class for Billing Form
 *
 * @author  <opasigna@insivia.com>
 */
class Billing extends Form implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    
    public function __construct($name)
    {
        parent::__construct($name);
        
        $this->add(new Element\TextName('shipping_name', array('maxlength' => 50)))
             ->add(new Element\TextName('address1', array('maxlength' => 250)))
             ->add(new Element\TextName('address2', array('maxlength' => 250)))
             ->add(new Element\TextName('phone1', array('maxlength' => 15)))
             ->add(new Element\TextName('phone2', array('maxlength' => 15)))
             ->add(new Element\TextName('city', array('maxlength' => 50)))
             ->add(new Element\TextName('email', array('maxlength' => 250)))
             ->add(new Element\TextName('company', array('maxlength' => 128)))
             ->add(new Element\TextName('postal_code', array('maxlength' => 5)))
             ->add(new Element\TextName('state', array('maxlength' => 4)))
             ->add(new Element\TextName('country', array('maxlength' => 100)));
    }
}

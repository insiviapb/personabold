<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Payment\Form;

use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Form\Form;
use Planbold\Form\Element;

/**
 * Class for Credit Card Form
 *
 * @author  <opasigna@insivia.com>
 */
class CreditCard extends Form implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    
    public function __construct($name)
    {
        parent::__construct($name);
        
        $this->add(new Element\TextName('card_name', array('maxlength' => 50)))
             ->add(new Element\TextName('number', array('maxlength' => 20)))
             ->add(new Element\TextName('exp_month', array('maxlength' => 2)))
             ->add(new Element\TextName('exp_year', array('maxlength' => 4)))
             ->add(new Element\TextName('cvc', array('maxlength' => 4)));
    }
}

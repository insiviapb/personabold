<?php

namespace Payment\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    private function init()
    {
    	$userService = $this->getServiceLocator()->get('user.service');
        //validate session
        if (!$userService->validateLogin()) {
            return $this->redirect()->toRoute('signin', array(), array( 'query' => 
                array('redirect' => $this->getRequest()->getRequestUri()) ) );
        } 
        return true;
	}
		
    public function indexAction()
    {
        
        return new ViewModel();
    }
	
    public function savedAction()
    {
        
        return new ViewModel();
    }
	
    public function historyAction()
    {
        
        return new ViewModel();
    }
	
    public function profileAction()
    {
        
        return new ViewModel();
    }
    
    
}

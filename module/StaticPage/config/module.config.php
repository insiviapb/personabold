<?php
return array(
    'router' => array(
        'routes' => array(
            'help' => array(
                'type' => 'Literal',
                'priority' => 1000,
                'options' => array(
                    'route' => '/help',
                    'defaults' => array(
                        'controller' => 'static.controller.index',
                        'action' => 'help',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'faq' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/faq',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'faq',
                            ),
                        ),
                    ),
                    'how-tos' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/how-tos',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'howTos',
                            ),
                        ),
                    ),
                    'user-guide' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/user-guide',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'userGuide',
                            ),
                        ),
                    ),
                    'getting-started' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/getting-started',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'gettingStarted',
                            ),
                        ),
                    ),
                    'account-basics' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/account-basics',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'accountBasics',
                            ),
                        ),
                    ),
                    'dashboard' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/dashboard',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'dashboard',
                            ),
                        ),
                    ),
                    'persona-management' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/persona-management',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'personaManagement',
                            ),
                        ),
                    ),
                    'persona-revision' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/persona-revision',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'personaRevision',
                            ),
                        ),
                    ),
                    'manage-client-settings' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/manage-client-settings',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'manageClientSettings',
                            ),
                        ),
                    ),
                    'user-and-role-profiles' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/user-and-role-profiles',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'userAndRoleProfiles',
                            ),
                        ),
                    ),
                    'managing-subscription' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/managing-subscription',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'managingSubscription',
                            ),
                        ),
                    ),
                    'customize-website' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/customize-website',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'customizeWebsite',
                            ),
                        ),
                    ),
                    'update-user-profile' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/update-user-profile',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'updateUserProfile',
                            ),
                        ),
                    ),
                    'upload-profile-pic' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/upload-profile-pic',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'updateProfilePic',
                            ),
                        ),
                    ),
                    'update-company-profile' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/update-company-profile',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'updateCompanyProfile',
                            ),
                        ),
                    ),
                    'upload-company-logo' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/upload-company-logo',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'uploadCompanyLogo',
                            ),
                        ),
                    ),
                    'receive-notification-alerts' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/receive-notification-alerts',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'receiveNotificationAlerts',
                            ),
                        ),
                    ),
                    'logout' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/logout',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'logout',
                            ),
                        ),
                    ),
                    'reset-password' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/reset-password',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'resetPassword',
                            ),
                        ),
                    ),'add-new-client' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/add-new-client',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'addNewClient',
                            ),
                        ),
                    ),
                    'update-client-details' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/update-client-details',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'updateClientDetails',
                            ),
                        ),
                    ),
                    'upload-files' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/upload-files',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'uploadFiles',
                            ),
                        ),
                    ),
                    'change-theme' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/change-theme',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'changeTheme',
                            ),
                        ),
                    ),
                    'view-users' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/view-users',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'viewUsers',
                            ),
                        ),
                    ),
                    'add-new-user' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/add-new-user',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'addNewuser',
                            ),
                        ),
                    ),
                    'edit-user-role' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/edit-user-role',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'editUserRole',
                            ),
                        ),
                    ),
                    'delete-user' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/delete-user',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'deleteUser',
                            ),
                        ),
                    ),
                    'update-billing-details' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/update-billing-details',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'update-billing-details',
                            ),
                        ),
                    ),
                    'change-payment-details' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/change-payment-details',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'changePaymentDetails',
                            ),
                        ),
                    ),
                    'view-current-subscription' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/view-current-subscription',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'viewCurrentSubscription',
                            ),
                        ),
                    ),
                    'change-subscription-plan' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/change-subscription-plan',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'changeSubscriptionPlan',
                            ),
                        ),
                    ),
                    'view-subscription-invoices-and-payments' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/view-subscription-invoices-and-payments',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'viewSubscriptionInvoicesAndPayments',
                            ),
                        ),
                    ),
                    'change-website-theme' => array(
                        'type' => 'Literal',
                        'priority' => 1000,
                        'options' => array(
                            'route' => '/change-website-theme',
                            'defaults' => array(
                                'controller' => 'static.controller.index',
                                'action' => 'changeWebsiteTheme',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ), // router
    'controllers' => array(
        'invokables' => array(
            'static.controller.index' => 'StaticPage\Controller\IndexController',
        ),
        'factories' => array(
            
        ),
    ), // controllers
    'service_manager' => array(
        'factories' => array(
        ),
        'invokables' => array(
            )
    ), // service_manager
    'view_helpers' => array(
        'invokables' => array(
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'template_map' => array(
            
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                __DIR__ . '/../asset',
            ),
        ),
    ),
);
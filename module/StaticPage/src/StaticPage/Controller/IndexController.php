<?php

namespace StaticPage\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class IndexController extends AbstractActionController
{
    /**
     * Index Action
     * @return ViewModel
     */
    public function helpAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function faqAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function howTosAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function userGuideAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function gettingStartedAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function accountBasicsAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function dashboardAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function personaManagementAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function personaRevisionAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function manageClientSettingsAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function userAndRoleProfilesAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function managingSubscriptionAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function customizeWebsiteAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function updateUserProfileAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }


    /**
     * Index Action
     * @return ViewModel
     */
    public function uploadProfilePicAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }


    /**
     * Index Action
     * @return ViewModel
     */
    public function updateCompanyProfileAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }


    /**
     * Index Action
     * @return ViewModel
     */
    public function uploadCompanyLogoAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function receiveNotificationAlertsAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function logoutAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function resetPasswordAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function addNewClientAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }


    /**
     * Index Action
     * @return ViewModel
     */
    public function updateClientDetailsAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function uploadFilesAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function changeThemeAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function viewUsersAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }


    /**
     * Index Action
     * @return ViewModel
     */
    public function addNewUserAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function editUserRoleAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }


    /**
     * Index Action
     * @return ViewModel
     */
    public function deleteUserAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function updateBillingDetailsAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function changePaymentDetailsAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }


    /**
     * Index Action
     * @return ViewModel
     */
    public function viewCurrentSubscriptionAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }


    /**
     * Index Action
     * @return ViewModel
     */
    public function changeSubscriptionPlanAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }

    /**
     * Index Action
     * @return ViewModel
     */
    public function viewSubscriptionInvoicesAndPaymentsAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }
    
    /**
     * Index Action
     * @return ViewModel
     */
    public function changeWebsiteThemeAction()
    {
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }
        return new ViewModel();
    }


    /*
     * validate login
     * @return boolean
     */
    public function validateLogin()
    {
        $user = $this->getServiceLocator()->get('user.service');
        //validate session
        if (!$user->validateLogin()) {
            return $this->redirect()->toRoute('signin', array(), array( 'query' =>
                array('redirect' => $this->getRequest()->getRequestUri()) ) );
        }
        return true;
    }

    /**
     * Validate Processes that called by ajax
     * @return boolean
     */
    public function validateProcesses()
    {
        $response = $this->getResponse();
        if (!$this->getRequest()->isXmlHttpRequest()) {
            // give empty response
            $response->setStatusCode(200);
            $response->setContent("");
            return $response;
        }

        // validate login session
        if($this->validateLogin() !== true) {
            return $this->validateLogin();
        }

        return true;
    }

    /**
     * ShortCut for GetServiceLocator
     * @param type $serviceName
     * @return type
     */
    public function getService($serviceName)
    {
        return $this->getServiceLocator()->get($serviceName);
    }
    
}

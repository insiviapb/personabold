<?php
ini_set('display_startup_errors',0);
ini_set('display_errors',0);
ini_set('error_reporting', E_ALL);
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */

chdir(dirname(__DIR__));
// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

// Setup autoloading
require 'init_autoloader.php';
require 'e_errorhandler.php';

if (!defined('APPLICATION_PATH')) {
    define('APPLICATION_PATH', realpath(__DIR__ . '/../'));
}

if (!defined('SITE_ENV')) {
    define('SITE_ENV', isset($_SERVER['SITE_ENV']) ? $_SERVER['SITE_ENV'] : '');
}

$appConfig = include APPLICATION_PATH . '/config/application.config.php';

if (file_exists(APPLICATION_PATH . '/config/development.config.php')) {
    $appConfig = Zend\Stdlib\ArrayUtils::merge($appConfig, include APPLICATION_PATH . '/config/development.config.php');
}

// Run the application!
// Zend\Mvc\Application::init(require 'config/application.config.php')->run();

// Run the application!
Zend\Mvc\Application::init($appConfig)->run();

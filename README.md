#PlanBold ZF2

## Installation
This project use `composer` to load all deppendecies. If you do not have `composer` please install it first. (Ignore this if you have `composer` installed)

```
curl -s https://getcomposer.org/installer | php --
```

Run composer selft update:

```
php composer.phar self-update
```



Then load all dependencies that required on this project

```
php composer.phar install
```

## Setup
This project use `Doctrine ORM` to work with database. Before trying the application please configure database connection first 

```
cp config/autoload/doctrine.local.php.dist config/autoload/doctrine.local.php
```

Open file `config/autoload/doctrine.local.php` and adjust this section with your database credential

```php
 'params' => array(
        'host'     => '127.0.0.1',
        'dbname'   => '',
        'user'     => '',
        'password' => '',
 ),
```

### Build Database
After set up database connection, we can build database by run this command from project directory

```
vendor/bin/doctrine-module orm:schema-tool:create
```

### Load Data Fixtures
***This is purposed for testing the application!***
Load sample data by run this command from project directory

```
vendor/bin/doctrine-module data-fixture:import
```